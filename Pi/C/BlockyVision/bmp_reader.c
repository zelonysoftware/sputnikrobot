
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "find_blocks.h"

#define BMP_OFFSET_POS 		10 
#define BMP_OFFSET_LEN		4
#define PIXEL_WIDTH_POS		18
#define PIXEL_WIDTH_LEN		4
#define PIXEL_HEIGHT_POS	22
#define PIXEL_HEIGHT_LEN	4 

#define BITS_PER_PIXEL		24  // should get from header

long fromLittleEndian(unsigned char* startPos, int len)
{
	long num = 0 ;
	long mult = 1 ;  
	
	for ( int i = 0 ; i < len ; ++i ) 
	{
		num = num + ((long)startPos[i] * mult); 
		mult = mult * 256;  
	}
    return num;
}

Image loadBitmapFile( char* filepath )
{
    printf("Opening file %s\n", filepath);
	FILE *fp = fopen( filepath, "r" ); 

    printf("Opened file, fp = %0xd\n", fp); 
	
	// Get the length of the file. 
	fseek(fp, 0, SEEK_END); 
	long fileSize = ftell(fp); 
	
	// Create a buffer large enough to fit the whole file. 
	unsigned char* fileBuffer = malloc(fileSize); 

	// Read the file. 
    fseek(fp, 0, SEEK_SET);
	int blocksRead = fread(fileBuffer, fileSize, 1, fp);

    printf("Read %d blocks from the file\n", blocksRead); 
	
	// Don't need the file anymore. 
	fclose(fp); 

	// Convert the file to a bitmap buffer. 
	long bmpOffset = fromLittleEndian(fileBuffer+BMP_OFFSET_POS, BMP_OFFSET_LEN); 
	long imageWidth = fromLittleEndian(fileBuffer+PIXEL_WIDTH_POS, PIXEL_WIDTH_LEN); 
	long imageHeight = fromLittleEndian(fileBuffer+PIXEL_HEIGHT_POS, PIXEL_HEIGHT_LEN); 
	
	Image image ;
	image.num_cols = (int) imageWidth ;
	image.num_rows = (int) imageHeight ;
	image.image = (unsigned char*) malloc(imageWidth * imageHeight * 3); 
	
	
	// each row is padded to have a multiple of 4 bytes. 
	long rowBytes = ((long)(((BITS_PER_PIXEL * imageWidth)+31.0)/32.0)) * 4L;

	for( int row = 0 ; row < (int) imageHeight ; ++row )
	{
		for ( int col = 0 ; col < (int) imageWidth ; ++col )
		{
			// bitmap in file has the last row 1st, so we want to invert it. 
			int bmpFileRow = (imageHeight - row) - 1; 
			int i = bmpOffset + (bmpFileRow * rowBytes) + (col * 3); 
			
			unsigned char* imagePixelPtr = image.image + (row*imageWidth*3) + (col * 3); 
			
			imagePixelPtr[0] = fileBuffer[i+2] ; // red 
			imagePixelPtr[1] = fileBuffer[i+1] ; // green 
			imagePixelPtr[2] = fileBuffer[i]   ; // blue 
		}
	}
	
	return image ; 
}

void releaseImage(Image image) 
{
	free(image.image); 
}

