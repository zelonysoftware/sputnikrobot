#ifndef _FIND_BLOCKS_H_
#define _FIND_BLOCKS_H_

typedef struct
{
    int num_cols ;
    int num_rows ;

    unsigned char *image ;

} Image ;

typedef struct 
{
    int start_col ;
    int end_col ;
    int start_row ;
    int end_row ;

} BBox ;

typedef struct 
{

    int min_block_cols;
    int min_block_rows;
    int threshold ;

} BVConfig ;

typedef struct 
{
    char* name ;

    // pixel to find
    unsigned char r;
    unsigned char g;
    unsigned char b;

    int top ;
    int bottom ;
    int left ;
    int right ;
    
    bool found ;

} Block ;

extern int find_blocks(Image image, BVConfig config, BBox bbox, int numBlocks, Block* blocks);

#endif
