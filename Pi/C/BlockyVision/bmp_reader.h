#ifndef _BMP_READER_H_
#define _BMP_READER_H_

extern Image loadBitmapFile( char* filepath );
extern void releaseImage(Image image); 

#endif
