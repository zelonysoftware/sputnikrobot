#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "find_blocks.h"
#include "bmp_reader.h"

int main( int argc, char* argv[] )
{
    //Image image = loadBitmapFile("/home/pi/sputnikrobot/Pi/Python/BlockyVision/run/blocks2_100x75.bmp"); 
    Image image = loadBitmapFile("/home/pi/sputnikrobot/Pi/Python/BlockyVision/run/blocks2.bmp"); 
    //Image image = loadBitmapFile("/home/pi/testbmp.bmp");

    printf("Loaded image %d x %d\n", image.num_cols, image.num_rows );
    
    BVConfig config ; 
    config.min_block_cols = 5 ;
    config.min_block_rows = 3 ;
    config.threshold = 20 * 20 ;

    BBox bbox;
    bbox.start_col=0; 
    bbox.end_col=image.num_cols-1; 
    bbox.start_row=0;
    bbox.end_row=image.num_rows-1; 
    
    Block blocks[3];

    blocks[0].name = "red"; 
    blocks[0].r = 252;
    blocks[0].g = 91;
    blocks[0].b = 129;
    blocks[0].found = false;
    blocks[0].left = 0 ;
    blocks[0].right = 0 ;
    blocks[0].top = 0 ;
    blocks[0].bottom = 0 ;

    blocks[1].name = "green"; 
    blocks[1].r = 0;
    blocks[1].g = 137;
    blocks[1].b = 120;
    blocks[1].found = false;
    blocks[1].left = 0 ;
    blocks[1].right = 0 ;
    blocks[1].top = 0 ;
    blocks[1].bottom = 0 ;

    blocks[2].name = "blue"; 
    blocks[2].r = 27;
    blocks[2].g = 89;
    blocks[2].b = 143;
    blocks[2].found = false;
    blocks[2].left = 0 ;
    blocks[2].right = 0 ;
    blocks[2].top = 0 ;
    blocks[2].bottom = 0 ;

    int numBlocks = 3 ;

    for ( int x = 0 ; x < 100000 ; ++x )
    {

        int n = find_blocks(image, config, bbox, numBlocks, blocks);
    
        printf("Found %d blocks\n", n); 

        for ( int i = 0 ; i < numBlocks ; ++i )
        {
            Block* foundBlock = &blocks[i]; 
            printf("Found block %s[%s]: t = %d, b = %d, l = %d, r = %d\n", 
                   foundBlock->name, foundBlock->found ? "found" : "not found",
                   foundBlock->top, foundBlock->bottom, 
                   foundBlock->left, foundBlock->right );
        }
    }

    releaseImage(image); 
     
    return 0 ;
}
