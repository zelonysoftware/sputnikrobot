
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils.h"
#include "find_blocks.h"

/******************************* FUNCTION DECLARATIONS ********************************/

#define GET_PIXEL(image,row,col) (image.image + ((row * (image.num_cols*3)) + (col * 3))) 

void applyThresholds(   Image           image, 
                        int             minColSize, 
                        int             minRowSize, 
                        BVConfig        config, 
                        BBox            bbox, 
                        unsigned char*  thresholdMap, 
                        int             numBlocks, 
                        Block*          blocks);
                        
int findBlockList(  Image           image, 
                    int             minColSize, 
                    int             minRowSize, 
                    BVConfig        config, 
                    BBox            bbox, 
                    unsigned char*  thresholdMap, 
                    int             numBlocks, 
                    Block*          blocks      );

bool isSimilarColour(int threshold,
                     unsigned char r1, unsigned char g1, unsigned char b1, 
                     unsigned char r2, unsigned char g2, unsigned char b2);
                     
bool isSimilarColourEuclideanDistance(int threshold, 
                                      unsigned char r1, unsigned char g1, unsigned char b1, 
                                      unsigned char r2, unsigned char g2, unsigned char b2);
                                      
void normaliseBrightness(unsigned char* r, unsigned char* g, unsigned char* b);

Block findBlock(Image image, BVConfig config, unsigned char* pixel,int row, int col, BBox bbox, unsigned char* isTraversed);

Block* getBlock(int numBlocks, Block* blocks, unsigned char* currentPixel);

void setBlock(int numBlocks, Block* blocks, unsigned char* currentPixel, Block block);

int getArea(Block block);

bool scanRow(Image image, BVConfig config, int row, int col, BBox bbox, unsigned char* blockPixel, unsigned char* isTraversed, int* returnLeftEdge, int* returnRightEdge);



/******************************* FUNCTION DEFINITIONS ********************************/

/* 
 * find_blocks
 *
 * main public function. Takes in an image and returns details of any blocks found 
 *
*/
int find_blocks(Image image, BVConfig config, BBox bbox, int numBlocks, Block* blocks)
{
    int minColSize = config.min_block_cols;
    int minRowSize = config.min_block_rows;

    // First we go through applying a threshold to each pixel to only show colours we're looking for. 
    int numCols = (bbox.end_col - bbox.start_col) + 1;
    int numRows = (bbox.end_row - bbox.start_row) + 1;

    unsigned char* thresholdMap = (unsigned char*) malloc(numRows * numCols);
    memset(thresholdMap, 0, numRows * numCols); 
    
    applyThresholds(image, minColSize, minRowSize, config, bbox, (unsigned char*) thresholdMap, numBlocks, blocks);

    // Now try and find the blocks. 
    int blocksFound = findBlockList(image, minColSize, minRowSize, config, bbox, (unsigned char*) thresholdMap, numBlocks, blocks);

    free( thresholdMap );

    return blocksFound ; 
}


void applyThresholds(   Image           image, 
                        int             minColSize, 
                        int             minRowSize, 
                        BVConfig        config, 
                        BBox            bbox, 
                        unsigned char*  thresholdMap,
                        int             numBlocks, 
                        Block*          blocks      )
{
    int numCols = (bbox.end_col - bbox.start_col) + 1;

    // Use the minimum size to minimise the number of pixels we need to scan. 
    int rowStepSize = max_int((int)(minRowSize-1)/2.0,1);
    int colStepSize = max_int((int)(minColSize-1)/2.0,1);

    int pixelsMarked = 0 ;
    
    for ( int row = bbox.start_row ; row <= bbox.end_row ; row += rowStepSize )
    {
        for ( int col = bbox.start_col ; col <= bbox.end_col ; col += colStepSize )
        {
            unsigned char* imagePixel = GET_PIXEL(image,row,col); 
            unsigned char red   = imagePixel[0]; 
            unsigned char green = imagePixel[1];
            unsigned char blue  = imagePixel[2];

            for ( int blockIndex = 0 ; blockIndex < numBlocks ; ++blockIndex )
            {
                if ( isSimilarColour(config.threshold, blocks[blockIndex].r, blocks[blockIndex].g, blocks[blockIndex].b, red, green, blue) )
                {
                    int offset = ((row - bbox.start_row) * numCols) + (col - bbox.start_col); 
                    imagePixel[0] = blocks[blockIndex].r; 
                    imagePixel[1] = blocks[blockIndex].g; 
                    imagePixel[2] = blocks[blockIndex].b; 
                    thresholdMap[offset] = 1; 
                    pixelsMarked++;
                    break;
                }
            }
        }
    }
}

bool isSimilarColour(int threshold,
                     unsigned char r1, unsigned char g1, unsigned char b1, 
                     unsigned char r2, unsigned char g2, unsigned char b2)
{
    return isSimilarColourEuclideanDistance(threshold, r1,g1,b1, r2,g2,b2);
}

bool isSimilarColourEuclideanDistance(int threshold, 
                                      unsigned char r1, unsigned char g1, unsigned char b1, 
                                      unsigned char r2, unsigned char g2, unsigned char b2)
{
    // Check for exact match to save some time 
    if ( r1 == r2 && g1 == g2 && b1 == b2 )
    {
        return true ;
    }

    normaliseBrightness(&r2, &g2, &b2);
    
    int distanceSquared = ((r1-r2)*(r1-r2)) + ((g1-g2)*(g1-g2)) + ((b1-b2)*(b1-b2));
    
    if ( distanceSquared < threshold ) 
    {
        return true ;
    }

    return false ;
}

void normaliseBrightness(unsigned char* r, unsigned char* g, unsigned char* b)
{
    unsigned char minValue = min_rgb(*r, *g, *b); 

    *r = *r - minValue;
    *g = *g - minValue;
    *b = *b - minValue;
}

int findBlockList(Image image, int minNumCols, int minNumRows, BVConfig config, BBox bbox, unsigned char* thresholdMap, int numBlocks, Block* blocks)
{
    // Go through the filtered bmp finding objects that consist of each colour.
    int numCols = (bbox.end_col - bbox.start_col) + 1 ;
    int numRows = (bbox.end_row - bbox.start_row) + 1 ;

    unsigned char* isTraversed = (unsigned char*) malloc(numRows * numCols);
    memset(isTraversed, 0, numRows * numCols); 

    // we don't need to traverse every row and column - since we're looking for objects larger than 
    // n % of the total sise 
    int rowStepSize = max_int((int)(minNumRows-1)/2.0,1);
    int colStepSize = max_int((int)(minNumCols-1)/2.0,1);
    int numBlocksFound = 0 ;

    for ( int row = bbox.start_row ; row <= bbox.end_row ; row += rowStepSize )
    {
        for ( int col = bbox.start_col ; col <= bbox.end_col ; col += colStepSize )
        {
            int offset = ((row-bbox.start_row) * numCols) + (col - bbox.start_col);

            if ( !isTraversed[offset] && thresholdMap[offset] )
            {
                // This pixel is one of the ones we're looking for.
                unsigned char* currentPixel = GET_PIXEL(image,row,col);

                Block block = findBlock(image, config, currentPixel,row,col,bbox, (unsigned char*) isTraversed);

                int blockRows = block.bottom - block.top ;
                int blockCols = block.right - block.left ;

                if ( (blockRows >= minNumRows) && (blockCols >= minNumCols) )
                {
                    // is this the biggest block so far. 
                    Block* biggestBlockSoFar = getBlock(numBlocks, blocks, currentPixel);

                    if ( biggestBlockSoFar != NULL && biggestBlockSoFar->found)
                    {
                        if ( getArea(*biggestBlockSoFar) < getArea(block) )
                        {
                            setBlock(numBlocks, blocks, currentPixel, block);   
                        }
                    }
                    else
                    {
                        // First block we've found of this colour. 
                        numBlocksFound += 1;
                        setBlock(numBlocks, blocks, currentPixel, block);   
                    }
                }
            }
        }
    }

    free(isTraversed); 

    return numBlocksFound ;
}

Block findBlock( Image image, BVConfig config, unsigned char* pixel, int row, int col, BBox bbox, unsigned char* isTraversed )
{
    // starting with this row, go up until we can't find any more rows.
    bool foundTop = false  ;

    int searchRow = row ; 
    int topRow = bbox.start_row ;
    int bottomRow = topRow ;
    int searchCol = col ;
    int maxLeftEdge = col ;
    int maxRightEdge = col ;
    int leftEdge = 0; 
    int rightEdge = 0; 
        
    while ((searchRow >= bbox.start_row) && !foundTop)
    {
        bool foundEdges = scanRow(image, config, searchRow, searchCol, bbox, pixel, isTraversed, &leftEdge, &rightEdge);
            
        if ( !foundEdges )
        { 
            topRow = searchRow + 1; 
            foundTop = true; 
        }
        else
        {
            if ( leftEdge < maxLeftEdge )
                maxLeftEdge = leftEdge ;
                   
            if ( rightEdge > maxRightEdge )
                maxRightEdge = rightEdge ;
               
            // start the next search at the middle of the current row.
            searchCol = (int)(leftEdge + rightEdge) / 2.0 ;
            searchRow = searchRow - 1 ;
        }
    }
                
    // now find the bottom of the block 
    bool foundBottom = false ;
    
    searchRow = row + 1;
    bottomRow = bbox.end_row ;
    searchCol = col ;
        
    while ((searchRow <= bbox.end_row) && !foundBottom)
    {
        bool foundEdges = scanRow(image, config, searchRow,searchCol,bbox, pixel, isTraversed, &leftEdge, &rightEdge);
            
        if ( !foundEdges )
        {
                bottomRow = searchRow - 1 ;
                foundBottom = true ;
        }
        else
        {
                
            if ( leftEdge < maxLeftEdge )
            {
                maxLeftEdge = leftEdge ;
            }
                    
            if ( rightEdge > maxRightEdge )
            {
                maxRightEdge = rightEdge ;
            }
                        
            // start the next search at the middle of the current row. 
            searchCol = (int)((leftEdge + rightEdge) / 2.0);
            searchRow = searchRow + 1; 
        }
    }
        
    // make sure we don't try to scan any pixels in this block again. 
    int numCols = (bbox.end_col - bbox.start_col) + 1 ;
        
    for (int row = topRow ; row <= bottomRow ; ++row )
    {
        for (int col = maxLeftEdge ; col <= maxRightEdge ; ++col )
        {
            int i = ((row-bbox.start_row) * numCols) + (col-bbox.start_col) ;
            isTraversed[i] = 1 ;
        }
    }

    Block foundBlock ;
    foundBlock.top = topRow;
    foundBlock.bottom = bottomRow;
    foundBlock.left = maxLeftEdge;
    foundBlock.right = maxRightEdge;
    
    return foundBlock ;
} 

bool scanRow(Image image, BVConfig config, int row, int col, BBox bbox, unsigned char* blockPixel, unsigned char* isTraversed, int* returnLeftEdge, int* returnRightEdge)
{    
    int numCols = (bbox.end_col - bbox.start_col) + 1 ;

    // make sure we mark the current pixel as traversed. 
    int i = ((row-bbox.start_row) * numCols) + (col-bbox.start_col); 
    isTraversed[i] = 1 ;
        
    unsigned char* pixel = GET_PIXEL(image, row, col); 
    if (!isSimilarColour(   config.threshold, 
                            blockPixel[0], blockPixel[1], blockPixel[2], 
                            pixel[0], pixel[1], pixel[2]                    )   )
    {
        return false; 
    }
    
    // Change the image 
    pixel[0] = blockPixel[0];
    pixel[1] = blockPixel[1];
    pixel[2] = blockPixel[2];
    
    int leftEdge = col ;
    int rightEdge = col ;

    int scanCol = col - 1 ;
    
    while (scanCol >= bbox.start_col)
    {
        i = ((row-bbox.start_row) * numCols) + (scanCol-bbox.start_col) ;
        isTraversed[i] = 1 ;

        pixel = GET_PIXEL(image, row, scanCol);
        if (!isSimilarColour(   config.threshold, 
                                blockPixel[0], blockPixel[1], blockPixel[2], 
                                pixel[0], pixel[1], pixel[2] )              )
        {
            break ;
        }

        pixel[0] = blockPixel[0];
        pixel[1] = blockPixel[1];
        pixel[2] = blockPixel[2];
        
        leftEdge = scanCol ;
        scanCol = scanCol - 1 ;
    }
    
    scanCol = col + 1 ;
     
    while (scanCol <= bbox.end_col)
    {
        i = ((row-bbox.start_row) * numCols) + (scanCol-bbox.start_col) ;
        isTraversed[i] = 1 ; 

        pixel = GET_PIXEL(image, row, scanCol);
        if (!isSimilarColour(   config.threshold, 
                                blockPixel[0], blockPixel[1], blockPixel[2], 
                                pixel[0], pixel[1], pixel[2]                ) )
        {
            break ;
        }

        pixel[0] = blockPixel[0];
        pixel[1] = blockPixel[1];
        pixel[2] = blockPixel[2];
 
        rightEdge = scanCol;
        scanCol = scanCol + 1;
    }  

    // Store the results in the return parameters. 
    *returnLeftEdge = leftEdge ;
    *returnRightEdge = rightEdge ;

    return true ;      
}

Block* getBlock(int numBlocks, Block* blocks, unsigned char* currentPixel)
{
    Block* foundBlock = NULL ;
    
    for ( int i = 0 ; (i < numBlocks) && (foundBlock == NULL) ; ++i )
    {
        if (    (blocks[i].r == currentPixel[0])
            &&  (blocks[i].g == currentPixel[1])
            &&  (blocks[i].b == currentPixel[2]) )
        {
            foundBlock = blocks + i ; 
        }
    }
    
    return foundBlock ;
}

void setBlock(int numBlocks, Block* blocks, unsigned char* currentPixel, Block block)
{
    Block* blockToSet = getBlock(numBlocks, blocks, currentPixel); 
    
    if ( blockToSet != NULL )
    {
        blockToSet->found = true ;
        blockToSet->left = block.left;
        blockToSet->right = block.right;
        blockToSet->top = block.top;
        blockToSet->bottom = block.bottom;
    }
}

int getArea(Block block)
{
    return (block.bottom-block.top) * (block.right - block.left);
}
