
#include "utils.h"

int max_int( int i1, int i2 ) 
{
    if ( i1 >= i2 ) 
    {
        return i1 ; 
    }

    return i2 ; 
}

/*
 * Find the minimum value from r, g and b when comparing pixels 
 */
unsigned char min_rgb( unsigned char r, unsigned char g, unsigned char b ) 
{
    if ( (r < g) && (r < b) ) 
    {
        return r ;
    }
    else if ( g < b ) 
    {
        return g ;
    }

    return b ;
}
