#!/bin/bash

{
 echo ------ STARTING NANNY MCPI ------- 
 date 

 source ~pi/.profile

 cd $ROBOTHOME/Python

 python3 ./NannyMain.py $*

} >/home/pi/nannymcpi.stdout 2>&1

