#!/bin/bash

{
 echo ------ STARTING OCTOBOT ------- 
 date 

 if [z$ROBOTHOME == z]
 then
    export ROBOTHOME=/home/pi/Sputnik/sputnikrobot/Pi
 fi

 cd $ROBOTHOME/Python

 #setsid python3 ./Main.py >>/home/pi/sputnik.stdout 2>&1 &
 python3 ./RedMain.py $*

} >/home/pi/Sputnik/redrobot.stdout 2>&1

