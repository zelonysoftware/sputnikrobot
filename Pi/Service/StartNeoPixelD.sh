#!/bin/bash

{
 echo ------ STARTING NEOPIXEL SERVER ------- 
 date 

 source ~pi/.profile

 cd $ROBOTHOME/Python

 python3 ./Utils/NeoPixelServer.py $*

} >/home/pi/neopixeld.stdout 2>&1

