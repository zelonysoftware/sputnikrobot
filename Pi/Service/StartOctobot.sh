#!/bin/bash

{
 echo ------ STARTING OCTOBOT ------- 
 date 

 if [z$ROBOTHOME == z]
 then
    export ROBOTHOME=/home/pi/Octobot/sputnikrobot/Pi
 fi

 cd $ROBOTHOME/Python

 #setsid python3 ./Main.py >>/home/pi/sputnik.stdout 2>&1 &
 python3 ./OctoMain.py $*

} >/home/pi/Octobot/octobot.stdout 2>&1

