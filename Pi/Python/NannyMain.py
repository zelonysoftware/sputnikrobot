#!/usr/bin/python3
import time
import RPi.GPIO as GPIO
import sys
import os
import logging
from Robot.Config.ConfigUsingIniFile import ConfigUsingIniFile
import Robot.Logging as Logging
from GamesController.camera_bv import VisualControllerBlockyVisionCamera


# ---------------Initialise Logging before we import the Robot modules  ----------------------------

robotHome = os.getenv('ROBOTHOME')
print("Robot Home = " + str(robotHome)) 
if ( robotHome == None ):
    robotHome = os.getenv('HOME')
if ( robotHome == None ):
    robotHome = "."

print("Robot Home = " + robotHome)

configFile = robotHome + '/Config/' + "nannymcpi.ini"
print("Using config file " + configFile)

configImpl = ConfigUsingIniFile(configFile)

# Now we've got the config file we can setup the logging properly 
Logging.initialiseLogger(configImpl)

# --------------------------------------------------------------------------------------------------

from Robot.Robot import Robot
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.RobotModeList import RobotModeList
from MotorController.TestMotorController import TestMotorController
from MotorController.RedboardMotorController import RedboardMotorController
from BatteryMonitor.RedboardBatteryMonitor import RedboardBatteryMonitor
from BatteryMonitor.BatteryMonitorBase import BatteryMonitorBase
from GamesController.PS3Controller import PS3Controller
from GamesController.PS4Controller import PS4Controller
from GamesController.PiHutController import PiHutController
from GamesController.HTMLVisualController import HTMLVisualController
from Robot.Config.RobotConfig import RobotConfig 
from Robot.Config.SerialCommsConfig import SerialCommsConfig
from Robot.Config.BlockyVisionConfig import BlockyVisionConfig
from Robot.SerialComms.SerialCommsHandler import SerialCommsHandler
from Robot.RobotMonitorThread import RobotMonitorThread
from Robot.Devices.SingleSonarSensor import SingleSonarSensor
from Robot.Environment.DistanceMonitor import DistanceMonitor
from Robot.Environment.BlockyVisionCamera import BlockyVisionCamera
import threading
import signal 



def createMotorController(config):

    motorControllerType = config.getMotorControllerType(default="RedBoard")

    motorController = None
    batteryMonitor = None
    
    if ( motorControllerType == 'TEST' ):
        logging.info("Using Test Motor Controller")
        motorController = TestMotorController()
        batteryMonitor = BatteryMonitorBase()
    elif (motorControllerType == 'RedBoard'):
        logging.info("Using RedBoard Motor Controller")
        motorController = RedboardMotorController()
        batteryMonitor = RedboardBatteryMonitor()
    else:
        logging.info("Unrecognised Motor Controller type " + motorControllerType + " - defaulting to TEST")
        motorController = TestMotorController()
        batteryMonitor = BatteryMonitorBase()

    return motorController, batteryMonitor



# ----------- MAIN ENTRY POINT ------------

robotConfig = RobotConfig(configImpl)
theRobot = None
serialCommsHandler = None 
monitorThread = None
distanceThread = None
blockyCamera = None
exitCleanly = False


def exitNicely(signum, frame):
    global exitCleanly
    exitCleanly = True

try:

    signal.signal(signal.SIGINT, exitNicely)
    signal.signal(signal.SIGTERM, exitNicely)

    GPIO.setmode(GPIO.BCM)
    
    motorController, batteryMonitor = createMotorController(robotConfig)

    mainLogger = logging.getLogger(__name__) 

    mainLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    # Create and start the robot (this should default to remote controlled
    # mode - so won't do anything until we connect the PS3 controller). 
    mainLogger.info("Creating the Robot")
    
    #theRobot = Robot( robotConfig, motorController, batteryMonitor, RobotModeList([RobotModes.MODE_NANNY_MC_PI_REMOTE, RobotModes.MODE_NANNY_MC_PI_DEMO]))
    
    modeList = RobotModeList([  RobotModes.MODE_NANNY_MC_PI_REMOTE, 
                                RobotModes.MODE_NANNY_MC_PI_DEMO ])
    
    theRobot = Robot( robotConfig, 
                      motorController, 
                      batteryMonitor, 
                      modeList              )


    # add a camera to the Robot's environments
    blockyCamera = None 
    if ( not 'nocamera' in sys.argv ):
        mainLogger.info("Adding Camera to robot environment")
        environment = theRobot.getEnvironment() 
        blockyCamera = BlockyVisionCamera(BlockyVisionConfig(configImpl)) 

        ''' Testing Code to see difference between standard mode and turbo mode '''
        blockyCamera.setCaptureMode(BlockyVisionCamera.BLOCK_MODE)
        blockyCamera.setSearchThreshold(25)
        blockyCamera.setSearchObjects([('blue', 12,109,207)])
        ''' TEST TEST TEST '''

        blockyCamera.startCaptureThread()
        environment.setSmartCamera(blockyCamera)


    # Create the games controller, passing the robot's callback handler. 
    mainLogger.info("Creating the Games Controller")
    gamesControllerHandler = theRobot.getGamesControllerHandler()
    ps3Controller = PS3Controller()
    ps3Controller.runAsThread(gamesControllerHandler)
    ps4Controller = PS4Controller()
    ps4Controller.runAsThread(gamesControllerHandler)
    piHutController = PiHutController()
    piHutController.runAsThread(gamesControllerHandler)
    
    htmlController = None 
    if ( blockyCamera is not None ):
        mainLogger.info("Creating Blocky Vision Camera for HTML controller") 
        controllerCamera = VisualControllerBlockyVisionCamera(blockyCamera)
        mainLogger.info("Creating HTML Visual Controller") 
        htmlController = HTMLVisualController()
        mainLogger.info("Starting HTML Controller") 
        htmlController.runAsThread(controllerCamera, gamesControllerHandler)
        mainLogger.info("Started HTML Controller") 

    # create and startup the serial comms handler.
    mainLogger.info("Creating the SerialCommsHandler")
    serialCommsHandler = SerialCommsHandler(SerialCommsConfig(configImpl), theRobot)
    theRobot.addStateListener(serialCommsHandler) 
    serialCommsHandler.start() 

    # Add the front mounted Sonar Sensor 
    distanceThread = None 
    if ( not 'nodistance' in sys.argv ):
        mainLogger.info("Adding Sonar Sensor to robot") 
        noseDistanceSensor = SingleSonarSensor(27,13) 
        distanceThread = DistanceMonitor(theRobot.getEnvironment(), noseDistanceSensor, 0)
        distanceThread.start()

    # create and startup the status monitor thread. This monitors and logs the status
    # of the robot
    monitorThread = RobotMonitorThread(theRobot)
    monitorThread.start() 

    time.sleep(1)
    theRobot.start()

    allThreads = threading.enumerate()

    for aThread in allThreads:
        mainLogger.info("Thread " + aThread.name + " isDaemon = " + str(aThread.daemon))

    while not exitCleanly:
        time.sleep(1)

except KeyboardInterrupt:
    mainLogger.warn("Interrupted")

finally:
    mainLogger.info("-----------------------------------------------------------------------")
    mainLogger.info("Stopping Robot")
    if ( theRobot != None ):
        theRobot.stopWheels()
        theRobot.stop()
    mainLogger.info("Robot Stopped")
    mainLogger.info("Stopping Serial Handlers")
    if ( serialCommsHandler != None ):
        serialCommsHandler.stop()
    mainLogger.info("Serial Handlers Stopped") 
    if ( monitorThread != None ):
        monitorThread.stop()
    mainLogger.info("Monitor Thread Stopped")
    mainLogger.info("Stopping Camera")
    if ( blockyCamera != None ):
        blockyCamera.stopServer()
    mainLogger.info("Blocky Camera Stopped") 
    mainLogger.info("Stopping Distance Thread")
    if ( distanceThread != None):
        distanceThread.stop()
    mainLogger.info("Distance Thread Stopped") 

    mainLogger.info("Cleanup GPIO")
    GPIO.setwarnings(False)
    GPIO.cleanup()
    mainLogger.info("GPIO Cleaned up")
    mainLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    #sys.exit commented out so we don't lose exception messages. 
    #sys.exit(0)

