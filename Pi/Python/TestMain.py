#!/usr/bin/python3
import time
import RPi.GPIO as GPIO
import sys
import os
from MotorController.RedboardMotorController import RedboardMotorController


def createMotorController(config):

    motorControllerType = config.getMotorControllerType(default="RedBoard")

    motorController = None
    batteryMonitor = None
    
    if ( motorControllerType == 'TEST' ):
        logging.info("Using Test Motor Controller")
        motorController = TestMotorController()
        batteryMonitor = BatteryMonitorBase()
    elif (motorControllerType == 'RedBoard'):
        logging.info("Using RedBoard Motor Controller")
        batteryMonitor = RedboardBatteryMonitor()
    else:
        logging.info("Unrecognised Motor Controller type " + motorControllerType + " - defaulting to TEST")
        motorController = TestMotorController()
        batteryMonitor = BatteryMonitorBase()

    return motorController, batteryMonitor



# ----------- MAIN ENTRY POINT ------------

try:

    motorController = RedboardMotorController()
    motorController.moveRightWheel(10) 
    motorController.moveLeftWheel(40) 
    time.sleep(1) 
    motorController.moveBothWheels(0)

    
except KeyboardInterrupt:
    print("Interupted")

    mainLogger.info("Cleanup GPIO")
    GPIO.setwarnings(False)
    GPIO.cleanup()
    mainLogger.info("GPIO Cleaned up")
    mainLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

