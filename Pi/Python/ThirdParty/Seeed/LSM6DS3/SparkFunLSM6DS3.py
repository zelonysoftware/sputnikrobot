
I2CMODE = 0
SPIMODE = 1 #Currently not implemented


def class LSM6DS3Core:

    commsMode = I2CMODE 

    def beginCore():

        returnError = IMU_SUCCESS

        if ( commsMode == I2CMODE ):

               

        else
            print("Mode not supported") 
            return IMU_HW_ERROR

        
                

    def readRegisterRegion(output, address, length):
        pass

    def readRegisterInt(output, offset):
        pass

    def writeRegister(address, value):
        pass

    def embeddedPage():
        pass

    def basePage():
        pass


def class LSM6DS(LSM6DS3Core):

    def begin():
        pass

    def readRawAccelX():
        pass

    def readRawAccelY():
        pass

    def readRawAccelZ():
        pass

    def readRawGyroX():
        pass

    def readRawGyroY():
        pass

    def readRawGyroZ():
        pass

    def readFloatAccelX():
        pass

    def readFloatAccelY():
        pass

    def readFloatAccelZ():
        pass

    def readRawTemp():
        pass

    def readTempC():
        pass

    def readTempF():
        pass

    def fifoBegin():
        pass
    
    def fifoClear():
        pass
    
    def fifoRead():
        pass
    
    def fifoGetStatus():
        pass
    
    def fifoEnd():
        pass
    
    def calcGyro():
        pass
    
    def calcAccel():
        pass
    
