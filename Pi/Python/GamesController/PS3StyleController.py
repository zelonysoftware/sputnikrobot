
import evdev
import time
import sys
from threading import Thread
import logging


class PS3StyleControllerTask(Thread):

    logger = logging.getLogger(__name__)

    def __init__(self, controller, cb):

        self.logger.info("Called PS3 Style Controller Task __init__()")

        super().__init__()

        self.controller = controller 
        self.callback = cb


        # Starting values of the inputs. 
        self.leftJSXPos = 0
        self.leftJSYPos = 0 
        self.rightJSXPos = 0
        self.rightJSYPos = 0

        self.L2Pos = 0
        self.R2Pos = 0

        self.xButtonValue = 0
        self.triangleButtonValue = 0
        self.circleButtonValue = 0
        self.squareButtonValue = 0

        self.R1ButtonValue = 0
        self.L1ButtonValue = 0

        self.R2ButtonValue = 0
        self.L2ButtonValue = 0

        self.R3ButtonValue = 0
        self.L3ButtonValue = 0
        
        self.upButtonValue = 0
        self.downButtonValue = 0
        self.leftButtonValue = 0
        self.rightButtonValue = 0

        self.startButtonValue = 0
        self.selectButtonValue = 0

    def run(self):

        controllerConnected = False

        self.logger.info("Running Task Loop for controller [%s]" % self.controller.getDeviceDisplayName())

        while True:

            while not controllerConnected:
                try:
                    for path in evdev.list_devices():
                        device = evdev.InputDevice(path)
                        if ( device.name == self.controller.getDeviceName() ):
                            controller = device 
                            controllerConnected = True 
                            self.callback.controllerConnected()
                            self.logger.info("Connected to %s controller" % self.controller.getDeviceDisplayName())
                            break 
                except FileNotFoundError:
                    self.logger.warn("Failed to open controller event device - will try again")
                except PermissionError:
                    self.logger.warn("Permmission error connecting controller - will try again")

                if ( not controllerConnected ):
                    #self.logger.info("Waiting for controller to connect")
                    time.sleep(1)

            while controllerConnected:
                try:
                    for event in controller.read_loop():
                        self.handleEvent(event)

                except:
                    self.logger.info("Unexpected exeption occurred reading controller events", sys.exc_info()[0])
                    controllerConnected = False
                    self.callback.controllerDisconnected() 


    def handleEvent(self, theevent):

        if ( self.controller.handleEvent(theevent) ):
            return ;

        if ( theevent.code == self.controller.EC_LEFT_JS_LEFT_RIGHT ) and ( theevent.type == 3 ):
            prevPos = self.leftJSXPos
            self.leftJSXPos = self.controller.mapJSPosition(theevent.value)
            if ( self.leftJSXPos != prevPos ):
                self.callback.joystickLeftMoved(self.leftJSXPos, self.leftJSYPos)

        elif ( theevent.code == self.controller.EC_LEFT_JS_UP_DOWN ) and ( theevent.type == 3 ):
            prevPos = self.leftJSYPos
            self.leftJSYPos = 0 - self.controller.mapJSPosition(theevent.value)
            if ( self.leftJSYPos != prevPos ):
                self.callback.joystickLeftMoved(self.leftJSXPos, self.leftJSYPos)

        if ( theevent.code == self.controller.EC_RIGHT_JS_LEFT_RIGHT ) and ( theevent.type == 3 ):
            prevPos = self.rightJSXPos
            self.rightJSXPos = self.controller.mapJSPosition(theevent.value)
            if ( self.rightJSXPos != prevPos ):
                self.callback.joystickRightMoved(self.rightJSXPos, self.rightJSYPos)

        elif ( theevent.code == self.controller.EC_RIGHT_JS_UP_DOWN ) and ( theevent.type == 3 ):
            prevPos = self.rightJSYPos
            self.rightJSYPos = 0 - self.controller.mapJSPosition(theevent.value)
            if ( self.rightJSYPos != prevPos ):
                self.callback.joystickRightMoved(self.rightJSXPos, self.rightJSYPos)

        elif ( theevent.code == self.controller.EC_L2_ANALOG ) and ( theevent.type == 3 ):
            prevPos = self.L2Pos
            self.L2Pos = self.controller.mapTriggerPosition(theevent.value)
            if ( self.L2Pos != prevPos ):
                self.callback.triggerL2AnalogChanged(self.L2Pos)

        elif ( theevent.code == self.controller.EC_R2_ANALOG ) and ( theevent.type == 3 ):
            prevPos = self.R2Pos
            self.R2Pos = self.controller.mapTriggerPosition(theevent.value)
            if ( self.R2Pos != prevPos ):
                self.callback.triggerR2AnalogChanged(self.R2Pos)

        elif ( theevent.code == self.controller.EC_L2_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.L2ButtonValue ):
                self.L2ButtonValue = theevent.value
                self.callback.buttonL2Changed(theevent.value)

        elif ( theevent.code == self.controller.EC_R2_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.R2ButtonValue ):
                self.R2ButtonValue = theevent.value
                self.callback.buttonR2Changed(theevent.value)

        elif ( theevent.code == self.controller.EC_R1_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.R1ButtonValue ):
                self.R1ButtonValue = theevent.value
                self.callback.buttonR1Changed(theevent.value)

        elif ( theevent.code == self.controller.EC_L1_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.L1ButtonValue ):
                self.L1ButtonValue = theevent.value
                self.callback.buttonL1Changed(theevent.value)

        elif ( theevent.code == self.controller.EC_R3_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.R3ButtonValue ):
                self.R3ButtonValue = theevent.value
                self.callback.buttonR3Changed(theevent.value)

        elif ( theevent.code == self.controller.EC_L3_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.L3ButtonValue ):
                self.L3ButtonValue = theevent.value
                self.callback.buttonL3Changed(theevent.value)

        elif ( theevent.code == self.controller.EC_X_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.xButtonValue ):
                self.xButtonValue = theevent.value
                self.callback.buttonXChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_CIRCLE_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.circleButtonValue ):
                self.circleButtonValue = theevent.value
                self.callback.buttonCircleChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_TRIANGLE_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.triangleButtonValue ):
                self.triangleButtonValue = theevent.value
                self.callback.buttonTriangleChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_SQUARE_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.squareButtonValue ):
                self.squareButtonValue = theevent.value
                self.callback.buttonSquareChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_KEYPAD_UP_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.upButtonValue ):
                self.upButtonValue = theevent.value
                self.callback.buttonUpChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_KEYPAD_DOWN_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.downButtonValue ):
                self.downButtonValue = theevent.value
                self.callback.buttonDownChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_KEYPAD_LEFT_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.leftButtonValue ):
                self.leftButtonValue = theevent.value
                self.callback.buttonLeftChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_KEYPAD_RIGHT_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.rightButtonValue ):
                self.rightButtonValue = theevent.value
                self.callback.buttonRightChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_START_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.startButtonValue ):
                self.startButtonValue = theevent.value
                self.callback.buttonStartChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_SELECT_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.selectButtonValue ):
                self.selectButtonValue = theevent.value
                self.callback.buttonSelectChanged(theevent.value)

        elif ( theevent.code == self.controller.EC_SQUARE_BUTTON ) and ( theevent.type == 1 ):
            if ( theevent.value != self.selectButtonValue ):
                self.selectButtonValue = theevent.value
                self.callback.buttonSelectChanged(theevent.value)



class PS3StyleController():

    def __init__(self, deviceName, deviceDisplayName):
        self.deviceName = deviceName ;
        self.deviceDisplayName = deviceDisplayName ;

        # The event codes - these are the official PS3 ones - other controllers may override the settings 
        # as part of their __init__() method. 
        self.EC_LEFT_JS_LEFT_RIGHT   = 0
        self.EC_LEFT_JS_UP_DOWN      = 1
        self.EC_RIGHT_JS_LEFT_RIGHT  = 3
        self.EC_RIGHT_JS_UP_DOWN     = 4
    
        self.EC_L2_ANALOG            = 2
        self.EC_R2_ANALOG            = 5
    
        self.EC_L2_BUTTON            = 312
        self.EC_R2_BUTTON            = 313
    
        self.EC_L1_BUTTON            = 310
        self.EC_R1_BUTTON            = 311
    
        self.EC_L3_BUTTON            = 317
        self.EC_R3_BUTTON            = 318
    
        self.EC_X_BUTTON             = 304
        self.EC_CIRCLE_BUTTON        = 305
        self.EC_TRIANGLE_BUTTON      = 307
        self.EC_SQUARE_BUTTON        = 308
    
        self.EC_KEYPAD_UP_BUTTON     = 544
        self.EC_KEYPAD_DOWN_BUTTON   = 545
        self.EC_KEYPAD_LEFT_BUTTON   = 546
        self.EC_KEYPAD_RIGHT_BUTTON  = 547
    
        self.EC_START_BUTTON         = 315
        self.EC_SELECT_BUTTON        = 314
        self.EC_PS_BUTTON            = 316

    """ Run the PS3 Controller loop as part of the current thread (never returns). """
    def run(self, cb):
        loop = PS3StyleControllerTask(self,cb)
        loop.run()

    """ Run the PS3 Controller loop as a new thread - this method will return immediately and the loop will run in the separate thread """
    def runAsThread(self, cb):
        loop = PS3StyleControllerTask(self,cb)
        loop.name = "PS3 Style Event Loop"
        loop.daemon = True 
        loop.start()


        
    def getDeviceName(self):
        return self.deviceName 
    
    def getDeviceDisplayName(self):
        return self.deviceDisplayName

    # Give controller a chance to handle or modify events as they come in. 
    def handleEvent(self, theevent):
        return False 

    def mapJSPosition(self, jsPos):
        """Map Joystick position in range 0-255 to range -100 to +100"""
        pos = ((jsPos - 128) * 200) / 255
        if ( abs(pos) < 10 ):
            pos = 0 
        return pos 

    def mapTriggerPosition(self, triggerPos):
        """Map Trigger position in range 0-255 to range 0-100"""
        pos = (triggerPos * 100) / 255 ;
        if ( pos < 10 ):
            pos = 0
        return pos 

