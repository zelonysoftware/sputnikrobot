
import logging
from flask import Flask, render_template, Response, request

app = None ; 

def init(serviceHandler):

    global app 

    if ( app != None ):
        return 

    # import camera driver
    camera = serviceHandler.getCamera()
    
    # if os.environ.get('CAMERA'):
    #     print('Getting camera from environment')
    #     Camera = import_module('GamesController.camera_' + os.environ['CAMERA']).Camera
    # else:
    #     print('Using pi camera as default')
    #     Camera = import_module('GamesController.camera_pi' ).Camera

    print('Creating flask app') 
    app = Flask(__name__)
    print('Created')

    # We don't wan't lots of logging everytime a service is called. 
    flaskLog = logging.getLogger('werkzeug')
    flaskLog.setLevel(logging.WARNING)

    @app.route('/')
    def index():
        """Video streaming home page."""
        return render_template('index.html')

    @app.route('/postJoystickXY', methods = ['GET','POST'])
    def postJoystickXY():
        x = request.form['x']
        y = request.form['y']
        serviceHandler.onJSPositionChange(x,y)
        return ""
    
    @app.route('/heartbeat', methods = ['GET','POST'])
    def heartbeart():
        serviceHandler.onHeartbeat()
        return "" 

    def gen(camera):
        """Video streaming generator function."""
        while True:
            frame = camera.get_frame()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


    @app.route('/video_feed')
    def video_feed():
        """Video streaming route. Put this in the src attribute of an img tag."""
        return Response(gen(camera),
                        mimetype='multipart/x-mixed-replace; boundary=frame')

    app.run(host='0.0.0.0', threaded=True)

