
import evdev
import time
import sys
import threading
import traceback

# The event codes
EC_LEFT_JS_LEFT_RIGHT   = 0
EC_LEFT_JS_UP_DOWN      = 1
EC_RIGHT_JS_LEFT_RIGHT  = 3
EC_RIGHT_JS_UP_DOWN     = 4

EC_L2_ANALOG            = 2
EC_R2_ANALOG            = 5

controllerConnected = False

def handleEvent(name, theevent):

    if ( theevent.type == 3 and theevent.code == 0):
        if ( theevent.value > 0 ):
            print("Analog Event: [%s] event.code = %d, event.type = %d, event.value = %d" % (name, theevent.code, theevent.type, theevent.value))
        pass

    elif ( theevent.type == 1 ):
        print("Button Event: event.code = %d, event.type = %d, event.value = %d" % (theevent.code, theevent.type, theevent.value))
        pass

    else:
        #print("Other Event: event.code = %d, event.type = %d, event.value = %d" % (theevent.code, theevent.type, theevent.value))
        pass


def event_thread_func(controller):
    controllerConnected = True

    print(f'Start reading events for controller [{controller.name}]')
    while controllerConnected:
        try:
            for event in controller.read_loop():
                handleEvent(controller.name, event)

        except:
            print("Unexpected exeption occurred reading controller events", sys.exc_info()[0])
            print(traceback.format_exc())
            controllerConnected = False
    print(f'End reading events for controller [{controller.name}]')

# Main start point
try:
   for path in evdev.list_devices():
      device = evdev.InputDevice(path)

      controller = device 
      controllerConnected = True 
      print("Found Remote Controller: %s" % device.name)
      if ( "Motion" not in controller.name ):
          event_thread = threading.Thread(target=event_thread_func, args=(controller,), daemon=True)
          event_thread.start()

except FileNotFoundError:
   print("Failed to open controller event device")
except PermissionError:
   print("Permmission error connecting controller")

while( True ):
    time.sleep(1)
