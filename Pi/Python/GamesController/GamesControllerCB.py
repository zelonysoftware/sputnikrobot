

class GamesControllerCB():

    def joystickLeftMoved(self, xpos, ypos):
        pass
   
    def joystickRightMoved(self, xpos, ypos):
        pass

    def triggerL2AnalogChanged(self, pos):
        pass

    def triggerR2AnalogChanged(self, pos):
        pass

    def buttonL2Changed(self, value):
        pass

    def buttonR2Changed(self, value):
        pass

    def buttonL1Changed(self, value):
        pass

    def buttonR1Changed(self, value):
        pass

    def buttonXChanged(self, value):
        pass

    def buttonTriangleChanged(self, value):
        pass

    def buttonSquareChanged(self, value):
        pass

    def buttonCircleChanged(self, value):
        pass

    def buttonUpChanged(self, value):
        pass

    def buttonLeftChanged(self, value):
        pass

    def buttonRightChanged(self, value):
        pass

    def buttonDownChanged(self, value):
        pass

    def buttonL3Changed(self, value):
        pass

    def buttonR3Changed(self, value):
        pass

    def buttonSelectChanged(self, value):
        pass

    def buttonStartChanged(self, value):
        pass

    def controllerConnected(self):
        pass

    def controllerDisconnected(self):
        pass


