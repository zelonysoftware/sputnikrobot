
from GamesController.PS3StyleController import PS3StyleController
from GamesController.PS3StyleController import PS3StyleControllerTask

class PS4Controller(PS3StyleController):
    
        def __init__(self):
            super().__init__("Wireless Controller", "PS4 Controller")
            
