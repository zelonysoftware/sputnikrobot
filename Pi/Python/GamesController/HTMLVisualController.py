
import time
import sys
import logging
import GamesController.HTMLVisualControllerFlaskApp
from threading import Thread
from threading import Event


logger = logging.getLogger(__name__)

HEARTBEAT_TIMEOUT=1.0 

class HTMLVisualControllerTask(Thread):

    def __init__(self, camera, controllerCallback):

        logger.info("Called HTMLVisualController Conrroller __init()__");

        self.camera = camera 

        self.controllerCallback = controllerCallback 
        self.joystickTask = HTMLVisualControllerJoystickTask(self.controllerCallback) 
        
        super().__init__()

    def onJSPositionChange(self, x, y):

        self.joystickTask.onJSPositionChange(x,y)
        
    def onHeartbeat(self):
        
        self.joystickTask.onHeartbeat() 

    def onConnection(self):
        self.controllerCallback.controllerConnected()
        logger.info("Visual Controller Connected") 
        return 
    
    def getCamera(self):
        
        return self.camera 

    def run(self):
        self.joystickTask.daemon = True 
        self.joystickTask.start()

        GamesController.HTMLVisualControllerFlaskApp.init(self)
    
''' 
    This thread handles the joystick change events. 
    By using a separate thread we don't need to worry about the web service call 
    handler having to do any time consuming tasks like control the motor 
'''        
class HTMLVisualControllerJoystickTask(Thread):
    
    def __init__(self, controllerCallback):

        # These define the position that we've last processed. 
        self.currentXPos = 0
        self.currentYPos = 0
        
        # These define the position we still need to process. 
        self.newXPos = 0 
        self.newYPos = 0
        
        # This is the event used to trigger the joystick task to send through 
        # a new joystick position 
        self.joystickEvent = Event()   

        self.controllerCallback = controllerCallback         
        super().__init__()

    def mapJSPosition(self, jsPos):
        """Map Joystick position giving some leeway for straight. """
        pos = jsPos
        if ( abs(pos) < 10 ):
            pos = 0 
        return pos 

    def onJSPositionChange(self, x, y):
        
        self.newXPos = x 
        self.newYPos = y 
        
        self.joystickEvent.set()
        
    def onHeartbeat(self):
        
        # Just set the event so that we process the joystick again and don't timeout. 
        self.joystickEvent.set() 

    def run(self):

        logger.info("HTML Visual Games Controller Joystick Task Running") 
        
        while True:

            if ( not self.joystickEvent.wait(timeout=HEARTBEAT_TIMEOUT) ):
                # timeout occurred - so stop the robot if we think we've moved it
                if ( self.currentXPos != 0 ) or ( self.currentYPos != 0 ):
                    self.currentXPos = 0 
                    self.currentYPos = 0 
                    self.controllerCallback.joystickLeftMoved(0,0)
            else:
                self.joystickEvent.clear()

                px = int(self.newXPos)
                py = int(self.newYPos) * -1 
    
                px = self.mapJSPosition(px);
                py = self.mapJSPosition(py);

                logger.info(f"Processing joystick move event {px}, {py}")
    
                prevXPos = self.currentXPos 
                self.currentXPos = px
    
                prevYPos = self.currentYPos
                self.currentYPos = py
    
                if ( self.currentXPos != prevXPos ) or ( self.currentYPos != prevYPos ):
                    print(f'currentXPos = {self.currentXPos}, currentYPos = {self.currentYPos}')
                    self.controllerCallback.joystickLeftMoved(self.currentXPos, self.currentYPos)
    
        logger.info("HTML Visual Games Controller Joystick Task Exiting") 
        return 


        

'''
    The main visual controller task to be instantiated in the main Robot code
'''
class HTMLVisualController:

    """ Run the Controller loop as part of the current thread (never returns). """
    def run(self, camera, cb):
        loop = HTMLVisualControllerTask(camera, cb)
        loop.run()

    """ Run the Controller loop as a new thread - this method will return immediately and the loop will run in the separate thread """
    def runAsThread(self, camera, cb):
        camera.start()
        
        loop = HTMLVisualControllerTask(camera, cb)
        loop.name = "Visual Controller Flask Listener"
        loop.daemon = True 
        loop.start()
        
