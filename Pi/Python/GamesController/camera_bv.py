'''
Created on 21 Oct 2021

@author: zelony
'''
from GamesController.base_camera import BaseCamera
from time import sleep
import io

class VisualControllerBlockyVisionCamera(BaseCamera):
    
    def __init__(self,bvCamera):
        
        self.bvCamera = bvCamera
        self.isActive = True  
        self.lastFrameId, _ = bvCamera.getImageWithFrameCount() 
        super().__init__()
        
    def frames(self):
        
        print("Creating generator") 
        
        stream = io.BytesIO()

        while True: 
            frameId = self.lastFrameId 
            
            while frameId == self.lastFrameId:
                
                frameId, pilImage = self.bvCamera.getImageWithFrameCount()

                if ( frameId != self.lastFrameId ):

                    self.lastFrameId = frameId
                    

                    pilImage.save(stream, format='JPEG') 

                    stream.seek(0)

                    yield stream.read()

                    stream.seek(0)
                    stream.truncate()
                
                else:
                    sleep(0)
        
    
    
