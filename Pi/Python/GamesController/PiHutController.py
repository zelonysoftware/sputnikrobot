
from GamesController.PS3StyleController import PS3StyleController
from GamesController.PS3StyleController import PS3StyleControllerTask

class PiHutController(PS3StyleController):
    
        def __init__(self):
           
            super().__init__("hongjingda HJD-X", "PiHut Controller")

            # Override any event codes that are different to the standard PS3 controller. 
            self.EC_RIGHT_JS_LEFT_RIGHT  = 2
            self.EC_RIGHT_JS_UP_DOWN     = 5

            self.EC_L2_ANALOG            = 9
            self.EC_R2_ANALOG            = 10
            
            self.EC_TRIANGLE_BUTTON      = 308
            self.EC_SQUARE_BUTTON        = 307
            
            self.EC_DPAD_LEFT_RIGHT      = 16
            self.EC_DPAD_UP_DOWN         = 17 
            
            self.dpadLeft                = 0 
            self.dpadRight               = 0 
            self.dpadUp                  = 0 
            self.dpadDown                = 0 
    

        # PiHut controller handles the d-pad buttons differently.             
        def handleEvent(self, theevent):

            if ( theevent.code == self.EC_DPAD_LEFT_RIGHT ) and ( theevent.type == 3 ):
                if ( theevent.value == 1 ):
                    self.dpadRight = 1 
                    theevent.code = self.EC_KEYPAD_RIGHT_BUTTON 
                    theevent.type = 1 
                    theevent.value = 1  
                elif ( theevent.value == -1 ):
                    self.dpadLeft = 1
                    theevent.code = self.EC_KEYPAD_LEFT_BUTTON 
                    theevent.type = 1 
                    theevent.value = 1  
                elif ( theevent.value == 0 ):
                    if ( self.dpadLeft == 1 ):
                        theevent.code = self.EC_KEYPAD_LEFT_BUTTON 
                    elif ( self.dpadRight == 1 ):
                        theevent.code = self.EC_KEYPAD_RIGHT_BUTTON
                    else:
                        # Nothing to do with this event. 
                        return True 
                    
                    theevent.type = 1 
                    theevent.value = 0 
                    self.dpadLeft = 0 
                    self.dpadRight = 0 
            
            elif( theevent.code == self.EC_DPAD_UP_DOWN ) and ( theevent.type == 3 ):
                if ( theevent.value == 1 ):
                    self.dpadDown = 1 
                    theevent.code = self.EC_KEYPAD_DOWN_BUTTON 
                    theevent.type = 1 
                    theevent.value = 1  
                elif ( theevent.value == -1 ):
                    self.dpadUp = 1
                    theevent.code = self.EC_KEYPAD_UP_BUTTON 
                    theevent.type = 1 
                    theevent.value = 1  
                elif ( theevent.value == 0 ):
                    if ( self.dpadDown == 1 ):
                        theevent.code = self.EC_KEYPAD_DOWN_BUTTON 
                    elif ( self.dpadUp == 1 ):
                        theevent.code = self.EC_KEYPAD_UP_BUTTON
                    else:
                        # Nothing to do with this event. 
                        return True 
                    
                    theevent.type = 1 
                    theevent.value = 0 
                    self.dpadDown = 0 
                    self.dpadUp = 0 
                     

            # Return False to indicate the main loop still needs to handle the event.             
            return False 

