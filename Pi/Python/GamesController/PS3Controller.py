
from GamesController.PS3StyleController import PS3StyleController
from GamesController.PS3StyleController import PS3StyleControllerTask

class PS3Controller(PS3StyleController):
    
        def __init__(self):
            super().__init__("PLAYSTATION(R)3 Controller", "Genuine PS3 Controller")
            
