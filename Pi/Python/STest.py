#!/usr/bin/python3
import time
import sys
import os
import logging
from Robot.Config.ConfigUsingIniFile import ConfigUsingIniFile
import Robot.Logging as Logging

# ---------------Initialise Logging before we import the Robot modules  ----------------------------

robotHome = os.getenv('ROBOTHOME')
print("Robot Home = " + str(robotHome)) 
if ( robotHome == None ):
    robotHome = os.getenv('HOME')
if ( robotHome == None ):
    robotHome = "."

configFile = robotHome + '/Config/' + "nannymcpi.ini"
print("Using config file " + configFile)

configImpl = ConfigUsingIniFile(configFile)

# Now we've got the config file we can setup the logging properly 
Logging.initialiseLogger(configImpl)

# --------------------------------------------------------------------------------------------------

#from GamesController.HTMLVisualController import HTMLVisualController
from Robot.Robot import Robot
from Robot.Config.RobotConfig import RobotConfig 
import Robot.RobotModes.RobotMode as RobotMode
from MotorController.TestMotorController import TestMotorController
from BatteryMonitor.BatteryMonitorBase import BatteryMonitorBase
from Robot.Config.RobotConfig import RobotConfig 
from Robot.Config.ConfigUsingIniFile import ConfigUsingIniFile
from Robot.RobotMonitorThread import RobotMonitorThread
import threading
import signal 



def createMotorController(config):


    motorController = None
    batteryMonitor = None
    
    logging.info("Using Test Motor Controller")
    motorController = TestMotorController()
    batteryMonitor = BatteryMonitorBase()

    return motorController, batteryMonitor

# ----------- MAIN ENTRY POINT ------------

robotConfig = RobotConfig(configImpl)
theRobot = None
monitorThread = None
exitCleanly = False


def exitNicely(signum, frame):
    global exitCleanly
    exitCleanly = True

try:

    signal.signal(signal.SIGINT, exitNicely)
    signal.signal(signal.SIGTERM, exitNicely)

    motorController, batteryMonitor = createMotorController(robotConfig)

    mainLogger = logging.getLogger(__name__) 

    mainLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    # Create and start the robot using only speech mode
    mainLogger.info("Creating the Robot")
    theRobot = Robot( robotConfig, motorController, batteryMonitor, [RobotMode.MODE_SPEECH_CONTROL] )

    # create and startup the status monitor thread. This monitors and logs the status
    # of the robot
    monitorThread = RobotMonitorThread(theRobot)
    monitorThread.start() 

    time.sleep(1)
    theRobot.start()

    allThreads = threading.enumerate()

    for aThread in allThreads:
        mainLogger.info("Thread " + aThread.name + " isDaemon = " + str(aThread.daemon))

    while not exitCleanly:
        time.sleep(1)

except KeyboardInterrupt:
    mainLogger.warn("Interrupted")

finally:
    mainLogger.info("-----------------------------------------------------------------------")
    mainLogger.info("Stopping Robot")
    if ( theRobot != None ):
        theRobot.stopWheels()
        theRobot.stop()
    mainLogger.info("Robot Stopped")
    if ( monitorThread != None ):
        monitorThread.stop()
    mainLogger.info("Monitor Thread Stopped")

    mainLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    #sys.exit commented out so we don't lose exception messages. 
    #sys.exit(0)

