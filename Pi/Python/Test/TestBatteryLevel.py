
from ThunderBorg import ThunderBorg
import time

tb = ThunderBorg() 

tb.Init()

tb.SetBatteryMonitoringLimits(7.0, 8.4); 
tb.SetLedShowBattery(True)

battCurrent = tb.GetBatteryReading()

print("battCurrent = " + str(battCurrent))

