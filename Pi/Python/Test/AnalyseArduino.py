
import os
import time
from threading import Thread
from serial import Serial


# Setup parameters
baudRate = 19200
timeout = 1.0

# Find the serial port
arduinoSerialDevice = None 

if ( os.path.isdir('/dev/serial/by-id') ):
    path = '/dev/serial/by-id';
    serialDevices = os.listdir(path)

    for serialDevice in serialDevices:
        fullDevicePath = os.path.join(path,serialDevice)
        if (fullDevicePath.find("Arduino") != -1):
            arduinoSerialDevice = fullDevicePath

    if (arduinoSerialDevice != None):
        print("arduinoSerialDevice = " + arduinoSerialDevice)
            
        serialDevice = Serial( port=arduinoSerialDevice, baudrate=baudRate, timeout=timeout )


        # Start reading and timestamping messages. 
        while( True ):

            message = serialDevice.readline()
            timestamp = int(time.time() * 1000); 

            print(str(timestamp) + ": " + message.decode("utf-8"));



