
def calcAngleDiff(startAngle, targetAngle):

    angleDiff = targetAngle - startAngle 
    
    if ( abs(angleDiff) > 180 ):
        if ( angleDiff >= 0 ):
            angleDiff = angleDiff - 360 
        else:
            angleDiff = angleDiff + 360 
            
    return angleDiff
    
    

# Entry point 

a1 = calcAngleDiff( 90, 280 )
a2 = calcAngleDiff( 270, 80 )
a3 = calcAngleDiff( 270, 100 ) 
a4 = calcAngleDiff( 0, 180 )

print("a1 = " + str(a1) + ", a2 = " + str(a2) + ", a3 = " + str(a3)+ ", a4 = " + str(a4))