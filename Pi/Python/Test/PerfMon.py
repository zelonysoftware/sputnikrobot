
import time 
import csv

class LogEntry:

    def __init__(self, index, timestamp):
        self.index = index 
        self.timestamp = timestamp
        self.checkpoints = []

    def add(self, name, value):
        self.checkpoints.append((name,value))
        


class PerfMon: 

    def __init__(self):    
        self.logIndex = 1 ;
        self.logEntries = [] 
        self.currentLogEntry = None
     
    def logStart(self, name):
        # Start a logging entry. 
        timestamp = time.perf_counter() 
        self.currentLogEntry = LogEntry(self.logIndex, timestamp)
        self.logIndex += 1 
        self.currentLogEntry.add(name, timestamp)
        self.logEntries.append(self.currentLogEntry)
        
    def checkpoint(self, name):
        # log a checkpoint. 
        timestamp = time.perf_counter()
        self.currentLogEntry.add(name,timestamp) 

    def dataPoint(self, name, value):
        # log a data value
        self.currentLogEntry.add(name, value)
      
    def dump(self, filename):
        # Dump performance log to file.
        if ( len(self.logEntries) == 0 ):
            return
        
        nameList = [] 
        for entry1 in self.logEntries:
            for checkpoint1 in entry1.checkpoints:
                if ( not (checkpoint1[0] in nameList) ):
                    nameList.append(checkpoint1[0])
        
        with open(filename, "a") as csvFile:
            csvWriter = csv.writer(csvFile)

            # Write out the header            
            row = ['index', 'timestamp'] + nameList  
            csvWriter.writerow(["index","timestamp"] + nameList) 
            
            for entry2 in self.logEntries:
                fileEntry = [entry2.index, entry2.timestamp] 
                fileEntry.extend([""] * len(nameList)); 
                
                for checkpoint2 in entry2.checkpoints:
                     if ( checkpoint2[0] in nameList ):
                         checkPointIndex = nameList.index(checkpoint2[0])
                         fileEntry[checkPointIndex+2] = str(checkpoint2[1]) 
                         
                csvWriter.writerow(fileEntry)
        
        self.logEntries = []
        currentLogEntry = None
