
from configparser import ConfigParser


parser = ConfigParser()

parser.read('./testconfig.ini')

value = parser.get("RemoteRobot", "MaxSpeedLeft", fallback="100")
print("RemoteRobot.MaxSpeedLeft = " + value)

value = parser.get("RemoteRobot", "MaxSpeedRight", fallback="100")
print("RemoteRobot.MaxSpeedRight = " + value)

value = parser.get("RemoteRobot", "SpinSpeed", fallback="50")
print("RemoteRobot.SpinSpeed = " + value)

