
import RPi.GPIO as GPIO

import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(24,GPIO.OUT, initial=GPIO.LOW)

while True:

   GPIO.output(24,GPIO.HIGH)
   time.sleep(2)
   GPIO.output(24,GPIO.LOW)
   time.sleep(2)
