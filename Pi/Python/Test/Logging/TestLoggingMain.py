#!/usr/bin/python3

import time
import sys
import os
import logging
import logging.config
import json

""" initialise Logging - this sets up the JSON file path from which we load the logging configuration dictionary.
"""
def initialiseLogger():

    loggingInitialised = False 

    configFilePath = "./logging.json"

    if ( os.path.isfile(configFilePath) ):
        # Load the logging config from the config file. 
        configFile = open(configFilePath)
        configDict = json.load(configFile)

        logging.config.dictConfig(configDict)
    else:
        print("Logging config file not found")

    loggingInitialised = True ;


    if ( not loggingInitialised ):
        # Just use basic console logging. 
        logging.basicConfig(level='INFO')
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel('INFO');
        consoleHandler.setFormatter(logging.Formatter('%(name)-12s %(message)s'))
        logging.getLogger('').addHandler(consoleHandler) 
		
# ----------- MAIN ENTRY POINT ------------

mainLogger = None

try:

    # Now we've got the config file we can setup the logging properly 
    initialiseLogger()

    mainLogger = logging.getLogger(__name__) 

    mainLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

except KeyboardInterrupt:
    if ( mainLogger != None ):
        mainLogger.warn("Interrupted")

finally:
    if ( mainLogger != None ):
        mainLogger.info("-----------------------------------------------------------------------")


