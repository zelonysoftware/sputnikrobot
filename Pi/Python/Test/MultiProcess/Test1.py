
import time
from multiprocessing import Process, SimpleQueue, Value


class Fraction():
    
    def __init__(self, numerator, denominator):
        self.numerator = numerator 
        self.denominator = denominator 
        
    def print(self):
        return str(self.numerator) + "/" + str(self.denominator)


def serverProc(queue):
    
    numList = [ Fraction(3,4), Fraction(1,2), Fraction(5,4), 5, Fraction(0,1)  ]
    
    for i in numList :
        time.sleep(0.5)
        queue.put(i)
    
    return 

if __name__ == '__main__':

    queue = SimpleQueue() 
    value = Value("i", lock=True)
    
    proc = Process(target=serverProc, args=(queue,))
    
    proc.start(); 
    
    x = queue.get();  
    
    while x.numerator > 0 :
        print("-- " + x.print() + " --")
        x = queue.get() 
        
    proc.join()
    
    exit() 
    
    
    