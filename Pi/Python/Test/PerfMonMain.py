

import time
from PerfMon import PerfMon

perfmon = PerfMon()

for i in range(0, 2):
    print(str(i))
    perfmon.logStart("entry")
    time.sleep(0.02)
    perfmon.checkpoint("check1") 
    time.sleep(0.03)
    perfmon.checkpoint("check2") 
    time.sleep(0.04)
    perfmon.checkpoint("check3") 
    
perfmon.dump("./perfmon.csv"); 

