#!/usr/bin/python3

from MotorController.TestMotorController import TestMotorController
from Robot.Robot import Robot


try:

    theRobot = Robot( TestMotorController() )

    theRobot.moveLeftWheel(200);
    theRobot.moveRightWheel(200);
    theRobot.stop;

except KeyboardInterrupt:
    print("Interrupted")

finally:
    pass

