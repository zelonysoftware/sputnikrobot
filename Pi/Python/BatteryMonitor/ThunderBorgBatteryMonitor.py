
from BatteryMonitor.BatteryMonitorBase import BatteryMonitorBase

class ThunderBorgBatteryMonitor(BatteryMonitorBase):

    def __init__(self, thunderBorg):
        self.thunderBorg = thunderBorg 
        self.showingModeLed = True 
        self.restoreLedValue = 0 

    def setBatteryMonitoringLimits(self, low, high):
        self.thunderBorg.SetBatteryMonitoringLimits(low, high)

    def getBatteryLevel(self):
        if ( self.thunderBorg.foundChip ):
            batteryReading = self.thunderBorg.GetBatteryReading()
            return batteryReading
        
        return None


        
