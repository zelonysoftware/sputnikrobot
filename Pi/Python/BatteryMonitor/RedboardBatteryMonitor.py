
from BatteryMonitor.BatteryMonitorBase import BatteryMonitorBase
import ThirdParty.RedBoard.redboard as red
import smbus
import time



class RedboardBatteryMonitor(BatteryMonitorBase):

    ADC_bat_conversion_Value = 1107.0

    def __init__(self):
        print("USING REDBOARD BATTERY MONITOR")
        self.bus = smbus.SMBus(1)
        self.address = 0x48

    def setBatteryMonitoringLimits(self, low, high):
        pass

    ''' Code taken from Neil Lambert's check_bat.sh script at red robotics git hub. 
    '''
    def getBatteryLevel(self):
        self.bus.write_i2c_block_data(self.address, 0x01, [0xc3, 0x83])
        time.sleep(0.01)
        adc = self.bus.read_i2c_block_data(self.address,0x00,2)
        conversion_0 = (adc[1])+(adc[0]<<8)
        volts_0 = conversion_0 / self.ADC_bat_conversion_Value

        return volts_0 


