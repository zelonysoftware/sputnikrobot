#!/usr/bin/python3
import time
import RPi.GPIO as GPIO
import os
import logging
from Robot.Config.RobotConfig import RobotConfig 
from Robot.Config.ConfigUsingIniFile import ConfigUsingIniFile
from Robot.Config.SerialCommsConfig import SerialCommsConfig
import Robot.Logging as Logging
from MotorController import TestMotorController
from MotorController.RedboardMotorController import RedboardMotorController
from BatteryMonitor.BatteryMonitorBase import BatteryMonitorBase
from GamesController.PS3Controller import PS3Controller
from GamesController.PiHutController import PiHutController
from Robot.RobotModes.RobotModeList import RobotModeList
import Robot.RobotModes.RobotModes as RobotModes


# ---------------Initialise Logging before we import the Robot modules  ----------------------------

robotHome = os.getenv('ROBOTHOME')
print("Robot Home = " + str(robotHome)) 
if ( robotHome == None ):
    robotHome = os.getenv('HOME')
if ( robotHome == None ):
    robotHome = "."

configFile = robotHome + '/Config/' + "redbot.ini"
print("Using config file " + configFile)

configImpl = ConfigUsingIniFile(configFile)
robotConfig = RobotConfig(configImpl)

# Now we've got the config file we can setup the logging properly 
Logging.initialiseLogger(configImpl)

# --------------------------------------------------------------------------------------------------

#from GamesController.HTMLVisualController import HTMLVisualController
from Robot.Robot import Robot
from Robot.SerialComms.SerialCommsHandler import SerialCommsHandler
from Robot.RobotMonitorThread import RobotMonitorThread
import threading
import signal 


def createMotorController(config):

    motorControllerType = config.getMotorControllerType(default="RedBoard")

    motorController = None
    
    if ( motorControllerType == 'TEST' ):
        logging.info("Using Test Motor Controller")
        motorController = TestMotorController()
    elif (motorControllerType == 'RedBoard'):
        logging.info("Using RedBoard Motor Controller")
        motorController = RedboardMotorController()
    else:
        logging.info("Unrecognised Motor Controller type " + motorControllerType + " - defaulting to TEST")
        motorController = TestMotorController()

    return motorController



# ----------- MAIN ENTRY POINT ------------

theRobot = None
serialCommsHandler = None 
monitorThread = None
exitCleanly = False

def exitNicely(signum, frame):
    global exitCleanly
    exitCleanly = True

try:

    signal.signal(signal.SIGINT, exitNicely)
    signal.signal(signal.SIGTERM, exitNicely)

    GPIO.setmode(GPIO.BCM)
    
    motorController = createMotorController(robotConfig)
    batteryMonitor = BatteryMonitorBase()

    mainLogger = logging.getLogger(__name__) 

    mainLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    # Create and start the robot (this should default to remote controlled
    # mode - so won't do anything until we connect the PS3 controller). 
    mainLogger.info("Creating the Robot")
    theRobot = Robot( robotConfig, motorController, batteryMonitor, RobotModeList([RobotModes.MODE_REMOTE]) )

    # Create the games controller, passing the robot's callback handler. 
    mainLogger.info("Creating the Games Controller")
    gamesControllerHandler = theRobot.getGamesControllerHandler()
    ps3Controller = PS3Controller()
    ps3Controller.runAsThread(gamesControllerHandler)
    piHutController = PiHutController()
    piHutController.runAsThread(gamesControllerHandler)

    # create and startup the serial comms handler.
    mainLogger.info("Creating the SerialCommsHandler")
    serialCommsHandler = SerialCommsHandler(SerialCommsConfig(configImpl), theRobot)
    theRobot.addStateListener(serialCommsHandler) 
    serialCommsHandler.start() 

    # create and startup the status monitor thread. This monitors and logs the status
    # of the robot
    monitorThread = RobotMonitorThread(theRobot)
    monitorThread.start() 

    time.sleep(1)
    theRobot.start()

    allThreads = threading.enumerate()

    for aThread in allThreads:
        mainLogger.info("Thread " + aThread.name + " isDaemon = " + str(aThread.daemon))

    while not exitCleanly:
        time.sleep(1)

except KeyboardInterrupt:
    mainLogger.warn("Interrupted")

finally:
    mainLogger.info("-----------------------------------------------------------------------")
    mainLogger.info("Stopping Robot")
    if ( theRobot != None ):
        theRobot.stopWheels()
        theRobot.stop()
    mainLogger.info("Robot Stopped")
    mainLogger.info("Stopping Serial Handlers")
    if ( serialCommsHandler != None ):
        serialCommsHandler.stop()
    mainLogger.info("Serial Handlers Stopped") 
    if ( monitorThread != None ):
        monitorThread.stop()
    mainLogger.info("Monitor Thread Stopped")
    mainLogger.info("Cleanup GPIO")
    GPIO.setwarnings(False)
    GPIO.cleanup()
    mainLogger.info("GPIO Cleaned up")
    mainLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    #sys.exit commented out so we don't lose exception messages. 
    #sys.exit(0)

