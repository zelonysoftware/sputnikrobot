'''
Created on 9 Jan 2020

@author: zelony
'''
from BlockyVision.ClientServer.Server.BVServer import BVServer
from BlockyVision.ClientServer.Client.BVClient import BVClient 
from PIL import Image 
import time 

class clientCallback:
    
    def imageReceived(self, id, image):
        print("Received image"); 
        image.save('./run/pic_test.jpg'); 

if __name__ == '__main__':
    
    # Create Server thread
    server = BVServer(20060)
    
    # Start listening
    server.start() 
    
    # Create Client thread.
    client = BVClient('localhost', 20060)
    
    # Connect client 
    client.connect() 
    
    # Start Receiving images.
    client.setCallback(clientCallback())
    client.start()
    
    while( True ):
        time.sleep(5)
        print('Just sleeping')
        
        image = Image.open('./run/pic10.bmp')
        server.broadcastImage(0,image)
        
    
    
    
    
