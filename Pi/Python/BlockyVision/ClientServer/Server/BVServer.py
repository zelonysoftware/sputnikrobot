'''
Created on 9 Dec 2019

@author: zelony
'''

import io
import socket 
import time
from threading import Thread
from sys import byteorder
import threading

class BVServer(Thread):

    MAGIC = b'BVMSG'
    
    # Message type is always a single byte.     
    IMAGE_TYPE = b'I' 
    CONFIG_TYPE = b'C'

    def __init__(self, port):
        
        Thread.__init__(self)
        self.name='BVServerSocketThread: ' + str(port)
        
        self.serverSocket = socket.socket() 
        self.serverSocket.bind(('',port))
        self.serverSocket.settimeout(1.0)
        self.connectedClients = list() 
        
        self.socketLock = threading.Lock()
        
        self.isStopped = False 
        
    def startListener(self):
        self.isStopped = False 
        super().start() 
         
    def run(self):

        # start listening on the server socket. 
        self.serverSocket.listen()
        
        # start of while loop to keep accepting connections. 
        while not self.isStopped:
            # Accept connection 
            try:
                conn,addr = self.serverSocket.accept() 
            
                if ( not self.isStopped ):
                    print('Socket connection accepted from address ' +  str(addr))
            
                    # add to list of connected sockets.
                    self.connectedClients.append(conn) 

            except socket.timeout: 
                pass 
        
    def stopServer(self):
        self.isStopped = True 
        time.sleep(2)
        self.serverSocket.close()
        self.join()

    def broadcastImage(self, imageId, image):
        
        with self.socketLock:
        
            for connection in self.connectedClients:
                self.sendImage(connection, imageId, image)
             
    def broadcastBlockConfig(self, blocks):
        
        with self.socketLock: 
            for connection in self.connectedClients:
                self.sendBlockConfig(connection, blocks) 
        
    def sendImage(self, connection, imageId, image):

        try:
        
            byteStream = io.BytesIO(); 
            image.save(byteStream, format='BMP'); 
            imageBytes = byteStream.getvalue()
        
            imageNumBytes = len(imageBytes)
        
            self.sendBytes(connection, self.MAGIC, len(self.MAGIC))
            self.sendBytes(connection, self.IMAGE_TYPE, 1)
            
            imageIdBytes = imageId.to_bytes(4, 'big') 
            self.sendBytes(connection, imageIdBytes, 4)

            lenBytes = imageNumBytes.to_bytes(4, 'big')
            self.sendBytes(connection, lenBytes, 4) 
            self.sendBytes(connection, imageBytes, len(imageBytes))       

        except ConnectionResetError:

            print('Client diconnected') 
            self.connectedClients.remove(connection)
            connection.close()

    def sendBlockConfig(self, connection, blocks):
        
        try:
        
            if blocks is not None: 

                self.sendBytes(connection, self.MAGIC, len(self.MAGIC))
                
                # Tell the client we're sending some config.             
                self.sendBytes(connection, self.CONFIG_TYPE, 1)
                
                # Send number of blocks as 4 byte number (could be zero). 
                numBlocks = len(blocks)
                numBlocksBytes = numBlocks.to_bytes(4, 'big')
                self.sendBytes(connection, numBlocksBytes, 4)
                
                # Now send the blocks if there are any. 
                for name, pixel in blocks:
                    
                    nameBytes = name.encode('utf-8')
                    nameLen = len(nameBytes) 
                    nameLenBytes = nameLen.to_bytes(4, 'big')
                    self.sendBytes(connection, nameLenBytes, 4); 
                    
                    self.sendBytes(connection, nameBytes, nameLen) 
                    
                    
                    r,g,b = pixel.getRGB()
                    
                    for colour in (r, g, b):
                        colourBytes = colour.to_bytes(2, 'big')
                        self.sendBytes(connection, colourBytes, 2)

        except ConnectionResetError:

            print('Client diconnected') 
            self.connectedClients.remove(connection)
            connection.close()

        
    def sendBytes(self, connection, bytesToSend, numBytesToSend):
        
        totalBytesSent = 0 ;
        
        while (totalBytesSent < numBytesToSend):
            bytesSent = connection.send(bytesToSend[totalBytesSent:])
            totalBytesSent += bytesSent  
            
        
