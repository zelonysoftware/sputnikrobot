'''
Created on 9 Dec 2019

@author: zelony
'''
import io
import socket
from threading import Thread
from PIL import Image


class BVClient(Thread):
    '''
    classdocs
    '''
    MAGIC = b'BVMSG'
    
    IMAGE_TYPE = b'I' 
    CONFIG_TYPE = b'C'

    def __init__(self, ipAddress, port):

        Thread.__init__(self)
        self.clientSocket = socket.socket() 
        self.ipAddress = ipAddress 
        self.port = port 
        self.connection = None
        self.callback = None  
        self.isStopped = False 
        
    def connect(self):

        self.connection = self.clientSocket.connect((self.ipAddress, self.port));
        
    def setCallback(self, callback):
        self.callback = callback 
        
    def run(self):
        
        try:
            while (not self.isStopped):
                
                msgType = self.receiveMessageHeader()
                
                if ( msgType == self.IMAGE_TYPE ):
                    image_id, image = self.receiveImage()
                    self.callback.imageReceived(image_id, image)
                elif ( msgType == self.CONFIG_TYPE ): 
                    blockList = self.receiveBlockConfig()
                    self.callback.blockConfigReceived(blockList)
                else:
                    # do nothing - we'll ignore the message and search for the 
                    # next valid header. 
                    pass  
              
        except socket.error as sockerr:
            self.isStopped = True 
            print("Error reading from socket: " + str(sockerr))
        finally:
            self.clientSocket.close()
        
    def stop(self):
        self.isStopped = True 

    def receiveMessageHeader(self):

        # receive magic
        magicReceived = False 
        expectedMagicIndex = 0  
        
        # The magic byte string should be the 1st byte string in the message, 
        # but if not, we'll search until we find one. 
        while not magicReceived: 
            # read byte from socket
            readByte = self.recvBytes(1)
            
            if ( readByte[0] == self.MAGIC[expectedMagicIndex] ):
                expectedMagicIndex += 1 
                if ( expectedMagicIndex >= len(self.MAGIC) ):
                    magicReceived = True 
            else:
                expectedMagicIndex = 0 

        # receive message type
        messageType = self.recvBytes(1) 
        
        return messageType 

    
    def receiveImage(self):

        # receive image id 
        imageIdNetworkByteOrder = self.recvBytes(4)
        imageId = int.from_bytes(imageIdNetworkByteOrder, 'big')
        
        # receive image length 
        imageLengthNetworkByteOrder = self.recvBytes(4)
        imageLength = int.from_bytes(imageLengthNetworkByteOrder, 'big')
        
        # receive image 
        imageBytes = self.recvBytes(imageLength) 
        
        # convert to Image type. 
        byteStream = io.BytesIO(imageBytes)
        image = Image.open(byteStream) 
        image.load()
        
        return imageId, image 

    def receiveBlockConfig(self):
        
        # receive number of blocks
        numBlocksNetworkByteOrder = self.recvBytes(4) 
        numBlocks = int.from_bytes(numBlocksNetworkByteOrder, 'big')
        
        
        blocks = list() 
        
        for i in range(0,numBlocks):
            
            # read the name length. 
            nameLengthNetworkByteOrder = self.recvBytes(4) 
            nameLength = int.from_bytes(nameLengthNetworkByteOrder, 'big')
            
            # read the name 
            nameBytes = self.recvBytes(nameLength) 
            name = nameBytes.decode('utf-8')
            
            redBytes = self.recvBytes(2)
            red = int.from_bytes(redBytes, 'big')

            greenBytes = self.recvBytes(2)
            green = int.from_bytes(greenBytes, 'big')

            blueBytes = self.recvBytes(2)
            blue = int.from_bytes(blueBytes, 'big')
            
            blocks.append((name, (red,green,blue)))
            
        return blocks 
            
        
    def recvBytes(self, numBytes):
        
        imageBuffer = bytearray() ;
        
        bytesRead = 0 
        
        while( bytesRead < numBytes ):
            readBuffer = self.clientSocket.recv(numBytes - bytesRead);
            imageBuffer.extend(readBuffer)
            bytesRead += len(readBuffer) 
        
        return imageBuffer 
        
    
         
        
        
        