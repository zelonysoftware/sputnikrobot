'''
Created on 9 Jan 2020

@author: zelony
'''
from BlockyVision.ClientServer.Server.BVServer import BVServer
from PIL import Image 
import time 


if __name__ == '__main__':
    
    # Create Server thread
    print(f'Listening on port 20060')
    server = BVServer(20060)
    
    # Start listening
    print('Starting Listening Thread'); 
    server.start() 
    print('Listener Thread Started'); 
    
    while( True ):
        time.sleep(0.2)
        
        print('Opening image 1'); 
        image = Image.open( 'D:\\Development\\Robot\\Sputnik\\sputnikrobot\\Pi\\Python\\BlockyVision\\run\\juggling_balls_1.jpg')
        print(f'Broadcasting image 1 to {len(server.connectedClients)} clients')
        server.broadcastImage(0,image)

        time.sleep(0.2)

        print('Opening image 2'); 
        image = Image.open( 'D:\\Development\\Robot\\Sputnik\\sputnikrobot\\Pi\\Python\\BlockyVision\\run\\juggling_balls_dark.jpg')
        print(f'Broadcasting image 2 to {len(server.connectedClients)} clients')
        server.broadcastImage(1,image)
        
    
    
    
    
