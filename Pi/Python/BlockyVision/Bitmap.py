'''
Created on 10 Oct 2019

@author: zelony
'''

class Bitmap(object):
    
    '''
    classdocs
    '''

    def __init__(self, numRows, numCols, initial=None):
        '''
        Constructor
        '''
        self.numRows = numRows 
        self.numCols = numCols 
        
        self.pixels = [initial] * (numRows * numCols)
        
    def getNumRows(self):
        return self.numRows 
    
    def getNumCols(self):
        return self.numCols
        
    def getPixel(self, rowIndex, colIndex):
        if ((rowIndex >= self.numRows) or (colIndex >= self.numCols)):
            return None
        return ( self.pixels[colIndex + (rowIndex * self.numCols)])
    
    def setPixel(self, rowIndex, colIndex, pixel):
        if ((rowIndex < self.numRows) and (colIndex < self.numCols)):
            self.pixels[colIndex + (rowIndex * self.numCols)] = pixel 
        
class Pixel(object):
    '''
    Pixel represents a pixel in a bitmap. 
    '''
    def __init__(self, rgb):
        self.red = rgb[0]
        self.green = rgb[1] 
        self.blue = rgb[2] 
        
        self.hue = 0 
        self.saturation = 0 
        self.value = 0 
        
        self.isConverted = False 

        
    def __eq__(self,other):
        return( self.red == other.red and self.green == other.green and self.blue == other.blue )
    
    def getRGB(self):
        return ( self.red, self.green, self.blue )
    
    def getHexString(self):
        return('#{:02X}{:02X}{:02X}'.format(self.red, self.green, self.blue))
    
    def getHSV(self):
        if ( not self.isConverted ):
            self.convertToHSV()
           
        return (self.hue,self.saturation,self.value)
    
    def convertToHSV(self):
    
        # Convert each value in range 0 - 1 for the calculations
        redValue = self.red / 255
        greenValue = self.green / 255
        blueValue = self.blue / 255
        
        cmax = max(redValue, greenValue, blueValue)
        cmin = min(redValue, greenValue, blueValue)
        delta = cmax - cmin 
        
        self.hue = 0
        
        if ( delta > 0 ): 
            if ( cmax == redValue ):
                self.hue = 60 * (((greenValue - blueValue) / delta) % 6)
            elif ( cmax == greenValue ):
                self.hue = 60 * (((blueValue - redValue) / delta) + 2)
            else:
                self.hue = 60 * (((redValue - greenValue) / delta) + 4)
            
        self.saturation = 0 
        if ( cmax > 0 ):
            self.saturation = (delta / cmax) * 100  
    
        self.value = cmax * 100 
        
        self.isConverted = True
        
        return
    
class PixelConstants(object):
    
    RED = Pixel((255,0,0))
    GREEN = Pixel((0,255,0))
    BLUE = Pixel((0,0,255))

    YELLOW = Pixel((255,255,0))
    CYAN = Pixel((0,255,255))
    MAGENTA = Pixel((255,0,255))
    
    BLACK = Pixel((0,0,0))
    WHITE = Pixel((255,255,255))
                
        
