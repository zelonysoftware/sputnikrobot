

import ctypes 
from ctypes import c_int 
from ctypes import c_char
from ctypes import c_bool
from ctypes import c_char_p
from BlockyVision.BVBlock import Block
from BlockyVision.Bitmap import Pixel

#Map the C structures
class CImage(ctypes.Structure):
    _fields_ = [("num_cols",   c_int),
                ("num_rows",   c_int),
                ("image",      ctypes.POINTER(c_char))]

class CBBox(ctypes.Structure):
    _fields_ = [("start_col",   c_int),
                ("end_col",     c_int),
                ("start_row",   c_int),
                ("end_row",     c_int) ]

class CBVConfig(ctypes.Structure):
    _fields_ = [("min_block_cols",  c_int),
                ("min_block_rows",  c_int), 
                ("threshold",       c_int) ]

class CBlock(ctypes.Structure):
    _fields_ = [("name",    c_char_p),
                ("r",       c_char),
                ("g",       c_char),
                ("b",       c_char),
                ("top",     c_int),
                ("bottom",  c_int),
                ("left",    c_int),
                ("right",   c_int),
                ("found",   c_bool) ]


class TurboBlockFinder(object):

    def __init__(self, libPath):
        self.sharedLib = ctypes.CDLL(libPath)
        self.findBlocksFunc = self.sharedLib.find_blocks
        self.findBlocksFunc.argtypes=[CImage, CBVConfig, CBBox, c_int, ctypes.POINTER(CBlock)]

    def findBlocks(self, image, cropX1, cropY1, cropX2, cropY2, threshold, minColSize, minRowSize, blocksToFind):

        # TODO - The tricky bit - we need to try and get the PIL buffer and send it to our C program!!!
        cImageBuffer = (c_char * (image.size[0] * image.size[1] * 3)).from_buffer_copy(image.tobytes())
        cimage = CImage(image.size[0], image.size[1], ctypes.cast(cImageBuffer,ctypes.POINTER(c_char)))

        cConfig = CBVConfig(minColSize, minRowSize, threshold) 
        cBBox = CBBox(cropX1,cropX2,cropY1,cropY2)

        cNumBlocks = len(blocksToFind) 
        cBlocksToFind = (CBlock * cNumBlocks)() 

        for i in range(0,cNumBlocks):

            blockName, blockColour = blocksToFind[i] 
            r,g,b = blockColour.getRGB() 
            cBlocksToFind[i] = CBlock(blockName.encode('utf-8'), r, g, b, False, 0, 0, 0, 0);

        numBlocksFound = self.findBlocksFunc(cimage, cConfig, cBBox, cNumBlocks, cBlocksToFind)

        blocksFound = list() 

        if ( numBlocksFound > 0 ):

            for cBlock in cBlocksToFind:

                if ( cBlock.found ):
                    blockFound = Block(cBlock.name.decode("utf-8"), Pixel((cBlock.r[0],cBlock.g[0],cBlock.b[0])), cBlock.top, cBlock.bottom, cBlock.left, cBlock.right)
                    blocksFound.append(blockFound)

        return blocksFound 
