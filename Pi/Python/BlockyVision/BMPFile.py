'''
Created on 12 Oct 2019

@author: zelony
'''
from BlockyVision.Bitmap import Bitmap
from BlockyVision.Bitmap import Pixel
from PIL import Image 

BMP_OFFSET_POS = 10 
BMP_OFFSET_LEN = 4
PIXEL_WIDTH_POS = 18
PIXEL_WIDTH_LEN = 4
PIXEL_HEIGHT_POS = 22
PIXEL_HEIGHT_LEN = 4 

BITS_PER_PIXEL = 24  # should get from header

class BMPFile(object):
    '''
    classdocs
    '''

    def __init__(self, filename):
        self.filename = filename
        self.fileArray = None
        self.bmpOffset = None  

    def getImage(self):
        
        return Image.open(self.filename) 
        
    def getBitmap(self):
               
        # open file and read contents into bytearray
        f = open(self.filename, "rb") 
        self.fileArray = bytearray(f.read())
        f.close() 

        # Extract the header information     
        self.bmpOffset = self.fromLittleEndian(self.fileArray, BMP_OFFSET_POS, BMP_OFFSET_LEN)
        imageWidth = self.fromLittleEndian(self.fileArray, PIXEL_WIDTH_POS, PIXEL_WIDTH_LEN)     
        imageHeight = self.fromLittleEndian(self.fileArray, PIXEL_HEIGHT_POS, PIXEL_HEIGHT_LEN)  
    
        # each row is padded to have a multiple of 4 bytes. 
        rowBytes = int(((BITS_PER_PIXEL * imageWidth)+31)/32) * 4 
    
        print(f'bmpOffset = {self.bmpOffset:02x}, width = {imageWidth}, height = {imageHeight}')   
        print(f'rowBytes = {rowBytes}')

        # create Bitmap object and return 
        bmpObject = Bitmap(imageHeight, imageWidth)
        
        # go through file bmp creating pixels
        for row in range(0, imageHeight):
            
            for col in range(0, imageWidth):
                
                # bmp in file has the last row 1st - so we want to effecively invert it. 
                bmpFileRow = (imageHeight - row) - 1
                i = self.bmpOffset + (bmpFileRow * rowBytes) + col * 3
                
                pixel = Pixel(  self.fileArray[i+2],    # red
                                self.fileArray[i+1],    # green 
                                self.fileArray[i]       # blue 
                                )
                #print(f'calling setPixel({row},{col})')
                bmpObject.setPixel(row,col,pixel)  

        return bmpObject 

    ''' 
        Rewrite the file using a Bitmap object. (We assume the size of the bitmap matches the header 
    '''
    def rewrite(self, newFilename, bmp):
        
        print(f'newFilename = {newFilename}, bmp = {bmp}')
        # each row is padded to have a multiple of 4 bytes. 
        rowBytes = int(((BITS_PER_PIXEL * bmp.numCols)+31)/32) * 4 
        
        # go through bitmap - writing the pixel values back to the file array
        for row in range(0, bmp.getNumRows()):
            
            for col in range(0, bmp.getNumCols()):

                # remember to invert again! 
                bmpFileRow = (bmp.getNumRows() - row) - 1
                i = self.bmpOffset + (bmpFileRow * rowBytes) + col * 3
                
                rgb = bmp.getPixel(row,col).getRGB() 
                
                self.fileArray[i] = rgb[2]
                self.fileArray[i+1] = rgb[1]
                self.fileArray[i+2] = rgb[0] 
            
        f = open(newFilename, "bw")
        f.write(self.fileArray)
        f.close()

    def fromLittleEndian(self, inBytes, pos, numBytes):
    
        num = 0  
        mult = 1 
        for i in range(pos, pos + numBytes):
            num = num + (inBytes[i] * mult) 
            mult = mult * 256 
            
        return num 
