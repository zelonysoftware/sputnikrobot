#!/usr/bin/python3
'''
Created on 7 Oct 2019

@author: zelony
'''

import time
from BlockyVision.BMPFile import BMPFile
from BlockyVision.Bitmap import Pixel
from BlockyVision.BVBlockFinder import BVBlockFinder
from PIL import Image


LOWRES_FILEROOT = './run/blocks2_100x75'
MIDRES_FILEROOT = './run/blocks2_500'
HIGHRES_FILEROOT = './run/blocks2'

def doFindStandard(fileroot):

    # open file
    bmpFile = BMPFile(fileroot + ".bmp")
    
    image = bmpFile.getImage()
    image.load()

    print("---- STANDARD MODE ----")
    print("---- Image Size =  %dx%d" % (image.size[0], image.size[1]))
    
    startTime = time.time()

    blockFinder = BVBlockFinder()
    blockFinder.setThreshold(35)
    blockFinder.setMinBlockWidthPcent(5)
    blockFinder.setMinBlockHeightPcent(5)

    blockFinder.setBlockColour('greenBlock', Pixel((2,136,120)))
    blockFinder.setBlockColour('redBlock', Pixel((192,56,85)))
    blockFinder.setBlockColour('blueBlock', Pixel((26,88,142)))

    blockList = blockFinder.findBlocks(image)
    
    endTime = time.time()
    
    for block in blockList:
        block.printBlock()
    
    bvTime = round((endTime - startTime) * 1000)
    print(f'---- Find block time = {bvTime}ms')

def doFindTurbo(fileroot):

    # open file
    bmpFile = BMPFile(fileroot + ".bmp")
    
    image = bmpFile.getImage()
    image.load()
    
    print("---- TURBO MODE ----")
    print("---- Image Size =  %dx%d" % (image.size[0], image.size[1]))

    startTime = time.time()

    blockFinder = BVBlockFinder()
    blockFinder.setThreshold(35)
    blockFinder.setMinBlockWidthPcent(5)
    blockFinder.setMinBlockHeightPcent(5)

    blockFinder.setBlockColour('greenBlock', Pixel((2,136,120)))
    blockFinder.setBlockColour('redBlock', Pixel((192,56,85)))
    blockFinder.setBlockColour('blueBlock', Pixel((26,88,142)))
    blockFinder.setTurboFinderLib("/home/pi/sputnikrobot/Pi/C/BlockyVision/libbv.so")

    blockList = blockFinder.findBlocks(image)
    
    endTime = time.time()
    
    for block in blockList:
        block.printBlock()
    
    bvTime = round((endTime - startTime) * 1000)
    print(f'Find block time = {bvTime}ms')

if __name__ == '__main__':

    doFindTurbo(LOWRES_FILEROOT)
    doFindTurbo(MIDRES_FILEROOT)
    doFindTurbo(HIGHRES_FILEROOT)

    doFindStandard(LOWRES_FILEROOT)
    doFindStandard(MIDRES_FILEROOT)
    doFindStandard(HIGHRES_FILEROOT)


