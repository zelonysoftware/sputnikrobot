'''
Created on 9 Jan 2020

@author: zelony
'''
from BlockyVision.ClientServer.Server.BVServer import BVServer
from PIL import Image 
import io
import time 
import picamera


if __name__ == '__main__':
    
    # Create Server thread
    print(f'Listening on port 20060')
    server = BVServer(20060)
    
    # Start listening
    print('Starting Listening Thread'); 
    server.start() 
    print('Listener Thread Started'); 
    
    stream = io.BytesIO() 

    with picamera.PiCamera() as camera:
        camera.resolution = (100,75)
        time.sleep(2)
       
        for camImage in camera.capture_continuous(stream, 'jpeg'):
            stream.seek(0)
            pilImage = Image.open(stream)
            server.broadcastImage(0,pilImage) 
            stream.seek(0)
            stream.truncate()
            
            time.sleep(0.2) 

        

    
    
    
    
