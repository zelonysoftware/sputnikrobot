'''
Created on 26 Nov 2020

@author: zelony
'''
import time 

from PIL import Image 
from PIL import ImageTk

from BlockyVision.BVHorizonFinder import BVHorizonFinder 

def showFilteredImages(gui):
    
    if ( gui.pilImage != None ):

        horizonFinder = BVHorizonFinder(gui.pilImage)
    
        startTime = time.time() 
        horizon = horizonFinder.findHorizon((40,0,60,100))
        endTime = time.time()

        #print("Horizon find time = %d milliseconds" % (int((endTime - startTime)*1000)))
    
        # image will now have been filtered.
        gui.pilFilteredImage = horizonFinder.getImage()
        resizedFilteredImage = gui.pilFilteredImage.resize((gui.canvas2.winfo_width(),gui.canvas2.winfo_height())) 
        gui.tkFilteredImage = ImageTk.PhotoImage(image=resizedFilteredImage); 
        gui.canvas2Image = gui.canvas2.create_image(0, 0, image = gui.tkFilteredImage, anchor="nw") 
         
        # Now generate a new bmp with just the found blocks. 
        #gui.pilBlockImage = Image.new("RGB",gui.pilImage.size)
        gui.pilBlockImage = gui.pilImage.copy()
    
        resizedBlockImage = gui.pilBlockImage.resize((gui.canvas3.winfo_width(),gui.canvas3.winfo_height())) 
        gui.tkBlockImage = ImageTk.PhotoImage(image=resizedBlockImage); 
        gui.canvas3Image = gui.canvas3.create_image(0, 0, image = gui.tkBlockImage, anchor="nw")
        
        # Now add the labels to the blocks.
        showHorizon(gui, horizon)


def showHorizon(gui, horizon):

    if ( gui.horizonLine is not None ):
        line, label = gui.horizonLine
        gui.canvas3.delete(line) 
        gui.canvas3.delete(label) 
        gui.horizonLine = None 
        
    if ( horizon is not None ):
        leftX, leftY = gui.translateImageToCanvas(gui.pilImage, gui.canvas3, 0, horizon.leftY)
        rightX, rightY = gui.translateImageToCanvas(gui.pilImage, gui.canvas3, gui.pilImage.width - 1, horizon.rightY)
        
        line = gui.canvas3.create_line(leftX,leftY,rightX,rightY,fill='orange',width=5)
        
        angle = horizon.getAngle()
        angleText = "{:0.2f} degrees".format(angle)
        posx = gui.canvas3.winfo_width() / 2
        posy = gui.canvas3.winfo_height() / 2
        label = gui.canvas3.create_text(posx, posy, text=angleText, fill='orange')

        gui.horizonLine = (line,label)

            
