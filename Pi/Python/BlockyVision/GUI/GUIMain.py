
import os
import time
import tkinter as tk
import tkinter.constants as tkc
from tkinter import filedialog
from PIL import Image 
from PIL import ImageTk
import BlockyVision.GUI.Dialogs.ServerDialog as ServerDialog
import BlockyVision.GUI.BlockModeFilters as BlockModeFilters
import BlockyVision.GUI.HorizonModeFilters as HorizonModeFilters
import BlockyVision.GUI.VectorModeFilters as VectorModeFilters
from BlockyVision.ClientServer.Client.BVClient import BVClient
import BlockyVision.GUI.Dialogs.ConfigureBlocksDialog as ConfigureBlocksDialog
from BlockyVision.GUI.GUIConfig import GUIConfig, BlockConfig
import BlockyVision.GUI.Dialogs.SaveSelectionAsBlockDialog as SaveSelectionAsBlockDialog
from BlockyVision.BVVectorFinder import BVVectorFinder

BLOCK_MODE = 0 
HORIZON_MODE = 1
VECTOR_MODE_BLACK = 2
VECTOR_MODE_WHITE = 3 

class BlockyVisionGui():
    
    class SocketCallback():
        
        def __init__(self, gui):
            self.gui = gui 
        
        def imageReceived(self, id, image):

            self.gui.pilImage = image
            
            if ( self.gui.isRecording ):
                print("Writing image to %s" % (self.gui.recordingFolder))
                filename = str(id) + ".bmp"
                filepath = os.path.join(self.gui.recordingFolder,filename)
                self.gui.pilImage.save(filepath)
            
            #print('Received new image')
            resizedImage = self.gui.pilImage.resize((self.gui.canvas1.winfo_width(),self.gui.canvas1.winfo_height())) 
            self.gui.tkImage = ImageTk.PhotoImage(image=resizedImage)
            self.gui.canvas1Image = self.gui.canvas1.create_image(0, 0, image = self.gui.tkImage, anchor = "nw")
            self.gui.showFilteredImages()
            
        def blockConfigReceived(self, blocks):
            
            newServerBlockList = list() 
            
            for name, rgb in blocks: 
                
                blockConfig = BlockConfig() 
                blockConfig.name = name  
                
                r, g, b = rgb 
                
                blockConfig.red = r 
                blockConfig.green = g 
                blockConfig.blue = b
                blockConfig.enabled = True 
                
                newServerBlockList.append(blockConfig) 
                
            self.gui.serverBlockList = newServerBlockList  
            
            self.gui.showFilteredImages()
    
    def __init__(self, window):
        
        self.config = GUIConfig()
        
        self.serverBlockList = None 
        
        self.client = None
        
        self.window = window 
        
        window.protocol("WM_DELETE_WINDOW", self.onClose)
        
        self.mainFrame = tk.Frame(self.window)
        self.thresholdFrame = tk.Frame(self.window)
        self.modeFrame = tk.Frame(self.window)
        
        mainMenu = tk.Menu(window)
        self.window.config(menu = mainMenu)
        
        self.fileMenu = tk.Menu(mainMenu)
        mainMenu.add_cascade(label="File", menu=self.fileMenu)
        self.fileMenu.add_command(label="Open Image...", command = self.openImageMenuCmd)
        self.fileMenu.add_command(label="Save Image...", command = self.saveImageMenuCmd)
        self.fileMenu.entryconfigure("Save Image...", state=tkc.DISABLED)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Load Config...", command = self.loadConfigMenuCmd)
        self.fileMenu.add_command(label="Save Config...", command = self.saveConfigMenuCmd)

        serverMenu = tk.Menu(mainMenu)
        mainMenu.add_cascade(label="Server", menu=serverMenu)
        serverMenu.add_command(label="Connect to Server...", command = self.openServerMenuCmd)
        serverMenu.add_command(label="Disconnect from Server", command = self.closeServerMenuCmd) 
        serverMenu.add_command(label="Start Recording...", command = self.startRecording) 
        serverMenu.add_command(label="Stop Recording", command = self.stopRecording)
        self.isRecording = False 
        self.recordingFolder = None 
        
        blocksMenu = tk.Menu(mainMenu)
        mainMenu.add_cascade(label="Blocks", menu=blocksMenu)
        blocksMenu.add_command(label="Configure...", command = self.configureBlocksCmd)
        
        # TODO - consider moving the image windows into a separate class 
        self.canvas1 = tk.Canvas(self.mainFrame,bg="black", bd=0)
        self.canvas1.pack(side="left", fill="both", expand=True)
        self.canvas1.bind("<Configure>", self.resizeImage) 
        self.canvas1.bind("<Button-1>", self.imageMouseButton) 
        self.canvas1.bind("<B1-Motion>", self.imageMouseMove) 
        self.canvas1.bind("<ButtonRelease-1>", self.imageMouseRelease) 
        
        self.canvas2 = tk.Canvas(self.mainFrame,bg="black", bd=0)
        self.canvas2.pack(side = "left", fill="both", expand=True)
        
        self.canvas3 = tk.Canvas(self.mainFrame,bg="black", bd=0)
        self.canvas3.pack(side = "left", fill="both", expand=True)

        self.mouseAnchor = (0,0) 
        self.lastRect = None 
        self.lastRgbLabel = None 
                
        self.mainFrame.pack(fill="both", expand=True)

        self.mode = tk.IntVar()
        self.mode.set(BLOCK_MODE) 
        self.modeFrame.pack(fill="both", expand=True)
        self.blockModeRadioButton = tk.Radiobutton(self.modeFrame, text="Block Mode", value=BLOCK_MODE, variable=self.mode, command=self.modeButtonChanged)
        self.blockModeRadioButton.pack()
        self.horizonModeRadioButton = tk.Radiobutton(self.modeFrame, text="Horizon Mode", value=HORIZON_MODE, variable=self.mode, command=self.modeButtonChanged)
        self.horizonModeRadioButton.pack()
        self.vectorModeBlackRadioButton = tk.Radiobutton(self.modeFrame, text="Vector Mode (Black Line)", value=VECTOR_MODE_BLACK, variable=self.mode, command=self.modeButtonChanged)
        self.vectorModeBlackRadioButton.pack()
        self.vectorModeWhiteRadioButton = tk.Radiobutton(self.modeFrame, text="Vector Mode (White Line)", value=VECTOR_MODE_WHITE, variable=self.mode, command=self.modeButtonChanged)
        self.vectorModeWhiteRadioButton.pack()

        self.thresholdFrame.pack(fill="both", expand=True) 

        self.thresholdJob = None 
        
        self.thresholdSlider = tk.Scale(self.thresholdFrame, from_=0, to=100, orient=tkc.HORIZONTAL, command=self.thresholdChanged, label = "Colour Threshold")
        self.thresholdSlider.pack() 
        
        self.colourThreshold = 25 
        self.thresholdSlider.set(self.colourThreshold) 
        
        self.pilImage = None 
        self.tkImage = None
        self.canvas1Image = None         
        
        self.pilFilteredImage = None
        self.tkFilteredImage = None
        self.canvas2Image = None 
        
        self.pilBlockImage = None
        self.tkBlockImage = None 
        self.canvas3Image = None 
        
        self.blockLabels = None  
        self.horizonLine = None 
        self.vectorLines = None
        self.vectorLabel = None

    def menuFunction(self):
        pass
    
    def onClose(self):
        # Make sure server is closed 
        self.closeServerMenuCmd()
        self.window.destroy()
    
    def openServerMenuCmd(self):
        
        if ( self.client != None ):
            self.client.stop() 

        # create dialog to get the connection details 
        host, port = ServerDialog.askServerHostPort(self.mainFrame, self.config)
        
        if ( host is not None ): 
            
            self.client = BVClient(host, port)
            self.client.setCallback(self.SocketCallback(self))
            self.client.connect() 
            self.client.start()
            
    def closeServerMenuCmd(self):
        
        try: 
            if ( self.client is not None ):
                self.client.stop() 
                print("Closing Socket Client")
                self.client.join(2.0)
                print("Client socket closed")
                self.client = None
                self.serverBlockList = None
        except RuntimeError as re: 
            print('Runtime error occurred closing client thread:' + re) 
            
    def startRecording(self):
        
        folder = filedialog.askdirectory() 
        
        if ( folder is not None ):
            self.recordingFolder = folder 
            self.isRecording = True 
        
    def stopRecording(self):
        
        self.isRecording = False 
        time.sleep(0.5) # make sure image capture thread has seen the change! 
        self.recordingFolder = None  
        
    def configureBlocksCmd(self):
        
        ConfigureBlocksDialog.showDialog(self.mainFrame, self.config)
            
        self.showFilteredImages()
    
    def openImageMenuCmd(self):
        
        fileName = filedialog.askopenfilename(title = "Open Image File", filetypes=(("Image Files", "*.bmp;*.jpg;*.png"),("All Files","*.*")))
        
        if (fileName):
            print("filename = " + fileName)
            self.pilImage = Image.open(fileName)
            resizedImage = self.pilImage.resize((self.canvas1.winfo_width(),self.canvas1.winfo_height())) 
            self.tkImage = ImageTk.PhotoImage(image=resizedImage)
            
            self.canvas1Image = self.canvas1.create_image(0, 0, image = self.tkImage, anchor = "nw")
    
            self.showFilteredImages()
            
            #self.mainFrame.title(fileName)

    def saveImageMenuCmd(self):
        
        fileName = filedialog.asksaveasfilename(defaultextension=".bmp")
        
        if (fileName is not None):
            print("save as filename = " + fileName)
            self.pilImage.save(fileName)
         
    def loadConfigMenuCmd(self):
        tk.messagebox.showwarning("Warning", "Sorry - I haven't got round to this bit yet")

    def saveConfigMenuCmd(self):
        tk.messagebox.showwarning("Warning", "Sorry - I haven't got round to this bit yet")
        
    def resizeImage(self, event):
        if ( self.pilImage != None ):
            resizedImage = self.pilImage.resize((self.canvas1.winfo_width(),self.canvas1.winfo_height()))
            self.tkImage = ImageTk.PhotoImage(image=resizedImage) 
            self.canvas1.itemconfig(self.canvas1Image,image=self.tkImage)

            resizedFilteredImage = self.pilFilteredImage.resize((self.canvas2.winfo_width(),self.canvas2.winfo_height()))
            self.tkFilteredImage = ImageTk.PhotoImage(image=resizedFilteredImage) 
            self.canvas2.itemconfig(self.canvas2Image,image=self.tkFilteredImage)

            resizedBlockImage = self.pilBlockImage.resize((self.canvas3.winfo_width(),self.canvas3.winfo_height()))
            self.tkBlockImage = ImageTk.PhotoImage(image=resizedBlockImage) 
            self.canvas3.itemconfig(self.canvas3Image,image=self.tkBlockImage)
            
            self.showFilteredImages()

         
    def imageMouseButton(self, event):
        
        if ( self.pilImage is not None ):
            imgX, imgY = self.translateCanvasToImage(self.canvas1, self.pilImage, event.x, event.y)
            print("mouse clicked x={}, y={}".format(imgX, imgY))
            self.mouseAnchor = (event.x, event.y)
            if ( self.lastRect is not None ):
                self.canvas1.delete(self.lastRect)
                
            if ( self.lastRgbLabel is not None ):
                self.canvas1.delete(self.lastRgbLabel) 
                
            self.lastRect = None
            self.lastRgbLabel = None   
        
        
    def imageMouseMove(self, event):
        if ( self.pilImage is not None ):
            imgX, imgY = self.translateCanvasToImage(self.canvas1, self.pilImage, event.x, event.y)
            print("mouse moved x={}, y={}".format(imgX, imgY))
            
            if ( self.lastRect != None ):
                self.canvas1.delete(self.lastRect)
                
            self.lastRect = self.canvas1.create_rectangle(self.mouseAnchor[0], self.mouseAnchor[1], event.x, event.y)
        
    def imageMouseRelease(self, event):
        if ( self.pilImage is not None ):
            print("mouse released x={}, y={}".format(event.x, event.y))
            
            # go through the pixel in the image.
            if ( self.lastRect is not None ):
                rectBbox = self.canvas1.bbox(self.lastRect)
                print("rect = " + str(rectBbox)) 
                leftX = max(0,rectBbox[0])
                topY = max(0,rectBbox[1])
                rightX = min(self.canvas1.winfo_width()-1, rectBbox[2])
                bottomY = min(self.canvas1.winfo_height()-1, rectBbox[3]) 
                print("rect2 = " + str((leftX, topY, rightX, bottomY)))
                leftCol, topRow = self.translateCanvasToImage(self.canvas1, self.pilImage, leftX, topY)
                rightCol, bottomRow = self.translateCanvasToImage(self.canvas1, self.pilImage, rightX, bottomY)
                
                totalR = 0 
                totalG = 0 
                totalB = 0 
                numPixels = 0 
                
                print("tr = {}, br = {}, lc = {}, rc = {}".format(topRow, bottomRow, leftCol, rightCol))
                for row in range(topRow, bottomRow):
                    for col in range(leftCol, rightCol):
                        
                        r,g,b = self.pilImage.getpixel((col,row))
                        totalR += r 
                        totalG += g 
                        totalB += b 
                        numPixels += 1 

                avgR = int(totalR / numPixels) 
                avgG = int(totalG / numPixels) 
                avgB = int(totalB / numPixels) 
                
                labelText = "R={}, G={}, B={}".format(avgR, avgG, avgB)
                print(labelText)
                self.lastRgbLabel = gui.canvas1.create_text(60, 20, text=labelText, fill='white')
                
                SaveSelectionAsBlockDialog.showDialog(self.mainFrame, self.config, avgR, avgG, avgB)
                
                self.showFilteredImages()
                
    def getBlockList(self):
        
        blockList = self.config.getBlocks() 
        
        if ( self.config.useServerBlockConfig() and (self.serverBlockList is not None)):
            blockList.extend(self.serverBlockList)
        
        return blockList

    def showFilteredImages(self):
        
        # Must be an image so enable save option
        if ( self.pilImage is not None ):
            self.fileMenu.entryconfigure("Save Image...", state=tkc.ACTIVE)
        
        if (self.mode.get() == BLOCK_MODE):
            BlockModeFilters.showFilteredImages(self)
            
        elif (self.mode.get() == HORIZON_MODE):
            HorizonModeFilters.showFilteredImages(self)
            
        elif (self.mode.get() == VECTOR_MODE_BLACK):
            VectorModeFilters.showFilteredImages(self, BVVectorFinder.BLACK)
            
        elif (self.mode.get() == VECTOR_MODE_WHITE):
            VectorModeFilters.showFilteredImages(self, BVVectorFinder.WHITE)

    def thresholdChanged(self, event):
        # Schedule with a half second buffer so we don't keep trying to react every
        # time the threshold is changed slightly. 
        if ( self.thresholdJob ): 
            self.window.after_cancel(self.thresholdJob) 
        
        self.thresholdJob = self.window.after(500, self.thresholdUpdatedJob) 
        
    def thresholdUpdatedJob(self):
        
        self.thresholdJob = None 
        
        self.colourThreshold = self.thresholdSlider.get() 
        
        self.showFilteredImages()
        
    def modeButtonChanged(self):
        
        self.showFilteredImages() 
        
        
    def translateCanvasToImage(self, canvas, image, cx, cy):
        imgX = int((cx * image.width) / canvas.winfo_width())
        imgY = int((cy * image.height) / canvas.winfo_height())
        
        return imgX, imgY
    
    def translateImageToCanvas(self, image, canvas, ix, iy):
        cvsX = int((ix * canvas.winfo_width()) / image.width)
        cvsY = int((iy * canvas.winfo_height()) / image.height)
        
        return cvsX, cvsY 
        
if __name__ == '__main__':

    window = tk.Tk()
    window.title("Blocky Vision") 
    window.option_add("*tearOff", tkc.FALSE)

    gui = BlockyVisionGui(window) 
    
    window.mainloop()



