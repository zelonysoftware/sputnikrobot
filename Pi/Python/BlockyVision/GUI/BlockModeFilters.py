'''
Created on 26 Nov 2020

@author: zelony
'''
import time 
import sys 
import os

from PIL import Image 
from PIL import ImageTk


from BlockyVision.BVBlockFinder import BVBlockFinder 
from BlockyVision.BVBlockFinder import Pixel 

def showFilteredImages(gui):
    
    if ( gui.pilImage != None ):

        blockFinder = BVBlockFinder()

        blockFinder.setThreshold(gui.colourThreshold); 
        blockFinder.setMinBlockWidthPcent(3)
        blockFinder.setMinBlockHeightPcent(3)

        turboPath = os.getenv("BVTURBOLIB")
        if ( turboPath is not None ):
            print("Importing TurboLib")
            blockFinder.setTurboFinderLib(turboPath)

        blockList = gui.getBlockList()
        
        for block in blockList:
            
            if block.enabled:
                blockName = block.name ;
                red = block.red
                green = block.green
                blue = block.blue  
        
                blockFinder.setBlockColour(blockName, Pixel((red,green,blue)))
    
        startTime = time.time() 
        foundBlockList = blockFinder.findBlocks(gui.pilImage,(0,0,100,100))
        endTime = time.time()

        print("Block find time = %d milliseconds" % (int((endTime - startTime)*1000)))
    
        for foundBlock in foundBlockList:
            foundBlock.printBlock()
    
        # image will now have been filtered.
        gui.pilFilteredImage = blockFinder.getImage()
        resizedFilteredImage = gui.pilFilteredImage.resize((gui.canvas2.winfo_width(),gui.canvas2.winfo_height())) 
        gui.tkFilteredImage = ImageTk.PhotoImage(image=resizedFilteredImage); 
        gui.canvas2Image = gui.canvas2.create_image(0, 0, image = gui.tkFilteredImage, anchor="nw") 
         
        # Now generate a new bmp with just the found blocks. 
        #gui.pilBlockImage = Image.new("RGB",gui.pilImage.size)
        gui.pilBlockImage = gui.pilImage.copy()
    
        resizedBlockImage = gui.pilBlockImage.resize((gui.canvas3.winfo_width(),gui.canvas3.winfo_height())) 
        gui.tkBlockImage = ImageTk.PhotoImage(image=resizedBlockImage); 
        gui.canvas3Image = gui.canvas3.create_image(0, 0, image = gui.tkBlockImage, anchor="nw")
        
        # Now add the labels to the blocks.
        createBlockLabels(gui, foundBlockList)


def createBlockLabels(gui, foundBlockList):

    if ( gui.blockLabels is not None ):
        for label, blockRect in gui.blockLabels:
            gui.canvas3.delete(label)
            gui.canvas3.delete(blockRect)  
            
        gui.blockLabels = None 
        
    if ( foundBlockList is not None ):
        gui.blockLabels = list()         
        for block in foundBlockList:
            centrex = int((block.right + block.left) / 2)
            centrey = int((block.bottom + block.top) / 2)
            cvsX, cvsY = gui.translateImageToCanvas(gui.pilImage, gui.canvas3, centrex, centrey)
            rectX1,rectY1 = gui.translateImageToCanvas(gui.pilImage, gui.canvas3, block.left, block.top)
            rectX2,rectY2 = gui.translateImageToCanvas(gui.pilImage, gui.canvas3, block.right, block.bottom)
            label = gui.canvas3.create_text(cvsX, cvsY, text=block.name, fill='white')
            blockRect = gui.canvas3.create_rectangle(rectX1,rectY1,rectX2,rectY2,outline='white')
            gui.blockLabels.append((label,blockRect))
            
            
            
