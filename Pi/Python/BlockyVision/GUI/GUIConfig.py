'''
Created on 12 Oct 2021

@author: zelony
'''
from configparser import ConfigParser

class BlockConfig():

    # each block has it's own section. 
    BLOCK_SECTION_PREFIX = "Block_"
    
    NAME_KEY    = "Name"
    RED_KEY     = "Red"
    BLUE_KEY    = "Blue"
    GREEN_KEY   = "Green" 
    ENABLED_KEY = "Enabled"

    
    def __init__(self):
        
        self.name = "" 

        self.red = 0 
        self.green = 0  
        self.blue = 0  
        
        self.enabled = False 
        
    def saveConfig(self, configParser): 
        
        section = BlockConfig.BLOCK_SECTION_PREFIX + self.name 
        
        if ( not configParser.has_section(section) ):
            configParser.add_section(section)
        
        configParser.set(section, BlockConfig.NAME_KEY, self.name)
        configParser.set(section, BlockConfig.RED_KEY, str(self.red))
        configParser.set(section, BlockConfig.BLUE_KEY, str(self.blue))
        configParser.set(section, BlockConfig.GREEN_KEY, str(self.green))
        configParser.set(section, BlockConfig.ENABLED_KEY, str(self.enabled))
        
    @staticmethod
    def fromConfig(configParser, configSection):
        
        blockConfig = BlockConfig() 
        
        blockConfig.red   = configParser.getint(configSection, BlockConfig.RED_KEY, fallback="0")
        blockConfig.green = configParser.getint(configSection, BlockConfig.GREEN_KEY, fallback="0")
        blockConfig.blue  = configParser.getint(configSection, BlockConfig.BLUE_KEY, fallback="0")
        
        blockConfig.enabled = configParser.getboolean(configSection, BlockConfig.ENABLED_KEY, fallback=False)
        
        blockConfig.name = configParser.get(configSection,BlockConfig.NAME_KEY, fallback="")
        
        return blockConfig 
        
class GUIConfig():
    
    CONFIG_FILE_PATH = "./gui.cfg"
    
    GENERAL_SECTION = "GENERAL"
    USE_SERVER_BLOCK_CONFIG_KEY = "UseServerBlockConfig"
    
    SERVER_SECTION = "SERVER" 
    SERVER_HOST_KEY = "Host"
    SERVER_PORT_KEY = "Port" 
    
    
    
    def __init__(self):
        
        self.configParser = ConfigParser()
        self.configFilePath = GUIConfig.CONFIG_FILE_PATH

        self.configParser.read(self.configFilePath)
        
        
    def getValue(self, section, key, default):

        if ( self.configParser.has_section(section) ):
            return self.configParser[section].get(key, fallback=default).strip()

        return default.strip()
    
    def getIntValue(self, section, key, default):
        
        if ( self.configParser.has_section(section) ):
            return self.configParser.getint(section,key, fallback=default)
        
        return default 
        
    
    def getDefaultServerConnection(self, defaultHost, defaultPort):
        
        host = self.getValue(   GUIConfig.SERVER_SECTION, 
                                GUIConfig.SERVER_HOST_KEY, 
                                defaultHost                     )
        
        port = self.getIntValue(   GUIConfig.SERVER_SECTION, 
                                   GUIConfig.SERVER_PORT_KEY,
                                   defaultPort                     )
        
        return host, port
    
    def setDefaultServerConnection(self, host, port):
        
        if ( not self.configParser.has_section(GUIConfig.SERVER_SECTION) ):
            self.configParser.add_section(GUIConfig.SERVER_SECTION) 
            
        self.configParser.set(GUIConfig.SERVER_SECTION, GUIConfig.SERVER_HOST_KEY, host); 
        self.configParser.set(GUIConfig.SERVER_SECTION, GUIConfig.SERVER_PORT_KEY, str(port));
        
    
    def getBlocks(self):
        
        allSections = self.configParser.sections(); 
        
        blockList = list() 
        
        for section in allSections :
            
            if ( section.startswith(BlockConfig.BLOCK_SECTION_PREFIX) ):
                
                blockConfig  = BlockConfig.fromConfig(self.configParser, section)
                
                blockList.append(blockConfig)  
                
        return blockList
    
    def setBlocks(self, blockList):
        
        # Remove all existing block sections
        for section in self.configParser.sections():
            
            self.configParser.remove_section(section) 
        
        # Now add the sections for the provided list.  
        for blockConfig in blockList:
            
            blockConfig.saveConfig(self.configParser) 
            
    def useServerBlockConfig(self):
        
        return self.configParser.getboolean(GUIConfig.GENERAL_SECTION, GUIConfig.USE_SERVER_BLOCK_CONFIG_KEY, fallback=False)
    
    def setUseServerBlockConfig(self, value):
        
        if ( not self.configParser.has_section(GUIConfig.GENERAL_SECTION) ):
            self.configParser.add_section(GUIConfig.GENERAL_SECTION) 
            
        self.configParser.set(GUIConfig.GENERAL_SECTION, GUIConfig.USE_SERVER_BLOCK_CONFIG_KEY, str(value))
        
    def save(self):
        
        with open(self.configFilePath, 'w') as configFile :
            
            self.configParser.write(configFile) 
            
        self.configParser.read(self.configFilePath) 
        
         
