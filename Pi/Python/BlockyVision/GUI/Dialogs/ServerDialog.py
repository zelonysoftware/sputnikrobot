'''
Created on 16 Nov 2020

@author: zelony
'''
import tkinter.constants as tkc
import tkinter as tk
from tkinter.simpledialog import Dialog 

def askServerHostPort(parent, config):
    dlg = ServerDialog(parent, config)
    
    return dlg.getValues();
    
class ServerDialog(Dialog):
    
    def __init__(self, parent, config):
        
        self.parent = parent 
        self.config = config

        host, port = config.getDefaultServerConnection("192.168.22.137", 20060) 
        
        self.initialServer = host
        self.initialPort = port   

        self.host = None
        self.port = None

        super().__init__(parent, title="Server Connection")  
    
    #Override 
    def body(self, master):
        
        mainLabel = tk.Label(master, text="Enter Server Connection host and port", justify=tkc.LEFT)
        mainLabel.grid(row=0, padx=5)

        self.label1 = tk.Label(master,text="host")
        self.label1.grid(row=1, padx=5, sticky=tkc.W)
        self.entry1 = tk.Entry(master)
        self.entry1.grid(row=1, padx=5, sticky=tkc.W+tkc.E)

        self.label2 = tk.Label(master,text="port")
        self.label2.grid(row=2, padx=5, sticky=tkc.W)
        self.entry2 = tk.Entry(master)
        self.entry2.grid(row=2, padx=5, sticky=tkc.W+tkc.E)
        
        if self.initialServer is not None:
            self.entry1.insert(0, self.initialServer)
            self.entry1.select_range(0, tkc.END)
            
        if self.initialPort is not None:
            self.entry2.insert(0, self.initialPort)
            self.entry2.select_range(0, tkc.END); 

        return self.entry1
    
    def ok(self, event=None):
        
        self.host = self.entry1.get()
        self.port = self.getint(self.entry2.get())
        
        self.config.setDefaultServerConnection(self.host, self.port)
        self.config.save()
        
        super().ok(event)
        
    def getValues(self):
        return (self.host, self.port)
    
