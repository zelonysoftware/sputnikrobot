'''
Created on 13 Oct 2021

@author: zelony
'''
import tkinter.constants as tkc
import tkinter as tk
from tkinter.simpledialog import Dialog
from BlockyVision.GUI.GUIConfig import BlockConfig

def showDialog(parent, config, red, green, blue):
    
    return SaveSelectionAsBlockDialog(parent, config, red, green, blue)
    

class SaveSelectionAsBlockDialog(Dialog):

    def __init__(self, parent, config, red, green, blue):
        
        self.parent = parent 
        
        self.config = config 
        self.red = red 
        self.green = green 
        self.blue = blue 
        
        super().__init__(parent, title="Save Selection?") 
        
    def body(self, master):
        
        rgbLabelText = "r=%d, g=%d b=%d" % (self.red, self.green, self.blue)
         
        rgbLabel = tk.Label(master, text=rgbLabelText)
        rgbLabel.grid(row=1, column=0, padx=5, pady=2) 
        
        bgColour = '#%02x%02x%02x' % (self.red, self.green, self.blue)
        colourCanvas = tk.Canvas(master, bg=bgColour, width=20, height=20)
        colourCanvas.grid(row=1, column=1, padx=5, pady=2, sticky=tkc.W)
        
        nameLabel = tk.Label(master,text="name")
        nameLabel.grid(row=2, column=0, padx=5, pady=2, sticky=tkc.E)
        self.nameEntry = tk.Entry(master)
        self.nameEntry.grid(row=2, column=1, padx=5, pady=2, sticky=tkc.W+tkc.E)
        
        master.pack()
        
        return self.nameEntry
        
    def ok(self, event=None):
        
        if ( self.nameEntry.get() ):
            
            blockList = self.config.getBlocks()
            
            newBlock = BlockConfig() 
            newBlock.name = self.nameEntry.get().strip()
            newBlock.red = self.red 
            newBlock.green = self.green 
            newBlock.blue = self.blue 
            newBlock.enabled = True 
            
            blockList.append(newBlock) 
            
            self.config.setBlocks(blockList) 
            self.config.save() 
        
        super().ok(event)

        
        

        
        

        