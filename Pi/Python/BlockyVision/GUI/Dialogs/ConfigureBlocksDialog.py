'''
Created on 12 Oct 2021

@author: zelony
'''
import tkinter.constants as tkc
import tkinter as tk
from tkinter.simpledialog import Dialog 
from tkinter import IntVar
from BlockyVision.GUI.GUIConfig import BlockConfig

MAX_NUM_BLOCK_CONFIGS = 8 

def showDialog(parent, config):
    
    dlg = ConfigureBlocksDialog(parent, config)
    
    return dlg 

class BlockEntry():
    
    def __init__(self, master, rowNum): 

        self.nameEntry = tk.Entry(master)
        self.nameEntry.grid(row=rowNum, column=0, padx=5, sticky=tkc.W )
        
        self.redEntry = tk.Entry(master)
        self.redEntry.grid(row=rowNum, column=1, padx=2, sticky=tkc.W )
        
        self.greenEntry = tk.Entry(master)
        self.greenEntry.grid(row=rowNum, column=2, padx=2, sticky=tkc.W )

        self.blueEntry = tk.Entry(master)
        self.blueEntry.grid(row=rowNum, column=3, padx=2, sticky=tkc.W )

        self.enabledVariable = IntVar()
        self.enabledVariable.set(0) 
        self.enabledCheckBox = tk.Checkbutton(master, variable=self.enabledVariable) 
        self.enabledCheckBox.grid(row=rowNum, column=4, padx=5, sticky=tkc.W)
        
        self.clearButton = tk.Button(master,text="Clear", command=self.clear)
        self.clearButton.grid(row=rowNum, column=5, padx=5, sticky=tkc.W)
        
    def clear(self):
        
        self.nameEntry.delete(0, tkc.END) 
        self.redEntry.delete(0, tkc.END)
        self.greenEntry.delete(0, tkc.END)
        self.blueEntry.delete(0, tkc.END)
        self.enabledVariable.set(0) 
        
    
class ConfigureBlocksDialog(Dialog):
    
    def __init__(self, parent, config):
        
        self.parent = parent
        
        self.config = config 
        self.useServerBlockConfig = IntVar(value=(1 if config.useServerBlockConfig() else 0))
          
        self.blockEntries = list()

        super().__init__(parent, title="Configure Blocks") 
        
        pass 
    
    #Override 
    def body(self, master):
        
        controlFrame = master

        controlFrame.columnconfigure(0,weight=10)
        controlFrame.columnconfigure(1,weight=1)
        controlFrame.columnconfigure(2,weight=1)
        controlFrame.columnconfigure(3,weight=1)
        controlFrame.columnconfigure(4,weight=1)
        
        #mainLabel = tk.Label(controlFrame, text="Block Configuration", justify=tkc.LEFT)
        #mainLabel.grid(row=0, padx=5)
        
        self.useServerBlockConfigCB = tk.Checkbutton(controlFrame, text="User Server Block Configuration", variable=self.useServerBlockConfig)
        self.useServerBlockConfigCB.grid(row=0, padx=5, pady=10)

        nameLabel = tk.Label(controlFrame, text="name") 
        nameLabel.grid(row=1, column=0, padx=5, sticky=tkc.W )

        redLabel = tk.Label(controlFrame, text="red")
        redLabel.grid(row=1, column=1, padx=2, sticky=tkc.W )

        greenLabel = tk.Label(controlFrame, text="green")
        greenLabel.grid(row=1, column=2, padx=2, sticky=tkc.W )

        blueLabel = tk.Label(controlFrame, text="blue")
        blueLabel.grid(row=1, column=3, padx=2, sticky=tkc.W )

        enabledLabel = tk.Label(controlFrame, text="enabled")
        enabledLabel.grid(row=1, column=4, padx=2, sticky=tkc.W )
        
        for i in range(0,MAX_NUM_BLOCK_CONFIGS):
            self.blockEntries.append(BlockEntry(controlFrame, 2 + i))
            
        i = 0 
        blockList = self.config.getBlocks()
        
        for block in blockList :
            
            blockEntry = self.blockEntries[i] 
            
            blockEntry.nameEntry.insert(0, block.name)
            blockEntry.redEntry.insert(0, str(block.red))
            blockEntry.greenEntry.insert(0, str(block.green))
            blockEntry.blueEntry.insert(0, str(block.blue)) 
            
            if ( block.enabled ):
                blockEntry.enabledVariable.set(1)
            else:
                blockEntry.enabledVariable.set(0)
            
            
            i += 1 
            if ( i >= MAX_NUM_BLOCK_CONFIGS ):
                i = 0 
                break
    
        return self.blockEntries[i].nameEntry 
    
    def ok(self, event=None):
        
        newBlockList = list() 
        
        # build up block list from the entries.
        for i in range(0,MAX_NUM_BLOCK_CONFIGS):
            blockEntry = self.blockEntries[i] 
            
            if blockEntry.nameEntry.get(): 
                blockConfig = BlockConfig() 
                blockConfig.name = blockEntry.nameEntry.get().strip() 
                blockConfig.red = int(blockEntry.redEntry.get())
                blockConfig.green = int(blockEntry.greenEntry.get())
                blockConfig.blue = int(blockEntry.blueEntry.get())
                blockConfig.enabled = True if blockEntry.enabledVariable.get() == 1 else False 
                
                newBlockList.append(blockConfig)
        
        self.config.setBlocks(newBlockList)
        self.config.setUseServerBlockConfig(True if self.useServerBlockConfig.get() == 1 else False) 
        self.config.save()  

        super().ok(event)
        
    def cancel(self, event=None):
        
        super().cancel(event) 
        
