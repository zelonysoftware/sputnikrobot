
import time 
import math 

import tkinter as tk
from PIL import Image 
from PIL import ImageTk

from BlockyVision.BVVectorFinder import BVVectorFinder 

def showFilteredImages(gui, vectorLineColour):
    
    if ( gui.pilImage != None ):

        startTime = time.time()
        
        vectorFinder = BVVectorFinder(gui.pilImage, lineColour=vectorLineColour)
        vectors = vectorFinder.findVectors()

        endTime = time.time()
        
        print("Vector find time = %d milliseconds" % (int((endTime - startTime)*1000)))
        print("Found vectors:")
        for vector in vectors:
            vector.printVector()
    
        # image will now have been filtered.
        gui.pilFilteredImage = vectorFinder.getImage()
        resizedFilteredImage = gui.pilFilteredImage.resize((gui.canvas2.winfo_width(),gui.canvas2.winfo_height())) 
        gui.tkFilteredImage = ImageTk.PhotoImage(image=resizedFilteredImage); 
        gui.canvas2Image = gui.canvas2.create_image(0, 0, image = gui.tkFilteredImage, anchor="nw") 
         
        # Now generate a new bmp with just the found blocks. 
        #gui.pilBlockImage = Image.new("RGB",gui.pilImage.size)
        gui.pilBlockImage = gui.pilImage.copy()
    
        resizedBlockImage = gui.pilBlockImage.resize((gui.canvas3.winfo_width(),gui.canvas3.winfo_height())) 
        gui.tkBlockImage = ImageTk.PhotoImage(image=resizedBlockImage); 
        gui.canvas3Image = gui.canvas3.create_image(0, 0, image = gui.tkBlockImage, anchor="nw")
        
        showVectors(gui, vectors)


def showVectors(gui, vectors):

    if ( gui.vectorLines is not None ):
        for vectorLine in gui.vectorLines:
            gui.canvas3.delete(vectorLine) 
            gui.vectorLines = None 
    if ( gui.vectorLabel is not None ):
        gui.canvas3.delete(gui.vectorLabel) 
        gui.vectorLabel = None
        
    if ( vectors is not None and len(vectors) > 0 ):
        
        gui.vectorLines = list() 
        
        idx = 0 
        
        anglesText = ""
        index = 0 
        
        for vector in vectors:
            
            originX, originY = vector.getOrigin() 
            originX, originY = gui.translateImageToCanvas(gui.pilImage, gui.canvas3, originX, originY)
            
            angle = math.radians(vector.getAngle())
            vectorLength = min(originX, gui.canvas3.winfo_width() - originX, originY) 
            vectorLength -= 10 
            print("vectorLength = {}, originX,originY = {},{}".format(vectorLength, originX, originY))

            xLen = int(vectorLength * math.sin(abs(angle)))
            yLen = int(vectorLength * math.cos(abs(angle)))
            
            px = 0 
            
            if (angle < 0 ):
                px = originX - xLen 
            else:
                px = originX + xLen  
            
            py = originY - yLen 
            
            print("xlen,ylen= {},{} px,py = {},{}".format(xLen,yLen,px,py))
            
            fillColour = 'orange'
            arrowWidth = 2 
            if ( idx == 0 ):
                fillColour = 'green'
                arrowWidth = 5
            elif ( idx == 1 ):
                fillColour = 'blue'
            elif ( idx == 2 ):
                fillColour = 'red'
            idx += 1 
                
            line = gui.canvas3.create_line(originX,originY,px,py,fill=fillColour,width=arrowWidth,arrow=tk.LAST)

            index += 1 
            anglesText = anglesText + "{:0.2f}".format(vector.getAngle())

            if ( index < len(vectors) ):
                anglesText += ", "
                
            gui.vectorLines.append(line)


        posx = gui.canvas3.winfo_width() / 2
        posy = gui.canvas3.winfo_height() / 2
        gui.vectorLabel = gui.canvas3.create_text(posx, posy, text=anglesText, fill='orange')

            
