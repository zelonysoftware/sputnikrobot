'''
Created on 7 Oct 2019

@author: zelony
'''

import time
from BlockyVision.BMPFile import BMPFile
from BlockyVision.Bitmap import Pixel
from BlockyVision.BVHorizonFinder import BVHorizonFinder
from PIL import Image
import numpy 


#FILEROOT = './run/blocks2_100x75'
FILEROOT = './run/blocks2_500'
#FILEROOT = './run/test_hz3'

#FILEROOT = './run/test'

        
if __name__ == '__main__':
    
    # open file
    time1 = time.time() 
    
    bmpFile = BMPFile(FILEROOT + ".bmp")
    
    image = bmpFile.getImage()
    image.load()
    
    time2 = time.time()

    horizonFinder = BVHorizonFinder(image)


    horizon = horizonFinder.findHorizon(bbox=(56,0,82,100))
    
    time3 = time.time()
    
    if ( horizon != None ):
        print(f'Found horizon: {horizon.leftY}, {horizon.rightY}, {horizon.width}, {horizon.getAngle()} deg')
    else:
        print("No Horizon")
    
    # Now generate a new bmp with just the found blocks. 
    horizonImage = horizonFinder.getImage()
    horizonImage.save(FILEROOT + "_horizon.bmp") 
    
    elapsedTime1 = round((time2 - time1) * 1000)
    elapsedTime2 = round((time3 - time1) * 1000)

    bvTime = round((time3 - time2) * 1000)
    print(f'Find horizon time = {bvTime}ms')

    print(f'times: 1={elapsedTime1}ms, 2={elapsedTime2}ms')
    print(f'fileopen time = {elapsedTime1}ms')
    print(f'conversion time = {elapsedTime2 - elapsedTime1}ms')

    # done 
    
