'''
Created on 12 Oct 2019

@author: zelony
'''

class Block(object):
    
    def __init__(self, name, pixel, top, bottom, left, right):
        
        self.name = name 
        self.pixel = pixel 
        self.top = top 
        self.bottom = bottom 
        self.left = left 
        self.right = right 

    def getName(self):
        return self.name 

    def getArea(self):
        return self.getNumCols() * self.getNumRows() 
        
    def getNumCols(self):
        return self.right - self.left
        
    def getNumRows(self):
        return self.bottom - self.top
    
    def getCentre(self):
        return ( int((self.right + self.left) / 2), int((self.top + self.bottom) / 2) )
    
    def printBlock(self):
        print("BLOCK[{} T:{}, B:{}, L:{}, R:{}]".format(self.name, self.top, self.bottom, self.left, self.right))

