'''
Created on 15 Jan 2021

@author: zelony
'''
import math
from PIL.Image import NONE
from builtins import set

class Vector(object):
    
    def __init__(self, originX, originY, angle):

        self.originX = originX
        self.originY = originY
        self.angle = angle 

    def getOrigin(self):
        
        return self.originX, self.originY 
    
    def getAngle(self):
        
        return self.angle 
    
    def printVector(self):
        print("VECTOR[CX:{}, CY:{}, ANGLE:{}]".format(self.originX, self.originY, self.angle))
        
class LineCrossSection(object):
    
    def __init__(self, x1, x2, y):
    
        self.x1 = x1 ;
        self.x2 = x2 ;
        self.y  = y ; 
        
    def print(self):
        print("LINE[X1:%d, X2:%d, Y:%d" % (self.x1, self.x2, self.y))

class BVVectorFinder(object):
    
    BLACK = 0 
    WHITE = 255 
    
    '''
    This class is used to take a bitmap and find vectors (lines) from a fixed origin - by default we assuming a black(dark) line
    and white(light) floor - but this can be inverted to follow a white line on a black background. 
    '''
    def __init__(self, image, lineColour=BLACK):
        '''
        Constructor
        '''
        # Convert to black and white. 
        self.image = image.convert(mode="1",dither=NONE)
        self.lineColour = lineColour 
         

    def getImage(self):
        return self.image 
            
        
    def findVectors(self):

        MIN_LINE_WIDTH_PCENT = 3 ;
        SCAN_HEIGHT_PCENT = 30 ;

        vectorList = list()
        
        minLineWidth = int((self.image.width * MIN_LINE_WIDTH_PCENT) / 100)
        scanHeight = int((self.image.height * SCAN_HEIGHT_PCENT) / 100)

        # get point from which angles are calculated. (in the middle and 25% of the picture height pixes behind the camera view). 
        cx = self.image.width / 2  
        cy = ((self.image.height * 25) / 100) 

        # First scan the bottom row to work out how many vectors we have. 
        bottomRowLines = self.findLines(0, self.image.width-1, self.image.height - 1, minLineWidth);
        
        if ( len(bottomRowLines) == 0 ):
            return vectorList 
        
        self.createVectors(bottomRowLines, cx, cy, vectorList) 
        
        # Then scan up from the bottom row - updating the angle of the vectors we've found,
        # stopping if we find more or less lines than we found on the bottom row. 
        rowCount = 1; 
        for y in range( self.image.height - 2, self.image.height - scanHeight, -1 ):
            
            lines = self.findLines(0,self.image.width-1, y, minLineWidth)
            
            if ( len(lines) == len(bottomRowLines) ):
                rowCount += 1
                self.updateVectors(lines, cx, cy, vectorList, rowCount);
            else: 
                break ;

        return vectorList ;


    def createVectors(self, lines, cx, cy, vectors):
        
        for line in lines:
            
            lineCentre = ((line.x1 + line.x2) / 2)
            lineCentreOffset = lineCentre - cx   
            angle = math.degrees(math.atan2(lineCentreOffset,cy))
            
            vector = Vector(lineCentre, self.image.height-1, angle)
            vectors.append(vector) 

    def updateVectors(self, lines, cx, cy, vectors, rowCount):
        
        index = 0 ; 
        for line in lines:

            lineCentre = ((line.x1 + line.x2) / 2)
            lineCentreOffset = lineCentre - cx   
            angle = math.degrees(math.atan2(lineCentreOffset,cy))
            
            #update the vector with a running average of the angle. 
            vector = vectors[index] 
            vector.angle = ((vector.angle * (rowCount-1)) + angle) / rowCount
            
            index += 1 

    def findLines(self, startCol, endCol, row, minLineWidth):
        
        lineCrossSections = list() 
        
        firstPixel = True 
        scanningLine = False 
        
        currentLine = None  
        
        y = row  
        
        for x in range(startCol,endCol+1):
             
            value = self.image.getpixel((x, y))
            
            if ( firstPixel ):
                if ( value == self.lineColour ):
                    scanningLine = True
                    currentLine = [x,x]; 
                else:
                    scanningLine = False 
                firstPixel = False 
                
            if ( value == self.lineColour ):
                # This is a line pixel 
                if ( scanningLine ):
                    currentLine[1] = x
                else:
                    # First line detected after seeing background colour.
                    scanningLine = True 
                 
                    # Start a new line. 
                    currentLine = [x,x] 
            else:

                # This is a background pixel 
                if ( scanningLine ):
                    # We've switched from a line pixel to a background pixel. 
                    if ( self.lineWidth(currentLine) >= minLineWidth ):
                        # this is a valid line. 
                        lineCrossSections.append(LineCrossSection(currentLine[0], currentLine[1], y))
                        
                    scanningLine = False 

        # Make sure we take account of any line we're currently scanning. 
        if ( scanningLine ):
            if ( self.lineWidth(currentLine) >= minLineWidth ):
                # this is a valid line. 
                lineCrossSections.append(LineCrossSection(currentLine[0], currentLine[1], y))

        return lineCrossSections; 
            
    def lineWidth(self, line):
        return ( line[1] - line[0] )
    
