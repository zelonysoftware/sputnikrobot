'''
Created on 12 Oct 2019

@author: zelony
'''

import math
from PIL.Image import NONE

class Horizon(object):
    
    def __init__(self, leftY, rightY, width):

        self.leftY = leftY
        self.rightY = rightY 
        self.width = width 

    def getSlope(self):
        
        m = (self.leftY - self.rightY) / self.width ;
        
        return m ; 
    
    def getAngle(self):
        
        return math.degrees(math.atan(float(self.getSlope()))) 
    
    def printHorizon(self):
        print("HORIZON[L:{}, R:{}, W:{}]".format(self.leftY, self.rightY, self.width))
        
class Point(object):
    
    def __init__(self, x, y):
        self.x = x 
        self.y = y 


class BVHorizonFinder(object):
    '''
    This class is used to take a bitmap and find the horizon, assuming that the wall will be black. 
    '''
    def __init__(self, image):
        '''
        Constructor
        '''
        # Convert to black and white. 
        self.image = image.convert(mode="1",dither=NONE)

    def getImage(self):
        return self.image         
        
    def findHorizon(self, bbox=None):
        
        # You can choose not to use the whole image - the bounding box gives a bounding box as percentages
        # of the image size.
        if ( bbox is None ):
            bbox = (0,0,100,100) 
            
        cropX1, cropY1, cropX2, cropY2 = self.cropPcentToImageCoordinates(bbox) 
        
        
        width = cropX2 - cropX1 
        
        # We want to get about 10 points across the image to determine the horizon. 
        colSteps = max(1,int(width / 10));
        
        horizonPointXs = list(); 
        horizonPointYs = list(); 
        
        for col in range(cropX1, cropX2, colSteps):
            
            horizonPoint = self.scanColumn(col, cropY1, cropY2 );
            
            if ( horizonPoint is not None ):
                horizonPointXs.append(float(horizonPoint.x)); 
                horizonPointYs.append(float(horizonPoint.y)); 
            
        
        # Now we have the points, we can extrapolate the start and end of the horizon.
        horizon = None 
        if ( len(horizonPointXs) > 0 ):
            horizon = self.calcHorizonFromPoints(horizonPointXs,horizonPointYs)
         
        return horizon 
    
    
    def scanColumn(self, x, yTop, yBottom):
        
        MIN_WHITE_LENGTH = 5 ; # TODO - this should be based on a percentage so the resolution is taken into account. 
        
        firstPixel = True 
        scanningBlack = False 
        
        biggestBlackStripe = None 
        currentBlackStripe = None  
        currentWhiteStripe = None 
        
        for y in range(yTop,yBottom): 
            value = self.image.getpixel((x, y))
            
            if ( firstPixel ):
                if ( value == 0 ):
                    scanningBlack = True
                    currentBlackStripe = [y,y]; 
                else:
                    scanningBlack = False 
                    currentWhiteStripe = [y,y]; 
                firstPixel = False 
                
            if ( value == 0 ):
                # This is a black pixel 
                if ( scanningBlack ):
                    currentBlackStripe[1] = y
                else:
                    # First black stripe after a white stripe.
                    scanningBlack = True 
                     
                    if  (   (self.stripeLength(currentWhiteStripe) <= MIN_WHITE_LENGTH)
                        and (self.stripeLength(currentBlackStripe) > MIN_WHITE_LENGTH)  ):
                        # We'll assume that this was a small white blip and continue with the current black strip. 
                        currentBlackStripe[1] = y 
                    else: 
                        # Start a new black stripe. 
                        currentBlackStripe = [y,y] 
            else:
                # This is a white pixel 
                if ( scanningBlack ):
                    # We've switched from a black pixel to a white pixel. Don't do anything for now but create 
                    # a new white stripe. We want to make sure it's a significant white stripe. 
                    currentWhiteStripe = [y,y] 
                    scanningBlack = False 
                else:
                    
                    if ( currentBlackStripe != None and self.stripeLength(currentWhiteStripe) > MIN_WHITE_LENGTH ):
                        # This is a significant white stripe, so process the last black stripe we saw. 
                        if ( self.stripeLength(currentBlackStripe) > self.stripeLength(biggestBlackStripe) ):
                            biggestBlackStripe = currentBlackStripe 
                            
                        currentBlackStripe = None 
                    
                    currentWhiteStripe[1] = y 

        # This is the end of the loop. We don't worry about the end of the loop. If we were scanning a black stripe, 
        # then we're too close to the wall to get a horizon. 
        # If we were scanning a white stripe, then we would have already processed the current black stripe if this was a 
        # significant white stripe. 
        if ( biggestBlackStripe is None ):
            return None 
        
        return Point(x,biggestBlackStripe[1])
                        
                    
    def stripeLength(self, stripe):
        if ( stripe == None ):
            return 0 ;
        
        return stripe[1] - stripe[0]                  
    
    def calcHorizonFromPoints(self, xPoints, yPoints):
        
        ''' Find the line of best fit - i.e. a line who's equation is y = mx + c  '''
        
        ''' First we calculate 'm' - using the least square method. A good explaination of this is at:
            https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
        '''
        
        # First get the averages. 
        avgX = sum(xPoints) / len(xPoints) 
        avgY = sum(yPoints) / len(yPoints)
        
        # Now use the least square method to find 'm'  
        numerator = 0.0  
        denominator = 0.0 
        
        for i in range(len(xPoints)):
            numerator += ((xPoints[i] - avgX) * (yPoints[i] - avgY)) 
            denominator += ((xPoints[i] - avgX) * (xPoints[i] - avgX))
            
        m = 0.0 

        if ( abs(denominator) > 0.0 ):
            m = numerator / denominator 
        
        # We can find the value of Y when X=0 using c = avgY - m(avgX) 
        leftY = int(avgY - (m * avgX))
        
        # extrapolate this to find Y when X = right most point. 
        rightY = int((m * (self.image.width-1)) + leftY)
        
        return Horizon(leftY, rightY, self.image.width) 
         
    
    def cropPcentToImageCoordinates(self, bbox):
        
        pcentX1, pcentY1, pcentX2, pcentY2 = bbox 
        
        # Convert percentages to real co-ordinates. 
        x1 = int((pcentX1 * self.image.width) / 100) 
        x1 = max(0,min(x1,self.image.width-1)) 

        x2 = int((pcentX2 * self.image.width) / 100)
        x2 = max(0,min(x2,self.image.width-1)) 

        y1 = int((pcentY1 * self.image.height) / 100)
        y1 = max(0,min(y1,self.image.height-1)) 

        y2 = int((pcentY2 * self.image.height) / 100) 
        y2 = max(0,min(y2,self.image.height-1)) 

        # Make sure x1,y1 is the top left and x2,y2 is the bottom right. 
        if ( y1 > y2 ):
            y1tmp = y1 
            y1 = y2 
            y2 = y1tmp 
            
        if ( x1 > x2 ):
            x1tmp = x1
            x1 = x2
            x2 = x1tmp 
            
        return( x1, y1, x2, y2 )

             
