'''
Created on 12 Oct 2019

@author: zelony
'''
import time
import colorsys
from BlockyVision.Bitmap import PixelConstants
from BlockyVision.Bitmap import Pixel
import numpy 

class Block(object):
    
    def __init__(self, name, pixel, top, bottom, left, right):
        
        self.name = name 
        self.pixel = pixel 
        self.top = top 
        self.bottom = bottom 
        self.left = left 
        self.right = right 

    def getName(self):
        return self.name 

    def getArea(self):
        return self.getNumCols() * self.getNumRows() 
        
    def getNumCols(self):
        return self.right - self.left
        
    def getNumRows(self):
        return self.bottom - self.top
    
    def getCentre(self):
        return ( int((self.right + self.left) / 2), int((self.top + self.bottom) / 2) )
    
    def printBlock(self):
        print("BLOCK[{} T:{}, B:{}, L:{}, R:{}]".format(self.name, self.top, self.bottom, self.left, self.right))

class BVBlockFinder(object):
    '''
    This class is used to take a bitmap and find objects of a particular colour.  
    The bitmap will be modified as it finds the colours. 
    '''
    EUCLIDEAN_THRESHOLD = 50
    
    MIN_WIDTH_PCENT = 5 
    MIN_HEIGHT_PCENT = 5 
    
    def __init__(self, image):
        '''
        Constructor
        '''
        self.numpyImage = image
        self.blockColourList = list() 
        self.blockNameList = list()
        self.blocksFound = dict()
        self.threshold = self.EUCLIDEAN_THRESHOLD
        self.minBlockWidthPcent = self.MIN_WIDTH_PCENT
        self.minBlockHeightPcent = self.MIN_HEIGHT_PCENT
        
        self.timeSimilar = 0
        self.countSimilar = 0
        self.timeNormalise = 0

    def getImage(self):
        return self.numpyImage         
    ''' 
    Set a named object that we want to find. 
    '''
    def setBlockColour(self, name, rgb):
        
        # normalise the brightness so we're only comparing the 
        # colour
        normalisedPixel = self.normaliseBrightness2(numpy.asarray(rgb))
        
        self.blockColourList.append(normalisedPixel)
        self.blockNameList.append(name) 
        
    def setThreshold(self,threshold):
        self.threshold = threshold  
        
    def setMinBlockWidthPcent(self, minBlockWidthPcent):
        self.minBlockWidthPcent = minBlockWidthPcent 

    def setMinBlockHeightPcent(self, minBlockHeightPcent):
        self.minBlockHeightPcent = minBlockHeightPcent 
        
    def findBlocks(self, bbox=None):
        
        # You can choose not to use the whole image - the bounding box gives a bounding box as percentages
        # of the image size.
        if ( bbox is None ):
            bbox = (0,0,100,100) 
            
        cropX1, cropY1, cropX2, cropY2 = self.cropPcentToImageCoordinates(bbox) 
         
        # Work out the minimum size of any blocks based on the size of the bmp and the percentage size we're after.
        numRows, numCols, _ = self.numpyImage.shape    
        minColSize = max(int((numCols * self.minBlockWidthPcent) / 100),1)
        minRowSize = max(int((numRows * self.minBlockHeightPcent) / 100),1)
        
        # First we go through applying a threshold to each pixel to only show colours we're looking for. 
        time1 = time.time() 
        self.applyThresholds(minColSize, minRowSize, cropX1, cropY1, cropX2, cropY2) 
        #print('1 Count similar time = {}ms, count = {}'.format(int(self.timeSimilar * 1000), self.countSimilar))
        time2 = time.time() 
        self.timeSimilar = 0 
        self.countSimilar = 0
        blockList = self.findBlockList(numCols, numRows, minColSize, minRowSize, cropX1, cropY1, cropX2, cropY2)
        #print('2 Count similar time = {}ms, count = {}'.format(int(self.timeSimilar * 1000), self.countSimilar))
        time3 = time.time() 
        
        #print('Apply Thresholds Time = {}ms'.format(int((time2 - time1)*1000)))
        #print('Find blocklist time = {}ms'.format(int((time3 - time2) * 1000)))
        #print('Total search time = {}ms'.format(int((time3 - time1) * 1000)))
        return blockList 
    
    
    def cropPcentToImageCoordinates(self, bbox):
        
        pcentX1, pcentY1, pcentX2, pcentY2 = bbox 

        height, width, _ = self.numpyImage.shape
        
        # Convert percentages to real co-ordinates. 
        x1 = int((pcentX1 * width) / 100) 
        x1 = max(0,min(x1,width-1)) 

        x2 = int((pcentX2 * width) / 100)
        x2 = max(0,min(x2,width-1)) 

        y1 = int((pcentY1 * height) / 100)
        y1 = max(0,min(y1,height-1)) 

        y2 = int((pcentY2 * height) / 100) 
        y2 = max(0,min(y2,height-1)) 

        # Make sure x1,y1 is the top left and x2,y2 is the bottom right. 
        if ( y1 > y2 ):
            y1tmp = y1 
            y1 = y2 
            y2 = y1tmp 
            
        if ( x1 > x2 ):
            x1tmp = x1
            x1 = x2
            x2 = x1tmp 
            
        return( x1, y1, x2, y2 )

             
    def applyThresholds(self, minColSize, minRowSize, startCol, startRow, endCol, endRow):

        numCols = (endCol - startCol) + 1 
        numRows = (endRow - startRow) + 1

        self.thresholdMap = [False] * (numRows * numCols)

        # Use the minimum size to minimise the number of pixels we need to scan. 
        rowStepSize = max(int((minRowSize-1)/2),1)
        colStepSize = max(int((minColSize-1)/2),1)

        i = 0 
        #print("numRows = {}".format(int((endRow+1 - startRow)/rowStepSize)))
        #print("numCols = {}".format(int((endCol+1 - startCol)/colStepSize)))
        for row in range(startRow, endRow+1, rowStepSize):

            for col in range(startCol, endCol+1, colStepSize):
                currentPixel = self.numpyImage[row,col]
                 
                for colourToFind in self.blockColourList:
                    
                    if ( self.isSimilarColour(colourToFind, currentPixel) ):
                        self.numpyImage[row,col] = colourToFind; 
                        i = ((row-startRow) * numCols) + (col-startCol) 
                        self.thresholdMap[i] = True
                        break
                            
    def isSimilarColour(self, pixel1, pixel2):

        result = self.isSimilarColourEuclideanDistance(pixel1, pixel2)
        return result 
    
    def isSimilarColourEuclideanDistance(self, pixel1, pixel2):
                
        self.countSimilar += 1 
        timeStart = time.time() 
        
        rgb1 = pixel1 ;
        rgb2 = pixel2 ; 
        
        # Check for exact match to save some time. 
        if ( (rgb1 == rgb2).any() ):
            return True 
        
        rgb2 = self.normaliseBrightness2(rgb2)
        
        #distanceSquared = ( ((rgb1[0]-rgb2[0]) * (rgb1[0]-rgb2[0]))
        #                  + ((rgb1[1]-rgb2[1]) * (rgb1[1]-rgb2[1]))         
        #                  + ((rgb1[2]-rgb2[2]) * (rgb1[2]-rgb2[2])) )
        #print(f'rgb1[0] = {rgb1[0]}, rgb2[0] = {rgb2[0]}')
        #print(f'rgb1[1] = {rgb1[1]}, rgb2[1] = {rgb2[1]}')
        #print(f'rgb1[2] = {rgb1[2]}, rgb2[2] = {rgb2[2]}')

#         distanceSquared = ( qc.SQUARES_TO_255[abs(int(rgb1[0])-int(rgb2[0]))]
#                           + qc.SQUARES_TO_255[abs(int(rgb1[1])-int(rgb2[1]))]
#                           + qc.SQUARES_TO_255[abs(int(rgb1[2])-int(rgb2[2]))] )
        distance = numpy.linalg.norm(rgb1-rgb2)
        #print(f'rgb1 = {rgb1}, rgb2 = {rgb2}, distance = {distance}')
        timeElapsed = time.time() - timeStart
        
        self.timeSimilar += timeElapsed 
        
        if ( distance < self.threshold ):
            return True 
        
        return False          

    ''' 
    Convert to HSV - put the brightness (V) at a fixed value and then convert back to RGB
    ''' 
    def normaliseBrightness(self,rgb):
        
        timeStart = time.time() 
        
        r = rgb[0]
        g = rgb[1]
        b = rgb[2] 
        
        hsv = colorsys.rgb_to_hsv(r/255, g/255, b/255)

        rgbOut = colorsys.hsv_to_rgb(hsv[0],hsv[1],0.5) 
        
        normalised = (int(rgbOut[0] * 255), int(rgbOut[1] * 255), int(rgbOut[2] * 255)) 
        
        timeElapsed = time.time() - timeStart 
        
        self.timeNormalise += timeElapsed 
        
        return normalised 
        
    def normaliseBrightness2(self,rgb):
        
        timeStart = time.time() 
        
        minValue = numpy.amin(rgb) 
        
        normalised = rgb - minValue 
        
        timeElapsed = time.time() - timeStart 
        
        self.timeNormalise += timeElapsed 
        
        return normalised 
            
        
    def findBlockList(self, numCols, numRows, minNumCols, minNumRows, startCol, startRow, endCol, endRow):

        # Go through the filtered bmp finding objects that consist of each colour.
        numCols = (endCol - startCol) + 1 
        numRows = (endRow - startRow) + 1
         
        self.isTraversed = [False] * (numRows * numCols)
        
        # we don't need to traverse every row and column - since we're looking for objects larger than 
        # n % of the total sise 
        rowStepSize = max(int((minNumRows-1)/2),1)
        colStepSize = max(int((minNumCols-1)/2),1)
        
        totalBtime = 0 
        numBlocksFound = 0 
        
        for row in range(startRow, endRow+1, rowStepSize):
            for col in range(startCol, endCol+1, colStepSize):
                # work out index into our flat array
                i = ((row-startRow) * numCols) + (col-startCol) 
                
                if ( not self.isTraversed[i] and self.thresholdMap[i] ):

                    currentPixel = self.numpyImage[row,col] 
                    
                    # If it's not black - it must be one of the colours we're looking for, 
                    # since we've already filtered out everything else. 
                    block, btime = self.findBlock(currentPixel,row,col,startRow,endRow,startCol,endCol)
                    totalBtime += btime 
                    numBlocksFound += 1
                    #print("XXX - found block {}: {}x{} {} {}".format(block.name, block.getNumCols(), block.getNumRows(), str((block.left,block.top,block.right,block.bottom)), str((col,row))))
                        
                    if ( block.getNumRows() >= minNumRows and block.getNumCols() >= minNumCols ):
                        # is this the biggest block so far. 
                        biggestBlockSoFar = self.blocksFound.get(numpy.array2string(currentPixel))
                            
                        if ( biggestBlockSoFar != None ):
                                
                            if ( biggestBlockSoFar.getArea() < block.getArea() ):
                                self.blocksFound[numpy.array2string(currentPixel)] = block 
                        else:
                            self.blocksFound[numpy.array2string(currentPixel)] = block
                 
        #print("XXX - totalBtime = {}, blocks found = {} ".format(totalBtime, numBlocksFound))
        return self.blocksFound.values() 
                                
    def findBlock(self,pixel,row,col,startRow,endRow,startCol,endCol):
         
        startTime = time.time()
        # starting with this row, go up until we can't find any more rows.
        foundTop = False 
        
        searchRow = row  
        topRow = startRow 
        searchCol = col 
        maxLeftEdge = col 
        maxRightEdge = col 
        
        while (searchRow >= startRow) and (not foundTop):
             
            rowEdges = self.scanRow(searchRow,searchCol,startRow,endRow,startCol,endCol,pixel) 
            
            if ( rowEdges == None ): 
                topRow = searchRow + 1 
                foundTop = True 
            else:
            
                leftEdge = rowEdges[0]     
                rightEdge = rowEdges[1] 
               
                if ( leftEdge < maxLeftEdge ):
                    maxLeftEdge = leftEdge 
                   
                if ( rightEdge > maxRightEdge ):
                    maxRightEdge = rightEdge 
               
                # start the next search at the middle of the current row.
                searchCol = int((leftEdge + rightEdge) / 2)
                searchRow = searchRow - 1 
                
        # now find the bottom of the block 
        foundBottom = False 
        
        searchRow = row + 1
        bottomRow = endRow 
        searchCol = col 
        
        while (searchRow <= endRow) and (not foundBottom):
            
            rowEdges = self.scanRow(searchRow,searchCol,startRow,endRow,startCol,endCol,pixel)
            
            if ( rowEdges == None ):
                bottomRow = searchRow - 1 
                foundBottom = True 
            else:
                
                leftEdge = rowEdges[0]
                rightEdge = rowEdges[1]   
          
                if ( leftEdge < maxLeftEdge ):
                    maxLeftEdge = leftEdge 
                    
                if ( rightEdge > maxRightEdge ):
                    maxRightEdge = rightEdge 
                        
                # start the next search at the middle of the current row. 
                searchCol = int((leftEdge + rightEdge) / 2)
                searchRow = searchRow + 1 
        
        
        colourIndex = 0  
        for blockColour in self.blockColourList:
            if ( (blockColour == pixel).all() ):
                break
            colourIndex += 1 
            
        #colourIndex = self.blockColourList.index(pixel) 
        blockName = self.blockNameList[colourIndex]
        
        # make sure we don't try to scan any pixels in this block again. 
        numCols = (endCol - startCol) + 1 
        
        for row in range(topRow,bottomRow+1):
            for col in range(maxLeftEdge, maxRightEdge):
                i = ((row-startRow) * numCols) + (col-startCol) 
                self.isTraversed[i] = True 
                
        return Block(blockName, Pixel(pixel), topRow, bottomRow, maxLeftEdge, maxRightEdge), (time.time() - startTime)
        
    def scanRow(self,row,col,startRow,endRow,startCol,endCol,blockPixel):
        
        numCols = (endCol - startCol) + 1 

        # make sure we mark the current pixel as traversed. 
        i = ((row-startRow) * numCols) + (col-startCol) 
        self.isTraversed[i] = True 
        
        if (not self.isSimilarColour(blockPixel, self.numpyImage[row,col])):
            return None 
        
        self.numpyImage[row,col] = blockPixel
        
        leftEdge = col 
        rightEdge = col 

        scanCol = col - 1 
        
        while (scanCol >= startCol):

            i = ((row-startRow) * numCols) + (scanCol-startCol) 
            self.isTraversed[i] = True 
         
            if ( not self.isSimilarColour(blockPixel, self.numpyImage[row,scanCol]) ):
                break 

            self.numpyImage[row,col] = blockPixel
            
            leftEdge = scanCol 
            scanCol = scanCol - 1 
            
        scanCol = col + 1
         
        while (scanCol <= endCol):

            i = ((row-startRow) * numCols) + (scanCol-startCol) 
            self.isTraversed[i] = True 

            if ( not self.isSimilarColour(blockPixel, self.numpyImage[row,scanCol]) ):
                break

            self.numpyImage[row,scanCol] = blockPixel
 
            rightEdge = scanCol 
            scanCol = scanCol + 1 
            
        return( (leftEdge, rightEdge) )
        
