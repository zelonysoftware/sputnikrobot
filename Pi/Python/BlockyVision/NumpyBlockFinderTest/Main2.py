'''
Created on 7 Oct 2019

@author: zelony
'''

import time
from BlockyVision.BMPFile import BMPFile
from BlockyVision.BVBlockFinder2 import BVBlockFinder
from PIL import Image
import numpy 


#FILEROOT = './run/blocks2_100x75'
FILEROOT = './run/blocks2_500'
#FILEROOT = './run/test'
        
if __name__ == '__main__':
    
    # open file
    time1 = time.time() 
    
    bmpFile = BMPFile(FILEROOT + ".bmp")
    
    image = bmpFile.getImage()
    image.load()
    
    time2 = time.time()

    numpyImage = numpy.array(image.convert(mode="RGB"))

    #print(numpyImage)

    blockFinder = BVBlockFinder(numpyImage)
    blockFinder.setThreshold(35)
    blockFinder.setMinBlockWidthPcent(5)
    blockFinder.setMinBlockHeightPcent(5)

    blockFinder.setBlockColour('greenBlock', (2,136,120))
    blockFinder.setBlockColour('redBlock', (192,56,85))
    blockFinder.setBlockColour('blueBlock', (26,88,142))

    blockList = blockFinder.findBlocks()
    
    time3 = time.time()
    
    for block in blockList:
        block.printBlock()
    
    # write back file
    image = Image.fromarray(blockFinder.getImage())
    image.save(FILEROOT + "_filtered_euc.bmp") 
     
    time4 = time.time() 
    
    # Now generate a new bmp with just the found blocks. 
    blockImage = Image.new("RGB",image.size)
    
    for block in blockList:
        for row in range(block.top, block.bottom):
            for col in range(block.left, block.right):
                blockImage.putpixel((col,row),block.pixel.getRGB())

    
    blockImage.save(FILEROOT + "_blocky_euc.bmp") 
    
    elapsedTime1 = round((time2 - time1) * 1000)
    elapsedTime2 = round((time3 - time1) * 1000)
    elapsedTime3 = round((time4 - time1) * 1000)

    bvTime = round((time3 - time2) * 1000)
    print(f'Find block time = {bvTime}ms')

    print(f'times: 1={elapsedTime1}ms, 2={elapsedTime2}ms, 3={elapsedTime3}ms')
    print(f'fileopen time = {elapsedTime1}ms')
    print(f'conversion time = {elapsedTime2 - elapsedTime1}ms')
    print(f'filewrite time = {elapsedTime3 - elapsedTime2}ms')
    
    print(f'normalising time = {round(blockFinder.timeNormalise * 1000)}ms')
    print(f'similar time = {round(blockFinder.timeSimilar * 1000)}ms')

    # done 
    
