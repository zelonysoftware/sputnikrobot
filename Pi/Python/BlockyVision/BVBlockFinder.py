'''
Created on 12 Oct 2019

@author: zelony
'''
import time
import colorsys
from BlockyVision.BVBlock import Block
from BlockyVision.Bitmap import PixelConstants
from BlockyVision.Bitmap import Pixel
from BlockyVision.TurboBlockFinder import TurboBlockFinder
import BlockyVision.QuickCalc as qc

class BVBlockFinder():
    '''
    This class is used to take a bitmap and find objects of a particular colour.  
    The bitmap will be modified as it finds the colours. 
    '''
    # TODO - make this an imput so we can change the configuration. 
    EUCLIDEAN_THRESHOLD = 50
    # square the threshold so we don't have to calculate a square root each time. 
    EUCLIDEAN_THRESHOLD_SQUARED = (EUCLIDEAN_THRESHOLD * EUCLIDEAN_THRESHOLD)
    
    MIN_WIDTH_PCENT = 5 
    MIN_HEIGHT_PCENT = 5 
    
    def __init__(self):
        '''
        Constructor
        '''
        self.image = None
        self.blocksToFind = list()
        self.threshold = self.EUCLIDEAN_THRESHOLD_SQUARED
        self.minBlockWidthPcent = self.MIN_WIDTH_PCENT
        self.minBlockHeightPcent = self.MIN_HEIGHT_PCENT
        
        self.timeSimilar = 0
        self.countSimilar = 0
        self.timeNormalise = 0

        self.turboFinder = None 

    def getImage(self):
        return self.image         
    ''' 
    Set a named object that we want to find. 
    '''
    def setBlockColour(self, name, pixel):
        
        # normalise the brightness so we're only comparing the 
        # colour
        pixelRgb = self.normaliseBrightness2(pixel.getRGB())
        normalisedPixel = Pixel(pixelRgb) 
        
        self.blocksToFind.append((name, normalisedPixel))
    
    def clearBlocksToFind(self):
        self.blocksToFind.clear()
        
    def setThreshold(self,threshold):
        self.threshold = (threshold * threshold) 
        
    def setMinBlockWidthPcent(self, minBlockWidthPcent):
        self.minBlockWidthPcent = minBlockWidthPcent 

    def setMinBlockHeightPcent(self, minBlockHeightPcent):
        self.minBlockHeightPcent = minBlockHeightPcent 

    def setTurboFinderLib(self, turboFinderLibPath):
        self.turboFinder = TurboBlockFinder(turboFinderLibPath)
        
    def findBlocks(self, image, bbox=None):

        # You can choose not to use the whole image - the bounding box gives a bounding box as percentages
        # of the image size.
        self.image = image.convert(mode="RGB")
        
        if ( bbox is None ):
            bbox = (0,0,100,100) 
            
        cropX1, cropY1, cropX2, cropY2 = self.cropPcentToImageCoordinates(bbox) 
         
        # Work out the minimum size of any blocks based on the size of the bmp and the percentage size we're after.    
        numCols = self.image.size[0]
        minColSize = max(int((numCols * self.minBlockWidthPcent) / 100),1)

        numRows = self.image.size[1] 
        minRowSize = max(int((numRows * self.minBlockHeightPcent) / 100),1)

        if ( self.turboFinder is not None ):
            # TODO - consider having standard and turbo block finders derived from a common base? 
            return self.turboFinder.findBlocks( self.image, cropX1, cropY1, cropX2, cropY2, 
                                                self.threshold, minColSize, minRowSize, 
                                                self.blocksToFind )
        else:
            return self.stdFindBlocks(cropX1,cropY1,cropX2,cropY2, minColSize, minRowSize)

    def stdFindBlocks(self, cropX1, cropY1, cropX2, cropY2, minColSize, minRowSize):
        
        # First we go through applying a threshold to each pixel to only show colours we're looking for. 
        time1 = time.time() 
        self.applyThresholds(minColSize, minRowSize, cropX1, cropY1, cropX2, cropY2) 
        #print('1 Count similar time = {}ms, count = {}'.format(int(self.timeSimilar * 1000), self.countSimilar))
        time2 = time.time() 
        self.timeSimilar = 0 
        self.countSimilar = 0
        blockList = self.findBlockList(minColSize, minRowSize, cropX1, cropY1, cropX2, cropY2)
        #print('2 Count similar time = {}ms, count = {}'.format(int(self.timeSimilar * 1000), self.countSimilar))
        time3 = time.time() 
        
        #print('Apply Thresholds Time = {}ms'.format(int((time2 - time1)*1000)))
        #print('Find blocklist time = {}ms'.format(int((time3 - time2) * 1000)))
        #print('Total search time = {}ms'.format(int((time3 - time1) * 1000)))
        return blockList 
    
    
    def cropPcentToImageCoordinates(self, bbox):
        
        pcentX1, pcentY1, pcentX2, pcentY2 = bbox 
        
        # Convert percentages to real co-ordinates. 
        x1 = int((pcentX1 * self.image.width) / 100) 
        x1 = max(0,min(x1,self.image.width-1)) 

        x2 = int((pcentX2 * self.image.width) / 100)
        x2 = max(0,min(x2,self.image.width-1)) 

        y1 = int((pcentY1 * self.image.height) / 100)
        y1 = max(0,min(y1,self.image.height-1)) 

        y2 = int((pcentY2 * self.image.height) / 100) 
        y2 = max(0,min(y2,self.image.height-1)) 

        # Make sure x1,y1 is the top left and x2,y2 is the bottom right. 
        if ( y1 > y2 ):
            y1tmp = y1 
            y1 = y2 
            y2 = y1tmp 
            
        if ( x1 > x2 ):
            x1tmp = x1
            x1 = x2
            x2 = x1tmp 
            
        return( x1, y1, x2, y2 )

             
    def applyThresholds(self, minColSize, minRowSize, startCol, startRow, endCol, endRow):

        numCols = (endCol - startCol) + 1 
        numRows = (endRow - startRow) + 1

        self.thresholdMap = [False] * (numRows * numCols)

        # Use the minimum size to minimise the number of pixels we need to scan. 
        rowStepSize = max(int((minRowSize-1)/2),1)
        colStepSize = max(int((minColSize-1)/2),1)

        i = 0 
        #print("numRows = {}".format(int((endRow+1 - startRow)/rowStepSize)))
        #print("numCols = {}".format(int((endCol+1 - startCol)/colStepSize)))
        for row in range(startRow, endRow+1, rowStepSize):

            for col in range(startCol, endCol+1, colStepSize):
                currentPixel = Pixel(self.image.getpixel((col,row)))
                 
                for _, colourToFind in self.blocksToFind:
                    
                    if ( self.isSimilarColour(colourToFind, currentPixel) ):
                        self.image.putpixel((col,row),colourToFind.getRGB())
                        i = ((row-startRow) * numCols) + (col-startCol) 
                        self.thresholdMap[i] = True
                        break
                            
    def isSimilarColour(self, pixel1, pixel2):

        result = self.isSimilarColourEuclideanDistance(pixel1, pixel2)

        return result 
    
    def isSimilarColourEuclideanDistance(self, pixel1, pixel2):
                
        self.countSimilar += 1 
        timeStart = time.time() 

        rgb1 = pixel1.getRGB()
        rgb2 = pixel2.getRGB()
        
        # Check for exact match to save some time. 
        if ( rgb1 == rgb2 ):
            return True 
        
        rgb2 = self.normaliseBrightness2(rgb2)
        
        
        #distanceSquared = ( ((rgb1[0]-rgb2[0]) * (rgb1[0]-rgb2[0]))
        #                  + ((rgb1[1]-rgb2[1]) * (rgb1[1]-rgb2[1]))         
        #                  + ((rgb1[2]-rgb2[2]) * (rgb1[2]-rgb2[2])) )

        distanceSquared = ( qc.SQUARES_TO_255[abs(rgb1[0]-rgb2[0])]
                          + qc.SQUARES_TO_255[abs(rgb1[1]-rgb2[1])]
                          + qc.SQUARES_TO_255[abs(rgb1[2]-rgb2[2])] )
        
        timeElapsed = time.time() - timeStart
        
        self.timeSimilar += timeElapsed 
        
        if ( distanceSquared < self.threshold ):
            return True 
        
        return False          

    ''' 
    Convert to HSV - put the brightness (V) at a fixed value and then convert back to RGB
    ''' 
    def normaliseBrightness(self,rgb):
        
        timeStart = time.time() 
        
        r = rgb[0]
        g = rgb[1]
        b = rgb[2] 
        
        hsv = colorsys.rgb_to_hsv(r/255, g/255, b/255)

        rgbOut = colorsys.hsv_to_rgb(hsv[0],hsv[1],0.5) 
        
        normalised = (int(rgbOut[0] * 255), int(rgbOut[1] * 255), int(rgbOut[2] * 255)) 
        
        timeElapsed = time.time() - timeStart 
        
        self.timeNormalise += timeElapsed 
        
        return normalised 
        
    def normaliseBrightness2(self,rgb):
        
        timeStart = time.time() 
        
        minValue = min(rgb) 
        
        normalised = (rgb[0] - minValue, rgb[1] - minValue, rgb[2] - minValue) 
        
        timeElapsed = time.time() - timeStart 
        
        self.timeNormalise += timeElapsed 
        
        return normalised 
            
        
        
    def isSimilarColourHSV(self, pixel1, pixel2):
        
        pixel1HSV = pixel1.getHSV()
        pixel1Hue, pixel1Sat, pixel1Val = pixel1HSV

        pixel2HSV = pixel2.getHSV()
        pixel2Hue, pixel2Sat, pixel2Val = pixel2HSV

        # Does the hue at the current pixel match the threshold range for the colour to find - taking 
        # into account the fact we could go across the 360 degree boundary
        compareHueLower = ((pixel1Hue - self.threshold) + 360) % 360 
        compareHueUpper = (pixel1Hue + self.threshold) % 360 
    
        if (    ((compareHueUpper > compareHueLower) and ((pixel2Hue > compareHueLower) and (pixel2Hue < compareHueUpper)))
             or ((compareHueUpper < compareHueLower) and ((pixel2Hue > compareHueLower) or (pixel2Hue < compareHueUpper))) ):
            # found hue 
            return True
                
        return False 

                    
    def findBlockList(self, minNumCols, minNumRows, startCol, startRow, endCol, endRow):

        # Go through the filtered bmp finding objects that consist of each colour.
        numCols = (endCol - startCol) + 1 
        numRows = (endRow - startRow) + 1
         
        self.isTraversed = [False] * (numRows * numCols)
        
        # we don't need to traverse every row and column - since we're looking for objects larger than 
        # n % of the total sise 
        rowStepSize = max(int((minNumRows-1)/2),1)
        colStepSize = max(int((minNumCols-1)/2),1)
        
        totalBtime = 0 
        numBlocksFound = 0 
        blocksFound = dict()
        
        for row in range(startRow, endRow+1, rowStepSize):
            for col in range(startCol, endCol+1, colStepSize):
                # work out index into our flat array
                i = ((row-startRow) * numCols) + (col-startCol) 
                
                if ( not self.isTraversed[i] and self.thresholdMap[i] ):

                    currentPixel = Pixel(self.image.getpixel((col,row))) 
                    
                    # If it's not black - it must be one of the colours we're looking for, 
                    # since we've already filtered out everything else. 
                    block, btime = self.findBlock(currentPixel,row,col,startRow,endRow,startCol,endCol)
                    totalBtime += btime 
                    numBlocksFound += 1
                    #print("XXX - found block {}: {}x{} {} {}".format(block.name, block.getNumCols(), block.getNumRows(), str((block.left,block.top,block.right,block.bottom)), str((col,row))))
                        
                    if ( block.getNumRows() >= minNumRows and block.getNumCols() >= minNumCols ):
                        # is this the biggest block so far. 
                        biggestBlockSoFar = blocksFound.get(currentPixel.getHexString())
                            
                        if ( biggestBlockSoFar != None ):
                                
                            if ( biggestBlockSoFar.getArea() < block.getArea() ):
                                blocksFound[currentPixel.getHexString()] = block 
                        else:
                            blocksFound[currentPixel.getHexString()] = block
                 
        #print("XXX - totalBtime = {}, blocks found = {} ".format(totalBtime, numBlocksFound))
        return blocksFound.values() 
                                
    def findBlock(self,pixel,row,col,startRow,endRow,startCol,endCol):
         
        startTime = time.time()
        # starting with this row, go up until we can't find any more rows.
        foundTop = False 
        
        searchRow = row  
        topRow = startRow 
        searchCol = col 
        maxLeftEdge = col 
        maxRightEdge = col 
        
        while (searchRow >= startRow) and (not foundTop):
             
            rowEdges = self.scanRow(searchRow,searchCol,startRow,endRow,startCol,endCol,pixel) 
            
            if ( rowEdges == None ): 
                topRow = searchRow + 1 
                foundTop = True 
            else:
            
                leftEdge = rowEdges[0]     
                rightEdge = rowEdges[1] 
               
                if ( leftEdge < maxLeftEdge ):
                    maxLeftEdge = leftEdge 
                   
                if ( rightEdge > maxRightEdge ):
                    maxRightEdge = rightEdge 
               
                # start the next search at the middle of the current row.
                searchCol = int((leftEdge + rightEdge) / 2)
                searchRow = searchRow - 1 
                
        # now find the bottom of the block 
        foundBottom = False 
        
        searchRow = row + 1
        bottomRow = endRow 
        searchCol = col 
        
        while (searchRow <= endRow) and (not foundBottom):
            
            rowEdges = self.scanRow(searchRow,searchCol,startRow,endRow,startCol,endCol,pixel)
            
            if ( rowEdges == None ):
                bottomRow = searchRow - 1 
                foundBottom = True 
            else:
                
                leftEdge = rowEdges[0]
                rightEdge = rowEdges[1]   
          
                if ( leftEdge < maxLeftEdge ):
                    maxLeftEdge = leftEdge 
                    
                if ( rightEdge > maxRightEdge ):
                    maxRightEdge = rightEdge 
                        
                # start the next search at the middle of the current row. 
                searchCol = int((leftEdge + rightEdge) / 2)
                searchRow = searchRow + 1 
        
        blockName,_ = self.getBlockToFindByPixel(pixel);

        # make sure we don't try to scan any pixels in this block again. 
        numCols = (endCol - startCol) + 1 
        
        for row in range(topRow,bottomRow+1):
            for col in range(maxLeftEdge, maxRightEdge):
                i = ((row-startRow) * numCols) + (col-startCol) 
                self.isTraversed[i] = True 
                
        
        return Block(blockName, pixel, topRow, bottomRow, maxLeftEdge, maxRightEdge), (time.time() - startTime)

    def getBlockToFindByPixel(self,pixel):
        for blockToFind in self.blocksToFind:
            _,colour = blockToFind ;
            if ( colour == pixel ):
                return blockToFind;

        return None ;
        
    def scanRow(self,row,col,startRow,endRow,startCol,endCol,blockPixel):
        
        numCols = (endCol - startCol) + 1 

        # make sure we mark the current pixel as traversed. 
        i = ((row-startRow) * numCols) + (col-startCol) 
        self.isTraversed[i] = True 
        
        if (not self.isSimilarColour(blockPixel, Pixel(self.image.getpixel((col,row)))) ):
            return None 
        
        self.image.putpixel((col,row),blockPixel.getRGB())
        
        leftEdge = col 
        rightEdge = col 

        scanCol = col - 1 
        
        while (scanCol >= startCol):

            i = ((row-startRow) * numCols) + (scanCol-startCol) 
            self.isTraversed[i] = True 
         
            if ( not self.isSimilarColour(blockPixel, Pixel(self.image.getpixel((scanCol,row)))) ):
                break 

            self.image.putpixel((scanCol,row),blockPixel.getRGB())
            
            leftEdge = scanCol 
            scanCol = scanCol - 1 
            
        scanCol = col + 1
         
        while (scanCol <= endCol):

            i = ((row-startRow) * numCols) + (scanCol-startCol) 
            self.isTraversed[i] = True 

            if ( not self.isSimilarColour(blockPixel, Pixel(self.image.getpixel((scanCol,row)))) ):
                break

            self.image.putpixel((scanCol,row),blockPixel.getRGB())
 
            rightEdge = scanCol 
            scanCol = scanCol + 1 
            
        return( (leftEdge, rightEdge) )


        
