'''
Created on 15 Oct 2021

@author: zelony
'''


'''
 HSV to RGB conversion - the formula is taken from the web page
 
 https://www.rapidtables.com/convert/color/hsv-to-rgb.html 
 
 inputs:
 
 h - h value in range 0 <= h < 360
 s - s value in range 0 <= s <= 100
 v - v value in range 0 <= v <= 100
 
 outputs:
 
 RGB values in range 0 - 255 
''' 
def HSVtoRGB(h, s, v):
    
    # v and s should be in range 0-1 - but we have them in range 0-100
    
    V = float(v) / 100.0
    S = float(s) / 100.0 
    
    C = V * S 
    
    X = C * (1- abs(((h / 60) % 2) - 1))
    
    m = V - C
    
    r1 = 0 
    g1 = 0 
    b1 = 0 
    
    if ( h < 60 ):
        r1 = C 
        g1 = X 
        b1 = 0 
    elif ( h < 120 ):
        r1 = X 
        g1 = C
        b1 = 0 
    elif ( h < 180 ):
        r1 = 0
        g1 = C
        b1 = X 
    elif ( h < 240 ):
        r1 = 0 
        g1 = X
        b1 = C
    elif ( h < 300 ):
        r1 = X
        g1 = 0 
        b1 = C
    else:
        r1 = C
        g1 = 0 
        b1 = X
        
    r = int((r1 + m) * 255)
    g = int((g1 + m) * 255)
    b = int((b1 + m) * 255)
        
    return r,g,b 

    
    
    
