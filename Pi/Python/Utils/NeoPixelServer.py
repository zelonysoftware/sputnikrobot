
import neopixel
import board
import socket 
import pickle

# TODO - take pin and pixel information from command line arguments
PIXELS_PIN=board.D12 
NUM_PIXELS=2

# TODO - take port from configuration or command line params. 
LOCAL_IP="127.0.0.1"
LISTEN_PORT=13700

class NeoPixelMessageType():
    
    SET=0
    FILL=1

def getPixels():
    return neopixel.NeoPixel(PIXELS_PIN, NUM_PIXELS)
    
if __name__ == '__main__':

    # Create UDP socket and bind to our port. 
    udpSocket = socket.socket(family=socket.AF_INET,
                              type=socket.SOCK_DGRAM)
    udpSocket.bind((LOCAL_IP,LISTEN_PORT))
    
    pixels = None 

    while True: 
        
        message, _ = udpSocket.recvfrom(1024)
        
        msgType, payload = pickle.loads(message)
        
        if ( msgType == NeoPixelMessageType.SET ):
            if ( not pixels ):
                pixels = getPixels()
                 
            pixel, r, g, b = payload  
            pixels[pixel]= ((r,g,b))
             
        elif ( msgType == NeoPixelMessageType.FILL ):
            if ( not pixels ):
                pixels = getPixels()
                 
            r,g,b = payload 
            pixels.fill((r,g,b))
                 
    


