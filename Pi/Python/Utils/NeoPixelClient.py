
from Utils.NeoPixelServer import NeoPixelMessageType
import socket
import pickle 


class NeoPixelClient(object):

    def __init__(self, host, port):
        
        self.serverAddress = (host,port) 
        self.clientSocket = socket.socket(family=socket.AF_INET,
                                          type=socket.SOCK_DGRAM)
        
    def set(self, pixel, red, green, blue ):
        
        pickledData = pickle.dumps((NeoPixelMessageType.SET, (pixel, red, green, blue)))
        self.clientSocket.sendto(pickledData, self.serverAddress)
        
    def fill(self, red, green, blue ): 

        pickledData = pickle.dumps((NeoPixelMessageType.FILL, (red, green, blue)))
        self.clientSocket.sendto(pickledData, self.serverAddress)

# test code
if __name__ == '__main__':

    import time 
    
    client = NeoPixelClient('127.0.0.1', 13700)
    
    client.set(0,127,0,0)
    time.sleep(1)
    client.set(1,0,127,0)
    time.sleep(1)
    client.fill(0,0,127) 
    time.sleep(1)
    
    client.fill(127,127,127) 
    
