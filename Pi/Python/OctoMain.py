#!/usr/bin/python3
import time
import RPi.GPIO as GPIO
import sys
import os
import logging
from Robot.Environment.BlockyVisionCamera import BlockyVisionCamera
from Robot.Config.RobotConfig import RobotConfig 
from Robot.Config.ConfigUsingIniFile import ConfigUsingIniFile
from Robot.Config.SerialCommsConfig import SerialCommsConfig
import Robot.Logging as Logging
from BatteryMonitor.BatteryMonitorBase import BatteryMonitorBase
from MotorController.RoboHatMotorController import RoboHatMotorController
from MotorController.TestMotorController import TestMotorController
from GamesController.PiHutController import PiHutController

# ---------------Initialise Logging before we import the Robot modules  ----------------------------

robotHome = os.getenv('ROBOTHOME')
print("Robot Home = " + str(robotHome)) 
if ( robotHome == None ):
    robotHome = os.getenv('HOME')
if ( robotHome == None ):
    robotHome = "."

configFile = robotHome + '/Config/' + "octobot.ini"
print("Using config file " + configFile)

configImpl = ConfigUsingIniFile(configFile)
robotConfig = RobotConfig(configImpl)

# Now we've got the config file we can setup the logging properly 
Logging.initialiseLogger(configImpl)

# --------------------------------------------------------------------------------------------------

from GamesController.PS3Controller import PS3Controller
from Robot.Robot import Robot
from Robot.RobotModes.RobotModeList import RobotModeList
import Robot.RobotModes.RobotModes as RobotModes
from Robot.SerialComms.SerialCommsHandler import SerialCommsHandler
from Robot.RobotMonitorThread import RobotMonitorThread
import threading
import signal 

def createMotorController(config):

    motorControllerType = config.getMotorControllerType(default="RoboHat")

    motorController = None
    
    if ( motorControllerType == 'TEST' ):
        logging.info("Using Test Motor Controller")
        motorController = TestMotorController()
    elif (motorControllerType == 'RoboHat'):
        logging.info("Using RoboHat Motor Controller")
        motorController = RoboHatMotorController()
    else:
        logging.info("Unrecognised Motor Controller type " + motorControllerType + " - defaulting to TEST")
        motorController = TestMotorController()

    return motorController


# ----------- MAIN ENTRY POINT ------------

theRobot = None
serialCommsHandler = None 
monitorThread = None
exitCleanly = False

def exitNicely(signum, frame):
    global exitCleanly
    exitCleanly = True

try:

    signal.signal(signal.SIGINT, exitNicely)
    signal.signal(signal.SIGTERM, exitNicely)

    GPIO.setmode(GPIO.BCM)
    
    mainLogger = logging.getLogger(__name__) 

    mainLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    # Create and start the robot (this should default to remote controlled
    # mode - so won't do anything until we connect the PS3 controller). 
    mainLogger.info("Creating the Robot")

    motorController = createMotorController(robotConfig); 
    batteryMonitor = BatteryMonitorBase()

    octobotModeList = RobotModeList([ RobotModes.MODE_REMOTE ]) 

    theRobot = Robot( robotConfig, motorController, batteryMonitor, modeList=octobotModeList )

    # Create the games controllers, passing the robot's callback handler. 
    mainLogger.info("Creating the Games Controllers")
    gamesControllerHandler = theRobot.getGamesControllerHandler()
    ps3Controller = PS3Controller()
    ps3Controller.runAsThread(gamesControllerHandler)
    piHutController = PiHutController()
    piHutController.runAsThread(gamesControllerHandler)

    # create and startup the serial comms handler.
    mainLogger.info("Creating the SerialCommsHandler")
    serialCommsHandler = SerialCommsHandler(SerialCommsConfig(configImpl), theRobot)
    theRobot.addStateListener(serialCommsHandler) 
    serialCommsHandler.start() 

    # add a camera to the Robot's environments
    if ( not 'nocamera' in sys.argv ):
        mainLogger.info("Adding Camera to robot environment")
        environment = theRobot.getEnvironment() 
        environment.setSmartCamera(BlockyVisionCamera(isInverted=False))

    print("Got this far [3]")

    # create and startup the status monitor thread. This monitors and logs the status
    # of the robot
    monitorThread = RobotMonitorThread(theRobot)
    monitorThread.start() 

    time.sleep(1)
    print("Starting Robot")
    theRobot.start()

    allThreads = threading.enumerate()

    for aThread in allThreads:
        mainLogger.info("Thread " + aThread.name + " isDaemon = " + str(aThread.daemon))

    while not exitCleanly:
        time.sleep(1)

except KeyboardInterrupt:
    mainLogger.warn("Interrupted")

finally:
    mainLogger.info("-----------------------------------------------------------------------")
    mainLogger.info("Stopping Robot")
    if ( theRobot != None ):
        theRobot.stopWheels()
        theRobot.stop()
    mainLogger.info("Robot Stopped")
    mainLogger.info("Stopping Serial Handlers")
    if ( serialCommsHandler != None ):
        serialCommsHandler.stop()
    mainLogger.info("Serial Handlers Stopped") 
    if ( monitorThread != None ):
        monitorThread.stop()
    mainLogger.info("Monitor Thread Stopped")
    mainLogger.info("Cleanup GPIO")
    GPIO.setwarnings(False)
    GPIO.cleanup()
    mainLogger.info("GPIO Cleaned up")
    mainLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    #sys.exit commented out so we don't lose exception messages. 
    #sys.exit(0)

