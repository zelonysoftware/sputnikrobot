

from Robot.RobotWheels import RobotWheels
from Robot.RobotModes.RobotModeList import RobotModeList
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotGamesControllerHandler import RobotGamesControllerHandler
from Robot.Environment.RobotEnvironment import RobotEnvironment 
import Robot.RobotEvent.RobotEvent as RobotEvent

import RPi.GPIO as GPIO

import time
import logging

logger = logging.getLogger(__name__)

class Robot():
    """Represents our robot"""
    
    # The list order defines which order the modes will be selected. 
    defaultModeList = RobotModeList([   RobotModes.MODE_REMOTE,
                                        RobotModes.MODE_STRAIGHT_LINE,
                                        RobotModes.MODE_SPACE_INVADERS,
                                        RobotModes.MODE_MINIMAL_MAZE,
                                        RobotModes.MODE_REMOTE,
                                        RobotModes.MODE_HUBBLE  ])

    def __init__(self, robotConfig, motorController, batteryMonitor, modeList=defaultModeList):
        self.robotConfig = robotConfig

        self.gamesControllerHandler = None
        self.stateListeners = [] 
        self.currentMode = None
        self.showingModeLed = True
        self.restoreLedValue = 0,0,0
        
        self.motorController = motorController
        self.batteryMonitor = batteryMonitor        
        self.robotWheels = RobotWheels(self.motorController)
        self.robotEnvironment = RobotEnvironment()
        self.ninetyDegreeTurnTime = int(robotConfig.getNinetyDegreeTurnTime("300"))
        self.useCompassHeading = robotConfig.getUseCompassHeading('False') == 'True' 
        logger.info("Using compass heading = %s", str(self.useCompassHeading))

        self.laserPin = int(robotConfig.getLaserGpioPin("0"))
        if ( self.laserPin > 0 ):
            GPIO.setup(self.laserPin, GPIO.OUT, initial=GPIO.LOW)

        self.modeList = modeList 

    def getConfig(self):
        return self.robotConfig

    def getGamesControllerHandler(self):
        if ( self.gamesControllerHandler == None ):
            self.gamesControllerHandler = RobotGamesControllerHandler(self)

        return self.gamesControllerHandler

    def addStateListener(self, listener):
        self.stateListeners.append(listener)

    def getEnvironment(self):
        return self.robotEnvironment

    def getXXXEnvironment(self):
        print("xxx - getEnvironment")
        return self.robotEnvironment

    def getBatteryLevel(self):
        batteryReading = self.batteryMonitor.getBatteryLevel()
        if ( batteryReading != None ):
            return "{:.2f}v".format(batteryReading)
        
        return "?" 

    def handleEvent(self, robotEvent):
        #print("ROBOT - handle event")

        eventHandledByMode = False 

        # Allow the current mode to have a go at handling the event first 
        if ( self.currentMode != None ):
            eventHandledByMode = self.currentMode.handleEvent(robotEvent)
        
        if ( not eventHandledByMode ):
            eventType = robotEvent.getEventType()

            # SELECT button changes the robot's operating mode. 
            if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE ):
                if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_SELECT ):
                    # Trigger mode change when the button is released. 
                    if ( robotEvent.getButtonValue() == 0 ):
                        self.advanceMode()
                elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_TRIANGLE ):
                    if ( robotEvent.getButtonValue() == 0 ):
                        # get the battery monitor to toggle between battery level and mode display (if possible) 
                        self.batteryMonitor.toggleDisplay()
                        for stateListener in self.stateListeners:
                            stateListener.switchedToBatteryDisplay(self.getBatteryLevel())

                elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_UP ):
                    if ( robotEvent.getButtonValue() == 0 ):
                        self.ninetyDegreeTurnTime += 10
                        logger.info("Ninety degree turn time increased to %d" % self.ninetyDegreeTurnTime)
                elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_DOWN ):
                    if ( robotEvent.getButtonValue() == 0 ):
                        self.ninetyDegreeTurnTime -= 10
                        logger.info("Ninety degree turn time reduced to %d" % self.ninetyDegreeTurnTime)


    def start(self):
        # Create 1st mode
        self.startNextMode()

    def stop(self):
        logging.info("Robot.stop() - calling stop on operating mode")
        if ( self.currentMode != None ):
            logging.info("Robot.stop current mode = %s", self.currentMode.name)
            self.currentMode.stop()
        for stateListener in self.stateListeners:
            stateListener.robotModeChanged(None)

    """ Start up a robot mode """        
    def startNextMode(self):

        self.currentMode = self.modeList.getNextMode(self)

        if ( self.currentMode != None ):
            self.currentMode.start()

        for stateListener in self.stateListeners:
            stateListener.robotModeChanged(self.currentMode.modeName)

    """ Move to the next mode in the list """
    def advanceMode(self):

        if ( self.currentMode != None ):
            self.currentMode.stop()
            # Make sure camera lamp switched off (really ought to get the mode to do this)
            self.getEnvironment().setCameraLamp(False)
            
        self.startNextMode()
        

    """Start moving right wheel at given speed in range -100 to +100"""
    def moveRightWheel(self, speed):
        self.robotWheels.moveRightWheel(speed)

    """Start moving left wheel at given speed in range -100 to +100"""
    def moveLeftWheel(self, speed):
        self.robotWheels.moveLeftWheel(speed)

    """Move both wheels at same speed"""
    def moveBothWheels(self, speed):
        self.robotWheels.moveBothWheels(speed)

    """Stop both wheels"""
    def stopWheels(self):
        self.robotWheels.stop()

    """Utility to calculate difference between a start and target angle """    
    def calcAngleDiff(self, startAngle, targetAngle):

        angleDiff = targetAngle - startAngle 
        
        if ( abs(angleDiff) > 180 ):
            if ( angleDiff >= 0 ):
                angleDiff = angleDiff - 360 
            else:
                angleDiff = angleDiff + 360 
                
        return angleDiff


    """ Turn by number of degrees -ve for left, +ve for right """
    def doRecursiveSpin(self, numDegrees, targetHeading, iterCount):

        logger.info("Turning by %d degrees", numDegrees);
        logger.info("Ninety Degree Turn Time = %d", self.ninetyDegreeTurnTime)
        self.stopWheels()

        turningLeft = False 

        if ( numDegrees < 0 ):
            turningLeft = True 

        numDegrees = abs(numDegrees)
        
        if ( numDegrees < 3 or numDegrees > 359 ):
            return

        turnTimeMs = (numDegrees * self.ninetyDegreeTurnTime) / 90 
        speed = 100 

        if ( turningLeft ):
            self.moveRightWheel(speed)
            self.moveLeftWheel(0 - speed);
        else:
            self.moveLeftWheel(speed)
            self.moveRightWheel(0 - speed);

        st = time.time()
        time.sleep(turnTimeMs / 1000)
        et = time.time()
        logger.info("Slept for %d ms", (et-st) * 1000)

        self.stopWheels()         

        logger.info("Stopped turning");

        if ( not self.useCompassHeading ):
            logger.info("Fine tuning using compass heading disabled") 
            return

        if ( targetHeading < 0 ):
            # We haven't got a heading from the microbit - so that's all we can do. 
            logger.info("Skipping fine tuning turning since we don't appear to have heading information")
            return 

        # Now - we need to fine tune the turn using the compass heading. This is called recursively 
        # until we're pointing the way we want to - or we've gone through the number of iterations we want to. 
        if ( iterCount > 0 ):
            logger.info("Fine tuning turn using compass heading")
            currentHeading = self.robotEnvironment.getStableHeading().value
            
            diffToTargetHeading = self.calcAngleDiff(currentHeading, targetHeading) 

            logger.info("Check spin heading: currentHeading = %d, targetHeading = %d, diffToTargetHeading = %d", 
                         currentHeading, targetHeading, diffToTargetHeading)
    
            if ( abs(diffToTargetHeading) > 3 ):
                self.doRecursiveSpin(diffToTargetHeading, targetHeading, iterCount - 1)            

                
    """ Do the spin and kick off the recursive fine tuning.  """
    def doSpin(self, numDegrees):
    
        targetHeading = -1 
        
        if ( (abs(numDegrees) < 3) or abs(numDegrees) > 359 ):
            logger.warn("Invalid number of degrees for spin")
            return 
        
        if ( self.useCompassHeading ): 
            startHeading = self.robotEnvironment.getStableHeading().value
            if ( startHeading >= 0 ):
                # we have a starting heading - so calculate the target Heading. 
                targetHeading = ((startHeading + 360) + numDegrees) % 360  
                logger.info("doSpin( numDegrees = %d, startHeading = %d, targetHeading = %d", numDegrees, startHeading, targetHeading);

        self.doRecursiveSpin(numDegrees, targetHeading, 4)
        
    
    """Turn right by a number of degrees"""
    def timedSpinRight(self, numDegrees):
        logger.info("Turning right by %d degrees", numDegrees)
        self.doSpin(numDegrees) 
        logger.info("Stopped turning right")

    """ Turn left by number of degrees """ 
    def timedSpinLeft(self, numDegrees):
        logger.info("Turning left by %d degrees", numDegrees)
        self.doSpin(0-numDegrees)
        logger.info("Stopped turning left")

    def moveForward(self, speed):
        self.robotWheels.forward(speed);

    def moveBackward(self, speed):
        self.robotWheels.reverse(speed) 
        

    """ Start spinning left - until told to stop """
    def startSpinLeft(self, spinSpeed):
        self.robotWheels.moveRightWheel(spinSpeed)
        self.robotWheels.moveLeftWheel(0 - spinSpeed) 

    """ Start spinning right - until told to stop """
    def startSpinRight(self, spinSpeed):
        self.robotWheels.moveLeftWheel(spinSpeed)
        self.robotWheels.moveRightWheel(0 - spinSpeed) 
                
    def setLaser(self, isOn):
        if ( isOn ):
            GPIO.output(self.laserPin, GPIO.HIGH)
        else:
            GPIO.output(self.laserPin, GPIO.LOW)
