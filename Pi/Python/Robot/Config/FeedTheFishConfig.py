


FEED_THE_FISH_SECTION="FeedTheFishMode"

POLL_TIME ="PollTime"
FIND_BLOCK_CROP_TOP = "FindBlockCropTop"
FIND_BLOCK_CROP_BTM = "FindBlockCropBtm" 
FISH_TANK_RGB = "FishTankRGB" 
COLOUR_THRESHOLD = "ColourThreshold"
HORIZON_CROP_LEFT = "HorizonCropLeft"
HORIZON_CROP_RIGHT = "HorizonCropRight"
MAX_STRAIGHT_LINE_VEER_TIME = "MaxStraightLineVeerTime"
MAX_STRAIGHT_LINE_VEER_MULTIPLIER = "MaxStraightLineVeerMultiplier"
NINETY_DEGREE_TURN_TIME = "NinetyDegreeTurnTime"

MOVE_TO_DISTANCE_SLOW_DOWN_DISTANCE = "MoveToDistanceSlowDownDistance" 
MOVE_TO_DISTANCE_SLOW_DOWN_SPEED = "MoveToDistanceSlowDownSpeed" 

BLOCK_CENTRE_LEFT = "BlockCentreLeft"
BLOCK_CENTRE_RIGHT = "BlockCentreRight"

LOCATE_BLOCK_SPIN_TIME = "LocateBlockSpinTime"

MIN_SPEED = "MinSpeed"
MAX_SPEED = "MaxSpeed"


class FeedTheFishConfig():
    
    def __init__(self, robotConfig):
        self.robotConfig = robotConfig

    def csvToRGBTuple(self,csv):
        values = csv.split(",")
        r = int(values[0])
        g = int(values[1])
        b = int(values[2])
        return (r,g,b)

    def getPollTime(self, default="50"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, POLL_TIME, default))

    def getMinSpeed(self, default="30"):
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, MIN_SPEED, default)) 

    def getMaxSpeed(self, default="100"):
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, MAX_SPEED, default)) 

    def getFindBlockCropTop(self, default = "30"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, FIND_BLOCK_CROP_TOP, default))

    def getFindBlockCropBtm(self, default = "80"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, FIND_BLOCK_CROP_BTM, default))

    def getColourThreshold(self, default="25"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, COLOUR_THRESHOLD, default))

    def getFishTankRGB(self, default = "255,127,39"):
        return self.csvToRGBTuple(self.robotConfig.getValue(FEED_THE_FISH_SECTION, FISH_TANK_RGB, default))
    
    def getHorizonCropLeft(self, default="40"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, HORIZON_CROP_LEFT, default))
            
    def getHorizonCropRight(self, default="60"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, HORIZON_CROP_RIGHT, default))

    def getMaxStraightLineVeerTime(self, default="0.2"):
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, MAX_STRAIGHT_LINE_VEER_TIME, default)) 
    
    def getStraightLineVeerMultiplier(self, default="0.1"):    
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, MAX_STRAIGHT_LINE_VEER_MULTIPLIER, default)) 

    def getLocateBlockSpinTime(self, default = "40"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, LOCATE_BLOCK_SPIN_TIME, default))

    def getNinetyDegreeTurnTime(self, default="250"):
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, NINETY_DEGREE_TURN_TIME, default))
    
    def getMoveToDistanceSlowDownDistance(self, default="20.0"):    
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, MOVE_TO_DISTANCE_SLOW_DOWN_DISTANCE, default)) 

    def getMoveToDistanceSlowDownSpeed(self, default="60"):    
        return float(self.robotConfig.getValue(FEED_THE_FISH_SECTION, MOVE_TO_DISTANCE_SLOW_DOWN_SPEED, default)) 

    def getBlockCentreLeft(self, default = "-5"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, BLOCK_CENTRE_LEFT, default))
    
    def getBlockCentreRight(self, default = "5"):
        return int(self.robotConfig.getValue(FEED_THE_FISH_SECTION, BLOCK_CENTRE_RIGHT, default))
