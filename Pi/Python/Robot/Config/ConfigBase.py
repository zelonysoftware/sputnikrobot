
from configparser import ConfigParser

""" This class is the interface to a Robot configuration. Normally we would always use the 
    ini file implementation (RobotConfigUsingIniFile). But - we could override this for testing """
class ConfigBase():

    def getValue(self, section, value, defaultValue):
        pass


