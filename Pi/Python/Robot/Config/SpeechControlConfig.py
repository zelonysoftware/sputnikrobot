
SPEECH_CONTROL_SECTION="SpeechControl"

SPEED_RIGHT="SpeedRight"
SPEED_LEFT="SpeedLeft"

STRAIGHT_UNIT_MS="ForwardUnitMs"
SPIN_UNIT_MS="SpinUnitMs"

CIRCLE_SPIN_UNIT_MS="CircleSpinUnitMs"
CIRCLE_FWD_UNIT_MS="CircleFwdUnitMs"

class SpeechControlConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getSpeedLeft(self, default = "60"):
        return int(self.robotConfig.getValue(SPEECH_CONTROL_SECTION, SPEED_LEFT, default))

    def getSpeedRight(self, default = "60"):
        return int(self.robotConfig.getValue(SPEECH_CONTROL_SECTION, SPEED_RIGHT, default))

    def getStraightUnitMs(self, default = "500"):
        return int(self.robotConfig.getValue(SPEECH_CONTROL_SECTION, STRAIGHT_UNIT_MS, default))

    def getSpinUnitMs(self, default = "100"):
        return int(self.robotConfig.getValue(SPEECH_CONTROL_SECTION, SPIN_UNIT_MS, default))

    def getCircleFwdUnitMs(self, default = "400"):
        return int(self.robotConfig.getValue(SPEECH_CONTROL_SECTION, CIRCLE_FWD_UNIT_MS, default))

    def getCircleSpinUnitMs(self, default = "100"):
        return int(self.robotConfig.getValue(SPEECH_CONTROL_SECTION, CIRCLE_SPIN_UNIT_MS, default))

