
TIDY_TOYS_SECTION="TidyToysMode"

POLL_TIME_KEY="PollTime"

NINETY_DEGREE_TURN_TIME_WITH_BLOCK = "NinetyDegreeTurnTimeWithBlock"
NINETY_DEGREE_TURN_TIME_WITHOUT_BLOCK = "NinetyDegreeTurnTimeWithoutBlock"

MOVE_TO_DISTANCE_SLOW_DOWN_DISTANCE = "MoveToDistanceSlowDownDistance" 
MOVE_TO_DISTANCE_SLOW_DOWN_SPEED = "MoveToDistanceSlowDownSpeed" 

BLOCK_CENTRE_LEFT = "BlockCentreLeft"
BLOCK_CENTRE_RIGHT = "BlockCentreRight"
BLOCK_CENTRE_OFFSET = "BlockCentreOffset"

LOCATE_BLOCK_SPIN_TIME = "LocateBlockSpinTime"

SPIN_SPEED = "SpinSpeed"

MIN_SPEED = "MinSpeed"
MAX_SPEED = "MaxSpeed"

MOVE_TO_COLOUR_SPEED = "MoveToColourSpeed"

FIND_BLOCK_CROP_TOP = "FindBlockCropTop"
FIND_BLOCK_CROP_BTM = "FindBlockCropBtm" 

MIN_BLOCK_DISTANCE = "MinBlockDistance"

COLOUR_THRESHOLD = "ColourThreshold" 
RED_BLOCK_RGB = "RedBlockRGB" 
GREEN_BLOCK_RGB = "GreenBlockRGB"
BLUE_BLOCK_RGB = "BlueBlockRGB"
YELLOW_BLOCK_RGB = "YellowBlockRGB"

MAX_STRAIGHT_LINE_VEER_TIME = "MaxStraightLineVeerTime"
MAX_STRAIGHT_LINE_VEER_MULTIPLIER = "StraightLineVeerMultiplier"

HORIZON_CROP_LEFT = "HorizonCropLeft"
HORIZON_CROP_RIGHT = "HorizonCropRight"

class TidyToysConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 
        
    def getPollTime(self, default = "20"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, POLL_TIME_KEY, default))
    
    def getNinetyDegreeTurnTimeWithBlock(self, default="550"):
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, NINETY_DEGREE_TURN_TIME_WITH_BLOCK, default))

    def getNinetyDegreeTurnTimeWithoutBlock(self, default="550"):
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, NINETY_DEGREE_TURN_TIME_WITHOUT_BLOCK, default))
    
    def getBlockCentreLeft(self, default = "-5"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, BLOCK_CENTRE_LEFT, default))
    
    def getBlockCentreRight(self, default = "5"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, BLOCK_CENTRE_RIGHT, default))

    def getBlockCentreOffset(self, default = "0"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, BLOCK_CENTRE_OFFSET, default))
    
    def getLocateBlockSpinTime(self, default = "100"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, LOCATE_BLOCK_SPIN_TIME, default))
    
    def getMaxSpeed(self, default = "70"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, MAX_SPEED, default))

    def getMinSpeed(self, default = "10"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, MIN_SPEED, default))

    def getSpinSpeed(self, default = "100"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, SPIN_SPEED, default))

    def getMoveToColourSpeed(self, default = "60"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, MOVE_TO_COLOUR_SPEED, default))

    def getMinBlockDistance(self, default = "10.0"):
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, MIN_BLOCK_DISTANCE, default))

    def getFindBlockCropTop(self, default = "50"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, FIND_BLOCK_CROP_TOP, default))

    def getFindBlockCropBtm(self, default = "100"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, FIND_BLOCK_CROP_BTM, default))
    
    def getColourThreshold(self, default = "20"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, COLOUR_THRESHOLD, default))
    
    def getRedBlockRGB(self, default = "192,56,85"):
        return self.csvToRGBList(self.robotConfig.getValue(TIDY_TOYS_SECTION, RED_BLOCK_RGB, default))
        
    def getGreenBlockRGB(self, default = "32,96,84"):
        return self.csvToRGBList(self.robotConfig.getValue(TIDY_TOYS_SECTION, GREEN_BLOCK_RGB, default))

    def getBlueBlockRGB(self, default = "33,62,85"):
        return self.csvToRGBList(self.robotConfig.getValue(TIDY_TOYS_SECTION, BLUE_BLOCK_RGB, default))

    def getYellowBlockRGB(self, default = "210,175,100"):
        return self.csvToRGBList(self.robotConfig.getValue(TIDY_TOYS_SECTION, YELLOW_BLOCK_RGB, default))
    
    def csvToRGBList(self,csv):
        values = csv.split(",")
        rgbList = list() 
        for value in values:
            rgbList.append(int(value))
        return rgbList

    def getMaxStraightLineVeerTime(self, default="0.2"):
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, MAX_STRAIGHT_LINE_VEER_TIME, default)) 
    
    def getStraightLineVeerMultiplier(self, default="0.1"):    
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, MAX_STRAIGHT_LINE_VEER_MULTIPLIER, default)) 

    def getMoveToDistanceSlowDownDistance(self, default="20.0"):    
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, MOVE_TO_DISTANCE_SLOW_DOWN_DISTANCE, default)) 

    def getMoveToDistanceSlowDownSpeed(self, default="60"):    
        return float(self.robotConfig.getValue(TIDY_TOYS_SECTION, MOVE_TO_DISTANCE_SLOW_DOWN_SPEED, default)) 
    
    def getHorizonCropLeft(self, default="40"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, HORIZON_CROP_LEFT, default))
    
    def getHorizonCropRight(self, default="60"):
        return int(self.robotConfig.getValue(TIDY_TOYS_SECTION, HORIZON_CROP_RIGHT, default)) 

