
CHASE_BALL_MODE_SECTION="ChaseBall"

POLL_TIME_KEY="PollTime"
MOVE_SPEED_KEY="MoveSpeed"
STOP_PERCENTAGE_KEY="StopPercentage"

class ChaseBallModeConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getPollTime(self, default = "50"):
        return self.robotConfig.getValue(CHASE_BALL_MODE_SECTION, POLL_TIME_KEY, default)

    def getMoveSpeed(self, default = "50"): 
        return self.robotConfig.getValue(CHASE_BALL_MODE_SECTION, MOVE_SPEED_KEY, default)

    def getStopPercentage(self, default = "80"): 
        return self.robotConfig.getValue(CHASE_BALL_MODE_SECTION, STOP_PERCENTAGE_KEY, default)

