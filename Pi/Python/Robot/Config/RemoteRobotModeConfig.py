
REMOTE_ROBOT_SECTION="RemoteRobotMode"

MAX_SPEED_RIGHT_KEY="MaxSpeedRight"
MAX_SPEED_LEFT_KEY="MaxSpeedLeft"
MIN_SPEED_RIGHT_KEY="MinSpeedRight"
MIN_SPEED_LEFT_KEY="MinSpeedLeft"
KICK_SPEED_RIGHT_KEY="KickSpeedRight"
KICK_SPEED_LEFT_KEY="KickSpeedLeft"
KICK_DURATION_KEY="KickDurationMs"
LASER_TIMEOUT_KEY="LaserTimeoutMs"
AIM_FORWARD_SPEED_KEY="AimForwardSpeed"
AIM_BACKWARD_SPEED_KEY="AimBackwardSpeed"

class RemoteRobotModeConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getMaxSpeedRight(self, default = "100"):
        return int(self.robotConfig.getValue(REMOTE_ROBOT_SECTION, MAX_SPEED_RIGHT_KEY, default))

    def getMaxSpeedLeft(self, default = "100"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, MAX_SPEED_LEFT_KEY, default)

    def getMinSpeedRight(self, default = "0"):
        return int(self.robotConfig.getValue(REMOTE_ROBOT_SECTION, MIN_SPEED_RIGHT_KEY, default))

    def getMinSpeedLeft(self, default = "0"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, MIN_SPEED_LEFT_KEY, default)

    def getKickSpeedRight(self, default = "100"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, KICK_SPEED_RIGHT_KEY, default)

    def getKickSpeedLeft(self, default = "100"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, KICK_SPEED_LEFT_KEY, default)

    def getKickDurationMs(self, default = "300"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, KICK_DURATION_KEY, default)

    def getLaserDurationMs(self, default = "5000"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, LASER_TIMEOUT_KEY, default)

    def getAimForwardSpeed(self, default = "40"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, AIM_FORWARD_SPEED_KEY, default)

    def getAimBackwardSpeed(self, default = "30"):
        return self.robotConfig.getValue(REMOTE_ROBOT_SECTION, AIM_BACKWARD_SPEED_KEY, default)
