
ROBOT_SECTION="Robot"
ROBOT_MOTOR_CONTROLLER_TYPE_KEY="MotorController"
ROBOT_NINETY_DEGREE_TURN_TIME_KEY="NinetyDegreeTurnTime"
ROBOT_USE_COMPASS_HEADING_KEY="UseCompassHeading"
ROBOT_LASER_GPIO_PIN_KEY="LaserGpioPin"

""" This class represents the basic robot configuration. """
class RobotConfig():

    def __init__(self, configImpl):
        self.configImpl = configImpl

    def getConfigImpl(self):
        return self.configImpl

    def getMotorControllerType(self,default = ""):
        return self.configImpl.getValue(ROBOT_SECTION, ROBOT_MOTOR_CONTROLLER_TYPE_KEY, default)

    def getNinetyDegreeTurnTime(self, default="500"):
        return self.configImpl.getValue(ROBOT_SECTION, ROBOT_NINETY_DEGREE_TURN_TIME_KEY, default)

    def getUseCompassHeading(self, default='False'):
        return self.configImpl.getValue(ROBOT_SECTION, ROBOT_USE_COMPASS_HEADING_KEY, default)

    def getLaserGpioPin(self, default='0'):
        return self.configImpl.getValue(ROBOT_SECTION, ROBOT_LASER_GPIO_PIN_KEY, default)

