
BLOCK_FORKLIFT_SECTION="BlockForkLift"

LIFT_BLOCK_HEIGHT_CM_KEY="LiftBlockHeightCm"
ROTATIONS_PER_CM_KEY="RotationsPerCm"

SERVO_LEFT_GPIO_PIN_KEY="ServoLeftGpioPin"
SERVO_LEFT_OPEN_POSITION_KEY="ServoLeftOpenPos"
SERVO_LEFT_CLOSE_POSITION_KEY="ServoLeftClosePos"

SERVO_RIGHT_GPIO_PIN_KEY="ServoRightGpioPin"
SERVO_RIGHT_OPEN_POSITION_KEY="ServoRightOpenPos"
SERVO_RIGHT_CLOSE_POSITION_KEY="ServoRightClosePos"

STEPPER_STEP_GPIO_PIN_KEY="StepperStepGpioPin"
STEPPER_DIR_GPIO_PIN_KEY="StepperDirGpioPin" 

class BlockForkLiftConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 
        
    def getLiftBlockHeight(self, default = "5.5"):
        return float(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, LIFT_BLOCK_HEIGHT_CM_KEY, default))

    def getRotationsPerCm(self, default = "50"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, ROTATIONS_PER_CM_KEY, default))

    def getServoLeftGpioPin(self, default = "7"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, SERVO_LEFT_GPIO_PIN_KEY, default))

    def getServoLeftOpenPos(self, default = "90"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, SERVO_LEFT_OPEN_POSITION_KEY, default))
    
    def getServoLeftClosePos(self, default = "0"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, SERVO_LEFT_CLOSE_POSITION_KEY, default))

    def getServoRightGpioPin(self, default = "8"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, SERVO_RIGHT_GPIO_PIN_KEY, default))

    def getServoRightOpenPos(self, default = "0"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, SERVO_RIGHT_OPEN_POSITION_KEY, default))
    
    def getServoRightClosePos(self, default = "90"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, SERVO_RIGHT_CLOSE_POSITION_KEY, default))
    
    def getStepperStepGpioPin(self, default = "21"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, STEPPER_STEP_GPIO_PIN_KEY, default))

    def getStepperDirGpioPin(self, default = "22"):
        return int(self.robotConfig.getValue(BLOCK_FORKLIFT_SECTION, STEPPER_DIR_GPIO_PIN_KEY, default))

        
        