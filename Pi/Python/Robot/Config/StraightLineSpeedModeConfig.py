
STRAIGHT_LINE_SPEED_SECTION="StraightLineSpeedMode"

MAX_SPEED_RIGHT_KEY="MaxSpeedRight"
MAX_SPEED_LEFT_KEY="MaxSpeedLeft"
MIN_SPEED_RIGHT_KEY="MinSpeedRight"
MIN_SPEED_LEFT_KEY="MinSpeedLeft"
FUDGE_FACTOR_KEY="FudgeFactor"
POLL_TIME_KEY="PollTime"
AVOID_KINKS_KEY="AvoidKinks"
AVOID_KINKS_DISTANCE_KEY="AvoidKinksDistance"
AVOID_KINKS_EMERGENCY_DISTANCE_KEY="AvoidKinksEmergencyDistance"
AVOID_KINKS_TURN_TIME_KEY="AvoidKinksTurnTimeMs"
AVOID_KINKS_MIN_APPROACH_DISTANCE_KEY="AvoidKinksMinApproachDistance"
AVOID_KINKS_MIN_APPROACH_COUNT_KEY="AvoidKinksMinApproachCount"
AVOID_KINKS_AVERAGE_COUNT_KEY="AvoidKinksMinApproachCount"
STOP_DISTANCE_KEY="StopDistance"
STRAIGHT_LINE_MODE_KEY="StraightLineMode"
EQUIDISTANT_USE_FRONT_SENSORS_KEY="EquidistantUseFrontSensors"

class StraightLineSpeedModeConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getMaxSpeedRight(self, default = "100"):
        return int(self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, MAX_SPEED_RIGHT_KEY, default))

    def getMaxSpeedLeft(self, default = "100"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, MAX_SPEED_LEFT_KEY, default)

    def getMinSpeedRight(self, default = "0"):
        return int(self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, MIN_SPEED_RIGHT_KEY, default))

    def getMinSpeedLeft(self, default = "0"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, MIN_SPEED_LEFT_KEY, default)

    def getFudgeFactor(self, default = "200"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, FUDGE_FACTOR_KEY, default)

    def getPollTime(self, default = "0.1"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, POLL_TIME_KEY, default)

    def getStopDistance(self, default = "30"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, STOP_DISTANCE_KEY, default)

    def getAvoidKinks(self, default = "True"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_KEY, default)

    def getAvoidKinksDistance(self, default = "50"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_DISTANCE_KEY, default)

    def getAvoidKinksEmergencyDistance(self, default = "20"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_EMERGENCY_DISTANCE_KEY, default)

    def getAvoidKinksTurnTime(self, default = "500"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_TURN_TIME_KEY, default)

    def getAvoidKinksMinApproachDistance(self, default = "50"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_MIN_APPROACH_DISTANCE_KEY, default)

    def getAvoidKinksMinApproachCount(self, default = "5"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_MIN_APPROACH_COUNT_KEY, default)

    def getAvoidKinksAverageCount(self, default = "5"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, AVOID_KINKS_AVERAGE_COUNT_KEY, default)

    def getStraightLineMode(self, default = "Parallel"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, STRAIGHT_LINE_MODE_KEY, default)

    def getEquidistantUseFrontSensors(self, default = "False"):
        return self.robotConfig.getValue(STRAIGHT_LINE_SPEED_SECTION, EQUIDISTANT_USE_FRONT_SENSORS_KEY, default)

