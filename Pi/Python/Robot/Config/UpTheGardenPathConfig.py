

UP_THE_GARDEN_PATH_SECTION="UpTheGardenPathMode"

POLL_TIME="PollTime"
MAX_SPEED="MaxSpeed"
MIN_SPEED="MinSpeed"
IS_WHITE_LINE="IsWhiteLine"

MAX_STRAIGHT_LINE_VEER_TIME = "MaxStraightLineVeerTime"
MAX_STRAIGHT_LINE_VEER_MULTIPLIER = "StraightLineVeerMultiplier"


class UpTheGardenPathConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 
        
    def getPollTime(self, default = "20"):
        return int(self.robotConfig.getValue(UP_THE_GARDEN_PATH_SECTION, POLL_TIME, default))

    def getMaxSpeed(self, default="100"):
        return float(self.robotConfig.getValue(UP_THE_GARDEN_PATH_SECTION, MAX_SPEED, default)) 

    def getMinSpeed(self, default="100"):
        return float(self.robotConfig.getValue(UP_THE_GARDEN_PATH_SECTION, MIN_SPEED, default)) 

    def getMaxStraightLineVeerTime(self, default="0.2"):
        return float(self.robotConfig.getValue(UP_THE_GARDEN_PATH_SECTION, MAX_STRAIGHT_LINE_VEER_TIME, default)) 
    
    def getStraightLineVeerMultiplier(self, default="0.1"):    
        return float(self.robotConfig.getValue(UP_THE_GARDEN_PATH_SECTION, MAX_STRAIGHT_LINE_VEER_MULTIPLIER, default))
    
    def isWhiteLine(self, default="False"):
        return self.robotConfig.getValue(UP_THE_GARDEN_PATH_SECTION, IS_WHITE_LINE, default) 

