
CANNON_SECTION="Cannon"

CANNON_PIN = "CannonPin"

class CannonConfig():
    
    def __init__(self, robotConfig):
        self.robotConfig = robotConfig

    def getCannonPin(self, default="21"):
        return int(self.robotConfig.getValue(CANNON_SECTION, CANNON_PIN, default)) 

