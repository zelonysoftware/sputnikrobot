
VOSK_VOICE_RECOGNISER_SECTION="VoskVoiceRecogniser"

MODEL_LOCATION="ModelLocation"
MODEL_SAMPLE_RATE="ModelSampleRate"

class VoskVoiceRecogniserConfig():
    
    def __init__(self, robotConfig):
        self.robotConfig = robotConfig

    def getModelLocation(self, default = ""):
        return self.robotConfig.getValue(VOSK_VOICE_RECOGNISER_SECTION, MODEL_LOCATION, default)

    def getModelSampleRate(self, default = "16000"):
        return int(self.robotConfig.getValue(VOSK_VOICE_RECOGNISER_SECTION, MODEL_SAMPLE_RATE, default))


