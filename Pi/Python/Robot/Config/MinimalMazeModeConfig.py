
MINIMAL_MAZE_SECTION="MinimalMazeMode"

STOP_DISTANCE_KEY="StopDistance"
MAX_SPEED_RIGHT_KEY="MaxSpeedRight"
MAX_SPEED_LEFT_KEY="MaxSpeedLeft"
MIN_SPEED_RIGHT_KEY="MinSpeedRight"
MIN_SPEED_LEFT_KEY="MinSpeedLeft"
MAX_SPIN_SPEED_KEY="MaxSpinSpeed"
SPIN_TIME_KEY="SpinTime"
POLL_TIME_KEY="PollTime"
FUDGE_FACTOR="FudgeFactor"
STRAIGHT_LINE_MODE_KEY="StraightLineMode"


class MinimalMazeModeConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getStopDistance(self, default = "30"):
        return int(self.robotConfig.getValue(MINIMAL_MAZE_SECTION, STOP_DISTANCE_KEY, default))

    def getMaxSpeedRight(self, default = "100"):
        return int(self.robotConfig.getValue(MINIMAL_MAZE_SECTION, MAX_SPEED_RIGHT_KEY, default))

    def getMaxSpeedLeft(self, default = "100"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, MAX_SPEED_LEFT_KEY, default)

    def getMinSpeedRight(self, default = "40"):
        return int(self.robotConfig.getValue(MINIMAL_MAZE_SECTION, MIN_SPEED_RIGHT_KEY, default))

    def getMinSpeedLeft(self, default = "40"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, MIN_SPEED_LEFT_KEY, default)

    def getMaxSpinSpeed(self, default = "60"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, MAX_SPIN_SPEED_KEY, default)

    def getSpinTime(self, default = "500"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, SPIN_TIME_KEY, default)

    def getFudgeFactor(self, default = "200"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, FUDGE_FACTOR, default)

    def getPollTime(self, default = "50"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, POLL_TIME_KEY, default)

    def getStraightLineMode(self, default = "Parallel"):
        return self.robotConfig.getValue(MINIMAL_MAZE_SECTION, STRAIGHT_LINE_MODE_KEY, default)

