
from configparser import ConfigParser
from Robot.Config.ConfigBase import ConfigBase

class ConfigUsingIniFile(ConfigBase):

    def __init__(self, configFilePath):

        self.configParser = ConfigParser()
        self.configParser.read(configFilePath)


    def getValue(self, section, key, default):

        if ( self.configParser.has_section(section) ):
            return self.configParser[section].get(key,default).strip()

        return default.strip() 
