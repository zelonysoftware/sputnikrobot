
HUBBLE_MODE_SECTION="Hubble"

POLL_TIME_KEY="PollTime"
MOVE_SPEED_KEY="MoveSpeed"
SPIN_SPEED_KEY="SpinSpeed"
MIN_FRONT_DISTANCE_KEY="MinFrontDistance"
USE_LAMP_KEY="UseLamp"
MIN_WIDTH_KEY="MinWidth"
PRE_SCAN_COLOURS_KEY="PreScanColours"

class HubbleModeConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getPollTime(self, default = "50"):
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, POLL_TIME_KEY, default)

    def getMoveSpeed(self, default = "50"): 
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, MOVE_SPEED_KEY, default)

    def getSpinSpeed(self, default = "50"):
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, SPIN_SPEED_KEY, default)

    def getMinFrontDistance(self, default = "10"):
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, MIN_FRONT_DISTANCE_KEY, default)

    def getUseLamp(self, default = "False"):
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, USE_LAMP_KEY, default)

    def getMinWidth(self, default = "20"):
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, MIN_WIDTH_KEY, default)

    def getPreScanColours(self, default = "True"):
        return self.robotConfig.getValue(HUBBLE_MODE_SECTION, PRE_SCAN_COLOURS_KEY, default)

