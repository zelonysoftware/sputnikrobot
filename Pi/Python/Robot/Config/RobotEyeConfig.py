
ROBOT_EYE_SECTION="RobotEyes"

NEOPIXEL_SERVER_HOST="NeoPixelServerHost"
NEOPIXEL_SERVER_PORT="NeoPixelServerPort"
LEFT_EYE_SERVO_PIN="LeftEyeServoPin"
RIGHT_EYE_SERVO_PIN="RightEyeServoPin"
LEFT_EYE_CENTRE_ANGLE="LeftEyeCentreAngle"
RIGHT_EYE_CENTRE_ANGLE="RightEyeCentreAngle"
EYE_MAX_ANGLE="EyeMaxAngle" # max servo angle change from centre.
EYE_MOVE_DELAY="EyeMoveDelay" 
EYE_MOVE_STEP_ANGLE="EyeMoveStepAngle" 

class RobotEyeConfig():
    
    def __init__(self, robotConfig):
        self.robotConfig = robotConfig

    def getNeoPixelServerHost(self, default = "127.0.0.1"):
        return self.robotConfig.getValue(ROBOT_EYE_SECTION, NEOPIXEL_SERVER_HOST, default)

    def getNeoPixelServerPort(self, default = "13700"):
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, NEOPIXEL_SERVER_PORT, default))

    def getLeftEyeServoPin(self, default = "12"):
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, LEFT_EYE_SERVO_PIN, default))

    def getRightEyeServoPin(self, default = "13"):
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, RIGHT_EYE_SERVO_PIN, default))

    def getRightEyeCentreAngle(self, default = "0"):
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, RIGHT_EYE_CENTRE_ANGLE, default))

    def getLeftEyeCentreAngle(self, default = "0"):
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, LEFT_EYE_CENTRE_ANGLE, default))

    def getEyeMaxAngle(self, default = "20"):
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, EYE_MAX_ANGLE, default))
        
    def getEyeMoveDelay(self, default = "40"): 
        return int(self.robotConfig.getValue(ROBOT_EYE_SECTION, EYE_MOVE_DELAY, default))

    def getEyeMoveStepAngle(self, default = "1.0"): 
        return float(self.robotConfig.getValue(ROBOT_EYE_SECTION, EYE_MOVE_STEP_ANGLE, default))
    

