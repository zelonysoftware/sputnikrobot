
SERIAL_SECTION='Serial'
SERIAL_DEV_PATH_KEY='DevicePath'
SERIAL_BAUD_RATE_KEY='BaudRate'
SERIAL_TIMEOUT_SECONDS_KEY='TimeoutSeconds'

""" This class represents the basic robot configuration. """
class SerialCommsConfig():

    def __init__(self, configImpl):
        self.configImpl = configImpl

    def getConfigImpl(self):
        return self.configImpl

    def getSerialDevicePath(self,default = '/dev/serial/by-id'):
        return self.configImpl.getValue(SERIAL_SECTION, SERIAL_DEV_PATH_KEY, default)

    def getBaudRate(self,default=19200):
        return int(self.configImpl.getValue(SERIAL_SECTION, SERIAL_BAUD_RATE_KEY, default))

    def getTimeoutSeconds(self,default=1.0):
        return float(self.configImpl.getValue(SERIAL_SECTION, SERIAL_TIMEOUT_SECONDS_KEY, default))
