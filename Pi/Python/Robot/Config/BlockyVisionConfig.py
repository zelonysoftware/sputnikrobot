
BLOCKYVISION_SECTION="BlockyVision"
BLOCKYVISION_CAMERA_ROTATION="CameraRotation"
BLOCKYVISION_ISO="Iso"
BLOCKYVISION_FRAMERATE="FrameRate"
BLOCKYVISION_IMAGE_WIDTH="ImageWidth"
BLOCKYVISION_IMAGE_HEIGHT="ImageHeight" 
BLOCKYVISION_THRESHOLD="Threshold" 
BLOCKYVISION_AWB_GAINS="AwbGains" 
BLOCKYVISION_SHUTTER_SPEED="ShutterSpeed" 
BLOCKYVISION_SERVER_PORT="ServerPort"
BLOCKYVISION_TURBO_LIB_PATH="TurboLibPath"
BLOCKYVISION_POLL_TIME="PollTime" 
BLOCKYVISION_SAVE_FOLDER="SaveFolder"


""" This class represents the BlockyVision camera configuration. """
class BlockyVisionConfig():

    def __init__(self, robotConfig):
        self.robotConfig = robotConfig 

    def getCameraRotation(self, default = "0"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_CAMERA_ROTATION, default))

    def getIso(self, default = "400"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_ISO, default))

    def getFrameRate(self, default = "20"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_FRAMERATE, default))

    def getImageWidth(self, default = "128"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_IMAGE_WIDTH, default))

    def getImageHeight(self, default = "96"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_IMAGE_HEIGHT, default))

    def getThreshold(self, default = "20"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_THRESHOLD, default))

    def getAwbGains(self, default = "1.3,2.7"):
        #Awb gains are specified as a (red,blue) tuple.
        awbGainString = self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_AWB_GAINS, default)
        values = awbGainString.split(",")
        red = float(values[0])
        blue = float(values[1])
        return (red,blue)

    def getShutterSpeed(self, default = "16985"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_SHUTTER_SPEED, default))

    def getServerPort(self, default = "20060"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_SERVER_PORT, default))

    # TODO - consider using the frames per second to work out the delay? 
    def getPollTime(self,default = "20"):
        return int(self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_POLL_TIME, default))

    def getTurboLibPath(self,default = ""):
        return self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_TURBO_LIB_PATH, default) 

    def getSaveFolder(self, default="."):
        return self.robotConfig.getValue(BLOCKYVISION_SECTION, BLOCKYVISION_SAVE_FOLDER, default) 
