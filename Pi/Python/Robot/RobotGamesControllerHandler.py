
from GamesController.GamesControllerCB import GamesControllerCB
from Robot.RobotEvent.RobotEvent import JoystickChangeEvent
from Robot.RobotEvent.RobotEvent import TriggerChangeEvent
from Robot.RobotEvent.RobotEvent import ButtonChangeEvent
from Robot.RobotEvent.RobotEvent import GamesControllerConnectionChangeEvent

""" THis class converts games controller callbacks into RobotEvent objects.  """
class RobotGamesControllerHandler(GamesControllerCB):

    def __init__(self, theRobot):
        self.robot = theRobot

    def joystickLeftMoved(self, xpos, ypos):
        self.robot.handleEvent(JoystickChangeEvent(JoystickChangeEvent.JOYSTICK_LEFT, xpos, ypos))
   
    def joystickRightMoved(self, xpos, ypos):
        self.robot.handleEvent(JoystickChangeEvent(JoystickChangeEvent.JOYSTICK_RIGHT, xpos, ypos))

    def triggerL2AnalogChanged(self, pos):
        self.robot.handleEvent(TriggerChangeEvent(TriggerChangeEvent.TRIGGER_L2, pos))

    def triggerR2AnalogChanged(self, pos):
        self.robot.handleEvent(TriggerChangeEvent(TriggerChangeEvent.TRIGGER_R2, pos))

    def buttonL2Changed(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_L2, value))

    def buttonR2Changed(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_R2, value))

    def buttonL1Changed(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_L1, value))

    def buttonR1Changed(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_R1, value))
        
    def buttonXChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_X, value))
        
    def buttonTriangleChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_TRIANGLE, value))
        
    def buttonSquareChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_SQUARE, value))

    def buttonCircleChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_CIRCLE, value))

    def buttonUpChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_KEYPAD_UP, value))

    def buttonLeftChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_KEYPAD_LEFT, value))

    def buttonRightChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_KEYPAD_RIGHT, value))

    def buttonDownChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_KEYPAD_DOWN, value))

    def buttonL3Changed(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_L3, value))

    def buttonR3Changed(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_R3, value))

    def buttonSelectChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_SELECT, value))

    def buttonStartChanged(self, value):
        self.robot.handleEvent(ButtonChangeEvent(ButtonChangeEvent.BUTTON_START, value))

    def controllerConnected(self):
        self.robot.handleEvent(GamesControllerConnectionChangeEvent(True))

    def controllerDisconnected(self):
        self.robot.handleEvent(GamesControllerConnectionChangeEvent(False))
        
