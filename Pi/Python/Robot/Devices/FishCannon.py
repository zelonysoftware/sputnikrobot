
from Robot.Config.CannonConfig import CannonConfig

import pigpio
import logging

logger = logging.getLogger(__name__)

class FishCannon():
    
    def __init__(self, config):
        self.cannonConfig = CannonConfig(config)
        self.magnetPin = self.cannonConfig.getCannonPin()
        logger.info("Fish Cannon - magnet pin = %d", self.magnetPin)
        self.pi = pigpio.pi()
        self.pi.set_mode(self.magnetPin, pigpio.OUTPUT)
        self.pi.write(self.magnetPin, 0)
        self.isMagnetOn = False 

    def magnetOn(self):
        if ( not self.isMagnetOn ):
            logger.info("Magnet On %d = 1", self.magnetPin);
            self.pi.write(self.magnetPin, 1)
            self.isMagnetOn = True 
        
    def magnetOff(self):
        if ( self.isMagnetOn ): 
            logger.info("Magnet Off");
            self.pi.write(self.magnetPin, 0)
            self.isMagnetOn = False 

    def fire(self):
        self.magnetOff()
