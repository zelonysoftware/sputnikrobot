from Robot.Config.BlockForkLiftConfig import BlockForkLiftConfig
from Robot.Devices.RedBoardForkLift import RedBoardForkLift 
import logging 
import time 

logger = logging.getLogger(__name__)

class BlockPosition():
    
    ''' fork lift Positions ''' 
    TOP = 2 
    MIDDLE = 1 
    BOTTOM = 0 

class BlockForkLift():

    def __init__(self, robotConfig):
        
        blockForkLiftConfig = BlockForkLiftConfig(robotConfig)
        
        # For now just use a Redboard forklift device. We'll need a way to inject this for other devices. 
        self.forkLift = RedBoardForkLift(blockForkLiftConfig.getServoLeftGpioPin(), 
                                         blockForkLiftConfig.getServoLeftOpenPos(),
                                         blockForkLiftConfig.getServoLeftClosePos(), 
                                         blockForkLiftConfig.getServoRightGpioPin(), 
                                         blockForkLiftConfig.getServoRightOpenPos(),
                                         blockForkLiftConfig.getServoRightClosePos(),
                                         blockForkLiftConfig.getStepperStepGpioPin(), 
                                         blockForkLiftConfig.getStepperDirGpioPin(),
                                         blockForkLiftConfig.getRotationsPerCm())
        
        self.currentPosition = BlockPosition.TOP ;  

        self.liftBlockHeightCm = blockForkLiftConfig.getLiftBlockHeight()


    def moveToPosition(self, newPosition):
        change = self.currentPosition - newPosition 
        if ( change != 0 ):
            if ( change > 0 ):
                self.forkLift.moveUpCm(abs(change) * self.liftBlockHeightCm)
            else:
                self.forkLift.moveDownCm(abs(change) * self.liftBlockHeightCm)
        self.currentPosition = newPosition

    def moveUp(self):
        logger.info("MOVE UP") 
        logger.info("Current Position = " + str(self.currentPosition))
        if ( self.currentPosition < BlockPosition.TOP ):
            self.moveToPosition(self.currentPosition+1) 
            
    def moveDown(self):
        logger.info("MOVE DOWN") 
        logger.info("Current Position = " + str(self.currentPosition))
        if ( self.currentPosition > BlockPosition.BOTTOM ):
            self.moveToPosition(self.currentPosition-1) 
            
    def openGrip(self):
        self.forkLift.openGrip()
        
    def closeGrip(self):
        self.forkLift.closeGrip(duration=1.0, numSteps=20)
        
    def stop(self):
        self.moveToPosition(BlockPosition.TOP)

        
