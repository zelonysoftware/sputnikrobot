'''
Created on 29 Nov 2020

@author: zelony
'''
'''
Created on 18 Oct 2020

@author: zelony
'''
import pigpio
import time

INITIAL_DELAY = 50000 # microseconds 
STEP_DELAY    = 500  # microseconds 
SAFETY_MARGIN = 500000 # microseconds

class PigpioA4988StepperMotor():
    
    def __init__(self, stepPin, dirPin):
        self.stepPin = stepPin 
        self.dirPin = dirPin 

        self.pi = pigpio.pi()

    def moveClockwise(self, steps):
        self.move(True, steps)
        
    def moveAntiClockwise(self, steps):
        self.move(False, steps) 

    def move(self, isClockwise, steps):
        try: 
            
            self.pi.set_mode(self.dirPin, pigpio.OUTPUT)
            if ( isClockwise ):
                print("Setting %d pin to 1" % (self.dirPin))
                self.pi.write(self.dirPin, 1)
            else:
                print("Setting %d pin to 0" % (self.dirPin))
                self.pi.write(self.dirPin, 0)

            self.sendWave(steps)
            
            # Don't return until we think the stepper movement has competed. 
            totalTime = (INITIAL_DELAY + (steps * STEP_DELAY * 2) + SAFETY_MARGIN)
            totalTimeSecs = totalTime / 1000000  
            time.sleep(totalTimeSecs) 

        finally:
            self.pi.write(self.stepPin, 0)
            #self.pi.write(self.dirPin, 0)

    def sendWave(self, steps):
        
        pulses = [] 
        
        # initial delay 
        pulses.append(pigpio.pulse(0,1<<self.stepPin,INITIAL_DELAY))
        
        for i in range(steps):
            # on
            pulses.append(pigpio.pulse(1<<self.stepPin, 0, STEP_DELAY))
            # off 
            pulses.append(pigpio.pulse(0, 1<<self.stepPin, STEP_DELAY))

        self.pi.wave_clear()
        self.pi.wave_add_generic(pulses) 
        wave = self.pi.wave_create()

        self.pi.wave_send_once(wave) 

        self.pi.wave_clear() 

def main():
    """main function loop"""
    
    # ====================== section A ===================
    print("TEST SECTION A")

    stepper = PigpioA4988StepperMotor(21,22)
    
    while True:
        input("TEST: Press <Enter> to continue  Full 360 turn Test1")
        stepper.move(False, 200)
        time.sleep(1)
        input("TEST: Press <Enter> to continue  full 360 clockwise Test2")
        stepper.move(True, 200)
        time.sleep(1)
    
# ===================MAIN===============================

if __name__ == '__main__':
   
    print("TEST START")
    main()
    print("TEST END")
    exit()
    
