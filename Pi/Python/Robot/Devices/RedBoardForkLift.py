
import time
from Robot.Devices.RedBoardServo import RedBoardServo 
from Robot.Devices.PigpioA4988StepperMotor import PigpioA4988StepperMotor 

class RedBoardForkLift():
    
    def __init__(self, 
                 servoLeftPin, servoLeftOpenPos, servoLeftClosePos,
                 servoRightPin, servoRightOpenPos, servoRightClosePos, 
                 stepperStepPin, stepperDirPin, stepperStepsPerCm):
        
        self.servoLeft = RedBoardServo(servoLeftPin) 
        self.servoLeftOpenPos = servoLeftOpenPos 
        self.servoLeftClosePos = servoLeftClosePos 
        print("ServoLeftPin = " + str(servoLeftPin))

        self.servoRight = RedBoardServo(servoRightPin) 
        self.servoRightOpenPos = servoRightOpenPos 
        self.servoRightClosePos = servoRightClosePos 
        print("ServoRightPin = " + str(servoRightPin))
        
        self.stepper = PigpioA4988StepperMotor(stepperStepPin, stepperDirPin)
        print("Stepper Step Pin = " + str(stepperStepPin))
        print("Stepper Dir Pin = " + str(stepperDirPin))
        self.stepperStepsPerCm = stepperStepsPerCm 
        
        
    def cmToSteps(self, cm):
        return int(cm * self.stepperStepsPerCm)
        
    def moveDownCm(self, distanceInCm):
        steps = self.cmToSteps(distanceInCm) 
        print("STEPPER MOVE " + str(steps) + " STEPS ANTI")
        self.stepper.moveClockwise(steps)
        
    def moveUpCm(self, distanceInCm):
        steps = self.cmToSteps(distanceInCm) 
        print("STEPPER MOVE " + str(steps) + " STEPS CLOCK")
        self.stepper.moveAntiClockwise(steps)
        
    def openGrip(self):
        self.servoLeft.move(self.servoLeftOpenPos)
        self.servoRight.move(self.servoRightOpenPos)
        
    def closeGrip(self,duration=1.0,numSteps=0):
        if ( numSteps == 0 ):
            self.servoLeft.move(self.servoLeftClosePos)
            self.servoRight.move(self.servoRightClosePos)
        else:
            # move slowly. 
            stepTime = duration / numSteps 
            leftStepAngle = float(self.servoLeftClosePos - self.servoLeftOpenPos) / numSteps
            rightStepAngle = float(self.servoRightClosePos - self.servoRightOpenPos) / numSteps
            self.openGrip()
            for  i in range(1, numSteps+1):
                time.sleep(stepTime)
                self.servoLeft.move(self.servoLeftOpenPos+(i * leftStepAngle))
                self.servoRight.move(self.servoRightOpenPos+(i * rightStepAngle))

        
