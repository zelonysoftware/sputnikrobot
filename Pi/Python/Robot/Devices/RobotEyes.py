from Robot.Config.RobotEyeConfig import RobotEyeConfig
from Robot.Devices.RedBoardServo import RedBoardServo
from Utils.NeoPixelClient import NeoPixelClient
from threading import Thread
import time ;

class EyeControlThread(Thread): 
    
    def __init__(self, robotEyes):
        
        self.theEyes = robotEyes ;
        self.isRunning = False ;  
        
        self.currentLeftAngle = 0 ; 
        self.currentRightAngle = 0 ; 
        
        self.targetLeftAngle = 0 ; 
        self.targetRightAngle = 0 ;
        
        self.delayTime = self.theEyes.eyeMoveDelay / 1000.0  

        super().__init__()
    
    def run(self):
        
        while self.isRunning:
            if ( self.targetLeftAngle > self.currentLeftAngle ): 
                self.currentLeftAngle = self.incrementServo(    self.theEyes.leftServo, 
                                                                self.targetLeftAngle, 
                                                                self.currentLeftAngle   )
            elif ( self.targetLeftAngle < self.currentLeftAngle ): 
                self.currentLeftAngle = self.decrementServo(    self.theEyes.leftServo, 
                                                                self.targetLeftAngle,
                                                                self.currentLeftAngle   )
                
            if ( self.targetRightAngle > self.currentRightAngle ): 
                self.currentRightAngle = self.incrementServo(    self.theEyes.rightServo, 
                                                                self.targetRightAngle, 
                                                                self.currentRightAngle   )
            elif ( self.targetRightAngle < self.currentRightAngle ): 
                self.currentRightAngle = self.decrementServo(    self.theEyes.rightServo, 
                                                                self.targetRightAngle,
                                                                self.currentRightAngle   )
            time.sleep(self.delayTime); 
    
    def start(self):
        self.isRunning = True ; 
        super().start(); 
        
    def stop(self):
        self.isRunning = False ; 
        self.join(1.0); 
        
    def setLeftAngle(self, leftAngle):
        self.targetLeftAngle = leftAngle

    def setRightAngle(self, rightAngle):
        self.targetRightAngle = rightAngle 

    def incrementServo( self, servo, targetAngle, currentAngle ):
        newAngle = min(currentAngle + self.theEyes.eyeMoveStepSize, targetAngle) 
        servo.move(newAngle); 
        return newAngle ;

    def decrementServo( self, servo, targetAngle, currentAngle ):
        newAngle = max(currentAngle - self.theEyes.eyeMoveStepSize, targetAngle) 
        servo.move(newAngle); 
        return newAngle ;

 
class RobotEyes:

    def __init__(self, robotConfig):
        
        eyeConfig = RobotEyeConfig(robotConfig) 
                 
        leftEyeServoPin = eyeConfig.getLeftEyeServoPin()
        rightEyeServoPin = eyeConfig.getRightEyeServoPin()
        self.leftEyeCentreAngle = eyeConfig.getLeftEyeCentreAngle()
        self.rightEyeCentreAngle = eyeConfig.getRightEyeCentreAngle()

        self.leftServo = RedBoardServo(leftEyeServoPin) 
        self.rightServo = RedBoardServo(rightEyeServoPin)
        
        self.threadStarted = False  

        self.eyeMaxAngle = eyeConfig.getEyeMaxAngle()
        
        self.eyeMoveDelay = eyeConfig.getEyeMoveDelay() 
        self.eyeMoveStepSize = eyeConfig.getEyeMoveStepAngle()

        neoPixelServerHost = eyeConfig.getNeoPixelServerHost() 
        neoPixelServerPort = eyeConfig.getNeoPixelServerPort() 

        self.neopixels = NeoPixelClient(neoPixelServerHost, neoPixelServerPort); 
        
        # Start with the eyes centred.
        self.leftServo.move(self.leftEyeCentreAngle) 
        self.rightServo.move(self.rightEyeCentreAngle) 

        self.controlThread = None 
        
    '''
    Start the eye control thread. This will slow down the eye movement to make it smoother. 
    '''
    def start(self):
        if ( self.controlThread is None ):
            self.controlThread = EyeControlThread(self) 
            self.controlThread.start(); 
        
    def stop(self):
        if ( self.controlThread is not None ):
            self.controlThread.stop();
            self.controlThread = None  
        self.setColour((0,0,0))
                 
    def moveBoth(self, targetAngle):
        self.moveRight(targetAngle)
        self.moveLeft(targetAngle)
        
    def moveRight(self, targetPosition):
        if ( self.controlThread is not None ):
            self.controlThread.setRightAngle(self.convertPositionToAngle(targetPosition, self.rightEyeCentreAngle))
        
    def moveLeft(self, targetPosition):
        if ( self.controlThread is not None ):
            self.controlThread.setLeftAngle(self.convertPositionToAngle(targetPosition, self.leftEyeCentreAngle))

    def setColour(self, rgb):
        red, green, blue = rgb 
        self.neopixels.fill(red,green,blue)
    '''
    Eye position will come in the range -100 -> 100. We need to convert this to our servo settings using the centre and max angles 
    '''
    def convertPositionToAngle(self, positionIn, centreAngle):
        
        # Make sure the input position is in range. 
        positionIn = min(positionIn, 100.0); 
        positionIn = max(positionIn, -100.0); 
        
        angle = (((self.eyeMaxAngle * 2.0) / 200.0) * positionIn) + centreAngle ;
        
        return angle ; 
        
