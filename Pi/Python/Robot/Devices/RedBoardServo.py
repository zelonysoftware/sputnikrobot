
import ThirdParty.RedBoard.redboard as red 

class RedBoardServo():
    
    # Map pin number to redboard functions.
    servoPinToRedboardFunct = { 5:  (red.servo5, red.servo5_off, red.servo5_P),
                                6:  (red.servo6, red.servo6_off, red.servo6_P),
                                7:  (red.servo7, red.servo7_off, red.servo7_P),
                                8:  (red.servo8, red.servo8_off, red.servo8_P),
                                9:  (red.servo9, red.servo9_off, red.servo9_P),
                                10: (red.servo10, red.servo10_off, red.servo10_P),
                                11: (red.servo11, red.servo11_off, red.servo11_P),
                                13: (red.servo13, red.servo13_off, red.servo13_P),
                                20: (red.servo20, red.servo20_off, red.servo20_P),
                                21: (red.servo21, red.servo21_off, red.servo21_P),
                                22: (red.servo22, red.servo22_off, red.servo22_P),
                                27: (red.servo27, red.servo27_off, red.servo27_P) }
    
    def __init__(self, servoPin):
        
        servoFunctions = self.servoPinToRedboardFunct.get(servoPin) 
        self.servo = servoFunctions[0] 
        self.servoOff = servoFunctions[1]
        self.servoPwm = servoFunctions[2] 
        
        
    def move(self, angle):
        
        self.servo(angle) 
        
    def off(self):
        
        self.servoOff()
        
        
