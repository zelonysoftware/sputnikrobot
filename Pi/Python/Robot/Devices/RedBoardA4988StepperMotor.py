'''
Created on 18 Oct 2020

@author: zelony
'''
import time
import ThirdParty.RedBoard.redboard as red 

INITIAL_DELAY = 0.05
STEP_DELAY    = 0.002 


class RedBoardA4988StepperMotor():
    
    def __init__(self, stepPin, dirPin):
        self.stepPin = stepPin 
        self.dirPin = dirPin 
    
    def moveClockwise(self, steps):
        self.move(True, steps)
        
    def moveAntiClockwise(self, steps):
        self.move(False, steps) 

    def move(self, isClockwise, steps):
        try: 
            red.output_pin(self.stepPin)
            red.output_pin(self.dirPin)
            
            red.setPin(self.dirPin, isClockwise)
            
            time.sleep(INITIAL_DELAY)
    
            for i in range(steps):
                red.setPin(self.stepPin, 1)
                time.sleep(STEP_DELAY)
                red.setPin(self.stepPin, 0)
                time.sleep(STEP_DELAY)

        finally:
            red.setPin(self.stepPin,0)
            red.setPin(self.dirPin,0)


def main():
    """main function loop"""
    
    # ====================== section A ===================
    print("TEST SECTION A")

    stepper = RedBoardA4988StepperMotor(21,22)
    
    while True:
        input("TEST: Press <Enter> to continue  Full 360 turn Test1")
        stepper.move(False, 200)
        time.sleep(1)
        input("TEST: Press <Enter> to continue  full 180 clockwise Test2")
        stepper.move(True, 200)
        time.sleep(1)
    
# ===================MAIN===============================

if __name__ == '__main__':
   
    print("TEST START")
    main()
    print("TEST END")
    exit()
    
