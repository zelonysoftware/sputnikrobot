
import pigpio 
import threading
import time
'''
SonarContext provides locked data about the last sonar trigger. 
'''
class SonarContext:

    def __init__(self):
        self.condition = threading.Condition()
        self.lastDistance = 0 
        self.isSet = False 


class SingleSonarSensor:

    # Static variables used by the static callback method 
    tickStartTimes = dict()     
    sonarContextByEchoGpio = dict() 

    def __init__(self, triggerGpio, echoGpio):

        self.triggerGpio = triggerGpio
        self.echoGpio = echoGpio 
        self.sonarContext = None 
        self.pi = pigpio.pi()
        self.pi.set_mode( triggerGpio, pigpio.OUTPUT )
        self.pi.set_mode( echoGpio, pigpio.INPUT )

        if ( echoGpio in SingleSonarSensor.sonarContextByEchoGpio ):
            # Another sensor is using this - but I guess we can use the same context. 
            self.sonarContext = SingleSonarSensor.sonarContextByEchoGpio[echoGpio] 
        else:
            self.sonarContext = SonarContext() 
            SingleSonarSensor.sonarContextByEchoGpio[echoGpio] = self.sonarContext 
            self.pi.callback(echoGpio, pigpio.EITHER_EDGE, SingleSonarSensor.echoGpioCallback) 

    def getDistance(self):

        self.sonarContext.condition.acquire() 
        self.sonarContext.isSet = False 

        self.pi.gpio_trigger( self.triggerGpio, 20, 1 )

        while not self.sonarContext.isSet:
            self.sonarContext.condition.wait() 

        distance = self.sonarContext.lastDistance

        self.sonarContext.condition.release()

        return distance 

    @staticmethod
    def echoGpioCallback(gpio, level, tick):
        if ( level == 1 ):
            # just record the tick when the echo pin goes high 
            SingleSonarSensor.tickStartTimes[gpio] = tick 

        if ( level == 0 ):
            # Calculate the distance in cm
            if (    (gpio in SingleSonarSensor.tickStartTimes) 
                and (gpio in SingleSonarSensor.sonarContextByEchoGpio) ):
                diff = pigpio.tickDiff(SingleSonarSensor.tickStartTimes[gpio], tick)
                """ Convert microseconds to cm. Sound travels at 343 m/s so need to multiply
                    time in microseconds by 0.0343 to get cm - and then divide by 2 since we're 
                    measuring the time there and back """ 
                cm = (diff * 0.0343) / 2 
                context = SingleSonarSensor.sonarContextByEchoGpio[gpio] 
                context.condition.acquire() 
                context.isSet = True 
                context.lastDistance = cm 
                context.condition.notifyAll() 
                context.condition.release()







