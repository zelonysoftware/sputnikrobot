
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Config.ChaseBallModeConfig import ChaseBallModeConfig
import time
import logging
from enum import Enum

logger = logging.getLogger(__name__)


"""
    Hubble Telescope Nebula Challenge 
    
    Order of colours to find = red, blue, yellow, green 

    On Start 
        Spin round noting order of colours 

        Find first colour 
        Go to it until keypress or sensor says stop. 
        Go back to centre. 
        repeat until all colours have been found. 

"""
class ChaseBallModeState(Enum):
    STOPPED = 0
    CHASING = 1  
    
class ChaseBallMode(AutomatedRobotMode):

    # Colour signatures
    BALL_SIGNATURE = "ball" 
    
    def __init__(self, robot):
        self.robot = robot
        self.config = ChaseBallModeConfig(robot.getConfig().getConfigImpl())        
        self.pollTime = float(self.config.getPollTime("200")) / 1000.0
        self.moveSpeed = int(self.config.getMoveSpeed("50")) 
        self.stopPercentage = int(self.config.getStopPercentage("80"))
        logger.info("Started Chase Ball Mode")
        self.watingForStart = False # temp - don't wait for start. 
        
        self.robot.getEnvironment().setCameraSearchObjects(('ball',121,40,23))        

        self.currentState = ChaseBallModeState.STOPPED

        print("ChaseBallMode started")
        
        super().__init__(RobotModes.MODE_CHASE_BALL)

    def handleEvent(self, robotEvent):

        # The super class handles the start button being pressed 
        eventHandled = super().handleEvent(robotEvent)
        logger.info("Chase Ball - event handled = " + str(eventHandled))
        return eventHandled 
            
    """
     The mainloop has to be responsive to actions, whilst also driving the robot. 
     We therefore use a state machine so we can keep an eye on the 'waiingForStart' attribute. 
    """
    def mainLoop(self):

        veerTime = 0 

        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        # Temporary
        self.waitingForStart = False ;

        if ( not self.waitingForStart ):
            
            print("Chase ball mode")
            # Is there a ball anywhere? 
            cameraObjects = self.robot.getEnvironment().getCameraObjects();

            print("got camera objects"); 
            
            ballFound = False 
            
            for cameraObject in cameraObjects:
                if ( cameraObject.type == self.BALL_SIGNATURE ):
                    ballFound = True 
                    if ( cameraObject.width >= self.stopPercentage ):
                        self.robot.stopWheels()
                        self.currentState = ChaseBallModeState.STOPPED
                        
                    else:
                        # The chase is on
                        self.currentState = ChaseBallModeState.CHASING 
                        
                        if ( cameraObject.x < -5 ):
                            self.veerLeft(veerTime)
                        elif ( cameraObject.x > 5 ):
                            self.veerRight(veerTime) 
                        else:
                            self.moveStraight()
                            
            if ( not ballFound ):
                
                self.robot.stopWheels()
                self.currentState = ChaseBallModeState.STOPPED

        else:
            self.robot.stopWheels()
            self.currentState = ChaseBallModeState.STOPPED

        time.sleep(max(0,self.pollTime - veerTime))

    def moveStraight(self):
        logger.debug('Move Straight')
        self.robot.moveBothWheels(self.moveSpeed)
    
    def veerRight(self, veerTime):
        logger.debug("Veer Right: %f", veerTime)
        self.robot.moveLeftWheel(self.moveSpeed)
        self.robot.moveRightWheel(max(self.moveSpeed - 20, 20))
        time.sleep(veerTime)
        return veerTime
        
    def veerLeft(self, veerTime):
        logger.debug("Veer Left: %f", veerTime)
        self.robot.moveRightWheel(self.moveSpeed)
        self.robot.moveLeftWheel(max(self.moveSpeed - 20, 20))
        time.sleep(veerTime)
        return veerTime


