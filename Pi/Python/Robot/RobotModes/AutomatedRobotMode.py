
from Robot.RobotModes.RobotMode import RobotMode
import Robot.RobotEvent.RobotEvent as RobotEvent

class AutomatedRobotMode(RobotMode):

    def __init__(self, modeName):
        self.waitingForStart = True
        super().__init__(modeName)

    def handleEvent(self, event):

        handledEvent = False 

        eventType = event.getEventType()

        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE):
            if ( event.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_START ):
                if ( event.getButtonValue() == 0 ):
                    self.waitingForStart = not self.waitingForStart
                handledEvent = True 

        return handledEvent

