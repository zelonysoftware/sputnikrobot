
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.RemoteRobotMode import RemoteRobotMode
import Robot.RobotEvent.RobotEvent as RobotEvent
import logging

logger = logging.getLogger(__name__)

"""
SpaceInvadersMode is a special type of remote mode - that handles the up and down arrows
to adjust the 'kick' speed' 
"""
class SpaceInvadersMode(RemoteRobotMode):

    def __init__(self, robot):
        super().__init__(robot)
        self.modeName = RobotModes.MODE_SPACE_INVADERS

    def handleEvent(self, robotEvent):

        eventHandled = False 
        eventType = robotEvent.getEventType()
        
        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE ):
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_UP ):
                if ( robotEvent.getButtonValue() == 0 ):
                    super().adjustKickTime(50)
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_DOWN ):
                if ( robotEvent.getButtonValue() == 0 ):
                    super().adjustKickTime(0-50)
                eventHandled = True

        eventHandled = eventHandled or super().handleEvent(robotEvent)

        return eventHandled 


