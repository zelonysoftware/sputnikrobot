
from threading import Thread
import time 
import logging
import traceback

import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.RemoteRobotMode import RemoteRobotMode
from Robot.RobotModes.SpaceInvadersMode import SpaceInvadersMode
from Robot.RobotModes.ChaseBallMode import ChaseBallMode
from Robot.RobotModes.TidyToysMode import TidyToysMode
from Robot.RobotModes.FeedTheFishMode import FeedTheFishMode
from Robot.RobotModes.SpeechControlMode import SpeechControlMode
from Robot.RobotModes.UpTheGardenPathMode import UpTheGardenPathMode
from Robot.RobotModes.StraightLineSpeedMode import StraightLineSpeedMode
from Robot.RobotModes.MinimalMazeMode import MinimalMazeMode
from Robot.RobotModes.HubbleMode import HubbleMode
from Robot.RobotModes.NannyMcPiRemoteMode import NannyMcPiRemoteMode
from Robot.RobotModes.NannyMcPiDemoMode import NannyMcPiDemoMode

logger = logging.getLogger(__name__)

class RobotModeList():

    def __init__(self, modeArray):
        self.modeList = modeArray
        self.nextModeIndex = 0  
        
    """ create and return the next mode in the list """        
    def getNextMode(self, robot):

        nextMode = None
        
        mode = self.modeList[self.nextModeIndex] 

        if ( mode == RobotModes.MODE_REMOTE ):
            nextMode = RemoteRobotMode(robot)

        elif ( mode == RobotModes.MODE_STRAIGHT_LINE ):
            nextMode = StraightLineSpeedMode(robot)

        elif ( mode == RobotModes.MODE_MINIMAL_MAZE ):
            nextMode = MinimalMazeMode(robot) 

        elif ( mode == RobotModes.MODE_HUBBLE ):
            nextMode = HubbleMode(robot) 

        elif ( mode == RobotModes.MODE_SPACE_INVADERS ):
            nextMode = SpaceInvadersMode(robot)

        elif ( mode == RobotModes.MODE_CHASE_BALL ):
            nextMode = ChaseBallMode(robot)

        elif ( mode == RobotModes.MODE_TIDY_TOYS ):
            nextMode = TidyToysMode(robot)

        elif ( mode == RobotModes.MODE_FEED_THE_FISH ):
            nextMode = FeedTheFishMode(robot)

        elif ( mode == RobotModes.MODE_SPEECH_CONTROL ):
            nextMode = SpeechControlMode(robot) 

        elif ( mode == RobotModes.MODE_UP_THE_GARDEN_PATH ):
            nextMode = UpTheGardenPathMode(robot)

        elif ( mode == RobotModes.MODE_NANNY_MC_PI_REMOTE ):
            nextMode = NannyMcPiRemoteMode(robot)
            
        elif ( mode == RobotModes.MODE_NANNY_MC_PI_DEMO ):
            nextMode = NannyMcPiDemoMode(robot)

        else:
            logging.info("Unknown mode " + mode + " requested")

        self.nextModeIndex += 1
        if ( self.nextModeIndex >= len(self.modeList) ):
            self.nextModeIndex = 0  

        return nextMode 

