
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.ParallelStraightLineMover import ParallelStraightLineMover
from Robot.RobotModes.EquidistanceStraightLineMover import EquidistantStraightLineMover
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Config.StraightLineSpeedModeConfig import StraightLineSpeedModeConfig
import time
import copy
import logging

logger = logging.getLogger(__name__)

class StraightLineSpeedMode(AutomatedRobotMode):

    
    def __init__(self, robot):
        self.robot = robot
        self.config = StraightLineSpeedModeConfig(robot.getConfig().getConfigImpl())

        #self.perfMon = PerfMon() 
        self.lastCheckTime = 0
        self.lastDistance = None 

        self.maxSpeedLeft = int(self.config.getMaxSpeedLeft("100"))
        self.maxSpeedRight = int(self.config.getMaxSpeedRight("100"))
        self.minSpeedLeft = int(self.config.getMinSpeedLeft("0"))
        self.minSpeedRight = int(self.config.getMinSpeedRight("0"))
        fudgeFactor = float(self.config.getFudgeFactor("200"))
        self.pollTime = float(self.config.getPollTime("100")) / 1000.0;

        straightLineMode = self.config.getStraightLineMode("Parallel")

        if ( straightLineMode == "Parallel" ):
            self.straightLineMover = ParallelStraightLineMover(robot, 
                                                       self.maxSpeedLeft, self.maxSpeedRight, 
                                                       self.minSpeedLeft, self.minSpeedRight, 
                                                       self.pollTime, fudgeFactor)
        else:
            self.useFrontSensors = (self.config.getEquidistantUseFrontSensors("True") == "True")
            logger.info("StraightLineSpeedMode - using EquidistantStraightLineMover")

            self.straightLineMover = EquidistantStraightLineMover(robot, 
                                                                  self.maxSpeedLeft, self.maxSpeedRight, 
                                                                  self.minSpeedLeft, self.minSpeedRight, 
                                                                  self.pollTime * 2, fudgeFactor, self.useFrontSensors)

        self.stopDistance = int(self.config.getStopDistance("30"))
        self.avoidKinks = self.config.getAvoidKinks("True") == "True"
        self.avoidKinksDistance = int(self.config.getAvoidKinksDistance("50"))
        self.avoidKinksEmergencyDistance = int(self.config.getAvoidKinksEmergencyDistance("20"))
        self.avoidKinksTurnTime = float(self.config.getAvoidKinksTurnTime("500"))
        self.avoidKinksMinApproachDistance = int(self.config.getAvoidKinksMinApproachDistance("50"))
        self.avoidKinksMinApproachCount = int(self.config.getAvoidKinksMinApproachCount("5"))
        self.avoidKinksAverageCount = int(self.config.getAvoidKinksAverageCount("5"))
        
        # variables to keep a running average of the front left and right sensors to iron out any spurious peaks. 
        self.frontRightSampleCount = 0
        self.frontRightAverage = 0  
        self.frontLeftSampleCount = 0
        self.frontLeftAverage = 0 

        self.approachingFrontLeftCount = 0 
        self.approachingFrontRightCount = 0 

        logger.info("Started Straight Line Speed Robot Mode");
        
        super().__init__(RobotModes.MODE_STRAIGHT_LINE)

    def handleEvent(self, robotEvent):

        #The super class handles the start button being pressed 
        return super().handleEvent(robotEvent)
            

    def mainLoop(self):

        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        if ( not self.waitingForStart ):

            #self.perfMon.logStart("mainloop active - start")

            # stop if there's something directly in front of us. 
            env = self.robot.getEnvironment()
            currentDistance = env.getDistance()

            #self.perfMon.dataPoint("distTime", currentDistance.timestamp)
            #self.perfMon.dataPoint("front", currentDistance.front)
            #self.perfMon.dataPoint("frontRight", currentDistance.frontRight)
            #self.perfMon.dataPoint("frontLeft", currentDistance.frontLeft)
            #self.perfMon.dataPoint("left", currentDistance.left)
            #self.perfMon.dataPoint("right", currentDistance.right)

            avgFrontRight = self.getFrontRightAverage(currentDistance.frontRight);
            avgFrontLeft = self.getFrontLeftAverage(currentDistance.frontLeft);

            #self.perfMon.dataPoint("frontRightAvg", avgFrontRight)
            #self.perfMon.dataPoint("frontLeftAvg", avgFrontLeft)
            
            #Following used for permon
            #turnRight = 0 
            #turnLeft = 0 

            if (self.lastDistance != None and (currentDistance.timestamp > self.lastDistance.timestamp)):


                logger.debug("currentDistance.front = " + str(currentDistance.front))
                if ( currentDistance.front > 0 and currentDistance.front < self.stopDistance ):
                    logger.info("stopping Robot - front distance = " + str(currentDistance.front))
                    self.robot.stopWheels()
                    #logger.info("mainloop end - " + str(time.time()));
                    return 

                if ( self.avoidKinks ):

                    if (  ((self.avoidKinksEmergencyDistance > 0) and (currentDistance.frontLeft < self.avoidKinksEmergencyDistance))
                       or ((avgFrontLeft > 0) and (avgFrontLeft < self.avoidKinksDistance))   ):
                        logger.debug("----- TURN RIGHT ------: " + str(avgFrontLeft))
                        self.turnRight(self.avoidKinksTurnTime)
                        #turnRight = 1 
                        self.approachingFrontLeftCount = 0 
                        self.frontLeftAverage = 0 
                        self.frontLeftSampleCount = 0  
                        self.frontRightAverage = 0 
                        self.frontRightSampleCount = 0  

                    if (  ((self.avoidKinksEmergencyDistance > 0) and (currentDistance.frontRight < self.avoidKinksEmergencyDistance))
                       or ((avgFrontRight > 0) and (avgFrontRight < self.avoidKinksDistance))   ):
                        logger.debug("----- TURN LEFT ------: " + str(avgFrontRight))
                        #turnLeft = 1 
                        self.turnLeft(self.avoidKinksTurnTime)
                        self.approachingFrontRightCount = 0
                        self.frontLeftAverage = 0 
                        self.frontLeftSampleCount = 0  
                        self.frontRightAverage = 0 
                        self.frontRightSampleCount = 0  

            #self.perfMon.dataPoint("turnLeft", turnLeft)
            #self.perfMon.dataPoint("turnRight", turnRight)

            # move in a straight line adjusting according to the proximity of the walls. 
            veerTime = self.straightLineMover.moveInStraightLine()

            #self.perfMon.dataPoint("veerTime", veerTime)

            self.lastDistance = copy.copy(currentDistance) 
            
            #self.perfMon.checkpoint("mainloop active - before sleep")
            
            time.sleep(max(0,self.pollTime-veerTime))
            
            #self.perfMon.checkpoint("mainloop active - after sleep")

            #logger.info("mainloop end - " + str(time.time()));

        if ( self.waitingForStart ):
            # make sure robot is stopped if we're waiting for the start button to be pressed.
            self.robot.stopWheels()
            # dump any performance stats we may have accumulated
            #self.perfMon.dump("/home/pi/Sputnik/straightLinePerformance.csv") 
            time.sleep(self.pollTime); 

        return 
            
            
    def getFrontRightAverage(self,newFrontRight):
        if ( newFrontRight > 0 ):
            if ( self.frontRightSampleCount < self.avoidKinksAverageCount ):
                self.frontRightAverage = ((self.frontRightAverage * self.frontRightSampleCount) + newFrontRight) / (self.frontRightSampleCount + 1)
                self.frontRightSampleCount += 1 
            else:
                self.frontRightAverage = ((self.frontRightAverage * (self.frontRightSampleCount - 1)) + newFrontRight) / self.frontRightSampleCount ;
            
        if ( self.frontRightSampleCount < self.avoidKinksAverageCount ): 
            return 0 ; # We don't trust the values until we've got a full range of samples. 
        else:
            return self.frontRightAverage 

    def getFrontLeftAverage(self,newFrontLeft):
        if ( newFrontLeft > 0 ):
            if ( self.frontLeftSampleCount < self.avoidKinksAverageCount ):
                self.frontLeftAverage = ((self.frontLeftAverage * self.frontLeftSampleCount) + newFrontLeft) / (self.frontLeftSampleCount + 1)
                self.frontLeftSampleCount += 1 
            else:
                self.frontLeftAverage = ((self.frontLeftAverage * (self.frontLeftSampleCount - 1)) + newFrontLeft) / self.frontLeftSampleCount ;
            
        if ( self.frontLeftSampleCount < self.avoidKinksAverageCount ): 
            return 0 ; # We don't trust the values until we've got a full range of samples. 
        else:
            return self.frontLeftAverage 
                
    def turnRight(self, turnTime):
        logger.debug("Turn Right: %f", turnTime)
        self.robot.moveRightWheel(0)
        self.robot.moveLeftWheel(self.maxSpeedLeft)
        time.sleep(turnTime / 1000)

    def turnLeft(self, turnTime):
        logger.debug("Turn Left: %f", turnTime)
        self.robot.moveRightWheel(self.maxSpeedRight)
        self.robot.moveLeftWheel(0)
        time.sleep(turnTime / 1000)

