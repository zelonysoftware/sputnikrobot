

from Robot.RobotModes.RemoteRobotMode import RemoteRobotMode
from Robot.Config.NannyMcPiConfig import NannyMcPiConfig
from Robot.Devices.RobotEyes import RobotEyes
from Robot.Devices.FishCannon import FishCannon

import Robot.RobotModes.RobotModes as RobotModes
import Robot.RobotEvent.RobotEvent as RobotEvent
import sys
import logging
from Robot.Devices.BlockForkLift import BlockForkLift
from Robot.Config.RemoteRobotModeConfig import RemoteRobotModeConfig
import math
from BlockyVision.Utils import ColourUtils
from time import sleep

logger = logging.getLogger(__name__)

NANNYMCPI_REMOTE_MODE_INIT_EYE_COLOUR = (0,0,255)
NANNYMCPI_REMOTE_MODE_EYE_COLOUR = (127,127,127)

"""
Nanny McPi specific joystick to wheel speed mapper. 
"""
class NannyMcPiJoystickMap(object):
    
    def __init__(self, minLeft, minRight, maxLeft, maxRight):

        self.minSpeedLeft = minLeft 
        self.minSpeedRight = minRight
        self.maxSpeedLeft = maxLeft
        self.maxSpeedRight = maxRight
    
    def doMapping(self, xpos, ypos):
        
        #logger.debug("mapLeftJoystick: x=%d, y=%d", xpos, ypos)

        # Don't move backwards until over a threshold so our spins are more controlled
        if ((ypos < 0) and (ypos > -30)):
            ypos = 0

        leftWheelSpeed = ypos ;
        rightWheelSpeed = ypos ;

        # Make driving in a straight line easier by only turning if x pos over a threshold
        if ( xpos > -10 and xpos < 10 ):
            xpos = 0 

        if ( xpos < 0 ):
            rightWheelSpeed += abs(xpos)
            leftWheelSpeed += xpos
        else:
            leftWheelSpeed += xpos
            rightWheelSpeed -= xpos

        leftWheelSpeed = min(100, leftWheelSpeed)
        leftWheelSpeed = max(-100, leftWheelSpeed)
        rightWheelSpeed = min(100, rightWheelSpeed)
        rightWheelSpeed = max(-100, rightWheelSpeed)

        # adjust the speed according to the minimum and maximum speed. 
        if ( leftWheelSpeed > 0 ):
            leftWheelSpeed = ((leftWheelSpeed * (self.maxSpeedLeft - self.minSpeedLeft)) / 100) + self.minSpeedLeft
        elif ( leftWheelSpeed < 0 ):
            leftWheelSpeed = ((leftWheelSpeed * (self.maxSpeedLeft - self.minSpeedLeft)) / 100) - self.minSpeedLeft

        if ( rightWheelSpeed > 0 ):
            rightWheelSpeed = ((rightWheelSpeed * (self.maxSpeedRight - self.minSpeedRight)) / 100) + self.minSpeedRight
        elif ( rightWheelSpeed < 0 ):
            rightWheelSpeed = ((rightWheelSpeed * (self.maxSpeedRight - self.minSpeedRight)) / 100) - self.minSpeedRight
            
        return leftWheelSpeed, rightWheelSpeed 

    def mapLeftJoystick(self, xpos, ypos):
        leftWheelSpeed, rightWheelSpeed = self.doMapping(xpos, ypos)
        
        return (leftWheelSpeed * 1.0), (rightWheelSpeed * 1.0)

    # Right joystick moves robot at half speed 
    def mapRightJoystick(self, xpos, ypos):
    
        leftWheelSpeed, rightWheelSpeed = self.doMapping(xpos, ypos)
        
        return (leftWheelSpeed * 0.6), (rightWheelSpeed * 0.6)
        

"""
NannyMcPiRemoteMode is a special type of remote mode - for Nanny McPi which allows control of the fish food cannon and 
toy forklift. 
"""
class NannyMcPiRemoteMode(RemoteRobotMode):

    def __init__(self, robot):

        nannyJoystickMap = None 
        
        try:

            logger.info("Started Nanny McPi Remote Mode")
            
            nannyMcPiConfig = NannyMcPiConfig(robot.getConfig().getConfigImpl())
            remoteRobotConfig = RemoteRobotModeConfig(nannyMcPiConfig.robotConfig)

            maxSpeedLeft = int(remoteRobotConfig.getMaxSpeedLeft("100"))
            maxSpeedRight = int(remoteRobotConfig.getMaxSpeedRight("100"))
            minSpeedLeft = int(remoteRobotConfig.getMinSpeedLeft("0"))
            minSpeedRight = int(remoteRobotConfig.getMinSpeedRight("0"))
            
            nannyJoystickMap = NannyMcPiJoystickMap(minSpeedLeft, minSpeedRight, maxSpeedLeft, maxSpeedRight)
            
            # Create Cannon object for firing in remote mode.             
            self.cannon = FishCannon(robot.getConfig().getConfigImpl())
            self.cannon.magnetOff() 

            # Create forklift object for operating forklift remotely. 
            self.forkLift = BlockForkLift(robot.getConfig().getConfigImpl())
          
            # The eyes 
            self.robotEyes = RobotEyes(robot.getConfig().getConfigImpl()) 
            self.robotEyes.start()

            self.robotEyes.setColour(NANNYMCPI_REMOTE_MODE_INIT_EYE_COLOUR)
            sleep(2)
            self.robotEyes.setColour(NANNYMCPI_REMOTE_MODE_EYE_COLOUR)
            
            self.isFatalError = False 
            self.robot = robot

            self.isControllingEyes = False 
    
            logger.info("NannyMcPi remote init complete")
            
        except:
            logger.error("Exception in NannyMcPiRemoteMode __init__() [%s]", sys.exc_info()[0], exc_info=sys.exc_info())

        super().__init__(robot, joystickMap=nannyJoystickMap)
        self.modeName = RobotModes.MODE_NANNY_MC_PI_REMOTE

    def handleEvent(self, robotEvent):

        eventHandled = False 
        eventType = robotEvent.getEventType()
        
        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE ):
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_CIRCLE ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.cannon.magnetOn()
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_X ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.cannon.fire()
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_SQUARE ):
                if ( robotEvent.getButtonValue() == 0 ): 
                    self.robot.getEnvironment().getSmartCamera().takePhoto() 
                eventHandled = True 

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_R3 ):
                if ( robotEvent.getButtonValue() == 0 ): 
                    self.isControllingEyes = not self.isControllingEyes 
                    if self.isControllingEyes:
                        # Make sure we're not moving since we're disabling the joysticks for movement.  
                        self.joystickRightMoved(0,0)
                        self.joystickLeftMoved(0,0)
                eventHandled = True 

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_L1 ):
                if ( robotEvent.getButtonValue() == 1 ):
                    self.robot.moveLeftWheel(0-self.maxSpeedLeft)
                    self.robot.moveRightWheel(self.maxSpeedRight)
                else:
                    self.robot.stopWheels()

                eventHandled = True
            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_R1 ):
                if ( robotEvent.getButtonValue() == 1 ):
                    self.robot.moveRightWheel(0-self.maxSpeedRight)
                    self.robot.moveLeftWheel(self.maxSpeedLeft)
                else:
                    self.robot.stopWheels()

                eventHandled = True
                
            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_UP ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.forkLift.moveUp()
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_DOWN ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.forkLift.moveDown()
                eventHandled = True
                
            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_LEFT ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.forkLift.openGrip()
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_RIGHT ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.forkLift.closeGrip()
                eventHandled = True

        elif ( eventType == RobotEvent.RE_GC_JOYSTICK_CHANGE ):
            if ( robotEvent.whichJoystick() == RobotEvent.JoystickChangeEvent.JOYSTICK_RIGHT ):
                if ( self.isControllingEyes ):
                    self.setEyeColour(robotEvent.getXPos(), robotEvent.getYPos())
                    eventHandled = True
                else:
                    # move the eyes as we're moving - don't set eventHandled - otherwise the 
                    # motors won't move.
                    self.robotEyes.moveBoth(robotEvent.getXPos())
                    
            elif ( robotEvent.whichJoystick() == RobotEvent.JoystickChangeEvent.JOYSTICK_LEFT ):
                # Always move the eyes with the left joystick. 
                self.robotEyes.moveBoth(robotEvent.getXPos())
                if ( self.isControllingEyes ):
                    # If we're controlling the eyes - don't move the motors - this allows us to move the eyes without 
                    # the robot moving. 
                    eventHandled = True
                    
        #For Nanny McPi we're using the triggers to drive tank style (doesn't work with PiHut controller though!)j
        elif ( eventType == RobotEvent.RE_GC_TRIGGER_CHANGE ):
            if ( robotEvent.whichTrigger() == RobotEvent.TriggerChangeEvent.TRIGGER_L2 ):
                pos = robotEvent.getTriggerPos()
                logger.info("LEFT TRIGGER POS = %d", pos);
                if ( pos > 5 ):
                    self.robot.moveLeftWheel((pos * (self.maxSpeedLeft - self.minSpeedLeft)/100) + self.minSpeedLeft) 
                else: 
                    self.robot.moveLeftWheel(0)   
                eventHandled = True 
            elif ( robotEvent.whichTrigger() == RobotEvent.TriggerChangeEvent.TRIGGER_R2 ):
                pos = robotEvent.getTriggerPos()
                logger.info("RIGHT TRIGGER POS = %d", pos);
                if ( pos > 5 ):
                    self.robot.moveRightWheel((pos * (self.maxSpeedRight - self.minSpeedRight)/100) + self.minSpeedRight) 
                else: 
                    self.robot.moveRightWheel(0)   
                eventHandled = True 
                     
        if ( not eventHandled ):
            eventHandled = super().handleEvent(robotEvent)

        return eventHandled 

    def setEyeColour(self, xPos, yPos):
        
        # Convert the joystick position to an HSV value and adjust the eye colour 
        # accordingly where: 
        #
        # H = angle joystick is from centre. 
        # S = magnitude of joystick distance from centre
        # V = always 100%
        # 
        
        angle, mag = self.getJoystickAngleAndMagnitude(xPos, yPos)

        
        mag = min(100, mag) 

        logger.debug(f"Joystick Angle = {angle}, Magnitude = {mag}")

        h = angle 
        s = mag 
        v = 100 
        
        red, green, blue = ColourUtils.HSVtoRGB(h, s, v) 

        logger.debug(f"Eye Colour HSV = {h}, {s}, {v}")
        logger.debug(f"Eye Colour RGB = {red}, {green}, {blue}")
                
        self.robotEyes.setColour((red,green,blue))

    # This effectively converts joystick cartesian coordinates into polar coordinates
    # but with 0 degrees being vertical.  
    def getJoystickAngleAndMagnitude(self, x, y): 

        magnitude = int(math.sqrt(x*x+y*y))
        
        if ( y == 0 ):
            # handle divide by zero issues when joystick centred on y axis 
            angle = 90 if x >= 0 else 270 
        else:
            # get the quadrant angle. 
            angle = int(math.degrees(math.atan(x/y)))
            
            # adjust depending on which quadrant. 
            if ( y < 0 ):
                angle += 180 
            elif( x < 0 ):
                angle += 360 
        
        return angle, magnitude   

    def stop(self):

        self.robotEyes.stop()
        self.forkLift.stop()

        super().stop()
