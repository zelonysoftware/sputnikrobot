'''
Created on 15 Oct 2021

@author: zelony
'''
import Robot.RobotEvent.RobotEvent as RobotEvent
import logging
import time
import sys
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Devices.RobotEyes import RobotEyes
from Robot.RobotModes import RobotModes

logger = logging.getLogger(__name__)

NANNYMCPI_DEMO_MODE_INIT_EYE_COLOUR = (0,127,0)
NANNYMCPI_DEMO_MODE_EYE_COLOUR = (255,255,255)

CIRCLE_BLOCK_ID     = "circle"
SQUARE_BLOCK_ID     = "square"
X_BLOCK_ID          = "cross"
TRIANGLE_BLOCK_ID   = "triangle"

DEFAULT_POLL_TIME = 20 
DEFAULT_MOTOR_SPEED = 50 

class NannyMcPiDemoMode(AutomatedRobotMode):

    def __init__(self, robot):

        try:

            logger.info("Started Nanny McPi Demo Mode")
            
            env = robot.getEnvironment()


            # Todo - create a config section for this mode
            self.pollTime = DEFAULT_POLL_TIME / 1000.0 
            self.maxMotorSpeed = DEFAULT_MOTOR_SPEED 
            
            # The eyes 
            self.robotEyes = RobotEyes(robot.getConfig().getConfigImpl()) 
            self.robotEyes.start()

            self.robotEyes.setColour(NANNYMCPI_DEMO_MODE_INIT_EYE_COLOUR)
            time.sleep(2)
            self.robotEyes.setColour(NANNYMCPI_DEMO_MODE_EYE_COLOUR)
            
            self.isFatalError = False 
            self.robot = robot
            
            # Get the blocky vision camera  
            self.bvCamera = env.getSmartCamera()
            
            self.lastCameraFrame = 0 
            
            self.blocksToFind = dict() 

            logger.info("NannyMcPi demo init complete")
            
        except:
            logger.error("Exception in NannyMcPiDemoMode __init__() [%s]", sys.exc_info()[0], exc_info=sys.exc_info())

        super().__init__(RobotModes.MODE_NANNY_MC_PI_DEMO )
        
    def handleEvent(self, robotEvent):

        eventHandled = False 
        eventType = robotEvent.getEventType()
        
        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE ):
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_CIRCLE ):
                self.sampleBlock(CIRCLE_BLOCK_ID)
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_X ):
                self.sampleBlock(X_BLOCK_ID)
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_SQUARE ):
                self.sampleBlock(SQUARE_BLOCK_ID)
                eventHandled = True 
                     
        if ( not eventHandled ):
            eventHandled = super().handleEvent(robotEvent)

        return eventHandled 



    '''
        The mainloop. 
        
        When waiting for start, we will look for blocks that match and move the eyes to point to the block. 
        When the start button is pressed, we'll try to move towards the nearest block until we get close - or lose the block. 
    '''
    def mainLoop(self):

        startTime = time.time() 
                
        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        if ( self.waitingForStart ):
            self.robot.stopWheels() 

        
        # get any blocks
        currentFrame, blocks = self.bvCamera.getCameraObjectsWithFrameCount()
        biggestBlock = None 

        logger.debug(f"Current frame = {currentFrame}, lastFrame = {self.lastCameraFrame}")
        
        if ( currentFrame != self.lastCameraFrame ): 

            self.lastCameraFrame = currentFrame 
            
            biggestBlock = self.getBiggestBlock(blocks) 

            if ( biggestBlock is not None ):

                logger.debug(f"Biggest block: {biggestBlock.type}, x = biggestBlock.x")
                
                # Set eye colour.
                colour = self.bvCamera.getSearchObject(biggestBlock.type)
                logger.debug(f"set eye colour: {colour.getRGB()}")
                self.robotEyes.setColour( colour.getRGB() )
                
                # Move eyes.
                self.robotEyes.moveBoth(biggestBlock.x)
            else: 

                self.robotEyes.moveBoth(0)
                self.robotEyes.setColour(NANNYMCPI_DEMO_MODE_EYE_COLOUR)
                
                self.robot.stopWheels()  
                
        if ( not self.waitingForStart and (biggestBlock is not None) ):

            # Start button has been pressed - so move towards the biggest block. 
            logger.info("--- MOVE TO BLOCK ---")

            distance = self.robot.getEnvironment().getDistance() 
            logger.info("Moving To Colour - F = %f" % (distance.front))

            if ( distance.front > 0 and distance.front <= 20):

                self.robot.stopWheels() 

            else:

                motorSpeed = self.maxMotorSpeed

                logger.info("Motor speed = %d" % (motorSpeed))

                logger.info("MOVING TO BLOCK: [%d] %s" % (self.lastCameraFrame, str((biggestBlock.type, biggestBlock.x, biggestBlock.y))))

                x = biggestBlock.x

                logger.info(f"x = {x}")
                if x < -50:
                    logger.info("veering left")
                    self.veerLeft(motorSpeed) 
                elif x > 50: 
                    logger.info("veering right")
                    self.veerRight(motorSpeed)
                else:
                    logger.info("going straight at speed %d" % motorSpeed)
                    self.robot.moveForward(motorSpeed)

        elapsedTime = time.time() - startTime 

        time.sleep(max(self.pollTime - elapsedTime, 0))

    def stop(self):

        self.robotEyes.stop()
        self.robot.stopWheels()

        super().stop()

    def getBiggestBlock(self, blockList):
        
        biggestBlock = None  
        
        for block in blockList:
            
            if ( biggestBlock is None ):
                biggestBlock = block 
            else:
                blockSize = block.height * block.width ;
                
                if ( blockSize > (biggestBlock.height * biggestBlock.width) ):
                    
                    biggestBlock = block 
        
        return biggestBlock 
            
    def veerRight(self, moveSpeed):
        self.robot.moveLeftWheel(moveSpeed)
        self.robot.moveRightWheel(0)
        
    def veerLeft(self, moveSpeed):
        self.robot.moveRightWheel(moveSpeed)
        self.robot.moveLeftWheel(0)
            
    def sampleBlock(self, blockName):
        
        # We're going to take the middle of the image and use it to set a search block. 
        _, currentImage = self.bvCamera.getImageWithFrameCount()
        
        sampleWidth = currentImage.width / 4 
        sampleHeight = currentImage.height / 4
        
        centreCol = currentImage.width / 2 
        centreRow = currentImage.height / 2  
        
        leftCol = int(centreCol - (sampleWidth / 2))
        rightCol = int(centreCol + (sampleWidth / 2))
        
        topRow = int(centreRow - (sampleHeight / 2)) 
        bottomRow = int(centreRow + (sampleHeight / 2)) 
        
        logger.info(f"Sample area: tr = {topRow}, br = {bottomRow}, lc = {leftCol}, rc = {rightCol}")
        
        totalR = 0 
        totalG = 0 
        totalB = 0 
        numPixels = 0 
        
        for row in range(topRow, bottomRow):
            for col in range(leftCol, rightCol):
                
                r,g,b = currentImage.getpixel((col,row))
                totalR += r 
                totalG += g 
                totalB += b 
                numPixels += 1 

        avgR = int(totalR / numPixels) 
        avgG = int(totalG / numPixels) 
        avgB = int(totalB / numPixels) 
        
        logger.info(f"Sample: name= {blockName}, R={avgR}, G={avgG}, B={avgB}")
        
        self.blocksToFind.pop(blockName, None) 
        self.blocksToFind[blockName] = (avgR, avgG, avgB) 
        
        blockList = list() 
        
        for name, rgb in self.blocksToFind.items(): 
            
            r,g,b = rgb 
            
            blockList.append((name, r, g, b))
            
        self.bvCamera.setSearchObjects(blockList) 
        
