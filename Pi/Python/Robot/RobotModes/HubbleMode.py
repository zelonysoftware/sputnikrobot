
import Robot.RobotEvent.RobotEvent as RobotEvent
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Config.HubbleModeConfig import HubbleModeConfig
import time
import logging
from enum import Enum

logger = logging.getLogger(__name__)


"""
    Hubble Telescope Nebula Challenge 
    
    Order of colours to find = red, blue, yellow, green 

    On Start 
        Spin round noting order of colours 

        Find first colour 
        Go to it until keypress or sensor says stop. 
        Go back to centre. 
        repeat until all colours have been found. 

"""
class HubbleModeState(Enum):
    START = 0 
    FINDING_COLOURS = 1 
    SPINNING_TO_NEXT_COLOUR = 2 
    SPINNING_RIGHT_TO_CENTRE_COLOUR = 3 
    SPINNING_LEFT_TO_CENTRE_COLOUR = 4 
    MOVING_TO_COLOUR = 5 
    MOVING_BACK_TO_CENTRE = 6

    
class HubbleMode(AutomatedRobotMode):

    # Colour signatures
    RED = 1 
    BLUE = 2 
    YELLOW = 3 
    GREEN = 4 

    """ 
    Create an array of 'found colours' which has the colours in the order we find them. 
    This is a class variable so it can be used again for subsequent runs. If something goes wrong, 
    the 'square' button can be pressed when the robot is stopped to clear this array and start again. 
    """
    foundColours = []

    # The following keep track of the status of the challenge as we go through the different states. 
    correctColourOrder = [RED, BLUE, YELLOW, GREEN] 
    
    def __init__(self, robot):
        self.robot = robot
        self.config = HubbleModeConfig(robot.getConfig().getConfigImpl())        
        self.pollTime = float(self.config.getPollTime("50")) / 1000.0
        self.moveSpeed = int(self.config.getMoveSpeed("50")) 
        self.spinSpeed = int(self.config.getSpinSpeed("50"))
        self.minFrontDistance = int(self.config.getMinFrontDistance("10"))
        self.minWidth = int(self.config.getMinWidth("50"))
        self.startFrontDistance = -1
        self.maxStartDistance = 80
        self.useLamp = self.config.getUseLamp("False") == "True"
        self.preScanColours = self.config.getPreScanColours("True") == "True"
        self.lastColour = -1 
        self.nextColour = self.correctColourOrder[0]
        self.zoneReachedButtonPressed = False
        logger.info("Started Hubble Robot Mode")
        self.robot.getEnvironment().setCameraLamp(self.useLamp)
        #self.perfMon = PerfMon()
        self.currentState = HubbleModeState.START
        self.lastState = HubbleModeState.START
        self.startMoveTimeMs = -1 
        self.endMoveTimeMs = -1 
        
        super().__init__(RobotModes.MODE_HUBBLE)

    def handleEvent(self, robotEvent):

        eventHandled = False

        eventType = robotEvent.getEventType()
        # Is this a keypress to tell us a zone has been reached? 
        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE):
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_CIRCLE ):
                if ( robotEvent.getButtonValue() == 1 ):
                    self.zoneReachedButtonPressed = True
                eventHandled = True
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_SQUARE ):
                if ( robotEvent.getButtonValue() == 1 ):
                    logger.info("Square button pressed - reseting found colours")
                    self.foundColours.clear()
                eventHandled = True
    
        # Is this a keypress to tell us to reset the challenge and start again. 
        #TODO
        
        # The super class handles the start button being pressed 
        eventHandled = eventHandled or super().handleEvent(robotEvent)
        logger.info("Hubble - event handled = " + str(eventHandled))
        return eventHandled 
            
    """
     The mainloop has to be responsive to actions, whilst also driving the robot. 
     We therefore use a state machine so we can keep an eye on the 'waiingForStart' attribute. 
    """
    def mainLoop(self):

        veerTime = 0 

        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        if ( not self.waitingForStart ):

            if ( self.lastState != self.currentState ):
                logger.info("CurrentState = " + str(self.currentState));
                self.lastState = self.currentState
            
            if ( self.currentState == HubbleModeState.START ):
                
                if ( self.startFrontDistance == -1 ):
                    self.startFrontDistance = self.robot.getEnvironment().getDistance().front 
                    self.maxStartDistance = (self.startFrontDistance * 120) / 100
                    logger.info("Start front distance = " + str(self.startFrontDistance) + ", max front = " + str(self.maxStartDistance))

                if ( self.preScanColours ):
                    # Start rotating keeping note of all colours until we've found all 4. 
                    self.robot.startSpinRight(self.spinSpeed)
                    self.currentState = HubbleModeState.FINDING_COLOURS 
                else:
                    # Just start looking for the 1st colour
                    self.currentState = HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR
                
            elif ( self.currentState == HubbleModeState.FINDING_COLOURS ):
                
                # Get the rightmost colour signature from the camera. 
                currentObjects = self.robot.getEnvironment().getCameraObjects() 
                prominentObject = self.getProminentObject(currentObjects) 
                
                if ( prominentObject != None ):
                    if ( not (prominentObject.type in self.foundColours) ):
                        # Yay - we found a new colour 
                        logger.debug("found colour " + str(prominentObject.type))
                        self.foundColours.append(prominentObject.type) 
                        logger.debug("len(self.foundColours) = " + str(len(self.foundColours)))
                        logger.debug("len(self.correctColourOrder) = " + str(len(self.correctColourOrder)))
                        if ( len(self.foundColours) == len(self.correctColourOrder)-1 ):
                            self.robot.stopWheels() 
                            # We've found n-1 colours - so we can deduce the last colour. 
                            finalColour = self.deduceFinalColour(self.foundColours) 
                            self.foundColours.append(finalColour)

                    if (len(self.foundColours) == len(self.correctColourOrder)):
                        self.robot.stopWheels()
                        self.currentColourPos = self.foundColours.index(prominentObject.type)
                        self.nextColour = self.correctColourOrder[0] # Start with the first colour. 
                        self.currentState = HubbleModeState.SPINNING_TO_NEXT_COLOUR 

            elif ( self.currentState == HubbleModeState.SPINNING_TO_NEXT_COLOUR ): 
                
                if ( self.preScanColours ):
                    # Work out where the first colour is relative to the colour we're facing and spin to it. 
                    nextColourIndex = self.foundColours.index(self.nextColour) ;
                
                    if ( nextColourIndex != self.currentColourPos ):
                        diff = nextColourIndex - self.currentColourPos
                        if ( diff != 0 ):
                            if ( abs(diff) == 3 ):
                                diff = int(diff / 3) * -1  # Change +3 to -1 and -3 to +1 to improve speed. 
                            for _ in range(0,abs(diff)):
                                # spin left or right 
                                if ( diff < 0 ):
                                    self.robot.timedSpinLeft(95)
                                else:
                                    self.robot.timedSpinRight(85) 
                
                    # Now we should be just about facing the colour we want, we need to center on it. 
                    currentObjects = self.robot.getEnvironment().getCameraObjects() 
                    nextColourObject = self.getFirstObjectOfColour(currentObjects, self.nextColour) 
                
                    if ( (nextColourObject == None) or (nextColourObject.x >= 168) ):
                        self.currentState = HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR
                        self.robot.startSpinRight(self.spinSpeed) 
                    else: 
                        self.currentState = HubbleModeState.SPINNING_LEFT_TO_CENTRE_COLOUR
                        self.robot.startSpinLeft(self.spinSpeed)
                else:
                    # Not scanning for colours - so just search for the next one. 
                    self.currentState = HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR
                    self.robot.startSpinRight(self.spinSpeed)
                    
            elif (  (self.currentState == HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR)
                    or (self.currentState == HubbleModeState.SPINNING_LEFT_TO_CENTRE_COLOUR) ):

                currentObjects = self.robot.getEnvironment().getCameraObjects()
                nextColourObject = self.getFirstObjectOfColour(currentObjects, self.nextColour)
                
                if ( (nextColourObject != None) ):
                    logger.info("Spinning to centre colour - first item of colour %d, x = %d, y=%d, width = %d, height = %d" % (nextColourObject.type, nextColourObject.x, nextColourObject.y, nextColourObject.width, nextColourObject.height))
                    if ( nextColourObject.x > 148 and nextColourObject.x < 168 ):
                    #if (  ((self.currentState == HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR) and (nextColourObject.x <= 158 ))
                    #   or ((self.currentState == HubbleModeState.SPINNING_LEFT_TO_CENTRE_COLOUR) and (nextColourObject.x >= 158 ))  ):
                        logger.info("Colour centred - will now move towards colour.")
                       
                        self.robot.stopWheels()
                        self.startMoveForwardTime = time.time() 
                        self.robot.moveForward(self.moveSpeed)
                        self.zoneReachedButtonPressed = False 
                        self.currentState = HubbleModeState.MOVING_TO_COLOUR 
                    elif (self.currentState == HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR and nextColourObject.x <= 148):
                        # Oops - wrong way - go back.  
                        self.robot.stopWheels()
                        self.currentState = HubbleModeState.SPINNING_LEFT_TO_CENTRE_COLOUR
                        self.robot.startSpinLeft(self.spinSpeed)
                    elif (self.currentState == HubbleModeState.SPINNING_LEFT_TO_CENTRE_COLOUR and nextColourObject.x >= 168):
                        # Oops - wrong way - go back.  
                        self.robot.stopWheels()
                        self.currentState = HubbleModeState.SPINNING_RIGHT_TO_CENTRE_COLOUR
                        self.robot.startSpinRight(self.spinSpeed)
                        
                    
            elif ( self.currentState == HubbleModeState.MOVING_TO_COLOUR ):

                distance = self.robot.getEnvironment().getDistance() 
                logger.info("Moving To Colour - FL = %d, F = %d, FR = %d" % (distance.frontLeft, distance.front, distance.frontRight))
                
                if (  self.zoneReachedButtonPressed 
                   or (distance.front > 0 and distance.front <= self.minFrontDistance)
                   or (distance.frontLeft > 0 and distance.frontLeft <= self.minFrontDistance)
                   or (distance.frontRight > 0 and distance.frontRight <= self.minFrontDistance) ):

                    self.robot.stopWheels() 
                    self.zoneReachedButtonPressed = False
                    totalMoveForwardTime = time.time() - self.startMoveForwardTime 
                    self.currentColourPos = self.foundColours.index(self.nextColour) 
                    nextColourOrderIndex = self.correctColourOrder.index(self.nextColour) 
                    nextColourOrderIndex += 1 
                    if ( nextColourOrderIndex >= len(self.correctColourOrder) ):
                        # Woo Woo - we've finished !!!!! 
                        logger.info("WAHAY!!!! WE FOUND ALL THE COLOURS");
                        self.waitingForStart = True 
                    else: 
                        logger.info("Found colour " + str(self.nextColour))
                        self.nextColour = self.correctColourOrder[nextColourOrderIndex]
                        self.currentState = HubbleModeState.MOVING_BACK_TO_CENTRE
                        self.stopMovingBackTime = time.time() + totalMoveForwardTime
                        self.robot.moveBackward(self.moveSpeed)
                else:
                
                    # Not there yet - let's just check we're still moving in the right direction. 
                    currentObjects = self.robot.getEnvironment().getCameraObjects()
                    prominentObject = self.getProminentObject(currentObjects, anyPos=True)

                    if ( (prominentObject == None) ):
                        # Never mind - keep the faith and keep going. 
                        logger.info("Lost colour " + str(self.nextColour) + " - keep going")
                    elif ( prominentObject.type == self.nextColour ):
                        if ( prominentObject.x < 148 ):
                            veerTime = self.veerLeft(0.2)
                        elif ( prominentObject.x > 168 ):
                            veerTime = self.veerRight(0.2)
                    else:
                        # Oh dear - we're going towards the wrong colour. 
                        # Hmm - I guess we need to move back and try to find the colour again. 
                        logger.info("Moving towards " + str(self.nextColour) + " but found " + str(prominentObject.type) + " - will go back and try again")
                        self.robot.stopWheels() 
                        totalMoveForwardTime = time.time() - self.startMoveForwardTime 
                        self.currentState = HubbleModeState.MOVING_BACK_TO_CENTRE
                        self.stopMovingBackTime = time.time() + totalMoveForwardTime
                        self.robot.moveBackward(self.moveSpeed)
                        
            elif ( self.currentState == HubbleModeState.MOVING_BACK_TO_CENTRE ):
            
                distance = self.robot.getEnvironment().getDistance()
                logger.info("Moving backwards (FL = " + str(distance.frontLeft) + ", FR = " + str(distance.frontRight) + ")")
                #if ( time.time() >= self.stopMovingBackTime or distance.frontLeft > self.startFrontDistance or distance.frontRight > self.startFrontDistance ):
                if (   (distance.frontLeft < self.maxStartDistance and distance.frontLeft > self.startFrontDistance)
                    or (distance.frontRight < self.maxStartDistance and distance.frontRight > self.startFrontDistance) ):

                    logger.info("Moving backwards distance reached (FL = " + str(distance.frontLeft) + ", FR = " + str(distance.frontRight) + ")")
                    # we should be back in the centre now - so spin for the next colour.
                    self.robot.stopWheels() 
                    logger.info("Stopped wheels")
                    self.currentState = HubbleModeState.SPINNING_TO_NEXT_COLOUR 
                    logger.info("Changed state to " + str(self.currentState));

            else:
                logger.error("Hubble Mode in unknown status: " + str(self.currentState))
                
        else:
            self.robot.stopWheels()
            self.currentState = HubbleModeState.START
            #self.perfMon.dump("/home/pi/Sputnik/HubbleData.csv")

        time.sleep(max(0,self.pollTime - veerTime))
                
    """ Go through list of camera objects to find the 1st one that matches the colour we're looking for """ 
    def getFirstObjectOfColour(self, cameraObjects, colour):
        logger.info("getFirstObject of colour - colour = %d" % colour)
        for cameraObject in cameraObjects :
            if ( cameraObject.type == colour ):
                logger.info("Found colour - returning object")
                return cameraObject 
        return None 
        
    def getProminentObject(self, cameraObjects, anyPos = False):
        promObject = None 
        maxAreaValue = -1 ;
        if ( cameraObjects != None ):
            logger.debug("Finding Prominent Object from:")
            for cameraObject in cameraObjects :
                #self.logCameraObject(cameraObject)
                objectArea = cameraObject.width * cameraObject.height
                if ( cameraObject.width > self.minWidth and objectArea > maxAreaValue ):
                    if ( anyPos or (cameraObject.x > 133 and cameraObject.x < 186) ):
                        maxAreaValue = objectArea
                        promObject = cameraObject 
                
        if (promObject != None):
            logger.info("getProminentObject: Returning: Type = " + str(promObject.type) +", x = " + str(promObject.x) + ", height = " + str(promObject.height) + ", width = " + str(promObject.width) )
        else:
            logger.info("getProminentObject: No prominent object found")

        return promObject 

    def logCameraObject(self, cameraObject):

        logger.info("getProminentObject: Type = " + str(cameraObject.type) +", x = " + str(cameraObject.x) + ", height = " + str(cameraObject.height) + ", width = " + str(cameraObject.width) )
        #self.perfMon.logStart("cameraObject")
        #self.perfMon.dataPoint("type", cameraObject.type)
        #self.perfMon.dataPoint("x", cameraObject.x)
        #self.perfMon.dataPoint("y", cameraObject.y)
        #self.perfMon.dataPoint("height", cameraObject.height)
        #self.perfMon.dataPoint("width", cameraObject.width) 
    
    def veerRight(self, veerTime):
        logger.debug("Veer Right: %f", veerTime)
        self.robot.moveLeftWheel(self.moveSpeed)
        self.robot.moveRightWheel(max(self.moveSpeed - 20, 20))
        time.sleep(veerTime)
        return veerTime
        
    def veerLeft(self, veerTime):
        logger.debug("Veer Left: %f", veerTime)
        self.robot.moveRightWheel(self.moveSpeed)
        self.robot.moveLeftWheel(max(self.moveSpeed - 20, 20))
        time.sleep(veerTime)
        return veerTime

    def deduceFinalColour(self, colourList):
        diff = set(self.correctColourOrder) - set(colourList) ;
        # should only be a single entry
        return (next(iter(diff)))

