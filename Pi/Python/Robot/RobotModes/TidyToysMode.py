
import sys
import Robot.RobotEvent.RobotEvent as RobotEvent
import logging
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Devices.BlockForkLift import BlockForkLift
from Robot.Devices.BlockForkLift import BlockPosition 
from Robot.Devices.RedBoardForkLift import RedBoardForkLift
from Robot.Devices.RobotEyes import RobotEyes
from Robot.Config.TidyToysConfig import TidyToysConfig
from Robot.Environment.BlockyVisionCamera import BlockyVisionCamera 
import time
from Robot.RobotModes.HorizonStraightLineMover import HorizonStraightLineMover

logger = logging.getLogger(__name__)

''' Colour to set the eyes when we first start the mode '''
TIDY_TOYS_MODE_EYE_COLOUR = (0, 127, 127);

''' Max time to spin (in secs) when locating colour '''
MAX_SPIN_TIME = 0.1  

''' Max time to veer (in secs) when moving to colour '''
MAX_VEER_TIME = 0.5

''' Program States '''
START = 0 
MOVE_BACK_TO_DISTANCE = 1 
MOVE_FWD_TO_DISTANCE = 2

SPIN_LEFT = 3
SPIN_RIGHT = 4 

LOCATE_COLOUR = 5
MOVE_TO_COLOUR = 6

PICK_UP_BLOCK = 7 
DROP_BLOCK = 8 

FINISH = 100 

''' We need to factor in whether we have a block or not in the forklift when calculating the 
    90 degree turn time '''
SPIN_WITHOUT_BLOCK = 0 
SPIN_WITH_BLOCK = 1  
 

''' Names of blocks we'll need to look for. '''
RED_BLOCK_NAME = 'red'
GREEN_BLOCK_NAME = 'green'
BLUE_BLOCK_NAME = 'blue'
YELLOW_BLOCK_NAME = 'yellow' 


class Step():
    
    def __init__(self, state, values=None):
        
        self.state = state
        self.values = values 
        

class TidyToysMode(AutomatedRobotMode):

    def __init__(self, robot):
        try:

            logger.info("Started Tidy Toys Mode")
            
            tidyToysConfig = TidyToysConfig(robot.getConfig().getConfigImpl())
    
            self.isFatalError = False 
            
            self.forkLift = BlockForkLift(robot.getConfig().getConfigImpl())
            
            self.ninetyDegreeTimeWithBlock = tidyToysConfig.getNinetyDegreeTurnTimeWithBlock()
            self.ninetyDegreeTimeWithoutBlock = tidyToysConfig.getNinetyDegreeTurnTimeWithoutBlock()
    
            self.moveToDistanceSlowDownDistance = tidyToysConfig.getMoveToDistanceSlowDownDistance()
            self.moveToDistanceSlowDownSpeed = tidyToysConfig.getMoveToDistanceSlowDownSpeed()
            
            self.pollTime = float(tidyToysConfig.getPollTime("50")) / 1000.0
    
            self.blockCentreLeft = tidyToysConfig.getBlockCentreLeft()
            self.blockCentreRight = tidyToysConfig.getBlockCentreRight()
            self.blockCentreOffset = tidyToysConfig.getBlockCentreOffset()
            
            self.locateBlockSpinTime = tidyToysConfig.getLocateBlockSpinTime()
            self.locateDirection = 0 

            self.maxSpeed = tidyToysConfig.getMaxSpeed() 
            self.minSpeed = tidyToysConfig.getMinSpeed()
            self.moveToColourSpeed = tidyToysConfig.getMoveToColourSpeed()
            self.currentSpeed = 0 
            self.spinSpeed = tidyToysConfig.getSpinSpeed()
    
            self.minBlockDistance = float(tidyToysConfig.getMinBlockDistance())
            
            self.findBlockCropTop = tidyToysConfig.getFindBlockCropTop()
            self.findBlockCropBtm = tidyToysConfig.getFindBlockCropBtm()
            
            self.redBlockRGB = tidyToysConfig.getRedBlockRGB()
            self.greenBlockRGB = tidyToysConfig.getGreenBlockRGB()
            self.blueBlockRGB = tidyToysConfig.getBlueBlockRGB()

            # Create a dictionary of the blocks so we can get them quickly. 
            self.blockColours = { RED_BLOCK_NAME    : tidyToysConfig.getRedBlockRGB(),
                                  GREEN_BLOCK_NAME  : tidyToysConfig.getGreenBlockRGB(),
                                  BLUE_BLOCK_NAME   : tidyToysConfig.getBlueBlockRGB(),
                                  YELLOW_BLOCK_NAME : tidyToysConfig.getYellowBlockRGB() }
    
            cropTop = tidyToysConfig.getFindBlockCropTop()
            cropBottom = tidyToysConfig.getFindBlockCropBtm() 
            self.findBlockCrop = 0,cropTop,100,cropBottom 
    
            cropLeft = tidyToysConfig.getHorizonCropLeft() 
            cropRight = tidyToysConfig.getHorizonCropRight()
            self.findHorizonCrop = cropLeft,0,cropRight,100
    
            self.lastCameraFrame = 0 
            
            self.lastDistanceTimestamp = 0 
    
            self.robot = robot
            
            # The eyes 
            self.robotEyes = RobotEyes(robot.getConfig().getConfigImpl()) 
            self.robotEyes.start()
            # Our colour is cyan 
            self.robotEyes.setColour(TIDY_TOYS_MODE_EYE_COLOUR)
            
            ''' Create the steps that we need to go through to solve the challenge from start to finish. '''
            self.fullSteps = [  Step(START),
                                Step(MOVE_BACK_TO_DISTANCE,55.0),
                                Step(SPIN_LEFT, SPIN_WITHOUT_BLOCK),
                                Step(MOVE_FWD_TO_DISTANCE,33.0),
                                Step(SPIN_LEFT, SPIN_WITHOUT_BLOCK),
                                Step(LOCATE_COLOUR,(BLUE_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(BLUE_BLOCK_NAME, self.minBlockDistance)),
                                Step(PICK_UP_BLOCK, self.minBlockDistance + 2),
                                Step(MOVE_BACK_TO_DISTANCE,75.0),
                                Step(SPIN_LEFT, SPIN_WITH_BLOCK), 
                                Step(LOCATE_COLOUR, (YELLOW_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(YELLOW_BLOCK_NAME, 30.0)),
                                Step(DROP_BLOCK, (BlockPosition.BOTTOM, 22.0)),
                                Step(MOVE_BACK_TO_DISTANCE,7.0),
                                Step(SPIN_RIGHT, SPIN_WITHOUT_BLOCK), 
                                Step(LOCATE_COLOUR, (GREEN_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(GREEN_BLOCK_NAME, self.minBlockDistance)),
                                Step(PICK_UP_BLOCK, self.minBlockDistance + 2),
                                Step(SPIN_LEFT, SPIN_WITH_BLOCK), 
                                Step(MOVE_BACK_TO_DISTANCE, 35.0),
                                Step(SPIN_RIGHT, SPIN_WITH_BLOCK), 
                                Step(MOVE_BACK_TO_DISTANCE, 75.0),
                                Step(SPIN_LEFT, SPIN_WITH_BLOCK),
                                Step(LOCATE_COLOUR, (BLUE_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(BLUE_BLOCK_NAME, 13.0)),
                                Step(DROP_BLOCK, (BlockPosition.MIDDLE, 8.5)),
                                Step(MOVE_BACK_TO_DISTANCE, 20.0),
                                Step(SPIN_RIGHT, SPIN_WITHOUT_BLOCK), 
                                Step(SPIN_RIGHT, SPIN_WITHOUT_BLOCK), 
                                Step(MOVE_FWD_TO_DISTANCE,75.0),
                                Step(SPIN_LEFT, SPIN_WITHOUT_BLOCK), 
                                Step(LOCATE_COLOUR, (RED_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(RED_BLOCK_NAME, self.minBlockDistance)),
                                Step(PICK_UP_BLOCK, self.minBlockDistance + 2),
                                Step(MOVE_BACK_TO_DISTANCE, 75.0),
                                Step(SPIN_LEFT, SPIN_WITH_BLOCK),
                                Step(LOCATE_COLOUR, (GREEN_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(GREEN_BLOCK_NAME, 14.0)),
                                Step(DROP_BLOCK, (BlockPosition.TOP, 8.5)),
                                Step(MOVE_BACK_TO_DISTANCE,10.0),
                                Step(FINISH) ]
    
            ''' TEST steps for demonstration without arena '''
            self.stepsTest1 = [ Step(START),
                                Step(LOCATE_COLOUR,(RED_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(RED_BLOCK_NAME, self.minBlockDistance)),
                                Step(PICK_UP_BLOCK, self.minBlockDistance),
                                Step(SPIN_LEFT), 
                                Step(LOCATE_COLOUR, (GREEN_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(GREEN_BLOCK_NAME, 10.0)),
                                Step(DROP_BLOCK, (BlockPosition.MIDDLE, 9.0)),
                                Step(FINISH) ]
    
            ''' Test to move to Blue block and pick it up '''
            self.stepsTest2 = [ Step(START),
                                Step(LOCATE_COLOUR,(RED_BLOCK_NAME,)),
                                Step(MOVE_TO_COLOUR,(RED_BLOCK_NAME, self.minBlockDistance)),
                                Step(PICK_UP_BLOCK, self.minBlockDistance),
                                Step(FINISH) ]

            ''' Test distance measurement '''
            self.stepsTest3 = [ Step(START),
                                Step(MOVE_BACK_TO_DISTANCE,100.0),
                                Step(MOVE_FWD_TO_DISTANCE,30.0),
                                Step(FINISH) ]
    
            #self.steps = self.fullSteps
            self.steps = self.fullSteps
            
            self.currentStep = 0 
    
            env = self.robot.getEnvironment()
            
            # At the moment we give up all pretence of using the environment - the Pixy camera and BlockyVision camera 
            # are too different. We assume we're using the blockvision camera. 
            self.bvCamera = env.getSmartCamera()
            
            # We can't do much if we don't have a camera, but we don't want the constructor to bomb out
            # if we're just passing through this mode without a camera, so we check for none. If the user actually 
            # tries to start this mode without a camera, then it will throw an exception the first time we try to use the 
            # camera.           
    
            if ( self.bvCamera != None ):
                self.bvCamera.setCropArea(self.findHorizonCrop)
                colourThreshold = tidyToysConfig.getColourThreshold()
                self.bvCamera.setSearchThreshold(colourThreshold)
                
            maxStraightLineVeerTimeSeconds = tidyToysConfig.getMaxStraightLineVeerTime()
            straightLineVeerMultiplier = tidyToysConfig.getStraightLineVeerMultiplier() 
            
            self.straightLineMover = HorizonStraightLineMover(  self.robot, 
                                                                self.bvCamera, 
                                                                maxStraightLineVeerTimeSeconds, 
                                                                straightLineVeerMultiplier      ) 
    
            if ( self.bvCamera != None ):
                self.bvCamera.setCaptureMode(BlockyVisionCamera.HORIZON_MODE)
    
            logger.info("TidyToysMode init complete")
            
        except:
            #logger.error("Exception in TidyToysMode __init__() [%s]", sys.exc_info()[0])
            #print("Exception in TidyToysMode __init__() [{}]".format(sys.exc_info()[0]))
            logger.error("Exception in TidyToysMode __init__() [%s]", sys.exc_info()[0], exc_info=sys.exc_info())

        super().__init__(RobotModes.MODE_TIDY_TOYS)

    def handleEvent(self, robotEvent):

        # Place holder in case we need to handle any remote control events in future. 
        return super().handleEvent(robotEvent) 

    # Override 
    def mainLoop(self):

        startTime = time.time() 
                
        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        if ( self.waitingForStart ):
            self.robot.stopWheels() 
            self.currentStep = 0


        if ( not self.waitingForStart ):

            step = self.steps[self.currentStep]
            currentState = step.state 

            logger.info("currentState = %d", currentState)
            
            if ( currentState == START ):
                logger.info("--- START ---")
                
                logger.info("Tidy Toys are go")

                # Make sure the fork lift is up so we can see where we're going and the jaws are open.  
                self.forkLift.moveToPosition(BlockPosition.TOP)
                self.forkLift.openGrip()
                
                self.lastCameraFrame = 0 
                
                self.moveToNextStep() 

            elif ( currentState == SPIN_LEFT ):
                logger.info("--- SPIN_LEFT ---")
                self.doSpin(SPIN_LEFT, step.values) 
                self.moveToNextStep() 
            
            elif ( currentState == SPIN_RIGHT ):
                logger.info("--- SPIN_RIGHT ---")
                self.doSpin(SPIN_RIGHT, step.values) 
                self.moveToNextStep() 
                
            elif ( currentState == MOVE_BACK_TO_DISTANCE ):
                logger.info("--- MOVE_BACK_TO_DISTANCE ---")
            
                targetDistance = step.values
                currentDistance = self.robot.getEnvironment().getDistance().front
                logger.info("Current Distance = %d", currentDistance) 
                
                if ( currentDistance >= targetDistance ):
                    self.robot.stopWheels()
                    # move to next step
                    self.moveToNextStep( )
                    
                else: 
                    # Set the motor speed based on the distance. 
                    #motorSpeed = self.maxSpeed - ((currentDistance / targetDistance) * (self.maxSpeed - self.minSpeed)) 
                    motorSpeed = self.maxSpeed 
                    diff = targetDistance - currentDistance 
                    if ( diff < self.moveToDistanceSlowDownDistance ):
                        #motorSpeed = (self.maxSpeed + self.minSpeed) / self.moveToDistanceSlowDownFactor
                        motorSpeed = self.moveToDistanceSlowDownSpeed   
                        
                    self.straightLineMover.moveBackInStraightLine(motorSpeed, motorSpeed, 0, 0)
                    #self.moveInStraightLine(motorSpeed * -1) 
                        
                
            elif ( currentState == MOVE_FWD_TO_DISTANCE ):
                logger.info("--- MOVE_FWD_TO_DISTANCE ---")
            
                targetDistance = step.values
                currentDistance = self.robot.getEnvironment().getDistance().front
                logger.info("Current Distance = %d", currentDistance) 
                
                if ( currentDistance <= targetDistance ):
                    self.robot.stopWheels()
                    # move to next step
                    self.moveToNextStep( )
                    
                else: 
                    # Set the motor speed based on the distance. 
                    diff = currentDistance - targetDistance 
                    #motorSpeed = self.minSpeed + ((diff / currentDistance) * (self.maxSpeed - self.minSpeed)) 
                    motorSpeed = self.maxSpeed
                    if ( diff < self.moveToDistanceSlowDownDistance ):
                        #motorSpeed = (self.maxSpeed + self.minSpeed) / self.moveToDistanceSlowDownFactor
                        motorSpeed = self.moveToDistanceSlowDownSpeed   
                    self.straightLineMover.moveFwdInStraightLine(motorSpeed, motorSpeed, 0, 0)
                    #self.moveInStraightLine(motorSpeed) 
                
            elif ( currentState == LOCATE_COLOUR ):
                logger.info("--- LOCATE_COLOUR ---")
                blockName = step.values[0] 
                logger.info("Tidy Toys locating %s, %s" % (blockName, self.blockColours[blockName]))

                # Get the camera objects. 
                currentFrame, blocks = self.bvCamera.getCameraObjectsWithFrameCount()

                logger.info("Tidy Toys currentFrame = %d, lastFrame = %d" % (currentFrame,self.lastCameraFrame)) 
                
                if ( currentFrame != self.lastCameraFrame ): 

                    self.lastCameraFrame = currentFrame
                    
                    if ( len(blocks) > 0 ):

                        for block in blocks:
                            # Must be a block we're looking for.  
                            logger.info("FOUND A BLOCK: " + str((block.type, block.x, block.y)))
                            x = block.x - self.blockCentreOffset
                            logger.info(f"x = {x}")
                            if x < self.blockCentreLeft:
                                spinTime = abs((x) / 100) * MAX_SPIN_TIME  
                                logger.info("Spin left for %f seconds" % (spinTime))
                                self.timedSpinLeft(spinTime) 
                                time.sleep(0.5)
                                self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                            elif x > self.blockCentreRight: 
                                spinTime = abs((x) / 100) * MAX_SPIN_TIME  
                                logger.info("Spin right for %f seconds" % (spinTime))
                                self.timedSpinRight(spinTime) 
                                time.sleep(0.5)
                                self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                            else:
                                # Block is centred enough - stop the camera searching for objects and move onto the next step. 
                                self.bvCamera.setSearchObjects([]) 
                                time.sleep(0.2)
                                self.moveToNextStep() 
                            break
                            
                    else:
                        
                        # Can't find the block - try rotating a little (if we haven't already).
                        if ( self.locateDirection == 0 ):
                            self.timedSpinLeft(self.locateBlockSpinTime/1000)
                            self.locateDirection = -1 
                            time.sleep(0.1)
                            self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                        elif ( self.locateDirection == -1 ):
                            self.timedSpinRight((self.locateBlockSpinTime * 2)/1000) 
                            self.locateDirection = 1 
                            time.sleep(0.1)
                            self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                        else:
                            # Don't look any more - maybe some kind soul will move us towards the block and we'll go from there! 
                            pass 

                logger.info("end LOCATE COLOUR case")

            elif ( currentState == MOVE_TO_COLOUR ):
                logger.info("--- MOVE_TO_COLOUR ---")
                
                blockName, targetDistance = step.values 
                
                distance = self.robot.getEnvironment().getDistance() 
                logger.info("Moving To Colour - F = %f" % (distance.front))

                if ( distance.front > 0 and distance.front <= targetDistance):

                    self.robot.stopWheels() 
                    logger.info("We found the block - move on. ")
                    self.bvCamera.setSearchObjects([]) 
                    time.sleep(0.5)
                    self.moveToNextStep() 

                else:
                    # Set the motor speed based on the distance. 
                    diff = distance.front - targetDistance
                    #motorSpeed = min(self.minSpeed + ((diff / 50.0) * (self.maxSpeed - self.minSpeed)),self.maxSpeed)

                    # for initial testing use a fixed value
                    motorSpeed = self.moveToColourSpeed 

                    logger.info("Motor speed = %d" % (motorSpeed))

                    # Not there yet - let's just check we're still moving in the right direction. 
                    currentFrame, blocks = self.bvCamera.getCameraObjectsWithFrameCount()

                    if ( currentFrame != self.lastCameraFrame ):

                        self.lastCameraFrame = currentFrame
                        
                        for block in blocks:
                            logger.info("FOUND A BLOCK: [%d] %s" % (currentFrame, str((block.type, block.x, block.y))))
                            # Must be blocks we're looking for.  
                            x = block.x - self.blockCentreOffset
                            logger.info(f"x = {x}")
                            if x < 0:
                                #veerTime = min(abs((x*3) / 100) * MAX_VEER_TIME,MAX_VEER_TIME)  
                                logger.info("veering left")
                                self.veerLeft(motorSpeed) 
                            elif x > 0: 
                                #veerTime = min(abs((x*3) / 100) * MAX_VEER_TIME,MAX_VEER_TIME)  
                                logger.info("veering right")
                                self.veerRight(motorSpeed)
                            else:
                                logger.info("going straight at speed %d" % motorSpeed)
                                self.robot.moveForward(motorSpeed-10)
                            break 

            elif ( currentState == PICK_UP_BLOCK ):
                logger.info("--- PICK_UP_BLOCK ---")
                # Make sure we're the correct distance from the block. 
                currentDistance = self.robot.getEnvironment().getDistance().front
                logger.info("PICK_UP_BLOCK - current distance = %d" % (currentDistance))
                targetDistance = step.values 
                logger.info("PICK_UP_BLOCK - target distance = %d" % (targetDistance))
                
                if ( currentDistance < self.minBlockDistance ): 
                    motorSpeed = self.minSpeed  
                    logger.info("PICK_UP_BLOCK - moving backwards at speed = %d" % (motorSpeed))
                    self.robot.moveBackward(motorSpeed)
                else: 
                    self.robot.stopWheels()
                    self.forkLift.openGrip()
                    self.forkLift.moveToPosition(BlockPosition.BOTTOM) 
                    
                    # Move forward a tiny bit to get block in optimum position. 
                    logger.info("PICK_UP_BLOCK - moving forwards for half a second")
                    self.robot.moveForward(self.minSpeed)
                    time.sleep(0.3) 
                    self.forkLift.closeGrip() 
                    self.robot.stopWheels()
                    time.sleep(0.5) 
                    
                    # Pick up the block. 
                    self.forkLift.moveToPosition(BlockPosition.TOP) 
                    
                    self.moveToNextStep()  
                
            elif ( currentState == DROP_BLOCK ):
                logger.info("--- DROP_BLOCK ---")
                
                dropPosition, targetDistance = step.values 
                
                # Make sure we're at the correct distance, within 2 cm.  
                currentDistance = self.robot.getEnvironment().getDistance().front

                logger.info("DROP_BLOCK - current distance = %d" % (currentDistance))
                logger.info("DROP_BLOCK - target distance = %d" % (targetDistance))
                
                if ( abs(currentDistance - targetDistance) > 1 ): 
                    if ( currentDistance > targetDistance ):
                        self.robot.moveForward(self.minSpeed)
                    else:
                        self.robot.moveBackward(self.minSpeed) 
                        
                else: 
                    self.robot.stopWheels( )
                    
                    self.forkLift.moveToPosition(dropPosition) 
                    self.forkLift.openGrip()
                    self.forkLift.moveToPosition(BlockPosition.TOP)
                    self.moveToNextStep()

            elif ( currentState == FINISH ):
                # All done 
                #time.sleep(0.2)
                self.robot.moveBackward(self.minSpeed)
                time.sleep(0.5) 
                self.robot.stopWheels()
                self.waitingForStart = True  

        elapsedTime = time.time() - startTime 

        time.sleep(max(self.pollTime - elapsedTime, 0))

    # Override
    def stop(self):
        self.forkLift.stop()
        super().stop() 

    def veerRight(self, moveSpeed):
        self.robot.moveLeftWheel(moveSpeed)
        self.robot.moveRightWheel(0)
        time.sleep(self.pollTime/2)
        self.robot.moveForward(moveSpeed)
        
    def veerLeft(self, moveSpeed):
        self.robot.moveRightWheel(moveSpeed)
        self.robot.moveLeftWheel(0)
        time.sleep(self.pollTime/2)
        self.robot.moveForward(moveSpeed)

    def oldveerRight(self, moveSpeed, veerTime):
        self.robot.moveLeftWheel(moveSpeed)
        self.robot.moveRightWheel(0)
        time.sleep(veerTime)
        return veerTime
        
    def oldveerLeft(self, moveSpeed, veerTime):
        self.robot.moveRightWheel(moveSpeed)
        self.robot.moveLeftWheel(0)
        time.sleep(veerTime)
        return veerTime

    def timedSpinLeft(self, spinTime):
        self.robot.moveLeftWheel(self.spinSpeed * -1) 
        self.robot.moveRightWheel(self.spinSpeed) 
        time.sleep(spinTime)
        self.robot.stopWheels()
        time.sleep(0.2) 

    def timedSpinRight(self, spinTime):
        self.robot.moveRightWheel(self.spinSpeed * -1) 
        self.robot.moveLeftWheel(self.spinSpeed) 
        time.sleep(spinTime)
        self.robot.stopWheels()
        time.sleep(0.2) 

    def doSpin(self, spinState, spinType):
        
        if ( spinType is None ):
            spinType = SPIN_WITHOUT_BLOCK  

        spinTime = 0 
        
        if ( spinType == SPIN_WITH_BLOCK ):
            spinTime = self.ninetyDegreeTimeWithBlock 
        else:
            spinTime = self.ninetyDegreeTimeWithoutBlock 
        
        if ( spinState == SPIN_RIGHT ):
            self.timedSpinRight(spinTime/1000)
        else:
            self.timedSpinLeft(spinTime/1000) 
            
        # Wait until we get a new distance reading now that we've turned. 
        time.sleep(0.1)
        lastDistanceTimeStamp = self.robot.getEnvironment().getDistance().timestamp 
        while self.robot.getEnvironment().getDistance().timestamp <= lastDistanceTimeStamp:
                time.sleep(0.1)
    

    def moveInStraightLine(self, speed):
        if ( speed > 0 ):
            self.robot.moveForward(speed)
        else:
            self.robot.moveBackward(abs(speed)) 
    
    def setSearchObjects(self, blockName, colours):
        
        searchList = list()
        blockIndex = 1 
        for i in range(0,len(colours),3): 
            r = colours[i+0]
            g = colours[i+1]
            b = colours[i+2]
            
            currentBlockName = blockName + str(blockIndex)    

            logger.info(f"Setting Search objects: {currentBlockName}: {r},{g},{b}")
            searchList.append((currentBlockName, r, g, b))
        
        self.bvCamera.setSearchObjects(searchList)
    
    def moveToNextStep(self):
        
        self.currentStep += 1 
        if ( self.currentStep >= len(self.steps) ):
            # We should handle this with the 'FINISH' clause - so this is just a safety net. 
            self.currentStep = 0
            self.waitingForStart = True   
        
        # Do any special transistioning here. (Or should I create a transistion step?).
        step = self.steps[self.currentStep] 
        newState = step.state
        
        if ( (newState == LOCATE_COLOUR) or (newState == MOVE_TO_COLOUR) ):
            ''' Make sure the camera is set up to the correct colour. '''
            self.bvCamera.setCaptureMode(BlockyVisionCamera.BLOCK_MODE)
            self.bvCamera.setCropArea(self.findBlockCrop)
            blockName = step.values[0] 
            colours = self.blockColours[blockName]
            self.setSearchObjects(blockName, colours)  
            self.locateDirection = 0
            ''' lets make the eyes change to the same colour as the block we're going for. '''
            #self.robotEyes.setColour((r,g,b)); 
            time.sleep(0.1)
            #reset the frame count to make sure we actually search for a block. 
            self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()

        if ( (newState == MOVE_BACK_TO_DISTANCE) or (newState == MOVE_FWD_TO_DISTANCE) ):
            self.bvCamera.setCaptureMode(BlockyVisionCamera.HORIZON_MODE)
            self.bvCamera.setCropArea(self.findHorizonCrop)
            #self.robotEyes.setColour((TIDY_TOYS_MODE_EYE_COLOUR))
            time.sleep(0.1)
            self.straightLineMover.resetFrameCount()

