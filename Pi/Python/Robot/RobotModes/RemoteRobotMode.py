
import Robot.RobotEvent.RobotEvent as RobotEvent
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.RobotMode import RobotMode
from Robot.Config.RemoteRobotModeConfig import RemoteRobotModeConfig
import time
import logging

logger = logging.getLogger(__name__)

TEST_SPIN_SPEED=60

class DefaultJoystickMap(object):
    
    def __init__(self, minLeft, minRight, maxLeft, maxRight):
        self.minSpeedLeft = minLeft 
        self.minSpeedRight = minRight
        self.maxSpeedLeft = maxLeft
        self.maxSpeedRight = maxRight
    
    def mapLeftJoystick(self, xpos, ypos):
        
        #logger.debug("mapLeftJoystick: x=%d, y=%d", xpos, ypos)

        # Don't move backwards until over a threshold so our spins are more controlled
        if ((ypos < 0) and (ypos > -30)):
            ypos = 0

        leftWheelSpeed = ypos ;
        rightWheelSpeed = ypos ;

        # Make driving in a straight line easier by only turning if x pos over a threshold
        if ( xpos > -10 and xpos < 10 ):
            xpos = 0 

        if ( xpos < 0 ):
            rightWheelSpeed += abs(xpos)
            leftWheelSpeed += xpos
        else:
            leftWheelSpeed += xpos
            rightWheelSpeed -= xpos

        leftWheelSpeed = min(100, leftWheelSpeed)
        leftWheelSpeed = max(-100, leftWheelSpeed)
        rightWheelSpeed = min(100, rightWheelSpeed)
        rightWheelSpeed = max(-100, rightWheelSpeed)

        # adjust the speed according to the minimum and maximum speed. 
        if ( leftWheelSpeed > 0 ):
            leftWheelSpeed = ((leftWheelSpeed * (self.maxSpeedLeft - self.minSpeedLeft)) / 100) + self.minSpeedLeft
        elif ( leftWheelSpeed < 0 ):
            leftWheelSpeed = ((leftWheelSpeed * (self.maxSpeedLeft - self.minSpeedLeft)) / 100) - self.minSpeedLeft

        if ( rightWheelSpeed > 0 ):
            rightWheelSpeed = ((rightWheelSpeed * (self.maxSpeedRight - self.minSpeedRight)) / 100) + self.minSpeedRight
        elif ( rightWheelSpeed < 0 ):
            rightWheelSpeed = ((rightWheelSpeed * (self.maxSpeedRight - self.minSpeedRight)) / 100) - self.minSpeedRight
            
        return leftWheelSpeed, rightWheelSpeed 
    
    # Right joystick moves robot at half speed 
    def mapRightJoystick(self, xpos, ypos):
    
        leftWheelSpeed, rightWheelSpeed = self.mapLeftJoystick(xpos, ypos)
        
        return (leftWheelSpeed * 0.6), (rightWheelSpeed * 0.6)
        
    




class RemoteRobotMode(RobotMode):

    def __init__(self, robot, joystickMap=None):
        logger.info("Started Remote Robot Mode");
        self.laserTimerStarted = False 
        self.laserEndTime = 0 
        self.robot = robot
        self.config = RemoteRobotModeConfig(robot.getConfig().getConfigImpl())
        self.maxSpeedLeft = int(self.config.getMaxSpeedLeft("100"))
        self.maxSpeedRight = int(self.config.getMaxSpeedRight("100"))
        self.minSpeedLeft = int(self.config.getMinSpeedLeft("0"))
        self.minSpeedRight = int(self.config.getMinSpeedRight("0"))
        self.kickDurationMs = int(self.config.getKickDurationMs("300"))
        self.kickSpeedRight = int(self.config.getKickSpeedRight("100"))
        self.kickSpeedLeft = int(self.config.getKickSpeedLeft("100"))
        self.laserDuration = int(self.config.getLaserDurationMs("5000"))
        self.aimForwardSpeed = int(self.config.getAimForwardSpeed("40"))
        self.aimBackwardSpeed = int(self.config.getAimBackwardSpeed("30"))
        
        if ( joystickMap ):
            self.joystickMap = joystickMap 
        else:
            self.joystickMap = DefaultJoystickMap(self.minSpeedLeft, self.minSpeedRight, 
                                                  self.maxSpeedLeft, self.maxSpeedRight) 
            
        
        super().__init__(RobotModes.MODE_REMOTE)

    def handleEvent(self, robotEvent):
        handledEvent = False 
        eventType = robotEvent.getEventType()

        if ( eventType == RobotEvent.RE_GC_JOYSTICK_CHANGE ):
            if ( robotEvent.whichJoystick() == RobotEvent.JoystickChangeEvent.JOYSTICK_RIGHT ):
                self.joystickRightMoved(robotEvent.getXPos(), robotEvent.getYPos())
                handledEvent = True 
            elif ( robotEvent.whichJoystick() == RobotEvent.JoystickChangeEvent.JOYSTICK_LEFT ):
                self.joystickLeftMoved(robotEvent.getXPos(), robotEvent.getYPos())
                handledEvent = True 

        elif ( eventType == RobotEvent.RE_GC_TRIGGER_CHANGE ):
            if ( robotEvent.whichTrigger() == RobotEvent.TriggerChangeEvent.TRIGGER_L2 ):
                self.triggerL2AnalogChanged(robotEvent.getTriggerPos())
                handledEvent = True 
            elif ( robotEvent.whichTrigger() == RobotEvent.TriggerChangeEvent.TRIGGER_R2 ):
                self.triggerR2AnalogChanged(robotEvent.getTriggerPos())
                handledEvent = True 

        elif ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE):
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_L1 ):
                self.buttonL1Changed(robotEvent.getButtonValue())
                handledEvent = True 
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_R1 ):
                self.buttonR1Changed(robotEvent.getButtonValue())
                handledEvent = True 
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_X ):
                self.buttonXChanged(robotEvent.getButtonValue())
                handledEvent = True 
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_SQUARE ):
                self.buttonSquareChanged(robotEvent.getButtonValue())
                handledEvent = True 
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_CIRCLE ):
                self.buttonCircleChanged(robotEvent.getButtonValue())
                handledEvent = True 
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_LEFT ):
                self.buttonKeyPadLeftChanged(robotEvent.getButtonValue())
                handledEvent = True 
            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_RIGHT ):
                self.buttonKeyPadRightChanged(robotEvent.getButtonValue())
                handledEvent = True 

        handledEvent = handledEvent or super().handleEvent(robotEvent)
        return handledEvent


    def mainLoop(self):
        # handle fatal errors here. 
        if ( self.isFatalError ):
            # Make sure the robot stops if there's been a fatal error 
            self.robot.stopWheels()

        if ( self.laserTimerStarted ):
            if ( time.time() > self.laserStopTime ):
                self.laserTimerStarted = False 
                self.laserStopTime = 0 
                self.robot.setLaser(False)
 
        time.sleep(0.1)

    def stop(self):
        super().stop()

    def joystickLeftMoved(self, xpos, ypos):

        #logger.debug("joystickLeftMoved: x=%d, y=%d", xpos, ypos)
        leftWheelSpeed, rightWheelSpeed = self.joystickMap.mapLeftJoystick(xpos, ypos)

        self.robot.moveLeftWheel(int(leftWheelSpeed))
        self.robot.moveRightWheel(int(rightWheelSpeed))
   
    def joystickRightMoved(self, xpos, ypos):
        leftWheelSpeed, rightWheelSpeed = self.joystickMap.mapRightJoystick(xpos, ypos)

        self.robot.moveLeftWheel(int(leftWheelSpeed))
        self.robot.moveRightWheel(int(rightWheelSpeed))

    def buttonL1Changed(self, value):
        if ( value == 1 ):
            self.robot.moveLeftWheel(0 - self.maxSpeedLeft)
            self.robot.moveRightWheel(self.maxSpeedRight)
        else:
            self.robot.stopWheels()

    def buttonR1Changed(self, value):
        if ( value == 1 ):
            self.robot.moveRightWheel(0 - self.maxSpeedRight)
            self.robot.moveLeftWheel(self.maxSpeedLeft)
        else:
            self.robot.stopWheels()

    def buttonCircleChanged(self, value):
        if ( value == 1 ):
            self.robot.moveRightWheel(self.maxSpeedRight)
            self.robot.moveLeftWheel(self.maxSpeedLeft)
        else:
            self.robot.stopWheels()

    def calcAimSpeed( self, maxSpeed, minSpeed, triggerPos ):
        aimSpeed = (((maxSpeed - minSpeed) / 100) * triggerPos) + minSpeed
        return aimSpeed

    def adjustKickTime(self, adjustment):
        self.kickDurationMs += adjustment
        logger.info("Kick time changed to " + str(self.kickDurationMs))

    # The L2 trigger aims slowly to the left
    def triggerL2AnalogChanged(self, pos):
        if ( pos > 0 ):
            fwdSpeed = self.calcAimSpeed(self.aimForwardSpeed, self.minSpeedRight, pos)
            bwdSpeed = self.calcAimSpeed(self.aimBackwardSpeed, self.minSpeedLeft, pos)
            self.robot.moveLeftWheel(0 - bwdSpeed)
            self.robot.moveRightWheel(fwdSpeed)
        else:
            self.robot.stopWheels()

    # The R2 trigger aims slowly to the right 
    def triggerR2AnalogChanged(self, pos):
        if ( pos > 0 ):
            fwdSpeed = self.calcAimSpeed(self.aimForwardSpeed, self.minSpeedRight, pos)
            bwdSpeed = self.calcAimSpeed(self.aimBackwardSpeed, self.minSpeedLeft, pos)
            self.robot.moveRightWheel(0 - bwdSpeed)
            self.robot.moveLeftWheel(fwdSpeed)
        else:
            self.robot.stopWheels()

    def buttonXChanged(self, value):
        if ( value == 1 ):
            self.robot.stopWheels()
            self.robot.moveLeftWheel(self.kickSpeedLeft);
            self.robot.moveRightWheel(self.kickSpeedRight);
            time.sleep(self.kickDurationMs / 1000)
            self.robot.stopWheels()
            time.sleep(0.10)
            self.robot.moveLeftWheel(0-self.kickSpeedLeft) 
            self.robot.moveRightWheel(0-self.kickSpeedRight) 
            time.sleep(self.kickDurationMs / 1000)
            self.robot.stopWheels()

    def buttonSquareChanged(self, value):
        if ( value == 1 ):
            if ( not self.laserTimerStarted ):
                self.laserStopTime = time.time() + (self.laserDuration / 1000)
                self.laserTimerStarted = True 
                self.robot.setLaser(True)
            else:
                # switch off manually
                self.robot.setLaser(False)
                self.laserTimerStarted = False
                self.laserStopTime = 0 

    def buttonKeyPadLeftChanged(self, value):
        if ( value == 0 ):
            self.robot.timedSpinLeft(90)

    def buttonKeyPadRightChanged(self, value):
        if ( value == 0 ):
            self.robot.timedSpinRight(90)
        
