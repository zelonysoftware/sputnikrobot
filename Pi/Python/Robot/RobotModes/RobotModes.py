
''' 
 Defines all the available modes 
'''

MODE_REMOTE = "RemoteControl"
MODE_STRAIGHT_LINE = "StraightLine"
MODE_MINIMAL_MAZE = "MinimalMaze"
MODE_HUBBLE = "Hubble"
MODE_SPACE_INVADERS = "SpaceInvaders"
MODE_CHASE_BALL = "ChaseBall"
MODE_TIDY_TOYS = "TidyToys"
MODE_FEED_THE_FISH = "FeedTheFish"
MODE_SPEECH_CONTROL = "SpeechControl"
MODE_UP_THE_GARDEN_PATH = "UpTheGardenPath"
MODE_NANNY_MC_PI_REMOTE = "NannyMcPiRemote"
MODE_NANNY_MC_PI_DEMO = "NannyMcPiDemo"
