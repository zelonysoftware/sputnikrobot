
import Robot.RobotEvent.RobotEvent as RobotEvent
from Robot.RobotModes.RobotMode import RobotMode
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Config.RemoteRobotModeConfig import RemoteRobotModeConfig
from Robot.Environment.RobotEnvironment import RobotEnvironment
from Robot.Environment.RobotEnvironment import Distance
from Robot.Config.StraightLineSpeedModeConfig import StraightLineSpeedModeConfig
import time
import sys
import copy
import logging

logger = logging.getLogger(__name__)

"""
Straight line mover class that tries to stay parallel to the nearest wall. 
"""

class ParallelStraightLineMover():

    def __init__(self, robot, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight, fudgeFactor):
        self.robot = robot
        self.maxSpeedLeft = maxSpeedLeft
        self.maxSpeedRight = maxSpeedRight
        self.minSpeedLeft = minSpeedLeft
        self.minSpeedRight = minSpeedRight
        self.fudgeFactor = fudgeFactor
        self.averageSpeed = (self.maxSpeedLeft + self.maxSpeedRight) / 2 
        self.fudgeFactor /= self.averageSpeed 
        self.lastCheckTime = 0
        self.lastDistance = None 
        logger.info("Created Straight Line Mover")

    def moveInStraightLine(self):

        env = self.robot.getEnvironment()
        currentDistance = env.getDistance()
        veerTime = 0 

        logger.debug("currentDistance.front = " + str(currentDistance.front))

        if ( self.lastDistance == None ):
            logger.debug("First time - go straight")
            self.lastDistance = copy.copy(currentDistance)
            self.goStraight() 
        elif (currentDistance.timestamp > self.lastDistance.timestamp):
            logger.debug("New distance received")
            # This is the main straight line speed processing logic.
            leftSideValid = (currentDistance.left > 0) and (self.lastDistance.left > 0)
            rightSideValid = (currentDistance.right > 0) and (self.lastDistance.right > 0)

            logger.debug("leftSideValue = %s, rightSideValue = %s", str(leftSideValid), str(rightSideValid))

            if ( leftSideValid or rightSideValid ):

                useLeftSide = False 
                distanceDelta = 0 
                
                if ( leftSideValid and (not rightSideValid or (currentDistance.left < currentDistance.right)) ):
                    useLeftSide = True 
                    logger.info("useLeftSide - current left = " + str(currentDistance.left) + ", last left = " + str(self.lastDistance.left))
                    distanceDelta = currentDistance.left - self.lastDistance.left 
                else:
                    useLeftSide = False 
                    logger.info("useRightSide - current right = " + str(currentDistance.right) + ", last right = " + str(self.lastDistance.right))
                    distanceDelta = currentDistance.right - self.lastDistance.right 

                if ( distanceDelta != 0 ):
                    timeDelta = currentDistance.timestamp - self.lastDistance.timestamp 
                    
                    deltaRatio = distanceDelta / timeDelta 

                    veerTime = min(abs(deltaRatio) * self.fudgeFactor, 0.1)  

                    logger.info("distanceDelta = " + str(distanceDelta)) 
                    logger.info("timeDelta = " + str(timeDelta)) 
                    logger.info("veerTime = " + str(veerTime))

                    if ( (deltaRatio < 0 and useLeftSide) or (deltaRatio > 0 and not useLeftSide) ):
                        self.veerRight(veerTime)
                    else:
                        self.veerLeft(veerTime) 



        self.lastDistance = copy.copy(currentDistance)
        self.goStraight()

        return veerTime


    def goStraight(self):
        logger.debug("Go Straight")
        self.robot.moveRightWheel(self.maxSpeedRight)
        self.robot.moveLeftWheel(self.maxSpeedLeft)

    def veerRight(self, veerTime):
        logger.debug("Veer Right: %f", veerTime)
        self.robot.moveLeftWheel(self.maxSpeedLeft)
        self.robot.moveRightWheel(self.minSpeedRight)
        time.sleep(veerTime)
        
    def veerLeft(self, veerTime):
        logger.debug("Veer Left: %f", veerTime)
        self.robot.moveRightWheel(self.maxSpeedRight)
        self.robot.moveLeftWheel(self.minSpeedLeft)
        time.sleep(veerTime)

        
