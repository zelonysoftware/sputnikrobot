
import sys
import logging
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Config.UpTheGardenPathConfig import UpTheGardenPathConfig
from Robot.Environment.BlockyVisionCamera import BlockyVisionCamera 
from Robot.Environment.VoskVoiceRecogniser import VoskVoiceRecogniser
import time
import threading
from Robot.RobotEvent import RobotEvent
from Robot.Devices.RobotEyes import RobotEyes

logger = logging.getLogger(__name__)

UP_THE_GARDEN_PATH_MODE_EYE_COLOUR=(127,127,0)
WAITING_FOR_VOICE_EYE_COLOUR=(127,0,0)
HANDLE_VOICE_COMMAND_EYE_COLOUR=(0,0,127) 


''' Program States '''
START = 0
FOLLOWING_LINE = 1
WAITING_FOR_VOICE_COMMAND = 2
FINISH = 100   

class UpTheGardenPathMode(AutomatedRobotMode):

    def __init__(self, robot):
        try:

            logger.info("Started Up The Garden Path Mode")
            
            config = UpTheGardenPathConfig(robot.getConfig().getConfigImpl())
    
            self.isFatalError = False 
            
            self.pollTime = float(config.getPollTime()) / 1000.0

            self.maxSpeed = config.getMaxSpeed() 
            self.minSpeed = config.getMinSpeed()
            
            self.fastM = (self.minSpeed - self.maxSpeed) / (90 * 90)
            self.slowM = (0 - (self.maxSpeed*1.5)) / 90 
    
            self.lastCameraFrame = 0 
    
            self.robot = robot
    
            env = self.robot.getEnvironment()
            
            # The eyes 
            self.robotEyes = RobotEyes(robot.getConfig().getConfigImpl()) 
            self.robotEyes.start()
            self.robotEyes.setColour(UP_THE_GARDEN_PATH_MODE_EYE_COLOUR)
            
            # The voice recogniser
            self.voiceRecogniser = VoskVoiceRecogniser(self, robot.getConfig().getConfigImpl())
            self.voiceRecogniser.start()
            
            # At the moment we give up all pretence of using the environment - the Pixy camera and BlockyVision camera 
            # are too different. We assume we're using the blockvision camera. 
            self.bvCamera = env.getSmartCamera()
            
            # We can't do much if we don't have a camera, but we don't want the constructor to bomb out
            # if we're just passing through this mode without a camera, so we check for none. If the user actually 
            # tries to start this mode without a camera, then it will throw an exception the first time we try to use the 
            # camera.           
    
            if ( self.bvCamera != None ):
                self.bvCamera.setCaptureMode(BlockyVisionCamera.VECTOR_MODE)
                
                lineColour = BlockyVisionCamera.VECTOR_LINE_WHITE \
                                if ( config.isWhiteLine() == "True" ) \
                                    else BlockyVisionCamera.VECTOR_LINE_BLACK
                                     
                self.bvCamera.setVectorLineColour(lineColour)
                

            self.currentState = START
            
            self.lastVectorAngle = 0  
    
            self.voiceCommandLock = threading.Lock() 
            
            logger.info("UpTheGardenPath init complete")
            
        except:
            logger.error("Exception in UpTheGardenPath __init__() [%s]", sys.exc_info()[0])
            print("Exception in UpTheGardenPath __init__() [{}]".format(sys.exc_info()[0]))

        super().__init__(RobotModes.MODE_UP_THE_GARDEN_PATH)

    # TODO - may want to test voice commands using remote controller. 
    #        and possibly voice commands are passed as robot events? 
    def handleEvent(self, robotEvent):

        eventHandled = False 
        eventType = robotEvent.getEventType()
        
        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE ):

            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_LEFT ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.handleVoiceCommand('left')
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_RIGHT ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.handleVoiceCommand('right')
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_KEYPAD_UP ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.handleVoiceCommand('forward')
                eventHandled = True

            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_X ):
                if ( robotEvent.getButtonValue() == 0 ): 
                    self.handleVoiceCommand('go') 
                eventHandled = True 
                
            elif ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_TRIANGLE ):
                if ( robotEvent.getButtonValue() == 0 ):
                    self.handleVoiceCommand('stop')
                eventHandled = True 
                     
        if ( not eventHandled ):
            eventHandled = super().handleEvent(robotEvent)

        return eventHandled 

    
    def handleVoiceCommand(self, command):

        if ( len(command) > 0 ):
            logger.info("Received voice command: " + command)

        if ( not self.waitingForStart ):

            self.robotEyes.setColour(HANDLE_VOICE_COMMAND_EYE_COLOUR) 
        
            with self.voiceCommandLock:  
    
                command = command.lower() 
                
                if ( self.currentState == WAITING_FOR_VOICE_COMMAND ):
                    # Has some trouble with 'left' - so we have a whole host of alternatives!  
                    if (    ( 'left' in command ) 
                         or ( 'let' in command ) 
                         or ('lap' in command) 
                         or ('laugh' in command) 
                         or ('beth' in command) 
                         or ('yeah' in command) 
                         or ('the' in command) 
                         or ('there' in command)    ):
                    
                        self.robot.startSpinLeft(100)
                        time.sleep(0.1)
                        self.robot.stopWheels()
                      
                    elif ( ('right' in command) or ('write' in command) ):
                    
                        self.robot.startSpinRight(100)
                        time.sleep(0.1)
                        self.robot.stopWheels()
                      
                    elif (     ( 'forward' in command ) 
                            or ( 'on' in command ) 
                            or ( 'ahead' in command ) 
                            or ( 'head' in command ) 
                            or ( 'straight' in command )):
                    
                        self.robot.moveForward(100)
                        time.sleep(0.1)
                        self.robot.stopWheels()
                    
                    elif ( 'back' in command ):
                    
                        self.robot.moveBackward(100)
                        time.sleep(0.1)
                        self.robot.stopWheels()
                      
                    elif ( 'go' in command ):
                        self.currentState = FOLLOWING_LINE
                     
                    else:
                        # Keep the eye colour blue for short while so we can see the voice command has been received but not understood. 
                        time.sleep(0.75)
                        
                # The only command we currently process whilst following the line is the 'stop' command. 
                if (   ('stop' in command) 
                    or ('cop' in command)
                    or ('gop' in command)
                    or ('up' in command)
                    or ('top' in command) ):
                    self.robot.stopWheels()
                    self.currentState = WAITING_FOR_VOICE_COMMAND
                    # Just in case the line following thread restarts the robot in the middle of it's processing - we wait and make sure 
                    # it's stopped.  
                    time.sleep(0.5)
                    self.robot.stopWheels()
                     
                else:
                    # Keep the eye colour blue for short while so we can see the voice command has been received but not understood. 
                    time.sleep(0.75)

    # Override 
    def mainLoop(self):

        try:

            startTime = time.time() 
                
            if ( self.isFatalError ):
                self.robot.stopWheels()
                return 

            if ( self.waitingForStart ):
                self.robot.stopWheels() 
                self.currentStep = START

            if ( not self.waitingForStart ):

                #logger.info("currentState = %d", self.currentState)
                
                # Get the current vector and set the eye colours accordingly. 
                # We want instant feedback on whether we can continue - no matter which mode we're in. 
                vectors = self.getVectors() 
            
                if ( self.currentState == START ):
                    logger.info("--- START ---")
                    
                    logger.info("Up The Garden Path is go")

                    self.lastCameraFrame = 0 
            
                    self.currentState = WAITING_FOR_VOICE_COMMAND  

                elif ( self.currentState == FOLLOWING_LINE ):

                    logger.info("UTGP - Following Line")
                
                    if ( vectors is not None ):
                    
                        # currently we can only process a single line. 
                        if ( len(vectors) == 1 ): # 1 means we've just found a single line. 
                            logger.info("UTGP - found vector, angle = %d", vectors[0].getAngle());
                            vector = vectors[0]

                            vectorAngle = vector.getAngle()
                            absAngle = abs(vectorAngle) 

                            # Complicated maths to calculate the slow wheel speed depending on how severe
                            # the vector angle is. 
                            if ( absAngle < 90 ):
                                divisor = 45

                                fastWheel = self.maxSpeed
                                m = (self.minSpeed - self.maxSpeed) / divisor
                                c = self.maxSpeed
                                slowWheel = int((m * absAngle) + c)

                                slowWheel = min(self.maxSpeed, slowWheel) 
                                slowWheel = max((self.maxSpeed * -1), slowWheel)

                                if ( slowWheel < 15 and slowWheel >= 0 ):
                                    slowWheel = -15 
                                elif ( slowWheel > -15 and slowWheel <= 0 ):
                                    slowWheel = 15 

                            else:
                                fastWheel = self.maxSpeed * 0.6
                                slowWheel = fastWheel * -1 

                            #slowWheel = 0 #(self.slowM * absAngle) + self.maxSpeed
                            logger.info("UTGP - fastWheel = %f, slowWheel = %f", fastWheel, slowWheel)

                            #if (abs(slowWheel) < self.minSpeed):
                            #    slowWheel = 0.0 
                        
                            if ( vectorAngle < 0 ):
                                self.robot.moveLeftWheel(slowWheel)
                                self.robot.moveRightWheel(fastWheel)
                            else:
                                self.robot.moveLeftWheel(fastWheel)
                                self.robot.moveRightWheel(slowWheel)

                            # Save the angle in case we lose the line. 
                            self.lastVectorAngle = vectorAngle ;

                        
                        elif ( len(vectors) == 0 ):
                            # We can't find any line - so start rotating in the same direction we were last going.
                            if ( self.lastVectorAngle < 50 ):
                                self.robot.startSpinLeft((self.maxSpeed + self.minSpeed) * 0.75 )
                            elif ( self.lastVectorAngle > 50 ):
                                self.robot.startSpinRight((self.maxSpeed + self.minSpeed) * 0.75 )
                            else:
                                logger.info("UTGP - stopping due to no lines after straight and waiting for voice command") 
                                self.robot.stopWheels()
                                self.currentState = WAITING_FOR_VOICE_COMMAND  
                        else:
                            logger.info("UTGP - stopping and waiting for voice command") 
                            self.robot.stopWheels()
                            self.currentState = WAITING_FOR_VOICE_COMMAND  

                elif ( self.currentState == WAITING_FOR_VOICE_COMMAND ):

                    # Voice command will be in separate thread.                     
                    pass 

            elapsedTime = time.time() - startTime 

            time.sleep(max(self.pollTime - elapsedTime, 0))

        except:
            #logger.error("Exception in UpTheGardenPath mainloop() [%s]", sys.exc_info()[0])
            #print("Exception in UpTheGardenPath mainloop() [{}]".format(sys.exc_info()[0]))
            logger.error("Exception in UpTheGardenPath mainloop() [%s]", sys.exc_info()[0], exc_info=sys.exc_info())

    def getVectors(self):
            # Get the camera vectors. 
            currentFrame, vectors = self.bvCamera.getVectorsWithFrame()

            #logger.info("Up The Garden Path currentFrame = %d, lastFrame = %d" % (currentFrame,self.lastCameraFrame)) 

            if ( currentFrame != self.lastCameraFrame ):
                logger.info("UTGP - len(vectors) = %d, currentFrame = %d", len(vectors), currentFrame)
                    
                self.lastCameraFrame = currentFrame 

                if ( len(vectors) == 1 ):
                    vector = vectors[0]
                    self.moveEyes(vector.getAngle())
                    self.robotEyes.setColour(UP_THE_GARDEN_PATH_MODE_EYE_COLOUR)
                else:
                    # Think about putting in animation here - as if robot is looking for the line. 
                    self.crossEyes()
                    self.robotEyes.setColour(WAITING_FOR_VOICE_EYE_COLOUR)
            else:
                vectors = None 
                
            return vectors  
        
    def moveEyes(self, angle):
        eyeAngle = min((angle / 90.0)*100.0,100.0)
        self.robotEyes.moveBoth(eyeAngle) 
        
    def crossEyes(self):
        self.robotEyes.moveRight(-100)
        self.robotEyes.moveLeft(100)

    # Override
    def stop(self):
        self.robotEyes.stop()
        self.voiceRecogniser.stop()
        super().stop() 

