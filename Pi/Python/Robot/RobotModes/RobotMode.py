
from threading import Thread
import time 
import logging
import traceback

logger = logging.getLogger(__name__)

class RobotMode(Thread):

    def __init__(self, modeName):
        super().__init__()
        # Set the name of the thread 
        self.name = modeName + " Thread"
        self.modeName = modeName
        self.isStopped = False
        self.isFatalError = False

    def handleEvent(self, event):
        return False

    def mainLoop(self):
        logger.info("Oh No")
        time.sleep(1)

    def run(self):
        while ( not self.isStopped ):
            try:
                self.mainLoop()
            except:
                traceback.print_exc()
                #logger.error(traceback.format_exc())
                self.isFatalError = True ;

    def stop(self):
        self.isStopped = True 
        # wait for thread to stop (not using a timeout so thread mode really needs to stop. 
        self.join()

