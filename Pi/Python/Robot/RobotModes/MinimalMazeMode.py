
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.RobotModes.ParallelStraightLineMover import ParallelStraightLineMover
from Robot.RobotModes.EquidistanceStraightLineMover import EquidistantStraightLineMover
from Robot.Config.MinimalMazeModeConfig import MinimalMazeModeConfig
import time
import logging
from enum import Enum

logger = logging.getLogger(__name__)

class MinimalModeState(Enum):
    MOVING_FORWARD = 1
    DECIDING_WHICH_WAY = 2
    TURNING_LEFT = 3
    TURNING_RIGHT = 4

class MinimalMazeMode(AutomatedRobotMode):



    def __init__(self, robot):
        self.robot = robot
        
        self.config = MinimalMazeModeConfig(robot.getConfig().getConfigImpl())
        self.maxSpeedLeft = int(self.config.getMaxSpeedLeft("100"))
        self.maxSpeedRight = int(self.config.getMaxSpeedRight("100"))
        self.minSpeedLeft = int(self.config.getMinSpeedLeft("60"))
        self.minSpeedRight = int(self.config.getMinSpeedRight("60"))
        self.maxSpinSpeed = int(self.config.getMaxSpinSpeed("100"))
        self.totalTurnTime = int(self.config.getSpinTime("500"))
        self.pollTime = float(self.config.getPollTime("50")) / 1000.0
        self.stopDistance = int(self.config.getStopDistance("30"))
        fudgeFactor = float(self.config.getFudgeFactor("200"))

        self.currentState = MinimalModeState.MOVING_FORWARD 
        self.lastCheckTime = 0
        self.lastDistance = None 

        straightLineMode = self.config.getStraightLineMode("Parallel")

        if ( straightLineMode == "Parallel" ):
            self.straightLineMover = ParallelStraightLineMover( self.robot, 
                                                        self.maxSpeedLeft, self.maxSpeedRight, 
                                                        self.minSpeedLeft, self.minSpeedRight, 
                                                        self.pollTime * 2, fudgeFactor )
        else:
            self.straightLineMover = EquidistantStraightLineMover( self.robot, 
                                                                   self.maxSpeedLeft, self.maxSpeedRight, 
                                                                   self.minSpeedLeft, self.minSpeedRight, 
                                                                   self.pollTime * 2, fudgeFactor, False )

        logger.info("Started Minimal Maze Robot Mode")
        
        super().__init__(RobotModes.MODE_MINIMAL_MAZE)

    def handleEvent(self, robotEvent):

        # The super class handles the start button being pressed 
        return super().handleEvent(robotEvent)
            

    # The mainloop has to be responsive to actions, whilst also driving the robot. 
    # We therefore use a state machine so we can keep an eye on the 'waiingForStart' attribute. 
    def mainLoop(self):

        veerTime = 0 

        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        if ( not self.waitingForStart ):

            env = self.robot.getEnvironment()
            currentDistance = env.getDistance()

            logger.debug("currentState = " + str(self.currentState));

            if ( self.currentState == MinimalModeState.MOVING_FORWARD ):
                
                logger.debug("currentDistance.front = " + str(currentDistance.front))

                if ( currentDistance.front > 0 and currentDistance.front < self.stopDistance ):

                    self.robot.stopWheels()
                    self.currentState = MinimalModeState.DECIDING_WHICH_WAY
                else: 
                    veerTime = self.straightLineMover.moveInStraightLine()

            elif ( self.currentState == MinimalModeState.DECIDING_WHICH_WAY ):

                logger.info("Decide which way to go")
                logger.info(" left = " + str(currentDistance.left) + ", right = " + str(currentDistance.right))

                # We take a zero reading to be the furthest. (Hopefully our filtering of bad values is working). 
                if ( currentDistance.right == 0 or ((currentDistance.left > 0) and (currentDistance.right > currentDistance.left) )):
                    # We need to turn right
                    self.currentState = MinimalModeState.TURNING_RIGHT
                    self.turningStartTime = int(time.time() * 1000)
                    logger.info("Spin right");
                    self.robot.timedSpinRight(90)
                else:
                    # We need to turn left
                    self.currentState = MinimalModeState.TURNING_LEFT
                    self.turningStartTime = int(time.time() * 1000)
                    logger.info("Spin left");
                    self.robot.timedSpinLeft(90) 

            # Next state not really used since we do the spin synchronously
            elif ( self.currentState == MinimalModeState.TURNING_RIGHT or
                   self.currentState == MinimalModeState.TURNING_LEFT ):
                #if ( ((time.time() * 1000) - self.turningStartTime) > self.totalTurnTime ):
                self.robot.stopWheels()
                self.currentState = MinimalModeState.MOVING_FORWARD

        else:

            self.robot.stopWheels()

        time.sleep(max(0,self.pollTime-veerTime))
                

