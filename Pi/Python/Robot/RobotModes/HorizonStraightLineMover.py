
import time
import logging

logger = logging.getLogger(__name__)
""" 
This is a mover that tries to keep the robot in a straight line by keeping equidistance between the 2 sides. 
Optionally - the front offset sensors can be included to help the robot avoid obstacles 
"""
class HorizonStraightLineMover():

    MIN_VALID_DISTANCE = 80 # Ignore distances > 80cm 
    MAX_DELTA_RATIO = 0.03 # Maximum diagonal direction we want to travel - based on scientific measuring :)
    MAX_CENTRE_DIFFERENCE = 30.0 # Distance at which the max delta comes into force. 
    MAX_VEER_TIME_SECONDS = 0.05 
    MIN_FRONT_SENSOR_DISTANCE = 50.0 
    FRONT_ADJ_PCENT = 10 

    def __init__(self, robot, blockyCamera, maxVeerTimeSeconds, moveMultiplier):
        self.robot = robot
        self.blockyCamera = blockyCamera 
        self.maxVeerTimeSeconds = maxVeerTimeSeconds
        self.moveMultiplier = moveMultiplier
        self.maxAngle = 5
        self.lastCaptureFrame = 0 
        logger.info("Created Horizon Straight Line Mover ")

    def resetFrameCount(self):
        self.lastCaptureFrame, _ = self.blockyCamera.getHorizonWithFrame()

    
    def moveFwdInStraightLine(self, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight):
        self.doMoveInStraightLine(maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight, True)

    def moveBackInStraightLine(self, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight):
        self.doMoveInStraightLine(maxSpeedLeft*-1, maxSpeedRight*-1, minSpeedLeft*-1, minSpeedRight*-1, False)


    """
    doMoveInStraightLine is called within a robot mode's mainloop, so we expect this to be called repeatedly.
    """     
    def doMoveInStraightLine(self, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight, isFwd):

        veerTime = 0 
        
        captureFrame, horizon = self.blockyCamera.getHorizonWithFrame(); 

        logger.info("Got captureFrame = {}, horizon = {}".format(captureFrame,horizon))
        
        if ((horizon is not None) and (captureFrame != self.lastCaptureFrame)):
            
            self.lastCaptureFrame = captureFrame

            angle = horizon.getAngle(); 
            logger.info("HSLM: Horizon angle = {}".format(horizon.getAngle()))

            # Now we now how much we need to turn, we can work out the time needed to acheive that. 
            veerTime = min(abs(angle) * self.moveMultiplier, self.maxVeerTimeSeconds)  

            logger.debug("angle = " + str(angle))
            logger.debug("veerTime = " + str(veerTime))

            # If we're moving backwards, then we invert the angle so we veer in the opposite direction. 
            if ( isFwd is not True ):
                angle = angle * -1  

            if ( abs(angle) > self.maxAngle ):
                # Probably found the wall, so steer away from it. 
                angle = angle * -1 

            if ( (angle > 0) ):
                self.veerLeft(veerTime, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight)
            elif (angle < 0) :
                self.veerRight(veerTime, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight) 
            else:
                veerTime = 0 

        self.goStraight(maxSpeedLeft, maxSpeedRight)

        return(veerTime); 


    def goStraight(self, maxSpeedLeft, maxSpeedRight):
        logger.info("Go Straight")
        self.robot.moveRightWheel(maxSpeedRight)
        self.robot.moveLeftWheel(maxSpeedLeft)

    def veerRight(self, veerTime, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight):
        logger.info("Veer Right: %f", veerTime)
        self.robot.moveLeftWheel(maxSpeedLeft)
        self.robot.moveRightWheel(minSpeedRight)
        time.sleep(veerTime)
        
    def veerLeft(self, veerTime, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight):
        logger.info("Veer Left: %f", veerTime)
        self.robot.moveRightWheel(maxSpeedRight)
        self.robot.moveLeftWheel(minSpeedLeft)
        time.sleep(veerTime)

