
import time 
import logging
from Robot.Config.SpeechControlConfig import SpeechControlConfig
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotModes.RobotMode import RobotMode

logger = logging.getLogger(__name__)


class SpeechControlMode(RobotMode):

    def __init__(self, robot):
        logger.info("Started Speech control Mode");
        self.robot = robot
        self.config = SpeechControlConfig(robot.getConfig().getConfigImpl())
        
        self.speedLeft = self.config.getSpeedLeft()
        self.speedRight = self.config.getSpeedRight()
        
        self.straightUnitTime = self.config.getStraightUnitMs() / 1000.0 
        self.spinUnitTime = self.config.getSpinUnitMs() / 1000.0
        self.circleFwdUnitTime = self.config.getCircleFwdUnitMs() / 1000.0  
        self.circleSpinUnitTime = self.config.getCircleSpinUnitMs() / 1000.0
        
        super().__init__(RobotModes.MODE_SPEECH_CONTROL)

    def mainLoop(self):
        # handle fatal errors here. 
        if ( self.isFatalError ):
            # Make sure the robot stops if there's been a fatal error 
            self.robot.stopWheels()

        # Do - nothing, the speech recognition code is running in a separate thread.
        logger.info("SPEECH: MOVE FORWARD");
        self.moveForward(1)
        logger.info("SPEECH: MOVE BACKWARD");
        self.moveBackward(1)
        logger.info("SPEECH: SPIN LEFT");
        self.spinLeft(1)
        logger.info("SPEECH: SPIN RIGHT");
        self.spinRight(1)
        logger.info("SPEECH: MOVE CLOCKWISE");
        self.moveClockwise(10)
        logger.info("SPEECH: MOVE ANTICLOCKWISE");
        self.moveAntiClockwise(10) 
          
        time.sleep(2)
        
    def stop(self):
        
        ''' TODO - stop speech recognition here ''' 
        super().stop() 

    def moveForward(self, numUnits):
        
        self.doMove(numUnits * self.straightUnitTime, self.speedLeft, self.speedRight)
        
    def moveBackward(self, numUnits):
        
        self.doMove(numUnits * self.straightUnitTime, 0 - self.speedLeft, 0 - self.speedRight)
        
    def doMove(self, moveTime, speedLeft, speedRight):
        
        self.robot.moveLeftWheel(speedLeft)
        self.robot.moveRightWheel(speedRight)
        time.sleep(moveTime)
        self.robot.stopWheels()
        
    def spinRight(self, numUnits):
        self.doSpinRight(numUnits * self.spinUnitTime)
            
    def doSpinRight(self, spinTime):
        self.robot.startSpinRight(100)
        time.sleep(spinTime)
        self.robot.stopWheels()
        
    def spinLeft(self, numUnits):
        self.doSpinLeft(numUnits * self.spinUnitTime)
        
    def doSpinLeft(self, spinTime):
        self.robot.startSpinLeft(100)
        time.sleep(spinTime)
        self.robot.stopWheels()
        
    def moveClockwise(self, numUnits):
        
        for _ in range(0,numUnits):
            self.doMove(self.circleFwdUnitTime, self.speedLeft, self.speedRight)
            self.doSpinRight(self.circleSpinUnitTime)
        
    def moveAntiClockwise(self, numUnits):
        
        for _ in range(0,numUnits):
            self.doMove(self.circleFwdUnitTime, self.speedLeft, self.speedRight)
            self.doSpinLeft(self.circleSpinUnitTime)

