
import Robot.RobotEvent.RobotEvent as RobotEvent
from Robot.RobotModes.RobotMode import RobotMode
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.Config.RemoteRobotModeConfig import RemoteRobotModeConfig
from Robot.Environment.RobotEnvironment import RobotEnvironment
from Robot.Environment.RobotEnvironment import Distance
from Robot.Config.StraightLineSpeedModeConfig import StraightLineSpeedModeConfig
import time
import sys
import copy
import logging

logger = logging.getLogger(__name__)
""" 
This is a mover that tries to keep the robot in a straight line by keeping equidistance between the 2 sides. 
Optionally - the front offset sensors can be included to help the robot avoid obstacles 
"""
class EquidistantStraightLineMover():

    MIN_VALID_DISTANCE = 80 # Ignore distances > 80cm 
    MAX_DELTA_RATIO = 0.03 # Maximum diagonal direction we want to travel - based on scientific measuring :)
    MAX_CENTRE_DIFFERENCE = 30.0 # Distance at which the max delta comes into force. 
    MAX_VEER_TIME_SECONDS = 0.05 
    MIN_FRONT_SENSOR_DISTANCE = 50.0 
    FRONT_ADJ_PCENT = 10 

    def __init__(self, robot, maxSpeedLeft, maxSpeedRight, minSpeedLeft, minSpeedRight, maxVeerTimeSeconds, fudgeFactor, useFrontSensors = False):
        self.robot = robot
        self.maxSpeedLeft = maxSpeedLeft
        self.maxSpeedRight = maxSpeedRight
        self.minSpeedLeft = minSpeedLeft
        self.minSpeedRight = minSpeedRight
        self.maxVeerTimeSeconds = maxVeerTimeSeconds
        self.fudgeFactor = fudgeFactor
        self.averageSpeed = (self.maxSpeedLeft + self.maxSpeedRight) / 2 
        self.fudgeFactor /= self.averageSpeed
        self.useFrontSensors = useFrontSensors      
        self.lastCheckTime = 0
        self.lastDistance = None 
        logger.info("Created Equidistant Straight Line Mover - useFrontSensors = " + str(self.useFrontSensors))

    """
    moveInStraightLine is called within a robot mode's mainloop, so we expect this to be called repeatedly.     
    """
    def moveInStraightLine(self):

        env = self.robot.getEnvironment()
        currentDistance = env.getDistance()
        veerTime = 0 

        logger.debug("ESLM currentDistance.front = " + str(currentDistance.front))

        if ( self.lastDistance == None ):
            logger.debug("First time - go straight")
            self.lastDistance = copy.copy(currentDistance)
            self.goStraight() 
        elif (currentDistance.timestamp > self.lastDistance.timestamp):
            logger.debug("New distance received")
            logger.debug("F  = " + str(currentDistance.front))
            logger.debug("FL = " + str(currentDistance.frontLeft))
            logger.debug("FR = " + str(currentDistance.frontRight))
            logger.debug("L  = " + str(currentDistance.left))
            logger.debug("R  = " + str(currentDistance.right))

            currentLeft = currentDistance.left ;
            currentRight = currentDistance.right ; 

            # This is the main straight line speed processing logic.
            leftSideValid = (currentLeft > 0) and (self.lastDistance.left > 0) and (currentLeft < self.MIN_VALID_DISTANCE)
            rightSideValid = (currentRight > 0) and (self.lastDistance.right > 0) and (currentRight < self.MIN_VALID_DISTANCE)

            logger.debug("leftSideValue = %s, rightSideValue = %s", str(leftSideValid), str(rightSideValid))

            # Work out which angle we're travelling according to the nearest wall. 
            if ( leftSideValid or rightSideValid ):
                timeDelta = 0 
                distanceDelta = 0 

                if ( self.useFrontSensors ):
                    # Take into account the front sensors - bearing in mind we're moving towards anything they point to, so 
                    # we reduce by n %
                    logger.debug("currentDistance.frontLeft = " + str(currentDistance.frontLeft))
                    logger.debug("MIN_FRONT_SENSOR_DISTANCE = " + str(self.MIN_FRONT_SENSOR_DISTANCE))
                    logger.debug("self.lastDistance.frontLeft = " + str(self.lastDistance.frontLeft))
                    logger.debug("1 - " + str((currentDistance.frontLeft > 0) and (currentDistance.frontLeft < self.MIN_FRONT_SENSOR_DISTANCE)))
                    logger.debug("2 - " + str(( (self.lastDistance.frontLeft > 0) and (self.lastDistance.frontLeft < self.MIN_FRONT_SENSOR_DISTANCE) ))) 
                    logger.debug("3 - " + str(( currentDistance.frontLeft < self.lastDistance.frontLeft )))
                    if (    ( (currentDistance.frontLeft > 0) and (currentDistance.frontLeft < self.MIN_FRONT_SENSOR_DISTANCE) )
                        and ( (self.lastDistance.frontLeft > 0) and (self.lastDistance.frontLeft < self.MIN_FRONT_SENSOR_DISTANCE) ) 
                        and ( currentDistance.frontLeft < self.lastDistance.frontLeft ) ):

                        adjustedFrontLeft = (float(currentDistance.frontLeft) * (100.0 - self.FRONT_ADJ_PCENT)) / 100.0
                        
                        if ( leftSideValid ):
                            currentLeft = min(currentLeft, adjustedFrontLeft) 
                        else:
                            currentLeft = adjustedFrontLeft 
                            leftSideValid = True 
                            
                    if (    ( (currentDistance.frontRight > 0) and (currentDistance.frontRight < self.MIN_FRONT_SENSOR_DISTANCE) )
                        and ( (self.lastDistance.frontRight > 0) and (self.lastDistance.frontRight < self.MIN_FRONT_SENSOR_DISTANCE) ) 
                        and ( currentDistance.frontRight < self.lastDistance.frontRight ) ):
                            
                        adjustedFrontRight = (float(currentDistance.frontRight) * (100.0 - self.FRONT_ADJ_PCENT)) / 100.0
                        
                        if ( rightSideValid ):
                            currentRight = min(currentRight, adjustedFrontRight) 
                        else:
                            currentRight = adjustedFrontRight 
                            rightSideValid = True 
                         
                
                    logger.debug("After front sensor adjustment, left = %d, right = %d", currentLeft, currentRight); 


                if ( leftSideValid and (not rightSideValid or (currentLeft < currentRight)) ):
                    logger.debug("useLeftSide - current left = " + str(currentLeft) + ", last left = " + str(self.lastDistance.left))
                    distanceDelta = currentLeft - self.lastDistance.left 
                else:
                    logger.debug("useRightSide - current right = " + str(currentRight) + ", last right = " + str(self.lastDistance.right))
                    # invert the distance delta when taking measurements from the right sensor since we want distances to be relative to left
                    distanceDelta = 0 - (currentRight - self.lastDistance.right)

                deltaRatio = 0 
                if ( distanceDelta != 0 ):
                    timeDelta = currentDistance.timestamp - self.lastDistance.timestamp 
                    deltaRatio = distanceDelta / timeDelta 

                logger.debug("EDSLM - current delta ratio = " + str(deltaRatio))

                # So - now we know which direction we're moving - we need to work out how to stay within the 2 walls 
                if ( leftSideValid and rightSideValid ):

                    logger.debug("Try to stay in centre, left = %d, right = %d", currentLeft, currentRight); 
                    
                    # Are we closer to the left or the right. 
                    centreDifference = currentRight - currentLeft
                    if ( abs(centreDifference) < 5 ):
                        centreDifference = 0 

                    logger.debug("EDSLM - centreDifference = %d", centreDifference)

                    # Work out what our desired angle is to correct the centre spacing. 
                    desiredRatio = min(float(centreDifference) / self.MAX_CENTRE_DIFFERENCE * self.MAX_DELTA_RATIO, self.MAX_DELTA_RATIO);

                    logger.debug("EDSLM - desiredRatio = " + str(desiredRatio))

                    # Now work out how much we have to adjust our current distance/time ratio to acheive that. 
                    deltaRatio = desiredRatio - deltaRatio ;

                    logger.debug("EDSLM - new delta ratio = " + str(deltaRatio))
                else:
                    # Only using one side - so invert the current delta to work out how far in the opposite direction we need 
                    # to go in order to stay parallel. 
                    deltaRatio = 0 - deltaRatio

                # Now we now how much we need to turn, we can work out the time needed to acheive that. 
                veerTime = min(abs(deltaRatio) * self.fudgeFactor, self.maxVeerTimeSeconds)  

                logger.debug("distanceDelta = " + str(distanceDelta)) 
                logger.debug("timeDelta = " + str(timeDelta)) 
                logger.debug("deltaRatio = " + str(deltaRatio))
                logger.debug("veerTime = " + str(veerTime))

                if ( (deltaRatio < 0) ):
                    self.veerLeft(veerTime)
                elif (deltaRatio > 0) :
                    self.veerRight(veerTime) 
                else:
                    veerTime = 0 

            self.lastDistance = copy.copy(currentDistance)
            self.lastDistance.left = currentLeft 
            self.lastDistance.right = currentRight

        self.goStraight()

        return(veerTime); 


    def goStraight(self):
        logger.debug("Go Straight")
        self.robot.moveRightWheel(self.maxSpeedRight)
        self.robot.moveLeftWheel(self.maxSpeedLeft)

    def veerRight(self, veerTime):
        logger.debug("Veer Right: %f", veerTime)
        self.robot.moveLeftWheel(self.maxSpeedLeft)
        self.robot.moveRightWheel(self.minSpeedRight)
        time.sleep(veerTime)
        
    def veerLeft(self, veerTime):
        logger.debug("Veer Left: %f", veerTime)
        self.robot.moveRightWheel(self.maxSpeedRight)
        self.robot.moveLeftWheel(self.minSpeedLeft)
        time.sleep(veerTime)

        
