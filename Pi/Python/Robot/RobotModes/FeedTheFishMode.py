
import sys
import logging
import time 
import Robot.RobotModes.RobotModes as RobotModes
from Robot.Config.FeedTheFishConfig import FeedTheFishConfig
from Robot.RobotModes.AutomatedRobotMode import AutomatedRobotMode
from Robot.RobotModes.HorizonStraightLineMover import HorizonStraightLineMover
from Robot.Environment.BlockyVisionCamera import BlockyVisionCamera 
from Robot.Devices.FishCannon import FishCannon
from Robot.RobotEvent import RobotEvent
from Robot.Devices.RobotEyes import RobotEyes

FEED_THE_FISH_MODE_EYE_COLOUR=(0,127,0)
MAGNET_ON_EYE_COLOUR=(0,0,127)

logger = logging.getLogger(__name__)

''' Max time to spin (in secs) when locating fish tank '''
MAX_SPIN_TIME = 0.1  

''' Distance accuracy (in cm) when aiming at the fish tank. ''' 
DISTANCE_ACCURACY = 4.0

''' Program States '''
START = 0 
MOVE_FWD_TO_DISTANCE = 1
MOVE_BACK_TO_DISTANCE = 2
MOVE_TO_DISTANCE = 3

SPIN_LEFT = 4
SPIN_RIGHT = 5 

LOCATE_FISH_TANK = 6
FIRE_FISH_FOOD = 7

WAIT_FOR_GO = 8

FINISH = 100 

FISH_TANK_BLOCK_NAME = 'fishtank' 


MTD_STOPPED = 1 
MTD_MOVING_FORWARD = 2 
MTD_MOVING_BACKWARD = 3  

# Uses the same step class as the TidyToys mode. Perhaps we can put this into a common base class 
# if we keep on using this method. 
class Step():
    
    def __init__(self, state, values=None):
        
        self.state = state
        self.values = values 


class FeedTheFishMode(AutomatedRobotMode):
    
    def __init__(self, robot):
        try:

            logger.info("Started Feed The Fish Mode")
            
            feedTheFishConfig = FeedTheFishConfig(robot.getConfig().getConfigImpl())

            self.robot = robot
    
            # The eyes 
            self.robotEyes = RobotEyes(robot.getConfig().getConfigImpl()) 
            self.robotEyes.start()
            self.robotEyes.setColour(FEED_THE_FISH_MODE_EYE_COLOUR)

            self.isFatalError = False 
            
            self.pollTime = float(feedTheFishConfig.getPollTime("20")) / 1000.0
    
            self.fishTankRGB = feedTheFishConfig.getFishTankRGB()
    
            self.findBlockCropTop = feedTheFishConfig.getFindBlockCropTop()
            self.findBlockCropBtm = feedTheFishConfig.getFindBlockCropBtm()
            self.findBlockCrop = (0,self.findBlockCropTop,100,self.findBlockCropBtm)

            cropLeft = feedTheFishConfig.getHorizonCropLeft() 
            cropRight = feedTheFishConfig.getHorizonCropRight()
            self.findHorizonCrop = cropLeft,0,cropRight,100
    
            self.cannon = FishCannon(robot.getConfig().getConfigImpl()); 
            self.cannon.magnetOff() 

            self.lastCameraFrame = 0 

            self.maxSpeed = feedTheFishConfig.getMaxSpeed() 
            self.minSpeed = feedTheFishConfig.getMinSpeed()

            self.ninetyDegreeTime = feedTheFishConfig.getNinetyDegreeTurnTime()
    
            self.moveToDistanceSlowDownDistance = feedTheFishConfig.getMoveToDistanceSlowDownDistance()
            self.moveToDistanceSlowDownSpeed = feedTheFishConfig.getMoveToDistanceSlowDownSpeed()

            self.blockCentreLeft = feedTheFishConfig.getBlockCentreLeft()
            self.blockCentreRight = feedTheFishConfig.getBlockCentreRight() 
            self.locateBlockSpinTime = feedTheFishConfig.getLocateBlockSpinTime()
            
            self.locateDirection = 0 
    
            self.goButtonPressed = False 

            self.moveToDistanceStatus = MTD_STOPPED 
    
            self.robot = robot
            
            ''' Create the steps required for a single run ''' 
            fishFoodRunSteps = [Step(WAIT_FOR_GO),
                                Step(MOVE_FWD_TO_DISTANCE,70.0),
                                Step(SPIN_LEFT), 
                                Step(LOCATE_FISH_TANK), 
                                Step(MOVE_TO_DISTANCE,55),
                                Step(FIRE_FISH_FOOD), 
                                Step(SPIN_LEFT), 
                                Step(MOVE_FWD_TO_DISTANCE,30.0) ]

            fishFoodLastRunSteps = [Step(WAIT_FOR_GO),
                                    Step(MOVE_FWD_TO_DISTANCE,70.0),
                                    Step(SPIN_LEFT), 
                                    Step(LOCATE_FISH_TANK), 
                                    Step(MOVE_TO_DISTANCE,55),
                                    Step(FIRE_FISH_FOOD) ] 
            
            ''' Create the steps that we need to go through to solve the challenge from start to finish. '''
            self.fullSteps = [ Step(START) ] 
            self.fullSteps.extend( fishFoodRunSteps )
            self.fullSteps.extend( fishFoodRunSteps )
            self.fullSteps.extend( fishFoodLastRunSteps )
            self.fullSteps.append( Step(FINISH) ) 

            self.testSteps = [ Step(START), Step(WAIT_FOR_GO), Step(SPIN_LEFT), Step(LOCATE_FISH_TANK), Step(MOVE_TO_DISTANCE, 60), Step(FIRE_FISH_FOOD), Step(FINISH) ]
    
            self.steps = self.fullSteps
            
            self.currentStep = 0 
    
            env = self.robot.getEnvironment()

            # We must be using the blocky camera for this mode.                 
            self.bvCamera = env.getSmartCamera()
            
            # We can't do much if we don't have a camera, but we don't want the constructor to bomb out
            # if we're just passing through this mode without a camera, so we check for none. If the user actually 
            # tries to start this mode without a camera, then it will throw an exception the first time we try to use the 
            # camera.           
    
            if ( self.bvCamera != None ):
                self.bvCamera.setCropArea(self.findHorizonCrop)
                colourThreshold = feedTheFishConfig.getColourThreshold()
                self.bvCamera.setSearchThreshold(colourThreshold)
                
            maxStraightLineVeerTimeSeconds = feedTheFishConfig.getMaxStraightLineVeerTime()
            straightLineVeerMultiplier = feedTheFishConfig.getStraightLineVeerMultiplier() 
            
            self.straightLineMover = HorizonStraightLineMover(  self.robot, 
                                                                self.bvCamera, 
                                                                maxStraightLineVeerTimeSeconds, 
                                                                straightLineVeerMultiplier      ) 
    
            if ( self.bvCamera != None ):
                self.bvCamera.setCaptureMode(BlockyVisionCamera.HORIZON_MODE)
    
            logger.info("FeedTheFish Mode init complete")
            
        except:
            #logger.error("Exception in FeedTheFishMode __init__() [%s]", sys.exc_info()[0])
            #print("Exception in FeedTheFishMode __init__() [{}]".format(sys.exc_info()[0]))
            logger.error("Exception in FeedTheFishMode __init__() [%s]", sys.exc_info()[0], exc_info=sys.exc_info())

        super().__init__(RobotModes.MODE_FEED_THE_FISH)

    def handleEvent(self, robotEvent):

        eventHandled = False 
        eventType = robotEvent.getEventType()
        
        if ( eventType == RobotEvent.RE_GC_BUTTON_CHANGE ):

            if ( robotEvent.whichButton() == RobotEvent.ButtonChangeEvent.BUTTON_X ):
                if ( robotEvent.getButtonValue() == 0 ): 
                    self.goButtonPressed = True 
                eventHandled = True 
                     
        if ( not eventHandled ):
            eventHandled = super().handleEvent(robotEvent)

        return eventHandled 

    
    def mainLoop(self):

        startTime = time.time() 
                
        if ( self.isFatalError ):
            self.robot.stopWheels()
            return 

        if ( self.waitingForStart ):
            self.robot.stopWheels() 
            self.currentStep = 0
            self.cannon.magnetOff() 
            self.robotEyes.setColour(FEED_THE_FISH_MODE_EYE_COLOUR)


        if ( not self.waitingForStart ):

            step = self.steps[self.currentStep]
            currentState = step.state 

            logger.info("currentState = %d", currentState)
            
            if ( currentState == START ):
                logger.info("--- START ---")
                
                logger.info("Let's Feed Some Fishes!")

                self.lastCameraFrame = 0
                self.cannon.magnetOn()  
                self.robotEyes.setColour(MAGNET_ON_EYE_COLOUR)
                
                self.moveToNextStep() 
                
            elif ( currentState == WAIT_FOR_GO ):
                if ( self.goButtonPressed ):
                    self.goButtonPressed = False 
                    self.moveToNextStep()

            elif ( currentState == SPIN_LEFT ):
                logger.info("--- SPIN_LEFT ---")
                self.doSpin(SPIN_LEFT) 
                self.moveToNextStep() 
            
            elif ( currentState == SPIN_RIGHT ):
                logger.info("--- SPIN_RIGHT ---")
                self.doSpin(SPIN_RIGHT)
                self.moveToNextStep() 
                
            elif ( currentState == MOVE_BACK_TO_DISTANCE ):
                logger.info("--- MOVE_BACK_TO_DISTANCE ---")
            
                targetDistance = step.values
                currentDistance = self.robot.getEnvironment().getDistance().front
                logger.info("Current Distance = %d", currentDistance) 
                
                if ( currentDistance >= targetDistance ):
                    self.robot.stopWheels()
                    # move to next step
                    self.moveToNextStep( )
                    
                else: 
                    # Set the motor speed based on the distance. 
                    motorSpeed = self.maxSpeed 
                    diff = targetDistance - currentDistance 
                    if ( diff < self.moveToDistanceSlowDownDistance ):
                        motorSpeed = self.moveToDistanceSlowDownSpeed   
                        
                    self.straightLineMover.moveBackInStraightLine(motorSpeed, motorSpeed, 0, 0)
                
            elif ( currentState == MOVE_FWD_TO_DISTANCE ):
                logger.info("--- MOVE_FWD_TO_DISTANCE ---")
            
                targetDistance = step.values
                currentDistance = self.robot.getEnvironment().getDistance().front
                logger.info("Current Distance = %d", currentDistance) 
                
                if ( currentDistance <= targetDistance ):
                    self.robot.stopWheels()
                    # move to next step
                    self.moveToNextStep( )
                    
                else: 
                    # Set the motor speed based on the distance. 
                    diff = currentDistance - targetDistance 

                    motorSpeed = self.maxSpeed
                    if ( diff < self.moveToDistanceSlowDownDistance ):
                        motorSpeed = self.moveToDistanceSlowDownSpeed   
                    self.straightLineMover.moveFwdInStraightLine(motorSpeed, motorSpeed, 0, 0)
                
            elif ( currentState == MOVE_TO_DISTANCE ):
                logger.info("--- MOVE_TO_DISTANCE ---")
                targetDistance = step.values

                currentDistance = self.robot.getEnvironment().getDistance().front
                
                logger.info("Current Distance = %d", currentDistance) 
                
                if ( abs(currentDistance-targetDistance) <= DISTANCE_ACCURACY ):
                    self.robot.stopWheels()
                    time.sleep(0.5)
                    currentDistance = self.robot.getEnvironment().getDistance().front
                    logger.info("Check - Current Distance = %d", currentDistance) 
                    if ( abs(currentDistance-targetDistance) <= DISTANCE_ACCURACY ):
                        # move to next step
                        self.moveToNextStep( )
                    
                else: 
                    # Move slowly backwards or forwards - stopping first if we need to change direction. 
                    if ( currentDistance < targetDistance ):
                        if ( self.moveToDistanceStatus == MTD_STOPPED ):
                            self.robot.moveBackward(self.minSpeed)
                            self.moveToDistanceStatus = MTD_MOVING_BACKWARD 
                            
                        elif( self.moveToDistanceStatus == MTD_MOVING_FORWARD ): 
                            self.robot.stopWheels()
                            self.moveToDistanceStatus = MTD_STOPPED 
                    else:
                        if ( self.moveToDistanceStatus == MTD_STOPPED ):
                            self.robot.moveForward(self.minSpeed)
                            self.moveToDistanceStatus = MTD_MOVING_FORWARD 
                            
                        elif( self.moveToDistanceStatus == MTD_MOVING_BACKWARD ): 
                            self.robot.stopWheels()
                            self.moveToDistanceStatus = MTD_STOPPED 

            elif ( currentState == LOCATE_FISH_TANK ):
                logger.info("--- LOCATE_FISH TANK ---")
                blockName = FISH_TANK_BLOCK_NAME 
                logger.info("Feed the Fish locating %s, %s" % (blockName, self.fishTankRGB))

                # Get the camera objects. 
                currentFrame, blocks = self.bvCamera.getCameraObjectsWithFrameCount()

                logger.info("Feed the Fish currentFrame = %d, lastFrame = %d" % (currentFrame,self.lastCameraFrame)) 
                
                if ( currentFrame != self.lastCameraFrame ): 

                    self.lastCameraFrame = currentFrame
                    
                    if ( len(blocks) > 0 ):

                        for block in blocks:
                            # should be only one 
                            logger.info("FOUND A BLOCK: " + str((block.type, block.x, block.y)))
                            if ( block.type == blockName ):
                                if block.x < self.blockCentreLeft:
                                    spinTime = abs((block.x) / 100) * MAX_SPIN_TIME  
                                    logger.info("Spin left for %f seconds" % (spinTime))
                                    self.timedSpinLeft(spinTime) 
                                    time.sleep(0.5)
                                    self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                                elif block.x > self.blockCentreRight: 
                                    spinTime = abs((block.x) / 100) * MAX_SPIN_TIME  
                                    logger.info("Spin right for %f seconds" % (spinTime))
                                    self.timedSpinRight(spinTime) 
                                    time.sleep(0.5)
                                    self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                                else:
                                    # Block is centred enough - stop the camera searching for objects and move onto the next step. 
                                    self.bvCamera.setSearchObjects([]) 
                                    time.sleep(0.2)
                                    self.moveToNextStep() 
                                break
                            
                    else:
                        
                        # Can't find the block - try rotating a little (if we haven't already).
                        if ( self.locateDirection == 0 ):
                            self.timedSpinLeft(self.locateBlockSpinTime/1000)
                            self.locateDirection = -1 
                            time.sleep(0.5)
                            self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                        elif ( self.locateDirection == -1 ):
                            self.timedSpinRight((self.locateBlockSpinTime * 2)/1000) 
                            self.locateDirection = 1 
                            time.sleep(0.5)
                            self.lastCameraFrame, _ = self.bvCamera.getCameraObjectsWithFrameCount()
                        else:
                            # Don't look any more - maybe some kind soul will move us towards the block and we'll go from there! 
                            pass 

                logger.info("end LOCATE FISH TANK case")
                
            elif ( currentState == FIRE_FISH_FOOD ):
                
                time.sleep(0.5)
                self.cannon.fire() 
                self.robotEyes.setColour(FEED_THE_FISH_MODE_EYE_COLOUR)
                time.sleep(2.0)
                self.moveToNextStep()

            elif ( currentState == FINISH ):
                # All done 
                self.robot.stopWheels()
                self.cannon.magnetOff()
                self.robotEyes.setColour(FEED_THE_FISH_MODE_EYE_COLOUR)
                self.waitingForStart = True  

        elapsedTime = time.time() - startTime 

        time.sleep(max(self.pollTime - elapsedTime, 0))
        
        # Override
    def stop(self):
        self.cannon.magnetOff()
        super().stop()
         
    def timedSpinLeft(self, spinTime):
        self.robot.moveLeftWheel(100 * -1) 
        self.robot.moveRightWheel(100)
        time.sleep(spinTime)
        self.robot.stopWheels()
        time.sleep(0.2) 

    def timedSpinRight(self, spinTime):
        self.robot.moveRightWheel(100 * -1) 
        self.robot.moveLeftWheel(100) 
        time.sleep(spinTime)
        self.robot.stopWheels()
        time.sleep(0.2) 

    def doSpin(self, spinState ):
        
        spinTime = self.ninetyDegreeTime 
        
        if ( spinState == SPIN_RIGHT ):
            self.timedSpinRight(spinTime/1000)
        else:
            self.timedSpinLeft(spinTime/1000) 

    def moveToNextStep(self):
        
        self.currentStep += 1 
        if ( self.currentStep >= len(self.steps) ):
            # We should handle this with the 'FINISH' clause - so this is just a safety net. 
            self.currentStep = 0
            self.waitingForStart = True   
        
        # Do any special transistioning here. (Or should I create a transistion step?).
        step = self.steps[self.currentStep] 
        newState = step.state
        
        if ( newState == LOCATE_FISH_TANK):
            ''' Make sure the camera is set up to the correct colour. '''
            self.bvCamera.setCaptureMode(BlockyVisionCamera.BLOCK_MODE)
            self.bvCamera.setCropArea(self.findBlockCrop)
            r,g,b = self.fishTankRGB 
            self.bvCamera.setSearchObjects([(FISH_TANK_BLOCK_NAME, r,g,b)])
            self.locateDirection = 0 
            time.sleep(0.5)

        if ( (newState == MOVE_FWD_TO_DISTANCE) or (newState == MOVE_BACK_TO_DISTANCE) ):
            self.bvCamera.setCaptureMode(BlockyVisionCamera.HORIZON_MODE)
            self.bvCamera.setCropArea(self.findHorizonCrop)
            time.sleep(0.1)
            self.straightLineMover.resetFrameCount()
            
        if ( newState == WAIT_FOR_GO ):
            
            #Make sure the magnet is on as we're ready to load. 
            self.cannon.magnetOn()
            self.robotEyes.setColour(MAGNET_ON_EYE_COLOUR)

            self.goButtonPressed = False 
            
        if ( newState == MOVE_TO_DISTANCE ): 
            # This state doesn't use the camera - so disable it by setting it to block mode and 
            # clearing out the list of blocks to detect. 
            self.bvCamera.setCaptureMode(BlockyVisionCamera.BLOCK_MODE)
            self.bvCamera.setSearchObjects(list()) 
            self.robot.stopWheels()
            self.moveToDistanceStatus = MTD_STOPPED 
            
