
import statistics
import threading 
import time
import logging
from Robot.Environment.RobotEnvironment import Distance

logger = logging.getLogger(__name__)

class DistanceMonitor(threading.Thread):

    # How long to wait between polls. 
    DELAY_S = (20.0 / 1000.0)
    DELAY2_S = (5.0 / 1000.0)
    
    # NUM_AVERAGED_SAMPLES 
    NUM_SAMPLES = 10
    
    # TODO -  consider taking a list of sensors. 
    def __init__(self, environment, sonarSensor, direction):

        self.environment = environment 
        self.sonarSensor = sonarSensor 
        self.direction = direction # TODO - assume this is always the front for now. 
        self.stopDistanceThread = False
        self.currentDistance = 0  
        super().__init__();
        self.name = "Distance Monitor Thread";

    def run(self):

        while not self.stopDistanceThread:

            # take n readings and use the max to eliminate spikes (it's been seen that spikes tend to see a distance too short).
            # in fact - we'll take the 2nd maximum value - just in case there is a single spike showing a distance that is too long.
            # (Note - although we don't need the distances list - it's there in case we want to calculate the mean and/or median for analysis).  
            distances = [0] * self.NUM_SAMPLES
            
            firstMaxDistance = -1 
            secondMaxDistance = -1 
            
            for i in range(0, self.NUM_SAMPLES):
                distances[i] = self.sonarSensor.getDistance()
                if ( distances[i] > firstMaxDistance ):
                    secondMaxDistance = firstMaxDistance
                    firstMaxDistance = distances[i]
                elif ( distances[i] > secondMaxDistance ):
                    secondMaxDistance = distances[i] 
                #time.sleep(DistanceMonitor.DELAY2_S)

            #maxDistance = max(distances) 
            #medianDistance = statistics.median(distances)
            #meanDistance = statistics.mean(distances)
            self.environment.setDistance(Distance(front=secondMaxDistance))

            #for i in range(0, self.NUM_SAMPLES):
            #    logger.info(f"DISTANCE_LOG: {i}, {distances[i]}, {maxDistance}, {secondMaxDistance}, {medianDistance}, {meanDistance}")
            
            time.sleep(DistanceMonitor.DELAY_S)


    def stop(self):
        self.stopDistanceThread = True 
        self.join(5) 
        

    
