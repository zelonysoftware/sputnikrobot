
import threading
import copy
import time
import logging

logger = logging.getLogger(__name__)

class Distance():

    def __init__( self, left=0.0, right=0.0, front=0.0, frontLeft=0.0, frontRight=0.0 ):
        self.left = left ;
        self.right = right ;
        self.front = front ;
        self.frontLeft = frontLeft ;
        self.frontRight = frontRight ;
        self.timestamp = int(time.time() * 1000) 

class Heading():

    def __init__( self, headingValue ):
        self.value = headingValue
        self.timestamp = int(time.time() * 1000)

class RobotEnvironment():

    def __init__(self):
        self.distance = Distance(0,0,0,0,0)
        self.heading = Heading(-1) 
        self.camera = None 
        
        self.distanceLock = threading.Lock()
        self.headingLock = threading.Lock()

    def setDistance( self, distance ):

        with self.distanceLock:

            # Only copy valid values. 
            if ( distance.left >= 0 ):
                self.distance.left = distance.left
            if ( distance.right >= 0 ):
                self.distance.right = distance.right
            if ( distance.front >= 0 ):
                self.distance.front = distance.front
            if ( distance.frontLeft >= 0 ):
                self.distance.frontLeft = distance.frontLeft
            if ( distance.frontRight >= 0 ):
                self.distance.frontRight = distance.frontRight
            self.distance.timestamp = int(time.time() * 1000)

            logger.debug("%d: L FL F FR R = %f, %f, %f, %f, %f",
                             distance.timestamp,
                             distance.left,
                             distance.frontLeft,
                             distance.front,
                             distance.frontRight,
                             distance.right )
                        
    def getDistance(self):
        
        with self.distanceLock:
            distanceCopy = copy.copy(self.distance)

        return distanceCopy

    def setHeading(self, heading):

        with self.headingLock:
            if ( heading >= 0 ):
                self.heading.value = heading
                self.heading.timestamp = int(time.time() * 1000)
                #logger.info("Received new heading: %d (%d)", self.heading.value, self.heading.timestamp)

    def getHeading(self):
        
        with self.headingLock:
            headingCopy = copy.copy(self.heading)

        return headingCopy

    
    """ The compass heading is liable to drift a bit so we need a way to try and get a stable heading. 
        To do this we check the samples until we get 3 different values that are within 3 degrees of each other. """
    def getStableHeading(self):

        maxSecondsToSearch = 2
        steadyHeadingFound = False 
        
        lastSample = self.getHeading()
        logger.info("getStableHeading() - initial heading = %d", lastSample.value)

        if ( lastSample.value < 0 ):
            # Not getting heading information - so return the invalid value
            return lastSample
    
        numStableSamples = 1
        startTime = time.time() 
        
        while ( not steadyHeadingFound and ((time.time() - startTime) < maxSecondsToSearch) ):
            currentSample = self.getHeading() 

            if ( currentSample.timestamp > lastSample.timestamp ):
                logger.info("getStableHeading() - new heading = %d", currentSample.value)
                # New sample received. 
                diff = abs(lastSample.value - currentSample.value)
                if ( (diff < 3) or ((360-diff) < 3) ):
                    numStableSamples += 1 
                    if ( numStableSamples == 3 ):
                        steadyHeadingFound = True 
                else:
                    numStableSamples = 1 
                
                lastSample = currentSample 
            
            time.sleep(0.05)
            
        if ( not steadyHeadingFound ):
            logger.warn("Steady Heading not found within %d seconds", maxSecondsToSearch) 
            
        return self.getHeading() 

    def setSmartCamera(self, camera):
        self.camera = camera 
        
    def getSmartCamera(self):
        return self.camera 
        
    def getCameraObjects(self): 
        if ( self.camera != None ):
            return self.camera.getCameraObjects()      
        return None

    def getCameraFrameRate(self):
        if ( self.camera != None ):
            return self.camera.getFrameRate() 
        return None
        
    def setCameraSearchObjects(self, searchObjects):
        logger.info("XXX - Set camera search objects"); 
        if ( self.camera != None ):
            logger.info("XXX - found camera")
            self.camera.setSearchObjects(searchObjects)   
            logger.info("XXX - done")

    def setCameraLamp(self, isLampOn):
        if ( self.camera != None ):
            return self.camera.setCameraLamp(isLampOn)
