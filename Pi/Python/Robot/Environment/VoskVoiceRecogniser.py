
import threading
import os
import sounddevice as sd
import vosk
import json
import logging

from Robot.Config.VoskVoiceRecogniserConfig import VoskVoiceRecogniserConfig

logger = logging.getLogger(__name__)

class VoskVoiceRecogniserThread(threading.Thread):

    def __init__(self, handler, modelLocation, samplerate): 
        self.voiceHandler = handler 
        self.modelLocation = modelLocation
        self.samplerate = samplerate
        self.isRunning = True 
        super().__init__()
        self.name="VoskVoiceRecogniser"

    def run(self):

        if not os.path.exists(self.modelLocation):
            logger.error(f"Model {self.modelLocation} not found")
            return

        model = vosk.Model(self.modelLocation)

        with sd.RawInputStream( samplerate=self.samplerate,
                                dtype='int16',
                                channels=1 ) as inputStream:


            rec = vosk.KaldiRecognizer(model, self.samplerate)

            while self.isRunning:
                inbuffer, _ = inputStream.read(4000)
                data = bytes(inbuffer)
                text = None
                if rec.AcceptWaveform(data):
                    jsonResult = rec.Result()
                    text = json.loads(jsonResult)["text"]
                    self.voiceHandler.handleVoiceCommand(text)

        logger.info("Exiting Vosk voice thread")


    def stop(self):
        self.isRunning = False 
        self.join()

class VoskVoiceRecogniser():

    def __init__(self, voiceHandler, robotConfig):
                
        voiceConfig = VoskVoiceRecogniserConfig(robotConfig) 
        
        self.voiceRecogniserThread = None 
        self.voiceHandler = voiceHandler
        self.modelLocation = voiceConfig.getModelLocation() 
        self.modelSampleRate = voiceConfig.getModelSampleRate() 

    def start(self):

        if ( self.voiceRecogniserThread is not None ):
            self.voiceRecogniserThread.stop() 
        
        self.voiceRecogniserThread = VoskVoiceRecogniserThread(self.voiceHandler, self.modelLocation, self.modelSampleRate)
        self.voiceRecogniserThread.start()

    def stop(self):

        if ( self.voiceRecogniserThread is not None ):
            self.voiceRecogniserThread.stop() 
            self.voiceRecogniserThread = None 


