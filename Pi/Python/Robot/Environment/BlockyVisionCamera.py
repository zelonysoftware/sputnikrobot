

from BlockyVision.ClientServer.Server.BVServer import BVServer
from PIL import Image 
import io
import os
import time 
import picamera
import logging
import threading 
import traceback

from Robot.Environment.SmartCamera import SmartCamera
from Robot.Environment.SmartCamera import CameraObject
from BlockyVision.BVBlockFinder import BVBlockFinder 
from BlockyVision.BVHorizonFinder import BVHorizonFinder 
from BlockyVision.BVVectorFinder import BVVectorFinder
from BlockyVision.Bitmap import Pixel 
from threading import Thread

logger = logging.getLogger(__name__)

class BlockyVisionCamera(SmartCamera):
    
    HORIZON_MODE = 0
    BLOCK_MODE = 1
    VECTOR_MODE = 2 
    
    VECTOR_LINE_BLACK = 0 
    VECTOR_LINE_WHITE = 1 

    def __init__(self, config):

        self.captureLock = threading.Lock()
        self.captureThread = None

        self.frameId = 0 
        self.currentImage = None 

        self.capturedObjects = list()  
        self.captureFrame = 0 
        self.cropArea = (0,0,100,100)

        self.takePhotoTrigger = False ;
        
        self.horizon = None
        
        self.vectors = None  
        
        self.captureMode = BlockyVisionCamera.BLOCK_MODE

        self.camera = picamera.PiCamera()
        
        self.camera.rotation = config.getCameraRotation(default="180") 

        self.camera.iso=config.getIso(default="400")             
        frameRate = config.getFrameRate(default="20")
        logger.info("Setting camera framerate to %d", frameRate) 
        self.camera.framerate=config.getFrameRate(default="20") 

        self.numRows = config.getImageHeight(default="96") 
        self.numCols = config.getImageWidth(default="128") 
        self.pollTimeMs = config.getPollTime(default="20")

        self.saveFolder = config.getSaveFolder(default=".")

        self.currentFrameRate = None 
        self.frameRateInterval = 2.0

        #using resize instead of resolution. 
        #self.camera.resolution = (self.numCols, self.numRows)
        self.searchCameraObjects = dict() 

        # Create a block finder so we don't have to keep creating one. 
        self.blockFinder = BVBlockFinder()
        self.blockFinder.setMinBlockWidthPcent(3) # TODO - add to config
        self.blockFinder.setMinBlockHeightPcent(3) # TODO - add to config

        searchThreshold = config.getThreshold(default="20")
        self.blockFinder.setThreshold(searchThreshold)
        
        # By default we look for a black line on a white background when finding vectors. 
        self.vectorLineColour = BVVectorFinder.BLACK
        
        self.turboLibPath = config.getTurboLibPath(default="")
        if ( self.turboLibPath ):
            self.blockFinder.setTurboFinderLib(self.turboLibPath)

        time.sleep(2)

        # Output the initial camera settings before we change them - so we can tune the config. 
        logger.info("CAMERA: awb_gains = %s, exposure_speed = %s", str(self.camera.awb_gains), str(self.camera.exposure_speed))

        self.camera.awb_mode = 'off' 
        self.camera.awb_gains = config.getAwbGains(default="1.3,2.7")
        self.camera.exposure_mode ='off' 
        self.camera.shutter_speed = config.getShutterSpeed(default="16985") 

        # Create a server so that we can see what's happening on the BlockyVision GUI
        serverPort = config.getServerPort(default="20060") 
        self.server = BVServer(serverPort)
        self.server.start()

    # Allow us to switch vector line colour between Black (dark line on light background) 
    # or White (light line on dark background). 
    def setVectorLineColour(self, vectorLineColour):
        if ( vectorLineColour == BlockyVisionCamera.VECTOR_LINE_BLACK ):
            self.vectorLineColour = BVVectorFinder.BLACK 
        else:
            self.vectorLineColour = BVVectorFinder.WHITE  
            
                
    def setSearchThreshold(self, threshold):
        self.blockFinder.setThreshold(threshold)
        
    def setSearchObjects(self, searchObjects):

        logging.info("CAMERA - set search objects %s", str(searchObjects))
        self.blockFinder.clearBlocksToFind() 
        
        with self.captureLock:
            #searchObjects is a list of tuples - each tuple having format ('name', r, g, b) 
            self.searchCameraObjects = dict()
            broadcastBlocks = list() # list for broadcasting blocks to any interested clients. 
            for searchObject in searchObjects:
                
                name = searchObject[0]
                r = searchObject[1]
                g = searchObject[2]
                b = searchObject[3] 

                logger.info(f"Setting search object {name}, {r},{g},{b}")
                
                pixel = Pixel((r,g,b))
                self.searchCameraObjects[name] = pixel
                self.blockFinder.setBlockColour(name, pixel)
                
                broadcastBlocks.append((name, pixel))
                
        self.server.broadcastBlockConfig(broadcastBlocks)

        return  
    
    '''
    Get search object pixel value given a type (block name). 
    '''
    def getSearchObject(self, blockType):
        
        return self.searchCameraObjects.get(blockType)
    
    def takePhoto(self): 
        with self.captureLock:
            self.takePhotoTrigger=True 
    
    def setCaptureMode(self, newMode):
        with self.captureLock:
            self.captureMode = newMode 
            
    """ override from SmartCamera. Set crop area in terms of percentages (0,0,100,100) means the whole image. """
    def setCropArea(self, x1y1x2y2):
        logging.info("CAMERA - set crop area %s", x1y1x2y2)
        with self.captureLock:
            self.cropArea = x1y1x2y2 
        return 
    
    def resetCropArea(self):
        logging.info("CAMERA - reset crop area")
        with self.captureLock:
            self.cropArea = (0,0,100,100)
        return 
    
    """ public method overides SmartCamera getCameraObjects() """
    def getCameraObjects(self):
        with ( self.captureLock ):
            if ( self.captureThread is not None ):
                return self.capturedObjects 
            
        return self.doGetCameraObjects() 
    
    def getCameraObjectsWithFrameCount(self):
        with ( self.captureLock ):
            if ( self.captureThread is not None ):
                return self.captureFrame, self.capturedObjects 
            
        return 0, self.doGetCameraObjects() 
    
    def getImageWithFrameCount(self): 
        with ( self.captureLock ):
            if ( self.captureThread is not None ):
                return self.frameId, self.currentImage  
            
        return 0, self.doGetCameraImage() 

    def getFrameRate(self):
        return self.currentFrameRate ;
    
    def getHorizon(self):
        with ( self.captureLock ):
            if ( self.captureThread is not None ):
                return self.horizon 
        
        return self.doGetCameraHorizon() 
    
    def getHorizonWithFrame(self):
        with ( self.captureLock ):
            if ( self.captureThread is not None ):
                return self.captureFrame, self.horizon 
        
        return 0, self.doGetCameraHorizon()
    
    def getVectorsWithFrame(self):
        with ( self.captureLock ):
            if ( self.captureThread is not None ):
                return self.captureFrame, self.vectors
            
        return 0, self.doGetCameraVectors() 
    
    

    """ private method that gets physical images - only used by this class and the capture thread """
    """ NOTE - this is not generally used - it is more efficient to start the capture thread and get the latest image that way. """ 
    def doGetCameraObjects(self):
        
        stream = io.BytesIO() 

        self.camera.capture(stream, 'bmp', resize=(self.numCols, self.numRows),use_video_port=True)

        stream.seek(0)
        pilImage = Image.open(stream)
        
        if ( len(self.searchCameraObjects) == 0 ):
            # Nothing to look for - so return an empty list
            return list() 

        objectList = self.doFindBlocks(pilImage)

        return objectList


    """ Again - here for convenience, but we really want to use the capture thread """     
    """ - although this might be fast enough that we can get it as we need it. """
    def doGetCameraHorizon(self):

        stream = io.BytesIO() 

        self.camera.capture(stream, 'bmp', resize=(self.numCols, self.numRows),use_video_port=True)

        stream.seek(0)
        pilImage = Image.open(stream)
        
        horizon = self.doFindHorizon(pilImage)

        return horizon
    
    """ As before - we don't normally call this one due to the capture thread """
    def doGetCameraVectors(self):
        
        stream = io.BytesIO()
        
        self.camera.capture(stream, 'bmp', resize=(self.numCols, self.numRows), use_video_port=True)

        stream.seek(0)
        pilImage = Image.open(stream) 
        
        vectors = self.doFindVectors(pilImage)
        
        return vectors 

    """ As before - don't use this - use the capture thread - this is here for a one off 
        if we haven't started a capture thread for some reason. """ 
    def doGetCameraImage(self):
        
        stream = io.BytesIO() 
        
        self.camera.capture(stream, 'bmp', resize=(self.numCols, self.numRows), use_video_port=True)
        
        stream.seek(0)
        pilImage = Image.open(stream)
        
        return pilImage 
         

    """ Common routine to find blocks in an image. """ 
    def doFindBlocks(self, pilImage):

        #logger.info("BlockyVisionCamera: Do Find Blocks") 
        objectList = list()

        if ( len(self.searchCameraObjects.items()) == 0 ):
            return objectList

        blockList = self.blockFinder.findBlocks(pilImage, self.cropArea) 

        for block in blockList:
            #block.printBlock()

            centre = block.getCentre()

            # normalise - we want a co-ordinate -100 to +100 on both axis. 
            x = ((centre[0] * 200) / self.numCols) - 100 
            y = ((centre[1] * 200) / self.numRows) - 100 
            
            # also normalise the width and height to be a percentage. 
            width = ((block.getNumCols() * 100) / self.numCols)
            height = ((block.getNumRows() * 100) / self.numRows)

            logger.debug(f"Found block {block.getName()}: x={x}, y={y}, width={width}, height={height}")

            objectList.append( CameraObject( block.getName(), 
                                x,
                                y,
                                width,
                                height ) )

        return objectList 

    def setCapturedObjects(self, cameraObjects):
        
        with self.captureLock: 
            self.capturedObjects = cameraObjects 
            self.captureFrame = self.frameId 

    def doFindHorizon(self, pilImage):

        horizonFinder = BVHorizonFinder(pilImage)
        horizon = horizonFinder.findHorizon(self.cropArea)
            
        return horizon  

    def setHorizon(self, horizon):
        
        with self.captureLock:
            self.horizon = horizon  
            self.captureFrame = self.frameId 
            #if ( horizon is not None ):
                #logger.info("Horizon angle = {}".format(horizon.getAngle()))
            #else:
                #logger.info("Horizon is None")

    def doFindVectors(self, pilImage):

        #originX = int(pilImage.width / 2) 
        #originY = pilImage.height - 1 
        
        with (self.captureLock):

            vectorFinder = BVVectorFinder(pilImage, lineColour=self.vectorLineColour)
            
            vectors = vectorFinder.findVectors()
            
        return vectors  
    
    def setVectors(self, vectors):
        
        with self.captureLock:
            self.vectors = vectors 
            self.captureFrame = self.frameId
        
    def stopServer(self):
        self.server.stopServer()
        if ( self.captureThread is not None ):
            self.stopCaptureThread()

    def setCameraLamp(self, switchLampOn):
        # Not applicable to pi camera (unless we add an external lamp). 
        pass 
        
    """ Consider implementing the capture thread in this class """
    def startCaptureThread(self):
        if ( self.captureThread is None ):
            self.captureThread = CaptureThread(self, self.pollTimeMs)
            self.captureThread.startThread() 
             
    
    def stopCaptureThread(self):
        if ( self.captureThread is not None ):
            self.captureThread.stopThread() 
            self.captureThread = None 

""" If we changed the Smart Camera base class to do the capture image call, then we could move this capture thread into the base class """
class CaptureThread(Thread):
    
    def __init__(self, camera, delayMs):

        super().__init__()
        
        self.theCamera = camera
        self.isRunning = False
        self.delaySecs = delayMs / 1000   
        self.frameRateCaptureInterval = self.theCamera.frameRateInterval
        
    def startThread(self):
        self.name = "CameraCaptureThread"
        self.isRunning = True
        self.start()
        
    def stopThread(self):
        self.isRunning = False
        self.join()  


    # TODO - ought to put the image processing within the capture lock.         
    def run(self):

        startFrameRateTime = time.time()
        numFramesCaptured = 0 
        
        while self.isRunning:
            
            try:
                stream = io.BytesIO() 

                startCaptureTime = time.time()
                
                for unused in self.theCamera.camera.capture_continuous(  stream,
                                                                         'bmp',
                                                                         resize=(self.theCamera.numCols, self.theCamera.numRows),
                                                                         use_video_port=True ):

                    if ( not self.isRunning ):
                        break 

                    logger.debug("Capture continuous took %d ms", int((time.time() - startCaptureTime)*1000))
                    numFramesCaptured += 1
                    stream.seek(0)
                    pilImage = Image.open(stream) 

                    self.theCamera.frameId += 1
                    self.theCamera.currentImage = pilImage
                    self.theCamera.server.broadcastImage(self.theCamera.frameId, pilImage)

                    """ Consider using a bitmap for the mode so that we can get coloured objects and the horizon at the same time """ 
                    if ( self.theCamera.captureMode == self.theCamera.BLOCK_MODE ):
                        objectList = self.theCamera.doFindBlocks(pilImage) 
                        self.theCamera.setCapturedObjects(objectList)
                        
                    if ( self.theCamera.captureMode == self.theCamera.HORIZON_MODE ):
                        horizon = self.theCamera.doFindHorizon(pilImage) 
                        self.theCamera.setHorizon(horizon)
                        
                    if ( self.theCamera.captureMode == self.theCamera.VECTOR_MODE ):
                        vectors = self.theCamera.doFindVectors(pilImage)
                        self.theCamera.setVectors(vectors)   
                        
                    if ( self.theCamera.takePhotoTrigger ):
                        self.theCamera.takePhotoTrigger = False 
                        if ( self.theCamera.saveFolder ):
                            fileName = "photo_" + time.strftime("%Y%m%d-%H%M%S") + ".bmp"
                            filePath = os.path.join(self.theCamera.saveFolder, fileName)
                            logger.info("Saving photo as [%s]", filePath)
                            pilImage.save(filePath)
                            
                        
                    stream.seek(0)
                    stream.truncate()

                    if ( (time.time() - startFrameRateTime) > self.frameRateCaptureInterval ):
                        self.theCamera.currentFrameRate = int(numFramesCaptured/(time.time()-startFrameRateTime))
                        startFrameRateTime = time.time() 
                        numFramesCaptured = 0 

                    sleepTime = max((self.delaySecs - (time.time() - startCaptureTime)),0.005)
                    logger.debug("sleepTime = %d ms", sleepTime*1000)
                    time.sleep(sleepTime)
                    
                    startCaptureTime = time.time()

            except:

                traceback.print_exc()
                #logger.error(traceback.format_exc())
                self.isRunning = False 


