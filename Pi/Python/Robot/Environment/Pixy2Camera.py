
import ThirdParty.Pixy2.pixy as pixy
from ThirdParty.Pixy2.pixy import *
from Robot.Environment.SmartCamera import SmartCamera
from Robot.Environment.SmartCamera import CameraObject

class Pixy2Camera(SmartCamera):


    def __init__(self):

        self.pixyConnected = False

        initResult = pixy.init()
        
        if ( initResult == 0 ):
            self.pixyConnected = True 
            pixy.change_prog("color_connected_components")
        
    def getCameraObjects(self):
    
        objectList = [] 
    
        if ( not self.pixyConnected ):
            return None 
            
        blocks = BlockArray(100)
        numBlocks = pixy.ccc_get_blocks(100, blocks) 
        
        for i in range(0, numBlocks) :
            objectList.append( CameraObject( blocks[i].m_signature, 
                                       blocks[i].m_x, 
                                       blocks[i].m_y, 
                                       blocks[i].m_width, 
                                       blocks[i].m_height    )   )
            
        return objectList 

    def setSearchObjects(self, searchObjects):
        # Pixy Camera must be configured externally. 
        return;  

    def setCameraLamp(self, switchLampOn):
        if ( self.pixyConnected ):
            if ( switchLampOn ):
                pixy.set_lamp(1,0)
            else:
                pixy.set_lamp(0,0)
        
