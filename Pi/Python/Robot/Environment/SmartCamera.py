
from threading import Thread

""" An object that the camera has recognised """
class CameraObject:

    def __init__(self, type, x, y, width, height):
        self.type = type 
        self.x = x 
        self.y = y 
        self.width = width 
        self.height = height 

    def __str__(self):
        return("{}: {},{} ({} x {})".format(self.type, self.x, self.y, self.width, self.height))

""" Base class for smart camers (i.e. ones that can find objects on their own. """
""" Override unimplemented methods in derived classes """ 
class SmartCamera:

    """ Overide getCameraObjects in child classes to actually get the objects from the camera """
    def getCameraObjects(self): 
        return [] 

    def setSearchObjects(self, searchObjects):
        return;  
    
    def setCropArea(self, x1y1x2y2):
        return; 
    
    def resetCropArea(self):
        return;

    def setCameraLamp(self, isLampOn):
        return

    def getFrameRate(self):
        return None

    def takePhoto(self):
        pass

    """ Consider implementing the capture thread in this class """
    def startCaptureThread(self,delayMs):
        return 
    
    def stopCaptureThread(self):
        return 
