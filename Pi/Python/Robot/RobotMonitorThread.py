
import threading 
import time
import logging

logger = logging.getLogger(__name__)

class RobotMonitorThread(threading.Thread):

    REPORT_INTERVAL = 5.0

    HEADING_REPORT_INTERVAL = 60.0 

    def __init__(self, robot):
        self.stopMonitorThread = False
        self.robot = robot 
        super().__init__();
        self.name = "Robot Monitor Thread";

    def run(self):

        nextReportTime = time.time() + self.REPORT_INTERVAL
        nextHeadingReportTime = time.time() + self.HEADING_REPORT_INTERVAL

        environment = self.robot.getEnvironment()

        while not self.stopMonitorThread:

            if ( time.time() > nextReportTime ):
                # Report the current status 
                distance = self.robot.getEnvironment().getDistance() 
                logger.info("%d: L FL F FR R = %0.2f, %0.2f, %0.2f, %0.2f, %0.2f",
                             distance.timestamp,
                             distance.left,
                             distance.frontLeft,
                             distance.front,
                             distance.frontRight,
                             distance.right )

                # Report the battery status. 
                batteryLevel = self.robot.getBatteryLevel() 
                if (batteryLevel != None):
                    logger.info("Battery Level = %s", batteryLevel) 

                # Report the camera performance
                cameraFrameRate = environment.getCameraFrameRate()
                if ( cameraFrameRate != None ):
                    logger.info("Camera FrameRate = %d/s", cameraFrameRate)

                nextReportTime = time.time() + self.REPORT_INTERVAL

            if ( time.time() > nextHeadingReportTime ):
                heading = self.robot.getEnvironment().getHeading()
                logger.info("Heading = %d", heading.value)
                nextHeadingReportTime = time.time() + self.HEADING_REPORT_INTERVAL
            
            time.sleep(0.1)

    def stop(self):
        self.stopMonitorThread = True 
        self.join() 


