
import time

class RobotWheels():

    def __init__(self, motorController):

        self.motorController = motorController

    def stop(self):
        self.motorController.stop()
        
    def forward(self, speed, time = -1):        
        self.motorController.moveRightWheel(speed)
        self.motorController.moveLeftWheel(speed)
        self.stopAfterTime(time)

    def reverse(self, speed, time = -1):
        self.motorController.moveRightWheel(0 - speed)
        self.motorController.moveLeftWheel(0 - speed)
        self.stopAfterTime(time)
    
    def moveRightWheel(self,speed,time = -1):
        """Move at the speed specified +ve for forward, -ve for reverse"""
        self.motorController.moveRightWheel(speed)
        self.stopAfterTime(time)

    def moveLeftWheel(self,speed,time = -1):
        """Move at the speed specified +ve for forward, -ve for reverse"""
        self.motorController.moveLeftWheel(speed)
        self.stopAfterTime(time)

    def moveBothWheels(self, speed, time = -1):
        """Move both wheels at the speed specified +ve for forward, -ve for reverse"""
        self.motorController.moveBothWheels(speed)
        self.stopAfterTime(time)

    def stopAfterTime(self, timeToWait):
        """Stop both wheels on robot after timeToWait seconds"""
        # Note - this blocks - do don't do it if you need to do anything whilst the
        # robot is moving (like stop it from crashing!!). 
        if ( timeToWait >= 0 ):
            time.sleep(timeToWait)
            self.stop()

            
