
import Robot.RobotModes.RobotModes as RobotModes
from Robot.RobotStateListener import RobotStateListener 

class ThunderBorgStateListener(RobotStateListener):

    def __init__(self, thunderBorg):
        self.thunderBorg = thunderBorg 

    def robotModeChanged(self, newMode):
        super().robotModeChanged(newMode)

        self.thunderBorg.SetLedShowBattery(False)

        if ( newMode == None ):
            self.thunderBorg.SetLeds(0,0,0) # Off 
        elif ( newMode == RobotModes.MODE_REMOTE ):
            self.thunderBorg.SetLeds(0,0,1) # Blue
        elif ( newMode == RobotModes.MODE_STRAIGHT_LINE ):
            self.thunderBorg.SetLeds(0,1,0) # Green
        elif ( newMode == RobotModes.MODE_MINIMAL_MAZE ):
            self.thunderBorg.SetLeds(1,0,1) # Magenta
        elif ( newMode == RobotModes.MODE_HUBBLE ):
            self.thunderBorg.SetLeds(1,0,0) # Red
        elif ( newMode == RobotModes.MODE_SPACE_INVADERS ):
            self.thunderBorg.SetLeds(1,0.65,0) # Orange
        else:
            self.thunderBorg.SetLedShowBattery(False)
            self.thunderBorg.SetLeds(1,1,1) # White

    def switchedToBatteryDisplay(self, batteryDisplay):
        # Show the battery level instead of the mode. 
        # Changing modes will bring back the mode LED. 
        self.thunderborg.SetLedShowBattery(True) 
        pass 


