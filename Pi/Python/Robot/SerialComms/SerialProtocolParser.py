
from Robot.SerialComms.SerialMessage import SerialMessage
import logging

START_MSG_CHAR = b'<'
END_MSG_CHAR = b'>'

PAYLOAD_ITEM_SEPARATOR=b';'

logger = logging.getLogger(__name__)

def parseMessage(message):

    # Message must have at least start char, end char, source and
    # message type.
    if ( len(message) < 4 ):
        #logger.warn("Short message received: " + str(message))
        return None

    # Check the message starts with the start character.
    if ( message.startswith(START_MSG_CHAR) ):
        
        endCharIndex = message.find(END_MSG_CHAR)

        if ( endCharIndex != -1 ):

            messageSource = message[1]
            #logger.info("messageSource = " + str(messageSource))
            messageType = message[2]
            #logger.info("messageType = " + str(messageType))
            payload = message[3:endCharIndex]
            #logger.info("payload = " + str(payload))

            return( SerialMessage(messageSource, messageType, payload) )
        else:
            logger.warn("Message with no end char received: " + str(message))
    else:
        logger.warn("Message with no start char received: " + str(message)) 

    return None

def msgToBytes(message):

    strMessage = START_MSG_CHAR 
    strMessage += message.getSource()
    strMessage += message.getType()
    strMessage += message.getPayload()
    strMessage += END_MSG_CHAR

    return strMessage 

def payloadToDict( payload ):
    
    dict = {} 

    startIndex = 0 ;

    endIndex = payload.find(PAYLOAD_ITEM_SEPARATOR, startIndex) 
    #logger.info("endIndex = " + str(endIndex))

    while endIndex != -1:
        
        tagValuePair = payload[slice(startIndex,endIndex)]
        #logger.info("tagValuePair = " + str(tagValuePair))

        tag=tagValuePair[0]
        value=tagValuePair[1:]
        dict[tag]=value

        startIndex = endIndex+1 
        endIndex = payload.find(PAYLOAD_ITEM_SEPARATOR, startIndex) 

    return dict

