

class SerialMessage():

    #define known message sources. 
    MSGSOURCE_PI=b'p'
    MSGSOURCE_MICROBIT=b'm'
    MSGSOURCE_ARDUINO=b'a'
    
    #define message types the pi might send or receive     
    MSGTYPE_MODE_CHANGED = b'm' # sent from pi when mode has changed.
    MSGTYPE_CHANGE_MODE = b'c' # sent to pi when mode change is requested. 
    MSGTYPE_START_STOP = b's' # sent to pi to start/stop current mode
    MSGTYPE_DISTANCE = b'd'
    MSGTYPE_HEADING = b'h'
    MSGTYPE_TEXT =b't' # Sent from pi to display a text message. 

    def __init__(self, source, msgType, payload):
        self.source = source
        self.msgType = msgType
        self.payload = payload

    def getSource(self):
        return self.source

    def getType(self):
        return self.msgType

    def getPayload(self):
        return self.payload


