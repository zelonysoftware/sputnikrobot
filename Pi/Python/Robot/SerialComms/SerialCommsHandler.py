
import os
import Robot.SerialComms.SerialProtocolParser as SerialProtocolParser
from Robot.SerialComms.SerialMessage import SerialMessage
from Robot.RobotEvent.RobotEvent import ButtonChangeEvent
from threading import Thread
from serial import Serial
from Robot.RobotStateListener import RobotStateListener
from Robot.Environment.RobotEnvironment import Distance
import logging

logger = logging.getLogger(__name__)

class SerialReader(Thread):


    # dictionary that describes whether messages should be sent or not.
    msgSendMap = { SerialMessage.MSGSOURCE_MICROBIT[0]:[SerialMessage.MSGTYPE_MODE_CHANGED, SerialMessage.MSGTYPE_TEXT], 
                   SerialMessage.MSGSOURCE_ARDUINO[0]:[] }

    def __init__(self, commsHandler, serialConfig, serialDevice):
        Thread.__init__(self)
        self.name='SerialReaderThread: ' + serialDevice
        self.commsHandler = commsHandler
        # type of the remote location (eg. b'a' for arduino, b'm' for micro:bit)
        self.remoteType = None

        baudRate = serialConfig.getBaudRate(19200)
        timeout = serialConfig.getTimeoutSeconds(1.0) 
        self.serial = Serial( port=serialDevice, baudrate=baudRate, timeout=timeout )
        #logger.info("Created serial device: " + self.serial.name + ", isopen = " + str(self.serial.is_open))

    def startReader(self):

        self.stopped = False ;
        super().start()
           
    def run(self):

        # Read from the serial port line by line, converting messages to classes and
        # passing them back to the comms handler 
        while( not self.stopped ):

            message = self.serial.readline()
            #logger.info("serial msg: " + str(message))

            # Convert the message. 
            parsedMsg = SerialProtocolParser.parseMessage(message);

            if ( parsedMsg != None ):
                self.remoteType = parsedMsg.getSource()
                self.commsHandler.handleSerialMessage(parsedMsg) 

    def send(self, msgType, msg):
        logger.debug("send message: %s, remoteType = %s", str(msg), str(self.remoteType))
        if ( self.remoteType != None ):
            if ( self.remoteType in self.msgSendMap ):
                logger.info("Remote type %s is in the msgSendMap", str(self.remoteType))
                supportedMessageTypes = self.msgSendMap[self.remoteType]
                logger.debug("Is msg type %s in the types %s ?", str(msgType), str(supportedMessageTypes))
                if ( msgType in supportedMessageTypes ):
                    logger.info("Sending message: %s", str(msg))
                    self.serial.write(msg + b'\n'); 
            

    def stop(self):
        self.stopped = True ;

class SerialCommsHandler(RobotStateListener):


    def __init__(self, serialConfig, robot):
        self.serialReaders = []
        self.config = serialConfig
        self.robot = robot 
        self.environment = robot.getEnvironment() 

    def start(self):
        # Go through all the serial devices creating readers.
        if ( os.path.isdir('/dev/serial/by-id') ):
            path = self.config.getSerialDevicePath('/dev/serial/by-id')
            serialDevices = os.listdir(path)

            for serialDevice in serialDevices:
                fullDevicePath = os.path.join(path,serialDevice)
                self.serialReaders.append(SerialReader(self, self.config, fullDevicePath))
            
            for serialReader in self.serialReaders:
                serialReader.startReader() 

    def stop(self):
        for serialReader in self.serialReaders:
            serialReader.stop()
            serialReader.join() 

    def robotModeChanged(self, newMode):

        if ( newMode == None ):
            newMode = "Start"
        
        # newMode is a string so we need to encode it. 
        bytesNewMode = newMode.encode('UTF-8')

        msg = SerialMessage(SerialMessage.MSGSOURCE_PI, SerialMessage.MSGTYPE_MODE_CHANGED, bytesNewMode)
        self.sendMessage(msg)
            
    def switchedToBatteryDisplay(self, batteryDisplay):
        #batteryDisplay is a string so we need to encode it. 
        bytesBatteryDisplay = batteryDisplay.encode('UTF-8')
                
        msg = SerialMessage(SerialMessage.MSGSOURCE_PI, SerialMessage.MSGTYPE_TEXT, bytesBatteryDisplay)
        self.sendMessage(msg)

    def sendMessage(self, msg):
    
        packedMessage = SerialProtocolParser.msgToBytes(msg)
                                   
        for serialReader in self.serialReaders:
            serialReader.send(msg.getType(), packedMessage)
        
    def payloadToDistance(self,msg):
            distanceDict = SerialProtocolParser.payloadToDict(msg.getPayload())
            #logger.info("distanceDict = " + str(distanceDict))

            try:

                if ( distanceDict != None ):
                    left = int(distanceDict[b'A'[0]]) if (b'A'[0] in distanceDict) else -1
                    right = int(distanceDict[b'D'[0]]) if (b'D'[0] in distanceDict) else -1
                    front = int(distanceDict[b'W'[0]]) if (b'W'[0] in distanceDict) else -1
                    frontLeft = int(distanceDict[b'Q'[0]]) if (b'Q'[0] in distanceDict) else -1
                    frontRight = int(distanceDict[b'E'[0]]) if (b'E'[0] in distanceDict) else -1

                return Distance(left=left, right=right, front=front, frontLeft=frontLeft, frontRight=frontRight)

            except ValueError:

                logger.warn("Invalid int value read from serial port")

            return None

    def handleSerialMessage(self, msg):
        #logger.info("Received message with type = " + str(msg.getType()))

        if (msg.getType() == SerialMessage.MSGTYPE_DISTANCE[0]):
            #logger.info("Handling distance message") 
            distance = self.payloadToDistance(msg)
            #logger.info("distance = " + str(distance))
            if ( distance != None ):
                self.environment.setDistance(distance)
            
        elif (msg.getType() == SerialMessage.MSGTYPE_HEADING[0]):
            # Heading message just contains a single int containing the heading in degrees. 
            heading = int(msg.payload)
            self.environment.setHeading(heading)

        elif (msg.getType() == SerialMessage.MSGTYPE_CHANGE_MODE[0]):
            # For change mode message - there is no payload - this just tells us to switch mode. 
            self.robot.advanceMode()

        elif (msg.getType() == SerialMessage.MSGTYPE_START_STOP[0]):
            # For start/stop message - there is no payload - this just tells us to stop or start a mode. 
            # We pass this as a controller event to emulate releasing the start button. 
            startStopEvent = ButtonChangeEvent(ButtonChangeEvent.BUTTON_START, 0)
            self.robot.handleEvent(startStopEvent)




