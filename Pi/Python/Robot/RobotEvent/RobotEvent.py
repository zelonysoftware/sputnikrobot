
# Games Controller Event codes
RE_GC_JOYSTICK_CHANGE   = 0
RE_GC_TRIGGER_CHANGE    = 1 
RE_GC_BUTTON_CHANGE     = 2
RE_GC_CONNECTION_CHANGE = 3
RE_MODE_CHANGE_REQUEST  = 4

""" RobotEvent class encapsulates an event that may cause the robot to change state. 
    The event consists of the event type and a data dictionary to hold the data values. 
    Subtypes are provided to access the data easier """
class RobotEvent():

    def __init__(self, eventType):
        self.eventType = eventType
        self.data = {}

    def getEventType(self):
        return self.eventType


class JoystickChangeEvent(RobotEvent):

    JOYSTICK_RIGHT = 0
    JOYSTICK_LEFT = 1

    WHICH_JOYSTICK_EVENT_KEY = 0
    X_POS_EVENT_KEY = 1
    Y_POS_EVENT_KEY = 2 

    def __init__(self, whichJoystick, xPos, yPos):
        super().__init__(RE_GC_JOYSTICK_CHANGE)
        self.data[self.WHICH_JOYSTICK_EVENT_KEY] = whichJoystick
        self.data[self.X_POS_EVENT_KEY] = xPos
        self.data[self.Y_POS_EVENT_KEY] = yPos 

    def whichJoystick(self):
        return self.data[self.WHICH_JOYSTICK_EVENT_KEY]

    def getXPos(self):
        return self.data[self.X_POS_EVENT_KEY]

    def getYPos(self):
        return self.data[self.Y_POS_EVENT_KEY]

class TriggerChangeEvent(RobotEvent):

    TRIGGER_L2 = 0 
    TRIGGER_R2 = 1 

    WHICH_TRIGGER_EVENT_KEY = 0
    TRIGGER_POS_EVENT_KEY = 1

    def __init__(self, whichTrigger, triggerPos):
        super().__init__(RE_GC_TRIGGER_CHANGE)
        self.data[self.WHICH_TRIGGER_EVENT_KEY] = whichTrigger
        self.data[self.TRIGGER_POS_EVENT_KEY] = triggerPos

    def whichTrigger(self):
        return self.data[self.WHICH_TRIGGER_EVENT_KEY]

    def getTriggerPos(self):
        return self.data[self.TRIGGER_POS_EVENT_KEY]

class ButtonChangeEvent(RobotEvent):

    BUTTON_X            = 0 
    BUTTON_CIRCLE       = 1
    BUTTON_TRIANGLE     = 2
    BUTTON_SQUARE       = 3

    BUTTON_L2           = 4
    BUTTON_R2           = 5

    BUTTON_L1           = 6
    BUTTON_R1           = 7

    BUTTON_L3           = 8
    BUTTON_R3           = 9

    BUTTON_X            = 10
    BUTTON_CIRCLE       = 11
    BUTTON_TRIANGLE     = 12
    BUTTON_SQUARE       = 13

    BUTTON_KEYPAD_UP    = 14
    BUTTON_KEYPAD_DOWN  = 15
    BUTTON_KEYPAD_LEFT  = 16
    BUTTON_KEYPAD_RIGHT = 17

    BUTTON_START        = 18
    BUTTON_SELECT       = 19
    BUTTON_PS           = 20

    WHICH_BUTTON_EVENT_KEY = 0
    BUTTON_VALUE_EVENT_KEY = 1

    def __init__(self, whichButton, buttonValue):
        super().__init__(RE_GC_BUTTON_CHANGE)
        self.data[self.WHICH_BUTTON_EVENT_KEY] = whichButton
        self.data[self.BUTTON_VALUE_EVENT_KEY] = buttonValue

    def whichButton(self):
        return self.data[self.WHICH_BUTTON_EVENT_KEY]

    def getButtonValue(self):
        return self.data[self.BUTTON_VALUE_EVENT_KEY]

class GamesControllerConnectionChangeEvent(RobotEvent):

    CONNECTION_STATUS_EVENT_KEY = 0 

    def __init__(self, connectionStatus):
        super().__init__(RE_GC_CONNECTION_CHANGE)
        self.data[self.CONNECTION_STATUS_EVENT_KEY] = connectionStatus


    def getConnectionStatus(self):
        return self.data[self.CONNECTION_STATUS_EVENT_KEY]


# ModeChangeRequestEvent has no data. 
class ModeChangeRequestEvent(RobotEvent):

    def __init__(self):
        super().__init(RE_MODE_CHANGE_REQUEST)


