#!/usr/bin/python3

import sys
import os
import json
import logging
import logging.config

""" initialise Logging - this sets up the JSON file path from which we load the logging configuration dictionary.
"""
def initialiseLogger(configImpl):

    loggingInitialised = False 

    try:

        configFilePath = configImpl.getValue("Logging", "ConfigFile", "./logging.json")

        if ( os.path.isfile(configFilePath) ):
            # Load the logging config from the config file. 
            configFile = open(configFilePath)
            configDict = json.load(configFile)

            logging.config.dictConfig(configDict)

            logger = logging.getLogger(__name__)
            logger.info("Initiallised Logger from JSON file") 

        loggingInitialised = True ;

    except:

        print("Failed to load the logging config - console logging will be used instead: ", sys.exc_info()[0])


    if ( not loggingInitialised ):
        # Just use basic console logging. 
        logging.basicConfig(level='INFO')
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel('INFO');
        consoleHandler.setFormatter(logging.Formatter('%(name)-12s %(message)s'))

        logging.getLogger('').addHandler(consoleHandler) 
		

