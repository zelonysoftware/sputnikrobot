
from .MotorControllerBase import MotorControllerBase
import ThirdParty.RedBoard.redboard as red

class RedboardMotorController(MotorControllerBase):

    def __init__(self):
        pass

    def moveRightWheel(self, speed):
        red.M2(speed)
    
    def moveLeftWheel(self, speed):
        red.M1(speed)
    
    def moveBothWheels(self, speed):
        red.M1(speed)
        red.M2(speed)
    
    def stop(self):
        red.M1(0)
        red.M2(0)
