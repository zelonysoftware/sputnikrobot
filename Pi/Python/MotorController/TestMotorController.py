from .MotorControllerBase import MotorControllerBase
import logging

logger = logging.getLogger(__name__)

class TestMotorController(MotorControllerBase):

   """Test class that just outputs the motor speeds"""
   def moveRightWheel(self, speed):
       logger.info("move right wheel: " + str(speed))

   def moveLeftWheel(self, speed):
       logger.info("move left wheel: " + str(speed))

   def moveBothWheels(self, speed):
       logger.info("move both wheels: " + str(speed))

   def stop(self):
       #logger.info("stop")
       pass


if __name__ == '__main__':
    x = TestMotorController()

    x.moveRightWheel(100)

