
from .MotorControllerBase import MotorControllerBase
from ThirdParty.PiBorg.ThunderBorg import ThunderBorg

class ThunderBorgMotorController(MotorControllerBase):
   """Class for a ThunderBorg Motor driver. This uses it's own API to drive the motors through I2C"""

   def __init__(self, thunderBorg):
       self.tb = thunderBorg

   def moveRightWheel(self, speed):
       self.tb.SetMotor2(speed / 100.0)

   def moveLeftWheel(self, speed):
       self.tb.SetMotor1(speed / 100.0)

   def moveBothWheels(self, speed):
       self.tb.SetMotors(speed / 100.0)

   def stop(self):
       self.tb.SetMotors(0)


