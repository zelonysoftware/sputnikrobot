
from .MotorControllerBase import MotorControllerBase
import RPi.GPIO as GPIO

class RobotWheel():

    def __init__(self, fwdPin, revPin):

        self.fwdPin = fwdPin
        self.revPin = revPin

        GPIO.setup(self.fwdPin, GPIO.OUT)
        GPIO.setup(self.revPin, GPIO.OUT)

        self.fwdPwm = GPIO.PWM(self.fwdPin,400)
        self.fwdPwm.start(0)

        self.revPwm = GPIO.PWM(self.revPin,400)
        self.revPwm.start(0)

        self.preSetSpeed = 0 

    def stop(self):
        self.fwdPwm.ChangeDutyCycle(0)
        self.revPwm.ChangeDutyCycle(0)

    def setSpeed(self,speed):
        self.preSetSpeed = speed ;

    def forward(self):        
        self.fwdPwm.ChangeDutyCycle(self.preSetSpeed)
        self.fwdPwm.ChangeFrequency(self.preSetSpeed+5)
        self.revPwm.ChangeDutyCycle(0)

    def reverse(self):
        self.fwdPwm.ChangeDutyCycle(0)
        self.revPwm.ChangeDutyCycle(self.preSetSpeed)
        self.revPwm.ChangeFrequency(self.preSetSpeed+5)

    def move(self,speed):
        """Move at the speed specified +ve for forward, -ve for reverse"""
        motorSpeed = abs(speed)
        self.setSpeed(motorSpeed)

        if ( speed < 0 ):
            self.reverse()
        elif ( speed > 0 ):
            self.forward()
        else:
            self.stop()

class RoboHatMotorController(MotorControllerBase):
   """Class for a 4Tronix Robo Hat Motor driver. This uses 4 PWM pin to drive the forward and reverse
      speeds of each motor"""

   # define the pins in BCM mode. 
   LEFT_FWD_SPEED_PIN = 16
   LEFT_REV_SPEED_PIN = 19

   RIGHT_FWD_SPEED_PIN = 13
   RIGHT_REV_SPEED_PIN = 12

   def __init__(self):
       self.rightWheel = RobotWheel(self.RIGHT_FWD_SPEED_PIN, self.RIGHT_REV_SPEED_PIN)
       self.leftWheel = RobotWheel(self.LEFT_FWD_SPEED_PIN, self.LEFT_REV_SPEED_PIN)

   def setLeftWheelSpeed(self, speed):
       """Preset speed for forward and reverse methods"""
       self.leftWheel.setSpeed(speed)

   def moveRightWheel(self, speed):
       self.rightWheel.move(speed)

   def moveLeftWheel(self, speed):
       self.leftWheel.move(speed)

   def moveBothWheels(self, speed):
       self.leftWheel.move(speed)
       self.rightWheel.move(speed)

   def stop(self):
       self.rightWheel.stop()
       self.leftWheel.stop()


