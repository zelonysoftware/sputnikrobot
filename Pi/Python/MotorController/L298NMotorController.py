
from .MotorControllerBase import MotorControllerBase
import RPi.GPIO as GPIO

class RobotWheel():

    def __init__(self, fwdPin, revPin, speedPin):

        self.fwdPin = fwdPin
        self.revPin = revPin
        self.speedPin = speedPin

        GPIO.setup(self.fwdPin, GPIO.OUT)
        GPIO.setup(self.revPin, GPIO.OUT)

        if ( speedPin >= 0 ):
            GPIO.setup(self.speedPin, GPIO.OUT)
            self.speedPwm = GPIO.PWM(self.speedPin,400)
            self.speedPwm.start(0) 

    def stop(self):
        GPIO.output(self.fwdPin, GPIO.LOW)
        GPIO.output(self.revPin, GPIO.LOW)

    def setSpeed(self,speed):
        if ( self.speedPin >= 0 ):
            self.speedPwm.ChangeDutyCycle(speed)

    def forward(self):        
        GPIO.output(self.fwdPin, GPIO.HIGH)
        GPIO.output(self.revPin, GPIO.LOW)

    def reverse(self):
        GPIO.output(self.fwdPin, GPIO.LOW)
        GPIO.output(self.revPin, GPIO.HIGH)

    def move(self,speed):
        """Move at the speed specified +ve for forward, -ve for reverse"""
        motorSpeed = abs(speed)
        self.setSpeed(motorSpeed)

        if ( speed < 0 ):
            self.reverse()
        elif ( speed > 0 ):
            self.forward()
        else:
            self.stop()

class L298NMotorController(MotorControllerBase):
   """Class for a Dual HBridge Motor driver such as the L298N. This has 2 digital pins for forward and reverse and 
      a separate PWM pin for motor speed """
   def __init__(self, rightFwdPin, rightRevPin, rightSpeedPin, leftFwdPin, leftRevPin, leftSpeedPin):
       self.rightWheel = RobotWheel(rightFwdPin, rightRevPin, rightSpeedPin)
       self.leftWheel = RobotWheel(leftFwdPin, leftRevPin, leftSpeedPin)

   def moveRightWheel(self, speed):
       self.rightWheel.move(speed)

   def moveLeftWheel(self, speed):
       self.leftWheel.move(speed)

   def moveBothWheels(self, speed):
       self.rightWheel.move(speed)
       self.leftWheel.move(speed)

   def stop(self):
       self.rightWheel.stop()
       self.leftWheel.stop()






            
