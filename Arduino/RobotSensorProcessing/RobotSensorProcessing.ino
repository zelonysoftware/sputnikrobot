
#include <NewPing.h>

const char SOURCE_ARDUINO = 'a';
const char MSGTYPE_DISTANCE = 'd';

void sendDistanceMessage(char* payload); 

struct RobotDistanceSensor
{
  String id ;
  NewPing* sensor ;

  inline RobotDistanceSensor( String p_id, NewPing* p_sensor )
  {
    id = p_id ;
    sensor = p_sensor ;
  }
};

// Create the array of robot sensors
RobotDistanceSensor distanceSensors[] = { //RobotDistanceSensor( String("Q"), new NewPing(6, 7, 200) )
                                         // RobotDistanceSensor( String("D"), new NewPing(12, 13, 200) ),
                                         RobotDistanceSensor( String("W"), new NewPing(8, 9, 200) )
                                         // RobotDistanceSensor( String("A"), new NewPing(4, 5, 200) ),
                                         // RobotDistanceSensor( String("E"), new NewPing(10, 11, 200) )
                                        };

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  int numDistanceSensors = sizeof(distanceSensors) / sizeof(distanceSensors[0]) ;

  String payload;

  for ( int i = 0 ; i < numDistanceSensors ; ++i )
  {
    delay(2000);

    RobotDistanceSensor distanceSensor = distanceSensors[i];

    int distance = distanceSensor.sensor->ping_cm();

    payload += distanceSensor.id ;
    payload += distance;
    payload += ";"; 
  }

  // Send message over serial port. 
  sendDistanceMessage(payload); 
}

void sendDistanceMessage(String payload)
{
  String message;

  message += SOURCE_ARDUINO ;
  message += MSGTYPE_DISTANCE ;
  message += payload ;
  
  Serial.println(message.c_str());
}
