
#include <NewPing.h>

NewPing frontSonarSensor = NewPing(13,12,500);

void setup() {
  Serial.begin(9600);
  
}

void loop() {

  int distance = frontSonarSensor.ping_cm();

  Serial.print("Distance = ");
  Serial.println(distance);

  delay(500);
}
