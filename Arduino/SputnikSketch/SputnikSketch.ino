#include <NewPing.h>

const char MSG_START_CHAR = '<';
const char MSG_END_CHAR = '>';
const char SOURCE_ARDUINO = 'a';
const char MSGTYPE_DISTANCE = 'd';

void sendDistanceMessage(char* payload); 

struct RobotDistanceSensor
{
  static const int MAX_IGNORE_ZERO_COUNT = 10 ;
  String id ;
  NewPing* sensor ;
  int zeroCount = -1 ;

  inline RobotDistanceSensor( String p_id, NewPing* p_sensor )
  {
    id = p_id ;
    sensor = p_sensor ;
  }
  
  inline int getDistance()
  {
    int distance = sensor->ping_cm(); 
    
    // Filter out spurious zeros
    if ( distance == 0 )
    {
      if ( (zeroCount >= 0) && (zeroCount < MAX_IGNORE_ZERO_COUNT) )
      {
        // ignore this zero reading
        distance = -1 ;
      }
      else
      {
        // pass through - we return the zero.
      }
      ++zeroCount ;
    }
    else
    {
      zeroCount = 0 ;
    }
    
    return distance ;
  }
};

// Create the array of robot sensors
RobotDistanceSensor distanceSensors[] = { RobotDistanceSensor( String("Q"), new NewPing(6, 7, 200) ),
                                          RobotDistanceSensor( String("D"), new NewPing(12, 13, 200) ),
                                          RobotDistanceSensor( String("W"), new NewPing(8, 9, 200) ),
                                          RobotDistanceSensor( String("A"), new NewPing(4, 5, 200) ),
                                          RobotDistanceSensor( String("E"), new NewPing(10, 11, 200) )
                                        };

void setup() {
  Serial.begin(19200);
}

void loop() {
  
  int numDistanceSensors = sizeof(distanceSensors) / sizeof(distanceSensors[0]) ;

  String payload;

  long startMs = millis();
  
  for ( int i = 0 ; i < numDistanceSensors ; ++i )
  {
    delay(5);

    RobotDistanceSensor distanceSensor = distanceSensors[i];

    int distance = distanceSensor.getDistance();

    if ( distance >= 0 )
    {
      payload += distanceSensor.id ;
      payload += distance;
      payload += ";"; 
    }
  }

  //String msg = "Time taken for a single scan = ";
  //msg += (millis() - startMs);
  //msg += "ms"; 

  //Serial.println(msg); 
  
  // Send message over serial port. 
  sendDistanceMessage(payload); 
}

void sendDistanceMessage(String payload)
{
  String message;

  message += MSG_START_CHAR ;
  message += SOURCE_ARDUINO ;
  message += MSGTYPE_DISTANCE ;
  message += payload ;
  message += MSG_END_CHAR ;
  
  Serial.println(message.c_str());
}
